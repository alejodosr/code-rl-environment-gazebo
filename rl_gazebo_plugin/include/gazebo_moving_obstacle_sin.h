#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <stdio.h>
#include <math.h>
#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>
#include <nav_msgs/Odometry.h>

#define MILLION 1E9
#define COUNTER_MAX 4000

using namespace std;

namespace gazebo
{
class ModelPush : public ModelPlugin
{

    private:
        physics::ModelPtr model;

        // Pointer to the update event connection
        event::ConnectionPtr updateConnection;

        ros::NodeHandle *node_handle_;
        ros::Subscriber moving_sub;
        ros::Subscriber pose_and_velocity_sub;
        std::string namespace_;
        nav_msgs::Odometry pose_and_velocity_;
        float object_speed_xy_;
        float object_speed_z_;
        bool object_in_movement_;
        int time_counter_;
        bool updated_;


    public:
        ModelPush();
        virtual ~ModelPush();
        void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

        // Called by the world update start event
        void OnUpdate(const common::UpdateInfo & /*_info*/);
        void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);
        void poseAndvelocityCallback(const nav_msgs::Odometry::ConstPtr& msg);

        double time_sec;
        double time_nsec;
        double time_sec_prev;
        double time_nsec_prev;
        double initial_time_sec;
        double initial_time_nsec;
        double vel_;
        int counter;
        math::Pose initial_pose_;
};
}
