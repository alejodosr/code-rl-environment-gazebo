#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>

//////////////////////////////////////////////////////////////////
/*
TO DO LIST: 
- Enable for random trajectories
# define RAMDOM 0

*/
//////////////////////////////////////////////////////////////////

//Simulation parameters (Don't change!)

#define GRAVITY 10.0286 //Gravity used in Gazebo sim. If changed the drone will slowly float away or fall to the ground
#define STEP 0.01 //Time step used in gazebo sim (100 Hz)

//////////////////////////////////////////////////////////////////

// Trajectory parameters (change as you wish!)
#define MIN_SPEED 0.5 //Min median speed factor
#define MAX_SPEED 2 //Max median speed factor

//Drone origin in meters
#define ORI_X 0
#define ORI_Y 0
#define ORI_Z 2

//Movement restriction in meters
#define BOUND_X 3
#define BOUND_Y 3
#define MIN_Z 1.1
#define MAX_Z 4
#define MARGIN 0.3

//Max movement frequency factor. The 3D trajectory is defined by the ratios of the frequencies (Lissajous figures)
#define MAX_FREQ 6

//
#define LOOPS 1

//////////////////////////////////////////////////////////////////

//Toggle printing debug info
//#define DEBUG

using namespace std;

namespace gazebo
{

class ModelPush : public ModelPlugin
{

/// \brief Constructor
    public: ModelPush();

    /// \brief Destructor
    public: virtual ~ModelPush();

public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    // Called by the world update start event
public:
    void OnUpdate(const common::UpdateInfo & /*_info*/);
    void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);
    float freqRatio();
    void newTrajectory();

    //void resetCallback(const std_msgs::Bool::ConstPtr& msg);


    // Pointer to the model
private: physics::ModelPtr model;

    // Pointer to the update event connection
private: event::ConnectionPtr updateConnection;
    
private:
    ros::NodeHandle *node_handle_;
    ros::Subscriber moving_sub;
    std::string namespace_;

public:

    //Time variables (seconds and nanoseconds)
    double time_sec{0.0};
    double time_nsec{0.0};
    double initial_time_sec{0.0};
    double initial_time_nsec{0.0};

    //Drone initial position (defined by .world file)
    math::Pose initial_pose_;

    //Desired trajectory origin position
    math::Vector3 origin{ORI_X, ORI_Y, ORI_Z};

    //Drone initial velocities (using gravity compensation)
    double vel_x_ {0.0}, vel_y_ {0.0}, vel_z_ {GRAVITY*STEP};

    //Sinusoidal trajectory variables
    int freq_x;
    int freq_y;
    int freq_z;

    float ampl_x;
    float ampl_y;
    float ampl_z;

    double A_;
    double T_;
    double w_;

    //Trajectory variables
    double period;

};

}
