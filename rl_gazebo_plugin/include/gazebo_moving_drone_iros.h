#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <stdio.h>
#include <math.h>
#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>
#include <std_msgs/Bool.h>


#define MILLION 1E9
#define COUNTER_MAX 20000

// Choose the platform mode
//#define X
//#define Y
//#define Z

#define SLOW
//#define MID
//#define FAST

#define STILL

#define EIGHT_SHAPE

using namespace std;

namespace gazebo
{
class ModelPush : public ModelPlugin
{

/// \brief Constructor
    public: ModelPush();

    /// \brief Destructor
    public: virtual ~ModelPush();

public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    // Called by the world update start event
public:
    void OnUpdate(const common::UpdateInfo & /*_info*/);
    void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);
    void resetCallback(const std_msgs::Bool::ConstPtr& msg);

    // Pointer to the model
private: physics::ModelPtr model;

    // Pointer to the update event connection
private: event::ConnectionPtr updateConnection;
    
private:
    ros::NodeHandle *node_handle_;
    ros::Subscriber moving_sub;
    std::string namespace_;
    ros::Subscriber reset_sub_;

public:
    double time_sec;
    double time_nsec;
    double time_sec_prev;
    double time_nsec_prev;
    double initial_time_sec;
    double initial_time_nsec;
    double vel_, vel_x_, b_, w1_, P_T_;
    bool updated_;
    int counter;
    math::Pose initial_pose_;
    double A_;
    double T_;
    double w_;


};
}
