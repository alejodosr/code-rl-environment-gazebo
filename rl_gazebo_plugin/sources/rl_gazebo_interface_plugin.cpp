#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "boost/thread.hpp"
#include "boost/bind.hpp"
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace boost::interprocess;

namespace gazebo
{
class RlGazeboInterface : public WorldPlugin
{
private:
    enum { EXTRA_ITERATIONS = 5 };

    boost::thread check_iterate_thread_;
    physics::WorldPtr _parent_;
    unsigned int num_iterations;
    unsigned int iteration_i_;
    bool iteration_finished_, on_update_, should_update_;
    boost::condition_variable cond_;
    boost::mutex mut_;
    boost::mutex thread_mut_;
    boost::mutex monitoring_mut_, monitoring_mut_bis_;

    enum { TIMEOUT_ITERATIONS = 1000000 };

    struct shared_memory_buffer
    {
        //       enum { NUM_ITERATIONS = 2 };

        shared_memory_buffer()
            : check_action_(0), iterate_(0), iteration_finished_(0)
        {}

        // Semaphores to protect and synchronize access
        boost::interprocess::interprocess_semaphore
        check_action_, iterate_, iteration_finished_;

        // Items to fill
        //       char items[NumItems];
        unsigned int num_iterations;

    };

    shared_memory_buffer * data_;
    mapped_region region_;
    // Create a shared memory object.
    shared_memory_object shm_;
    void * addr_;

    // Gazebo variables
    // Pointer to the world_controller
    transport::NodePtr node_;
    transport::PublisherPtr pub_;

    // Pointer to the update event connection
    event::ConnectionPtr updateConnection_;

    // Configure the initial message to the system
    msgs::WorldControl worldControlMsg_;

    // Thread to update the plugin
    boost::thread on_update_thread_;

    // Thread to update the plugin
    boost::thread monitoring_thread_;

public:
    void CheckIterate(){
        // Print info
        std::cout << "RL_PLUGIN_INFO: Thread successfully initiated" << std::endl;

        // Interact with Gazebo
        bool exit = false;
        while(!exit){
            // Print info
            std::cout << "RL_PLUGIN_INFO: Waiting to iterate.." << std::endl;
            // Wait for the action from the agent
            data_->iterate_.wait();
            // Print info
            std::cout << "RL_PLUGIN_INFO: Iteration initiated.." << std::endl;
            // Read number of iterations
            num_iterations = data_->num_iterations;
            // Reset counter
            iteration_i_ = 0;
            // Print info
            std::cout << "RL_PLUGIN_INFO: Iterating " << num_iterations << " times" << std::endl;
            // Set the world to paused
            worldControlMsg_.set_pause(1);
            // Set the step flag to true
            worldControlMsg_.set_step(1);
            // Iterate N times
            pub_->Publish(worldControlMsg_);
            monitoring_mut_bis_.lock();
            should_update_ = true;
            monitoring_mut_bis_.unlock();
            // Wait for iterations to finish
            boost::unique_lock<boost::mutex> lock(mut_);
            while(!iteration_finished_)
            {
                cond_.wait(lock);
            }
            iteration_finished_ = false;
            // Print info
            std::cout << "RL_PLUGIN_INFO: Iteration concluded successfully on iteration number " << iteration_i_<< std::endl;
            // Wait for iteration to finish
            data_->iteration_finished_.post();
            //            // Set the multi step flag to true
            //            worldControlMsg_.set_multi_step(EXTRA_ITERATIONS);
            //            // Iterate N times
            //            pub_->Publish(worldControlMsg_);
        }
    }

    void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/){
        // Initialize member variable
        _parent_ = _parent;

        // Initialize iteration
        iteration_i_ = EXTRA_ITERATIONS;
        num_iterations = EXTRA_ITERATIONS;
        iteration_finished_=false;
        on_update_ = false;
        should_update_ = false;

        // Create a new transport node
        node_.reset(new transport::Node());

        // Initialize the node with the world name
        node_->Init(_parent->GetName());

        // Create a publisher
        pub_ = this->node_->Advertise<msgs::WorldControl>("~/world_control");

        // Listen to the update event. Event is broadcast every simulation
        // iteration.
        this->updateConnection_ = event::Events::ConnectWorldUpdateEnd(
                    boost::bind(&RlGazeboInterface::OnUpdate, this));

        //Erase previous shared memory
        shared_memory_object::remove("CheckActionShm");

        // Init shared memory
        shm_ = shared_memory_object
                (create_only                 //only create
                 ,"CheckActionShm"            //name
                 ,read_write                  //read-write mode
                 );

        // Set size
        shm_.truncate(sizeof(shared_memory_buffer));

        // Map the whole shared memory in this process
        region_ = mapped_region
                (shm_                   //What to map
                 ,read_write            //Map it as read-write
                 );

        // Get the address of the mapped region
        addr_ = region_.get_address();

        // Construct the shared structure in memory
        data_ = new (addr_) shared_memory_buffer;

        // Call check iterate thread
        check_iterate_thread_ = boost::thread(&RlGazeboInterface::CheckIterate, this);
        // Call monitoring thread
        monitoring_thread_ = boost::thread(&RlGazeboInterface::monitoring_thread, this);

        // Print info
        std::cout << "RL_PLUGIN_INFO: Plugin sucessfully loaded and shared memory created" << std::endl;

    }

    //    // Called by the world update start event.
public:

    void monitoring_thread(){
        bool exit = false;
        int iterations = 0;
        while(!exit){
            monitoring_mut_bis_.lock();
            if (should_update_)
                iterations++;
            monitoring_mut_bis_.unlock();
            // Receive from onupdate
            monitoring_mut_.lock();
            if (on_update_){
                iterations = 0;
                on_update_ = false;
            }
            monitoring_mut_.unlock();
            // Timeout executed
            if (iterations == TIMEOUT_ITERATIONS){
                monitoring_mut_.lock();
                on_update_ = false;
                monitoring_mut_.unlock();
                iterations = 0;
                worldControlMsg_.set_step(1);
                this->pub_->Publish(worldControlMsg_);
                monitoring_mut_bis_.lock();
                should_update_ = true;
                monitoring_mut_bis_.unlock();
                std::cout << "RL_PLUGIN_ERROR: TIMEOUT ONUPDATE " << std::endl;
            }

        }
    }

    void on_update_thread(){
        // Lock the mutex
        thread_mut_.lock();
        if (iteration_i_ < num_iterations - 1){
            iteration_i_++;
//            std::cout << "Publishing OnUpdate on iteration " << iteration_i_ << "and number of iterations " << num_iterations << std::endl;
            worldControlMsg_.set_step(1);
            this->pub_->Publish(worldControlMsg_);
            monitoring_mut_bis_.lock();
            should_update_ = true;
            monitoring_mut_bis_.unlock();
        }
        else if (iteration_i_ == num_iterations - 1) {
            std::cout << "Finished iterating " << iteration_i_ << std::endl;
            iteration_i_ = num_iterations;
            boost::lock_guard<boost::mutex> lock(mut_);
            iteration_finished_=true;
            cond_.notify_one();
        }
        // Unlock the mutex
        thread_mut_.unlock();
    }

    void OnUpdate(){
        // Tell monitoring thread
        monitoring_mut_.lock();
        on_update_ = true;
        should_update_ = false;
        monitoring_mut_.unlock();
        // Detach thread
        on_update_thread_.detach();
        // Initiate CheckAction thread
        on_update_thread_ = boost::thread(&RlGazeboInterface::on_update_thread, this);
    }
};

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(RlGazeboInterface)
}
