#include "gazebo_moving_obstacle.h"

using namespace std;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)

void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);

gazebo::ModelPush modelPush;

/////////////////////////////////////////////////
/// \brief ModelPush::ModelPush
///
ModelPush::ModelPush()
{
}

/////////////////////////////////////////////////
/// \brief RayPlugin::~RayPlugin
///
ModelPush::~ModelPush()
{
}

void ModelPush::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
        // Store the pointer to the model
        this->model = _parent;

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&ModelPush::OnUpdate, this, _1));

        // Exit if no ROS
        if (!ros::isInitialized())
        {
            gzerr << "Not loading Optical Flow plugin since ROS hasn't been "
                  << "properly initialized.  Try starting gazebo with ros plugin:\n"
                  << "  gazebo -s libgazebo_ros_api_plugin.so\n";
            return;
        }

        if (_sdf->HasElement("robotNamespace"))
          namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
        else
          gzwarn << "[gazebo_moving_platform] Please specify a robotNamespace.\n";

        this->node_handle_ = new ros::NodeHandle(namespace_);

        // Clock subscriber
        this->moving_sub = this->node_handle_->subscribe("clock", 10, &ModelPush::getSimulationClockTime, this);

        // Initial pose and velocity vector subscriber
        this->pose_and_velocity_sub = this->node_handle_->subscribe("/obstacle_pose_and_velocity", 10, &ModelPush::poseAndvelocityCallback, this);


        counter = 0;

        // Get initial pose
        initial_pose_ = this->model->GetWorldPose();

        // Set velocity
        pose_and_velocity.twist.twist.linear.x = 0;
        pose_and_velocity.twist.twist.linear.y = 0;
        pose_and_velocity.twist.twist.linear.z = 0;

        cout << "Obstacle plugin successfuly loaded" <<endl;
    }

void ModelPush::poseAndvelocityCallback(const nav_msgs::Odometry::ConstPtr& msg){
    cout << "Received data" << endl;

    // Update pose and velocity
    pose_and_velocity = *msg;

    // Set initial position
    math::Pose pose;
    pose.pos.x = msg->pose.pose.position.x;
    pose.pos.y = msg->pose.pose.position.y;
    pose.pos.z = msg->pose.pose.position.z;
    pose.rot.x = 0;
    pose.rot.y = 0;
    pose.rot.z = 0;
    pose.rot.w = 1;
    this->model->SetWorldPose(pose);

    // Store velocity
    pose_and_velocity.twist.twist.linear.x = msg->twist.twist.linear.x;
    pose_and_velocity.twist.twist.linear.y = msg->twist.twist.linear.y;
    pose_and_velocity.twist.twist.linear.z = msg->twist.twist.linear.z;
}

// Called by the world update start event
void ModelPush::OnUpdate(const common::UpdateInfo & /*_info*/)
{
    this->model->SetLinearVel(math::Vector3(pose_and_velocity.twist.twist.linear.x, pose_and_velocity.twist.twist.linear.y, pose_and_velocity.twist.twist.linear.z));
}

void ModelPush::getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg){

//    ros::Time time = msg->clock;
//    math::Pose pose = this->model->GetWorldPose();

//    // Restart in the origin
//    if ((pose.pos.y > -0.01) && (pose.pos.y < 0) && (vel_ > 0)){
//        initial_time_sec = time.sec;
//        initial_time_nsec = time.nsec;

//        // Reorient model
//        pose.pos.x = initial_pose_.pos.x;
//        pose.pos.y = initial_pose_.pos.y;
//        pose.pos.z = initial_pose_.pos.z;
//        pose.rot.x = 0;
//        pose.rot.y = 0;
//        pose.rot.z = 0;
//        pose.rot.w = 1;
//        this->model->SetWorldPose(pose);

//    }

//    modelPush.time_sec = time.sec - initial_time_sec;
//    modelPush.time_nsec = time.nsec - initial_time_nsec;

}
