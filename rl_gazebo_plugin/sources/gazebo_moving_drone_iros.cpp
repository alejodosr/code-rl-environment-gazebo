#include "gazebo_moving_drone_iros.h"

using namespace std;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)

void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);

gazebo::ModelPush modelPush;

/////////////////////////////////////////////////
/// \brief ModelPush::ModelPush
///
ModelPush::ModelPush()
{
}

/////////////////////////////////////////////////
/// \brief RayPlugin::~RayPlugin
///
ModelPush::~ModelPush()
{
}

void ModelPush::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
        // Store the pointer to the model
        this->model = _parent;

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&ModelPush::OnUpdate, this, _1));

        // Exit if no ROS
        if (!ros::isInitialized())
        {
            gzerr << "Not loading Optical Flow plugin since ROS hasn't been "
                  << "properly initialized.  Try starting gazebo with ros plugin:\n"
                  << "  gazebo -s libgazebo_ros_api_plugin.so\n";
            return;
        }

        if (_sdf->HasElement("robotNamespace"))
          namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
        else
          gzwarn << "[gazebo_moving_platform] Please specify a robotNamespace.\n";

        this->node_handle_ = new ros::NodeHandle(namespace_);

        this->moving_sub = this->node_handle_->subscribe("/clock", 10, &ModelPush::getSimulationClockTime, this);
        this->reset_sub_ = this->node_handle_->subscribe("/moving_platform/reset", 10, &ModelPush::resetCallback, this);

        counter = 0;

        // Get initial pose
        initial_pose_ = this->model->GetWorldPose();
        
        // Init variables
        updated_ = false;
    }

    // Called by the world update start event
void ModelPush::OnUpdate(const common::UpdateInfo & /*_info*/)
    {
        // Get Sim time
        common::Time  time = common::Time::GetWallTime();
        double sec = (modelPush.time_sec - modelPush.time_sec_prev) + (modelPush.time_nsec - modelPush.time_nsec_prev) / MILLION;
        //cout << sec << endl;
#ifdef SLOW
	// Slow
        A_ = 2.7;
        T_ = 13.5 * M_PI;
        w_ = 2 * M_PI / T_;
#endif

#ifdef MID
	// Mid
        A_ = 2.7;
        T_ = 4.5 * M_PI;
        w_ = 2 * M_PI / T_;
#endif

#ifdef FAST
	// Fast
        A_ = 2.7;
        T_ = 3.75 * M_PI;
        w_ = 2 * M_PI / T_;
#endif

#if defined SLOW || defined MID || defined FAST
        // Compute velocity
        vel_ = A_*w_*cos(w_*sec);
#endif

#ifdef X
        // Apply a small linear velocity to the model.
        this->model->SetLinearVel(math::Vector3(vel_, 0, 0.1));
#endif

#ifdef Y
        // Apply a small linear velocity to the model.
        this->model->SetLinearVel(math::Vector3(0, vel_, 0.1));
#endif

#ifdef Z
        // Apply a small linear velocity to the model.
        this->model->SetLinearVel(math::Vector3(0, 0,vel_+ 0.1));
#endif



#ifdef EIGHT_SHAPE

        // 8-shape trajectory
        double P_a=3.0;
        double P_b=1.5;
        double P_c=1.0;
        double P_n=-1.2;
	   
        P_T_=15;
	   
        double P_w1=2*M_PI/P_T_;
        double P_w2=P_w1/2;
        double P_w3=P_w1/2;

        double t=sec;
        double a=P_a;
        b_=P_b;
        double c=P_c;

        w1_=P_w1;
        double w2=P_w2;
        double w3=P_w3;
        double n=P_n;
        //out=[psi_r dot_psi_r ddot_psi_r];
        //ytraj=[a*cos(w2*t);b*sin(w1*t);n+c*sin(w3*t);0];
        vel_x_ = b_*w1_*cos(w1_*(t- P_T_ / 2.0) );
        double vel_y = -a*w2*sin(w2*(t- P_T_ / 2.0));
        double vel_z = c*w3*cos(w3*t);
        T_ = 2 * P_T_;

        // Apply a small linear velocity to the model.
#ifdef SLOW
        vel_x_ = vel_x_/2;
        vel_y = vel_y/2;
        vel_z= vel_z/2;
#endif

#ifdef MED
        vel_x_ = vel_x_;
        vel_y = vel_y;
        vel_z= vel_z;
#endif

#ifdef FAST
        vel_x_ = 1.5*vel_x_;
        vel_y = 1.5*vel_y;
        vel_z= 1.5*vel_z;
#endif

#ifdef STILL
        if (fmod(sec, 60.0)< 10.0){
            vel_x_ = 0;
            vel_y = 0;
            vel_z= 0;
        }
#endif


        this->model->SetLinearVel(math::Vector3(vel_x_, vel_y,vel_z+0.1));



 #endif

}


void ModelPush::getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg){

    ros::Time time = msg->clock;
    math::Pose pose = this->model->GetWorldPose();

    double sec = (modelPush.time_sec - modelPush.time_sec_prev) + (modelPush.time_nsec - modelPush.time_nsec_prev) / MILLION;

    if(!updated_){
        initial_time_sec = time.sec;
        initial_time_nsec = time.nsec;
        updated_ = true;
    }

    modelPush.time_sec = time.sec - initial_time_sec;
    modelPush.time_nsec = time.nsec - initial_time_nsec;

}

void ModelPush::resetCallback(const std_msgs::Bool::ConstPtr& msg){
    // Restore pose of the model to the origin
    math::Pose pose = this->model->GetWorldPose();

    pose.pos.x = initial_pose_.pos.x;
    pose.pos.y = initial_pose_.pos.y;
    pose.pos.z = initial_pose_.pos.z;
#ifdef Z
    pose.pos.z = initial_pose_.pos.z+2;
#endif
    pose.rot.x = 0;
    pose.rot.y = 0;
    pose.rot.z = 0;
    pose.rot.w = 1;
    this->model->SetWorldPose(pose);

    // Restart time
    updated_ = false;


}
