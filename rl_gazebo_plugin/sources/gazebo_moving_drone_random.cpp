#include "gazebo_moving_drone_random.h"

using namespace std;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)

bool poseEquals(math::Pose posA, math::Pose posB, float tol);

/////////////////////////////////////////////////

gazebo::ModelPush modelPush;

ModelPush::ModelPush()
{
}

ModelPush::~ModelPush()
{
}

void ModelPush::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
        // Store the pointer to the model
        model = _parent;

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&ModelPush::OnUpdate, this, _1));

        // Exit if no ROS
        if (!ros::isInitialized())
        {
            gzerr << "Not loading gazebo_moving_drone_random movement plugin since ROS hasn't"
                  << "been properly initialized. Try starting gazebo with ros plugin:\n"
                  << "  gazebo -s libgazebo_ros_api_plugin.so\n";
            return;
        }

        if (_sdf->HasElement("robotNamespace"))
            namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
        else
            gzwarn << "[gazebo_moving_platform] Please specify a robotNamespace.\n";

        node_handle_ = new ros::NodeHandle(namespace_);

        moving_sub = node_handle_->subscribe("clock", 10, &ModelPush::getSimulationClockTime, this);

        //this->reset_sub_ = this->node_handle_->subscribe("/moving_platform/reset", 10, &ModelPush::resetCallback, this);

        // Get initial pose
        initial_pose_ = model->GetWorldPose();

        //Move to desired origin
        math::Pose pose = model->GetWorldPose();
        pose.pos = origin;
        model->SetWorldPose(pose);

        cout << "[PLUGIN INFO]: Gazebo plugin gazebo_moving_drone_random succesfully initialized" << endl;
        //cout << "[PLUGIN INFO]: Plugin parameter values:\n" << "\t p1: v1" << "\t p2: v2" << endl; maybe later
        newTrajectory();
    }

// Called by the world update event
void ModelPush::OnUpdate(const common::UpdateInfo & /*_info*/)
    {
        // Sim time
        double sec = this->time_sec + this->time_nsec / 1E9;

        //Cancel rotation
        math::Pose pose = this->model->GetWorldPose();
        pose.rot.x = 0;
        pose.rot.y = 0;
        pose.rot.z = 0;
        pose.rot.w = 1;
        
        //print info
        #ifdef DEBUG
            std::cout << endl << "sec: " << sec << "; POS - X: " << pose.pos.x << "; Y: " << pose.pos.y << "; Z: " << pose.pos.z << std::endl;
        #endif

        model->SetWorldPose(pose);

        //Trajectory generation
        //Comment corresponding defines in .h to enable/disable movement in each axis.
        //X axis parameters are required for the plugin to function correctly

        float kf = 1.0; //Frecuency compensation to keep everything in sync
        vel_z_ = GRAVITY*STEP; //Gravity compensation

        //x axis movement
        //if(freq_x)
        {
            T_ = period/freq_x; //fix X as base
            kf = 1 / (T_ * freq_x);
            w_ = 2 * M_PI / T_;
            vel_x_ =  ampl_x*w_*cos(w_*sec);
        }

        //y axis movement
        //if(freq_y)
        {
            //kf = 1 / (T_ * freq_y);
            T_ = 1 / (kf*freq_y);
            w_ = 2 * M_PI / T_;
            vel_y_ =  ampl_y*w_*cos(w_*sec);
        }

        //z axis movement
        //if(freq_z)
        {
            T_ = 1 / (kf*freq_z);
            w_ = 2 * M_PI / T_;
            vel_z_ += ampl_z*w_*cos(w_*sec);
        }

/*
        //X avis movement
        #if defined(AMPL_X) && defined(FREQ_X)
            A_ = AMPL_X;
            T_ = PERIOD/FREQ_X; //fix X as base
            kf = 1 / (T_ * FREQ_X);
            w_ = 2 * M_PI / T_;
            vel_x_ =  A_*w_*cos(w_*sec);
        #endif

        //Y axis movement
        #if defined(AMPL_Y) && defined(FREQ_Y)
            A_ = AMPL_Y;

            #if !defined(AMPL_X) || !defined(FREQ_X)
                kf = 1 / (T_ * FREQ_Y);
            #endif

            T_ = 1 / (kf*FREQ_Y);
            w_ = 2 * M_PI / T_;
            vel_y_ =  A_*w_*cos(w_*sec);
        #endif

        //Z axis movement
        #if defined(AMPL_Z) && defined(FREQ_Z)
            A_ = AMPL_Z;
            T_ = 1 / (kf*FREQ_Z);
            w_ = 2 * M_PI / T_;
            vel_z_ += A_*w_*cos(w_*sec);
        #endif
*/

        //Update model        
        model->GetLink("base_link")->SetLinearVel({vel_x_, vel_y_, vel_z_}); //Update velocities
        model->GetLink("base_link")->SetAngularVel({0, 0, 0}); //Keep model from rotating

        //Info
        #ifdef DEBUG
            cout << "VEL - X: " << vel_x_ << "; Y: " << vel_y_ << "; Z: " << vel_z_ << endl;
        #endif
    }

//Get sime time from clock info
void ModelPush::getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg){

    //Get sim time
    ros::Time time = msg->clock;

    //update current time
    time_sec = time.sec - initial_time_sec;
    time_nsec = time.nsec - initial_time_nsec;

    //Get model pose
    math::Pose pose = model->GetWorldPose();
    

    // Check for trajectory end
    if (time_sec >= period*0.9 && poseEquals(initial_pose_, pose, 0.1)){ //due to precision, period is not exact
    //if(time_sec >= 1){    
        //Set current time as starting time
        initial_time_sec = time.sec;
        initial_time_nsec = time.nsec;

        //Reset position
        pose.pos = origin;
        model->SetWorldPose(pose);

        cout << "[PLUGIN INFO]: Position and time reset. Generating new trajectory." << endl;
        newTrajectory();
    }
}

//Return frequency ratio
float ModelPush::freqRatio()
{
    //
}

//Generate new random trajectory
void ModelPush::newTrajectory()
{
    std::srand(time(NULL));
    
    //Movement frequency
    do
    {
        freq_x = 1 + std::rand() % (MAX_FREQ - 1);
        freq_y = 1 + std::rand() % (MAX_FREQ - 1);
        freq_z = 1 + std::rand() % (MAX_FREQ - 1);
    }while(freq_x == 0 && freq_y == 0 && freq_z == 0);

    //Movement margin
    float margin_x = abs(abs(ORI_X) - BOUND_X) - MARGIN;
    float margin_y = abs(abs(ORI_Y) - BOUND_Y) - MARGIN;
    float margin_z = min((int)abs(ORI_Z - MIN_Z), abs(MAX_Z - ORI_Z)) - MARGIN;

    //Movement amplitude
    ampl_x = abs(-1 * margin_x + ((float)std::rand()/RAND_MAX) * 2 * margin_x);
    ampl_y = abs(-1 * margin_y + ((float)std::rand()/RAND_MAX) * 2 * margin_y);
    ampl_z = abs(-1 * margin_z + ((float)std::rand()/RAND_MAX) * 2 * margin_z);

    //Trajectory length representation to bound speed
    double length = 2*M_PI*(ampl_x*freq_x + ampl_y*freq_y + ampl_z*freq_z);

    //Speed
    double speed = MIN_SPEED + (MAX_SPEED - MIN_SPEED) * (float)std::rand()/RAND_MAX;
    period = length/speed;

    cout << "**** New trajectory ****" << endl;
    cout << "Origin:\n\tx: " << ORI_X << "\n\ty: " << ORI_Y << "\n\tz: " << ORI_Z << endl;
    cout << "Frecuency:\n\tx: " << freq_x << "\n\ty: " << freq_y << "\n\tz: " << freq_z << endl;
    cout << "Amplitude:\n\tx: " << ampl_x << "\n\ty: " << ampl_y << "\n\tz: " << ampl_z << endl;
    cout << "Length:\n\t: " << length << endl;
    cout << "Speed:\n\t: " << speed << endl;
    cout << "Period:\n\t: " << period << endl;

}

// Check pose equality with tolerance
bool poseEquals(math::Pose posA, math::Pose posB, float tol)
{
    math::Pose diff = posA - posB;
    if(abs(diff.pos.x) > tol || abs(diff.pos.y) > tol || abs(diff.pos.z) > tol)
        return false;
    else
        return true;
}


/*
#ifdef RUNAWAY
void ModelPush::resetCallback(const std_msgs::Bool::ConstPtr& msg){
    // Restore pose of the model to the origin
    math::Pose pose = this->model->GetWorldPose();

    pose.pos.x = initial_pose_.pos.x;
    pose.pos.y = initial_pose_.pos.y;
    pose.pos.z = initial_pose_.pos.z;
    pose.rot.x = 0;
    pose.rot.y = 0;
    pose.rot.z = 0;
    pose.rot.w = 1;
    this->model->SetWorldPose(pose);
    this->model->SetLinearVel(math::Vector3(0, 0, 0));
    this->model->SetAngularVel(math::Vector3(0.0, 0.0, 0.1));

}
#endif
*/
