#include "rl_environment_swarm_obstacle.h"

bool RlEnvironmentSwarmObstacle::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response){
    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Getting state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_obstacle, y_obstacle, z_obstacle, dx_obstacle, dy_obstacle;
    float x_relative, y_relative, dx_relative, dy_relative;
    float x_rel_obstacle, y_rel_obstacle, dx_rel_obstacle, dy_rel_obstacle;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetObstacleState(x_obstacle, y_obstacle, z_obstacle, dx_obstacle, dy_obstacle);
    x_rel_obstacle = x_obstacle - x_uav;
    y_rel_obstacle = y_obstacle - y_uav;
    dx_rel_obstacle = dx_obstacle - dx_uav;
    dy_rel_obstacle = dy_obstacle - dy_uav;
    x_relative = virtual_leader_pose_x_ - x_uav;
    y_relative = virtual_leader_pose_y_ - y_uav;
    dx_relative = dx_uav;
    dx_relative = dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    x_rel_obstacle /= (float)  MAX_POSE_X;
    y_rel_obstacle /= (float)  MAX_POSE_Y;

    // Print info
//    std::cout << "RL_ENV_DEBUG: obstacle x_relative " << x_rel_obstacle * (float)  MAX_POSE_X << " y_relative " << y_rel_obstacle * (float)  MAX_POSE_X << std::endl;

    // Compute reward
    float shaping = - 100 * sqrt(pow(x_relative,2) + pow(y_relative,2)) - 10 * sqrt(pow(dx_relative, 2) + pow(dy_relative, 2)) - sqrt(pow(request.action[0], 2)
            + pow(request.action[1], 2));
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;

    // Is it terminal?
    if ((fabs(x_uav / ((float) MAX_POSE_X / 2)) > 1) || (fabs(y_uav / ((float) MAX_POSE_Y / 2)) > 1)){
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else if (sqrt(pow(fabs(x_rel_obstacle * (float)  MAX_POSE_X), 2) + pow(fabs(y_rel_obstacle * (float)  MAX_POSE_Y), 2)) < MIN_DISTANCE_TO_OBSTACLE){
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else
        response.terminal_state = false;

    // Wait for environment to render
    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();

    // Read next state
    std::vector<float> state_next;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetObstacleState(x_obstacle, y_obstacle, z_obstacle, dx_obstacle, dy_obstacle);
    x_rel_obstacle = x_obstacle - x_uav;
    y_rel_obstacle = y_obstacle - y_uav;
    dx_rel_obstacle = dx_obstacle - dx_uav;
    dy_rel_obstacle = dy_obstacle - dy_uav;
    x_relative = virtual_leader_pose_x_ - x_uav;
    y_relative = virtual_leader_pose_y_ - y_uav;
    dx_relative = dx_uav;
    dx_relative = dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    x_rel_obstacle /= (float)  MAX_POSE_X;
    y_rel_obstacle /= (float)  MAX_POSE_Y;
    state_next.push_back(x_relative);
    state_next.push_back(y_relative);
    state_next.push_back(dx_relative);
    state_next.push_back(dy_relative);
    state_next.push_back(x_rel_obstacle);
    state_next.push_back(y_rel_obstacle);
    state_next.push_back(dx_rel_obstacle);
    state_next.push_back(dy_rel_obstacle);
    response.obs_real = state_next;

    // Successful return
    return true;
}

bool RlEnvironmentSwarmObstacle::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;

    // Action max
    std::vector<float> actions_max = {1.0, 1.0};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {-1.0, -1.0};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentSwarmObstacle::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv service called" << std::endl;

    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
//    if (exp_rec_.getRecording())
//        long_iteration_ = true;

    std::vector<float> s;
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_obstacle, y_obstacle, z_obstacle, dx_obstacle, dy_obstacle;
    float x_relative, y_relative, dx_relative, dy_relative;
    float x_rel_obstacle, y_rel_obstacle, dx_rel_obstacle, dy_rel_obstacle;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetObstacleState(x_obstacle, y_obstacle, z_obstacle, dx_obstacle, dy_obstacle);
    x_rel_obstacle = x_obstacle - x_uav;
    y_rel_obstacle = y_obstacle - y_uav;
    dx_rel_obstacle = dx_obstacle - dx_uav;
    dy_rel_obstacle = dy_obstacle - dy_uav;
    x_relative = virtual_leader_pose_x_ - x_uav;
    y_relative = virtual_leader_pose_y_ - y_uav;
    dx_relative = dx_uav;
    dx_relative = dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    x_rel_obstacle /= (float)  MAX_POSE_X;
    y_rel_obstacle /= (float)  MAX_POSE_Y;
    s.push_back(x_relative);
    s.push_back(y_relative);
    s.push_back(dx_relative);
    s.push_back(dy_relative);
    s.push_back(x_rel_obstacle);
    s.push_back(y_rel_obstacle);
    s.push_back(dx_rel_obstacle);
    s.push_back(dy_rel_obstacle);
    response.state = s;

    return true;
}

void RlEnvironmentSwarmObstacle::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg){
    // Get position of the uav in the vector
    int i_position;
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(UAV_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Set uav state
    SetUavState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);

    // Get position of the platform in the vector
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(OBSTACLE_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Set uav state
    SetObstacleState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);
}

void RlEnvironmentSwarmObstacle::InitChild(ros::NodeHandle n){
    // Print info
    std::cout << "RL_ENV_INFO: SWARM OBSTACLE ENVIRONMENT" << std::endl;

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;
    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_sub_ = n.subscribe(param_string, 10, &RlEnvironmentSwarmObstacle::PoseVelocityCallback, this);

    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_altitude_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~obstacle_pos_vel", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    obstacle_ref_pub_ = n.advertise<nav_msgs::Odometry>(param_string, 1000);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
//    this->data_->num_iterations = (unsigned int) NUM_ITERATIONS;

//    // Print
//    std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;

    // Init recording experiment
//    exp_rec_.Open(n);

    // Reset environment
    Reset();
}

bool RlEnvironmentSwarmObstacle::Reset(){
    // Check experiment recorder
//    if(exp_rec_.getRecording()){
//        exp_rec_.RecordExperiment();
//        exp_rec_.setRecording(false);
//    }

    // Get state
    float x, y, z, dx, dy;
    GetUavState(x, y, z, dx, dy);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Random UAV initial point
    std::srand(time(NULL));
    int seed = std::rand() % 10;
    int r_x = ((-1 * ((int) MAX_POSE_X * 100 / 2)) + (std::rand() % ((int) MAX_POSE_X * 100 / 2)));
    int r_y = ((-1 * ((int) MAX_POSE_Y * 100 / 2)) + (std::rand() % ((int) MAX_POSE_Y * 100 / 2)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    float init_x = (float) r_x / 100.f;
    float init_y = (float) r_y / 100.f;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: initial altitude: " << Z_INITIAL << std::endl;

    // Spawn UAV to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = 1.3;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting UAV in x: " << init_x << " and y " << init_y << " and z " << z << std::endl;

    if (gazebo_client_.call(model_msg)){
//        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    // Spawn UAV to a random location
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg)){
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
//        return false;
    }

    // Random virtual leader initial point
    r_x = ((-1 * ((int) MAX_POSE_X * 100 / 2)) + (std::rand() % ((int) MAX_POSE_X * 100 / 2)));
    r_y = ((-1 * ((int) MAX_POSE_Y * 100 / 2)) + (std::rand() % ((int) MAX_POSE_Y * 100 / 2)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    init_x = (float) r_x / 100.f;
    init_y = (float) r_y / 100.f;

    virtual_leader_pose_x_ = init_x;
    virtual_leader_pose_y_ = init_y;

    model_msg.request.model_state.model_name = VIRTUAL_LEADER_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = 0;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting VIRTUAL LEADER in x: " << init_x << " and y " << init_y << " and z " << 0 << std::endl;

    if (gazebo_client_.call(model_msg)){
//        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    // Random obstacle initial point
    r_x = ((-1 * ((int) MAX_POSE_X * 100 / 2)) + (std::rand() % ((int) MAX_POSE_X * 100 / 2)));
    r_y = ((-1 * ((int) MAX_POSE_Y * 100 / 2)) + (std::rand() % ((int) MAX_POSE_Y * 100 / 2)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    init_x = (float) r_x / 100.f;
    init_y = (float) r_y / 100.f;

    // Call obstacle plugin to set the obstacle pose and velocity
    nav_msgs::Odometry obstacle_msg;
//    obstacle_msg.header.stamp = ros::Time::now();
    obstacle_msg.pose.pose.position.x = init_x;
    obstacle_msg.pose.pose.position.y = init_y;
    obstacle_msg.pose.pose.position.z = 1.3;
    obstacle_msg.pose.pose.orientation.x = 0;
    obstacle_msg.pose.pose.orientation.y = 0;
    obstacle_msg.pose.pose.orientation.z = 0;
    obstacle_msg.pose.pose.orientation.w = 1;

    // Compute speeds
    float x_dir = virtual_leader_pose_x_ - init_x;
    float y_dir = virtual_leader_pose_y_ - init_y;
    float dir_module = sqrt(pow(x_dir, 2) + pow(y_dir, 2));
    x_dir /= dir_module;
    y_dir /= dir_module;

    // Generate random velocity
    int max_speed = 5;
    r_x = ((-1 * ((int) max_speed * 100)) + (std::rand() % ((int) max_speed * 100)));
    r_y = ((-1 * ((int) max_speed * 100)) + (std::rand() % ((int) max_speed * 100)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    float v_module = fabs((float) r_x / 1000.f);
    float v_x = v_module * x_dir;
    float v_y = v_module * y_dir;

    obstacle_msg.twist.twist.linear.x = v_x;
    obstacle_msg.twist.twist.linear.y = v_y;
    obstacle_msg.twist.twist.linear.z = 0;

    obstacle_ref_pub_.publish(obstacle_msg);

    return true;
}



