#include "experiment_recorder.h"

ExperimentRecorder::ExperimentRecorder(){

}

ExperimentRecorder::~ExperimentRecorder(){

}


void ExperimentRecorder::Open(ros::NodeHandle n){
    // Default values of camera pose
    pose_.clear();

    pose_.push_back(0);
    pose_.push_back(-5);
    pose_.push_back(3);
    pose_.push_back(-0.1577);
    pose_.push_back(0.1578);
    pose_.push_back(0.6890);
    pose_.push_back(0.6896);

    recording_experiment_ = false;
    switch_experiment_ = false;

    node_handle_ = n;

    // Advertise service
    srv_server_record_ = node_handle_.advertiseService("/experiment_record_srv", &ExperimentRecorder::RecordExperimentSrv, this);

    // Service clients
    spawn_model_client_ = node_handle_.serviceClient<gazebo_msgs::SpawnModel>("/gazebo/spawn_sdf_model");
    delete_model_client_ = node_handle_.serviceClient<gazebo_msgs::DeleteModel>("/gazebo/delete_model");

}

void ExperimentRecorder::CreateCamera(){
    // Spawn camera model
    gazebo_msgs::SpawnModel msg;
    msg.request.initial_pose.position.x = pose_[0];
    msg.request.initial_pose.position.y = pose_[1];
    msg.request.initial_pose.position.z = pose_[2];
    msg.request.initial_pose.orientation.x = pose_[3];
    msg.request.initial_pose.orientation.y = pose_[4];
    msg.request.initial_pose.orientation.z = pose_[5];
    msg.request.initial_pose.orientation.w = pose_[6];

    msg.request.model_name = "world_cam";

    msg.request.model_xml = "<sdf version='1.4'> <model name='world_cam'> <link name='sensors_link'> <inertial> <pose>0.01 0.025 0.025 0 -0 0</pose> <mass>0.01</mass> <inertia> <ixx>4.15e-6</ixx> <ixy>0</ixy> <ixz>0</ixz> <iyy>2.407e-6</iyy> <iyz>0</iyz> <izz>2.407e-6</izz> </inertia> </inertial> <visual name='visual'> <geometry> <box> <size>0.02 0.05 0.05</size> </box> </geometry> </visual> <sensor name='world_cam' type='camera'> <camera> <horizontal_fov>1.22</horizontal_fov> <vertical_fov>0.66</vertical_fov> <image> <width>640</width> <height>480</height> </image> <clip> <near>0.1</near> <far>100</far> </clip> <noise> <type>gaussian</type> <mean>0.0</mean> <stddev>0.005</stddev> </noise> </camera> <plugin name='camera_controller' filename='libgazebo_ros_camera.so'> <robotNamespace>/</robotNamespace> <cameraName>world_cam</cameraName> <imageTopicName>image_raw</imageTopicName> <cameraInfoTopicName>camera_info</cameraInfoTopicName> <frameName>world_cam</frameName> </plugin> <always_on>1</always_on> <update_rate>30</update_rate> <visualize>true</visualize> </sensor> </link> <static>1</static> </model> </sdf>";

    spawn_model_client_.call(msg);
}

void ExperimentRecorder::DeleteCamera(){
    gazebo_msgs::DeleteModel msg;
    msg.request.model_name = "world_cam";

    delete_model_client_.call(msg);
}

void ExperimentRecorder::SetSubscribers(std::vector<std::string> subscribers_name){

}


void ExperimentRecorder::ReleaseResources(){
    // Unsubscribe
    camera_sub_.shutdown();

    // Close bag
    bag_.close();

    // Delete camera
    DeleteCamera();

    // Adjusting  members
    recording_experiment_ = false;
}

void ExperimentRecorder::ShutdownSubscriber(int subscriber){

}

void ExperimentRecorder::SetCameraPose(std::vector<float> pose){
    pose_.clear();

    pose_.push_back(pose[0]);
    pose_.push_back(pose[1]);
    pose_.push_back(pose[2]);
    pose_.push_back(pose[3]);
    pose_.push_back(pose[4]);
    pose_.push_back(pose[5]);
    pose_.push_back(pose[6]);
}

void ExperimentRecorder::CameraCallback(sensor_msgs::ImageConstPtr msg){
    // Write
    bag_.write("/world_cam/image_raw", ros::Time::now(), *msg);
}

bool ExperimentRecorder::RecordExperimentSrv(rl_srvs::RecordExperimentSrv::Request &request, rl_srvs::RecordExperimentSrv::Response &response){
    // Enable record experiment
    switch_experiment_ = true;

    record_with_camera_ = request.record_with_camera;
    experiment_path_ = request.path_dir;
    episode_num_ = request.episode_num;

    return true;
}

void ExperimentRecorder::RecordExperiment(){
    if (!recording_experiment_){
        // Create folder
        boost::filesystem::path dir(experiment_path_);
        boost::filesystem::create_directory(dir);

        // Open bag
        std::string bag_path = experiment_path_ + "/" + std::to_string(episode_num_) + ".bag";
        bag_.open(bag_path, rosbag::bagmode::Write);


        // If required, spawn camera model
        if (record_with_camera_){
            CreateCamera();
            camera_sub_ = node_handle_.subscribe("/world_cam/image_raw", 10, &ExperimentRecorder::CameraCallback, this);
        }

        // Experiment recorder enabled
        recording_experiment_ = true;
    }
    else{
        ReleaseResources();
    }
}


