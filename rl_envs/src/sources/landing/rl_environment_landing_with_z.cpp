#include "rl_environment_landing_with_z.h"

bool RlEnvironmentLandingWithZ::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response){
    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Getting state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_platform, y_platform, z_platform, dx_platform, dy_platform;
    float x_relative, y_relative, z_relative, dx_relative, dy_relative;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    dx_relative = dx_platform - dx_uav;
    dy_relative = dy_platform - dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different

    // Increase/Decrease altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    current_z_ -= (request.action[2] * Z_STEP);
    altitude_msg.position_command.z = current_z_;
    uav_altitude_ref_pub_.publish(altitude_msg);

    // Is the UAV on top of the platform?
    int state;
    GetBumperState(state);

    // Print info
//    std::cout << "RL_ENV_DEBUG: UAV x_relative " << x_relative << " y_relative " << y_relative << std::endl;

    // Compute reward
    float shaping = - 100 * sqrt(pow(x_relative,2) + pow(y_relative,2) + pow(z_relative,2)) - 10 * sqrt(pow(dx_relative, 2) + pow(dy_relative, 2)) - sqrt(pow(request.action[0], 2)
            + pow(request.action[1], 2) + pow(request.action[2], 2)) + 10 * (float) state * (1 - fabs(request.action[0])) + 10 * (float) state * (1 - fabs(request.action[1])) + 10 * (float) state * (1 - fabs(request.action[2]));
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;

    // Is it terminal?
    if ((fabs(x_uav / ((float) MAX_POSE_X / 2)) > 1) || (fabs(y_uav / ((float) MAX_POSE_Y / 2)) > 1) || z_uav > MAX_ALLOWED_ALTIUDE){
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else if((z_uav <= 0.1) && (state == 0)){
        response.reward = -50;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else
        response.terminal_state = false;

    // Wait for environment to render
    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();

    // Read next state
    std::vector<float> state_next;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    dx_relative = dx_platform - dx_uav;
    dy_relative = dy_platform - dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different
    state_next.push_back(x_relative);
    state_next.push_back(y_relative);
    state_next.push_back(dx_relative);
    state_next.push_back(dy_relative);
    state_next.push_back(z_relative);
    state_next.push_back((float) state);
    response.obs_real = state_next;

    // Successful return
    return true;
}

bool RlEnvironmentLandingWithZ::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;

    // Action max
    std::vector<float> actions_max = {1.0, 1.0, 1};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {-1.0, -1.0, -1};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentLandingWithZ::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv service called" << std::endl;

    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
//    if (exp_rec_.getRecording())
//        long_iteration_ = true;

    int state;
    GetBumperState(state);

    std::vector<float> s;
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_platform, y_platform, z_platform, dx_platform, dy_platform;
    float x_relative, y_relative, z_relative, dx_relative, dy_relative;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    dx_relative = dx_platform - dx_uav;
    dy_relative = dy_platform - dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different
    s.push_back(x_relative);
    s.push_back(y_relative);
    s.push_back(dx_relative);
    s.push_back(dy_relative);
    s.push_back(z_relative);
    s.push_back((float) state);
    response.state = s;

    return true;
}

void RlEnvironmentLandingWithZ::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg){
    // Get position of the uav in the vector
    int i_position;
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(UAV_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Set uav state
    SetUavState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);

    // Get position of the platform in the vector
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(PLATFORM_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Set uav state
    SetPlatformState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);
}

void RlEnvironmentLandingWithZ::BumperStatesCallback(const gazebo_msgs::ContactsState::ConstPtr& msg){
    if (msg->states.size() > 0){
        float force = msg->states[0].wrenches[0].force.z;
        int state = 0;
        if (msg->states.size() == 2){
            std::string collision = msg->states[0].collision1_name;
            std::size_t found = collision.find(UAV_NAME);
            if (found!=std::string::npos){
                if (force > 3){
                    state = 1;
                }
            }
        }

        if (state == 1)     std::cout << "RL_ENV_DEBUG: Bumper state: " << state << std::endl;

        // Set uav state
        SetBumperState(state);

    }
}

void RlEnvironmentLandingWithZ::InitChild(ros::NodeHandle n){
    // Print info
    std::cout << "RL_ENV_INFO: LANDING CONSTANT ENVIRONMENT" << std::endl;

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;
    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_sub_ = n.subscribe(param_string, 10, &RlEnvironmentLandingWithZ::PoseVelocityCallback, this);

    ros::param::get("~bumper_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    bumper_states_sub_ = n.subscribe(param_string, 10, &RlEnvironmentLandingWithZ::BumperStatesCallback, this);

    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_altitude_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
//    this->data_->num_iterations = (unsigned int) NUM_ITERATIONS;

//    // Print
//    std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;

    // Init recording experiment
//    exp_rec_.Open(n);

    // Reset environment
    Reset();
}

bool RlEnvironmentLandingWithZ::Reset(){
    // Check experiment recorder
//    if(exp_rec_.getRecording()){
//        exp_rec_.RecordExperiment();
//        exp_rec_.setRecording(false);
//    }

    // Get state
    float x, y, z, dx, dy;
    GetUavState(x, y, z, dx, dy);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Random initial point
    std::srand(time(NULL));
    int seed = std::rand() % 10;
    int r_x = ((-1 * ((int) MAX_POSE_X * 100 / 2)) + (std::rand() % ((int) MAX_POSE_X * 100 / 2)));
    int r_y = ((-1 * ((int) MAX_POSE_Y * 100 / 2)) + (std::rand() % ((int) MAX_POSE_Y * 100 / 2)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    float init_x = (float) r_x / 100.f;
    float init_y = (float) r_y / 100.f;

    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = Z_INITIAL;
    uav_altitude_ref_pub_.publish(altitude_msg);

    // Reset variable
    current_z_ = Z_INITIAL;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: initial altitude: " << Z_INITIAL << std::endl;

    // Spawn UAV to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = Z_INITIAL + 2.0;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << " and y " << init_y << std::endl;

    if (gazebo_client_.call(model_msg)){
//        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg)){
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
//        return false;
    }

    return true;
}



