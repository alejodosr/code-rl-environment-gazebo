#include "rl_environment_landing_constant_test.h"

bool RlEnvironmentLandingConstantTest::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response){
    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];

    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Getting state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_platform, y_platform, z_platform, dx_platform, dy_platform;
    float x_relative, y_relative, z_relative, dx_relative, dy_relative;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    dx_relative = dx_platform - dx_uav;
    dy_relative = dy_platform - dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different

    // Decrease altitude
//    if (!REAL_TEST){
        droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
        altitude_msg.position_command.x = 0;
        altitude_msg.position_command.y = 0;

        current_z_ -= Z_STEP;
        altitude_msg.position_command.z = current_z_;
        uav_altitude_ref_pub_.publish(altitude_msg);
//    }

    // Is the UAV on top of the platform?
    int state;
    GetBumperState(state);

    // Print info
    //    std::cout << "RL_ENV_DEBUG: UAV x_relative " << x_relative << " y_relative " << y_relative << std::endl;

    // Compute reward
    float shaping = - 100 * sqrt(pow(x_relative,2) + pow(y_relative,2)) - 10 * sqrt(pow(dx_relative, 2) + pow(dy_relative, 2)) - sqrt(pow(request.action[0], 2)
            + pow(request.action[1], 2)) + 10 * (float) state * (1 - fabs(request.action[0])) + 10 * (float) state * (1 - fabs(request.action[1]));
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;

    // Is it terminal?
    /*    if ((fabs(x_uav / ((float) MAX_POSE_X / 2)) > 1) || (fabs(y_uav / ((float) MAX_POSE_Y / 2)) > 1)){
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else */if((z_uav <= 0.1) && (state == 0) && (!REAL_TEST)){
        response.reward = -50;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else
        response.terminal_state = false;

    // Wait for environment to render
    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();

    // Read next state
    std::vector<float> state_next;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    dx_relative = dx_platform - dx_uav;
    dy_relative = dy_platform - dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different
    state_next.push_back(x_relative);
    state_next.push_back(y_relative);
    state_next.push_back(dx_relative);
    state_next.push_back(dy_relative);
    state_next.push_back(z_relative);
    state_next.push_back((float) state);
    response.obs_real = state_next;

    // Successful return
    return true;
}

bool RlEnvironmentLandingConstantTest::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;

    // Action max
    std::vector<float> actions_max = {1.0, 1.0};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {-1.0, -1.0};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentLandingConstantTest::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv service called" << std::endl;

    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
    //    if (exp_rec_.getRecording())
    //        long_iteration_ = true;

    int state;
    GetBumperState(state);

    std::vector<float> s;
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_platform, y_platform, z_platform, dx_platform, dy_platform;
    float x_relative, y_relative, z_relative, dx_relative, dy_relative;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    dx_relative = dx_platform - dx_uav;
    dy_relative = dy_platform - dy_uav;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different
    s.push_back(x_relative);
    s.push_back(y_relative);
    s.push_back(dx_relative);
    s.push_back(dy_relative);
    s.push_back(z_relative);
    s.push_back((float) state);
    response.state = s;

    return true;
}

void RlEnvironmentLandingConstantTest::MocapCallback(const nav_msgs::Odometry::ConstPtr& msg){
    // Include noise in the measurements
    // construct a trivial random generator engine from a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator (seed);

    std::normal_distribution<double> distribution (0.0,MAX_NOISE_DISTRIBUTION);
    distribution(generator);

    if (ENABLE_NOISY_MEASUREMENTS){
        // Set uav state
        SetUavState(msg->pose.pose.position.x + distribution(generator)/*/ (float) MAX_POSE_X*/, msg->pose.pose.position.y + distribution(generator)/*/ (float) MAX_POSE_Y*/,
                    msg->pose.pose.position.z + distribution(generator), msg->twist.twist.linear.x + distribution(generator),
                    msg->twist.twist.linear.y + distribution(generator));
    }
    else{
        // Set uav state
        SetUavState(msg->pose.pose.position.x /*/ (float) MAX_POSE_X*/, msg->pose.pose.position.y/*/ (float) MAX_POSE_Y*/,
                    msg->pose.pose.position.z, msg->twist.twist.linear.x,
                    msg->twist.twist.linear.y);
    }

//    // Set uav state
//    SetPlatformState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);

}

void RlEnvironmentLandingConstantTest::MocapCallback2(const nav_msgs::Odometry::ConstPtr& msg){
    // Include noise in the measurements
    // construct a trivial random generator engine from a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator (seed);

    std::normal_distribution<double> distribution (0.0,MAX_NOISE_DISTRIBUTION);
    distribution(generator);

    if (ENABLE_NOISY_MEASUREMENTS){
        // Set platform state
        SetPlatformState(msg->pose.pose.position.x + distribution(generator) /*/ (float) MAX_POSE_X*/, msg->pose.pose.position.y + distribution(generator)/*/ (float) MAX_POSE_Y*/,
                         msg->pose.pose.position.z + distribution(generator), msg->twist.twist.linear.x + distribution(generator), msg->twist.twist.linear.y + distribution(generator));
    }
    else{
        // Set platform state
        SetPlatformState(msg->pose.pose.position.x /*/ (float) MAX_POSE_X*/, msg->pose.pose.position.y - 0.2/*/ (float) MAX_POSE_Y*/,
                         msg->pose.pose.position.z, msg->twist.twist.linear.x , msg->twist.twist.linear.y);

    }
}

void RlEnvironmentLandingConstantTest::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg){
    // Get position of the uav in the vector
    int i_position;
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(UAV_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Include noise in the measurements
    // construct a trivial random generator engine from a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator (seed);

    std::normal_distribution<double> distribution (0.0,MAX_NOISE_DISTRIBUTION);
    distribution(generator);

    if (ENABLE_NOISY_MEASUREMENTS){
        // Set uav state
        SetUavState(msg->pose[i_position].position.x + distribution(generator)/*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y + distribution(generator)/*/ (float) MAX_POSE_Y*/,
                    msg->pose[i_position].position.z /*+ distribution(generator)*/, msg->twist[i_position].linear.x + distribution(generator),
                    msg->twist[i_position].linear.y + distribution(generator));
    }
    else{
        // Set uav state
        SetUavState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/,
                    msg->pose[i_position].position.z, msg->twist[i_position].linear.x,
                    msg->twist[i_position].linear.y);
    }   

//    // Compute speeds from pose ground truth
//    ros::Time current_timestamp = ros::Time::now();

//    double x_raw_t = msg->pose[i_position].position.x;
//    double y_raw_t = msg->pose[i_position].position.y;

//    time_t tv_sec; suseconds_t tv_usec;
//    {
//    tv_sec  = current_timestamp.sec;
//    tv_usec = current_timestamp.nsec / 1000.0;
//    filtered_derivative_wcb_x_.setInput( x_raw_t, tv_sec, tv_usec);
//    filtered_derivative_wcb_y_.setInput( y_raw_t, tv_sec, tv_usec);
//    }

//    double x_t, dx_t;
//    double y_t, dy_t;
//    filtered_derivative_wcb_x_.getOutput( x_t,  dx_t);
//    filtered_derivative_wcb_y_.getOutput( y_t,  dy_t);

//    // Publish velocities
//    droneMsgsROS::droneSpeeds velocities;
//    velocities.dx = dx_t;
//    velocities.dy = dy_t;

//    velocities_pub_.publish(velocities);

    // Get position of the platform in the vector
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(PLATFORM_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Set platform state
    SetPlatformState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);

}

void RlEnvironmentLandingConstantTest::BumperStatesCallback(const gazebo_msgs::ContactsState::ConstPtr& msg){
    if (msg->states.size() > 0){
        float force = msg->states[0].wrenches[0].force.z;
        int state = 0;
        if (msg->states.size() == 2){
            std::string collision = msg->states[0].collision1_name;
            std::size_t found = collision.find(UAV_NAME);
            if (found!=std::string::npos){
                if (force > 3){
                    state = 1;
                }
            }
        }

        if (state == 1)     std::cout << "RL_ENV_DEBUG: Bumper state: " << state << std::endl;

        // Set uav state
        SetBumperState(state);

    }
}

void RlEnvironmentLandingConstantTest::InitChild(ros::NodeHandle n){
    // Print info
    std::cout << "RL_ENV_INFO: LANDING CONSTANT ENVIRONMENT TEST" << std::endl;

    // Init subscribers
    uav_pose_velocity_sub_ = n.subscribe("/gazebo/model_states", 10, &RlEnvironmentLandingConstantTest::PoseVelocityCallback, this);
    bumper_states_sub_ = n.subscribe("/bumper_states", 10, &RlEnvironmentLandingConstantTest::BumperStatesCallback, this);
    mocap_sub_ = n.subscribe("/vrpn_client_node/RigidBody_1/pose", 10, &RlEnvironmentLandingConstantTest::MocapCallback, this);
    mocap2_sub_ = n.subscribe("/vrpn_client_node/RigidBody_2/pose", 10, &RlEnvironmentLandingConstantTest::MocapCallback2, this);

    if (!REAL_TEST){
        // Init publishers
        uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone7/droneSpeedsRefs", 1000);
        uav_altitude_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone7/dronePositionRefs", 1000);
        velocities_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone7/computed_velocities", 1000);

        // Init service
        gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
        estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone7/droneOdometryStateEstimator/reset");
    }
    else{
        // Init publishers
        uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone4/droneSpeedsRefs", 1000);
        uav_altitude_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone4/dronePositionRefs", 1000);
        velocities_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone4/computed_velocities", 1000);

        // Init service
        gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
        estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone4/droneOdometryStateEstimator/reset");
    }
    // Set number of iterations of Gazebo
//    if (!REAL_TEST) this->data_->num_iterations = (unsigned int) NUM_ITERATIONS;

    // Init circular buffer
    filtered_derivative_wcb_x_.setTimeParameters( 0.0015,0.0015,0.150,1.0,100.000);
    filtered_derivative_wcb_y_.setTimeParameters( 0.0015,0.0015,0.150,1.0,100.000);

    filtered_derivative_wcb_x_.reset();
    filtered_derivative_wcb_y_.reset();

    // Print
//    if (!REAL_TEST) std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;

    // Set uav state
    SetPlatformState(0, 0, 0, 0, 0);

    // Init recording experiment
    //    exp_rec_.Open(n);

    // Reset environment
    Reset();
}

bool RlEnvironmentLandingConstantTest::Reset(){
    // Check experiment recorder
    //    if(exp_rec_.getRecording()){
    //        exp_rec_.RecordExperiment();
    //        exp_rec_.setRecording(false);
    //    }

    if (!REAL_TEST){

        // Get state
        float x, y, z, dx, dy;
        GetUavState(x, y, z, dx, dy);

        // Send null action
        droneMsgsROS::droneSpeeds action_msg;
        action_msg.dx = 0;
        action_msg.dx = 0;
        action_msg.dz = 0;
        uav_speed_ref_pub_.publish(action_msg);

        // Random initial point
        std::srand(time(NULL));
        int seed = std::rand() % 10;
        int r_x = ((-1 * ((int) MAX_POSE_X * 100)) + (std::rand() % ((int) MAX_POSE_X * 100)));
        int r_y = ((-1 * ((int) MAX_POSE_Y * 100)) + (std::rand() % ((int) MAX_POSE_Y * 100)));
        if (seed < 3){
            r_x *= -1;
            r_y *= -1;
        }
        else if(seed < 6){
            r_x *= -1;
            r_y *= 1;
        }
        else if(seed < 8){
            r_x *= 1;
            r_y *= -1;
        }
        else{
            r_x *= 1;
            r_y *= 1;
        }

        float init_x = (float) r_x / 100.f;
        float init_y = (float) r_y / 100.f;

        // Set initial altitude
        droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
        altitude_msg.header.stamp.sec = 0;
        altitude_msg.header.stamp.nsec = 0;
        altitude_msg.header.frame_id = "";
        altitude_msg.header.seq = 0;
        altitude_msg.position_command.x = 0;
        altitude_msg.position_command.y = 0;
        altitude_msg.position_command.z = Z_INITIAL;
        uav_altitude_ref_pub_.publish(altitude_msg);

        // Reset variable
        current_z_ = Z_INITIAL;

        // Reseting environemnt
        std::cout << "RL_ENV_INFO: initial altitude: " << Z_INITIAL << std::endl;

        // Spawn UAV to origin
        gazebo_msgs::SetModelState model_msg;
        model_msg.request.model_state.model_name = UAV_NAME;
        model_msg.request.model_state.pose.position.x = init_x;
        model_msg.request.model_state.pose.position.y = init_y;
        model_msg.request.model_state.pose.position.z = Z_INITIAL + 2.0;

        // Reseting environemnt
        std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << " and y " << init_y << std::endl;

        if (gazebo_client_.call(model_msg)){
            //        return true;
        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
            //        return false;
        }

        // Spawn model to origin
        std_srvs::Empty estimator_msg;

        if (estimator_client_.call(estimator_msg)){
            ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
            //        return false;
        }
    }
    else{
        // Reset variable
        current_z_ = Z_INITIAL;
    }

    return true;
}



