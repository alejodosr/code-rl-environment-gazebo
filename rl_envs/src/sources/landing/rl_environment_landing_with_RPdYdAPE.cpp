#include "rl_environment_landing_with_RPdYdAPE.h"

bool RlEnvironmentLandingWithRPdYdAPE::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response){
    // Send roll pitch
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    roll_pitch_msg.rollCmd = SCALE_RP * request.action[0];  // Roll
    roll_pitch_msg.pitchCmd = SCALE_RP * request.action[1];  // Pitch
    roll_pitch_ref_pub_.publish(roll_pitch_msg);

    // Send yaw
    droneMsgsROS::droneDYawCmd dyaw_msg;
    //    dyaw_msg.dYawCmd = 0; // No dYaw
    dyaw_msg.dYawCmd = /* SCALE_ACTION **/ SCALE_YAW * request.action[2];
    dYaw_ref_pub_.publish(dyaw_msg);

    // Send yaw
    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    //    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    daltitude_msg.dAltitudeCmd = SCALE_ALTITUDE * request.action[3];  // dAltitude
    dAltitude_ref_pub_.publish(daltitude_msg);

    // Getting state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_platform, y_platform, z_platform, dx_platform, dy_platform;
    float x_relative, y_relative, z_relative, dx_relative, dy_relative;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    //    dx_relative = dx_platform - dx_uav;
    //    dy_relative = dy_platform - dy_uav;
    dx_relative = (prev_x_relative_ - x_relative ) / (float) MAX_POSE_X;
    dy_relative = (prev_y_relative_ - y_relative) / (float) MAX_POSE_Y;
    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different
    float yaw = current_yaw_;
//        std::cout << "yaw: " << yaw << std::endl;

    // Increase/Decrease altitude
    //    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    //    altitude_msg.position_command.x = 0;
    //    altitude_msg.position_command.y = 0;
    //    current_z_ -= (request.action[2] * Z_STEP);
    //    altitude_msg.position_command.z = current_z_;
    //    uav_altitude_ref_pub_.publish(altitude_msg);

    // Is the UAV on top of the platform?
    int state;
    GetBumperState(state);

    // Print info
    //    std::cout << "RL_ENV_DEBUG: UAV x_relative " << x_relative << " y_relative " << y_relative << std::endl;

    // Compute reward
    float shaping = - 100 * sqrt(pow(x_relative,2) + pow(y_relative,2) + pow(z_relative,2) + pow(yaw,2)) - 10 * sqrt(pow(dx_relative, 2)
                                                                                                                     + pow(dy_relative, 2)) - sqrt(pow(request.action[0], 2) + pow(request.action[1], 2) + pow(request.action[2], 2) + pow(request.action[3], 2));
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;

    //    std::cout << "ENV DEBUG: Emergency action " << request.action[4] << std::endl;

    if (reset_executed_ && (request.action[4] < EMERGENCY_THRESHOLD)){
        landing_executed_ = false;
        reset_executed_ = false;
    }

    if ((request.action[4] > EMERGENCY_THRESHOLD) && !landing_executed_){
        if (!ENABLE_EMERGENCY_INHIBITION || ((z_relative * (float) Z_INITIAL) < MAX_EMERGENCY_ALTITUDE)){
            landing_executed_ = true;
            emergency_altitude_ = z_relative * (float) Z_INITIAL;
            std::cout << "ENV DEBUG: Emergency altitude " << emergency_altitude_ << std::endl;
            droneMsgsROS::droneCommand msg_command;
            msg_command.command = droneMsgsROS::droneCommand::RESET;
            drone_command_publ_.publish(msg_command);
        }
    }

    // Is it terminal?
    if ((fabs(x_uav / ((float) MAX_POSE_X / 2)) > 1) || (fabs(y_uav / ((float) MAX_POSE_Y / 2)) > 1) || z_uav > MAX_ALLOWED_ALTIUDE){
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else if((z_uav <= 0.1) && (state == 0)){
        response.reward = -50;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else if((landing_executed_) && (state == 1)){
        if (emergency_altitude_ < MAX_EMERGENCY_ALTITUDE){
            float MAX_REWARD = 100.0;
            float MIN_REWARD = 50.0;
            float K = std::log(MIN_REWARD / MAX_REWARD) / (DESIRED_EMERGENCY_ALTITUDE - MAX_EMERGENCY_ALTITUDE);
            response.reward = MAX_REWARD * std::exp(K * (DESIRED_EMERGENCY_ALTITUDE - emergency_altitude_));
            std::cout << "ENV DEBUG: reward " << response.reward << std::endl;
        }
        else{
            response.reward = -50;
        }

        response.terminal_state = true;
        reset_env_ = true;
    }
    else
        response.terminal_state = false;

    // Wait for environment to render
    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();

    // Read next state
    std::vector<float> state_next;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    //    dx_relative = dx_platform - dx_uav;
    //    dy_relative = dy_platform - dy_uav;
    dx_relative = (prev_x_relative_ - x_relative) / (float) MAX_POSE_X;
    dy_relative = (prev_y_relative_ - y_relative) / (float) MAX_POSE_Y;

    // Save previous state
    prev_x_relative_ = x_relative;
    prev_y_relative_ = y_relative;


    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different
    state_next.push_back(x_relative);
    state_next.push_back(y_relative);
    state_next.push_back(dx_relative);
    state_next.push_back(dy_relative);
    state_next.push_back(z_relative);
    state_next.push_back((float) state);
    state_next.push_back(yaw);
    response.obs_real = state_next;

    //    std::cout << x_relative << "\t" << y_relative << std::endl;

    // Successful return
    return true;
}

bool RlEnvironmentLandingWithRPdYdAPE::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;

    // Action max
    std::vector<float> actions_max = {1.0, 1.0, 1.0, 1.0, 1.0};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {-1.0, -1.0, -1.0, -1.0, -1.0};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentLandingWithRPdYdAPE::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv service called" << std::endl;

    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
    //    if (exp_rec_.getRecording())
    //        long_iteration_ = true;

    int state;
    GetBumperState(state);

    std::vector<float> s;
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    float x_platform, y_platform, z_platform, dx_platform, dy_platform;
    float x_relative, y_relative, z_relative, dx_relative, dy_relative;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);
    GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
    x_relative = x_platform - x_uav;
    y_relative = y_platform - y_uav;
    z_relative = z_platform - z_uav;
    //    dx_relative = dx_platform - dx_uav;
    //    dy_relative = dy_platform - dy_uav;
    dx_relative = (x_relative - prev_x_relative_) / (float) MAX_POSE_X;
    dy_relative = (y_relative - prev_y_relative_) / (float) MAX_POSE_Y;

    // Save previous state
    prev_x_relative_ = x_relative;
    prev_y_relative_ = y_relative;

    // NOTE: normalization is carried out here
    x_relative /= (float)  MAX_POSE_X;
    y_relative /= (float)  MAX_POSE_Y;
    z_relative = z_uav / (float)  Z_INITIAL;    // z_relative is slightly different
    s.push_back(x_relative);
    s.push_back(y_relative);
    s.push_back(dx_relative);
    s.push_back(dy_relative);
    s.push_back(z_relative);
    s.push_back((float) state);
    s.push_back(current_yaw_);
    response.state = s;



    return true;
}

void RlEnvironmentLandingWithRPdYdAPE::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg){
    // Get position of the uav in the vector
    int i_position;
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(UAV_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Compute yaw
    // Calculating Roll, Pitch, Yaw
    tf::Quaternion q(msg->pose[i_position].orientation.x, msg->pose[i_position].orientation.y, msg->pose[i_position].orientation.z, msg->pose[i_position].orientation.w);
    tf::Matrix3x3 m(q);

    //convert quaternion to euler angels
    double y, p, r;
    m.getEulerYPR(y, p, r);

    if (std::isfinite(y) && (fabs(y) / (double) M_PI) <= 1)
        current_yaw_ = (float) y / (float) M_PI;
    else
        std::cout << "RL_DEBUG: Yaw out of ranges - " << current_yaw_  << std::endl;

    // Set uav state
    SetUavState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);

    // Get position of the platform in the vector
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(PLATFORM_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Set uav state
    SetPlatformState(msg->pose[i_position].position.x /*/ (float) MAX_POSE_X*/, msg->pose[i_position].position.y /*/ (float) MAX_POSE_Y*/, msg->pose[i_position].position.z + Z_PLATFORM_OFFSET, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);
}

void RlEnvironmentLandingWithRPdYdAPE::BumperStatesCallback(const gazebo_msgs::ContactsState::ConstPtr& msg){
    if (msg->states.size() > 0){
        float force = msg->states[0].wrenches[0].force.z;
        int state = 0;
        if (msg->states.size() == 2){
            std::string collision = msg->states[0].collision1_name;
            std::size_t found = collision.find(UAV_NAME);
            if (found!=std::string::npos){
                if (force > 3){
                    state = 1;
                }
            }
        }

//        if (state == 1)     std::cout << "RL_ENV_DEBUG: Bumper state: " << state << std::endl;

        // Set uav state
        SetBumperState(state);

    }
}

void RlEnvironmentLandingWithRPdYdAPE::PoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg){
//    current_yaw_ = msg->yaw / M_PI;
}

void RlEnvironmentLandingWithRPdYdAPE::StatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg){
    //    std::cout << msg->status << std::endl;
    SetStatus((int) msg->status);
}

void RlEnvironmentLandingWithRPdYdAPE::InitChild(ros::NodeHandle n){
    // Print info
    std::cout << "RL_ENV_INFO: LANDING CONSTANT ENVIRONMENT" << std::endl;

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;
    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_sub_ = n.subscribe(param_string, 10, &RlEnvironmentLandingWithRPdYdAPE::PoseVelocityCallback, this);

    ros::param::get("~bumper_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    bumper_states_sub_ = n.subscribe(param_string, 10, &RlEnvironmentLandingWithRPdYdAPE::BumperStatesCallback, this);

    ros::param::get("~uav_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    pose_sub_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 10, &RlEnvironmentLandingWithRPdYdAPE::PoseCallback, this);

    ros::param::get("~status", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    status_sub_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 10, &RlEnvironmentLandingWithRPdYdAPE::StatusCallback, this);

    // Init publishers
    ros::param::get("~roll_pitch", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    roll_pitch_ref_pub_ = n.advertise<droneMsgsROS::dronePitchRollCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~dYaw", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    dYaw_ref_pub_ = n.advertise<droneMsgsROS::droneDYawCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~dAltitude", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    dAltitude_ref_pub_ = n.advertise<droneMsgsROS::droneDAltitudeCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_altitude_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~control_mode", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_command_publ_= n.advertise<droneMsgsROS::droneCommand>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);


    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
    //    this->data_->num_iterations = (unsigned int) NUM_ITERATIONS;

    //    // Print
    //    std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;

    // Init recording experiment
    //    exp_rec_.Open(n);

    SetStatus(droneMsgsROS::droneStatus::FLYING);

    episode_no_ = 0;

    // Reset environment
    Reset();
}

bool RlEnvironmentLandingWithRPdYdAPE::Reset(){

    // Check experiment recorder
    //    if(exp_rec_.getRecording()){
    //        exp_rec_.RecordExperiment();
    //        exp_rec_.setRecording(false);
    //    }

    // Take off
    if (landing_executed_){
        droneMsgsROS::droneCommand msg_command;
        msg_command.command = droneMsgsROS::droneCommand::TAKE_OFF;
        drone_command_publ_.publish(msg_command);
    }

    // Variables init
    prev_x_relative_ = 0;
    prev_y_relative_= 0;


    // Get state
    float x, y, z, dx, dy;
    GetUavState(x, y, z, dx, dy);


    // Send null action
    // Send roll pitch
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    roll_pitch_msg.rollCmd = 0;  // Roll
    roll_pitch_msg.pitchCmd = 0;  // Pitch
    roll_pitch_ref_pub_.publish(roll_pitch_msg);

    // Send yaw
    droneMsgsROS::droneDYawCmd dyaw_msg;
    dyaw_msg.dYawCmd = 0; // No dYaw
    dYaw_ref_pub_.publish(dyaw_msg);

    // Send yaw
    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    dAltitude_ref_pub_.publish(daltitude_msg);

    // Random initial point
    std::srand(time(NULL));
    int seed = std::rand() % 10;
    int r_x = ((-1 * (((int) MAX_POSE_X - 1) * 100 / 2)) + (std::rand() % (((int) MAX_POSE_X - 1) * 100 / 2)));
    int r_y = ((-1 * (((int) MAX_POSE_Y - 1) * 100 / 2)) + (std::rand() % (((int) MAX_POSE_Y - 1)* 100 / 2)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    float init_x = (float) r_x / 100.f;
    float init_y = (float) r_y / 100.f;

    // Set initial altitude
    //    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    //    altitude_msg.header.stamp.sec = 0;
    //    altitude_msg.header.stamp.nsec = 0;
    //    altitude_msg.header.frame_id = "";
    //    altitude_msg.header.seq = 0;
    //    altitude_msg.position_command.x = 0;
    //    altitude_msg.position_command.y = 0;
    //    altitude_msg.position_command.z = Z_INITIAL;
    //    uav_altitude_ref_pub_.publish(altitude_msg);

    // Reset variable
    current_z_ = Z_INITIAL;
    current_yaw_ = 0;

    float init_z = Z_INITIAL;

    // If enable guided states
    if(ENABLE_GUIDED_STATES && (episode_no_ % GUIDED_STATES_INTERVAL == 0)){
        float x_platform, y_platform, z_platform, dx_platform, dy_platform;
        GetPlatformState(x_platform, y_platform, z_platform, dx_platform, dy_platform);
        init_x = x_platform;
        init_y = y_platform;
        init_z = MAX_EMERGENCY_ALTITUDE * 0.8;
    }

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: initial altitude: " << Z_INITIAL << std::endl;

    // Spawn UAV to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = init_z /*+ 2.0*/;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << " and y " << init_y << std::endl;

    if (gazebo_client_.call(model_msg)){
        //        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
        //        return false;
    }

    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg)){
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        //        return false;
    }

    // Move
    if (landing_executed_){
        droneMsgsROS::droneCommand msg_command;
        msg_command.command = droneMsgsROS::droneCommand::MOVE;
        drone_command_publ_.publish(msg_command);
    }

    // Init variable
    reset_executed_ = true;
    episode_no_++;

    return true;
}



