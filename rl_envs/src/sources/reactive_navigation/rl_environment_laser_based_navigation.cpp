#include "rl_environment_laser_based_navigation.h"

#define DEBUG_MODE_STATE 0
#define DEBUG_UAV_TARGET_POSITON 0
#define DEBUG_SHAPING_REWARD 0
#define DEBUG_LASER_MEASUREMENTS 0
#define DEBUG_POTENTIAL_FIELDS 0

#define STATE_BASED_ON_GROUND_TRUTH_DATA 1
#define STATE_BASED_ON_GROUP_OF_LASER_RANGES 1
#define STATE_BASED_ON_UAV_POS_AND_SPEED 1
#define STATE_BASED_ON_UAV_POS_AND_DIFF_PREVIOUS_POSE 0
#define COMPUTE_REPULSIVE_POTENTIAL_FIELD 1

#define IMSHOW_LASER_STATE_SCANS 1
#define IMSHOW_LASER_STATE_SCANS_AND_GOAL 1
#define IMSHOW_OBSTACLES_BOUNDARY 0
#define IMSHOW_ALL_SATURATED_RANGES 0


#define TEST_MODE 0
#define TEST_GOAL_SEQUENCE_MODE 0
#define MOVING_OBSTACLE_FOR_TRAINING 0


RlEnvironmentLaserBasedNavigation::RlEnvironmentLaserBasedNavigation()
{
    test_goal_sequence_mode_flag_ = false;
    imshow_laser_state_scans_ = false;
    imshow_laser_state_scans_and_goal_ = false;
    imshow_all_saturated_ranges_ = false;
    imshow_obstacles_boundary_ = false;
    cont_ind_goal_in_sequence_ = 0;
    num_goals_in_sequence_ = 0;
    UAV_NAME = "hummingbird2";
    TARGET_NAME = "goal";

    if(TEST_MODE) //Use house_3 for a 8x8 house or house_3c for a 5x8 house
        environment_info_.name_ = "house_3c";
    else
        environment_info_.name_ = "house_6";
    environment_info_.num_episode_steps_ = 500;
    environment_info_.num_iterations_ = 4;
    environment_info_.actions_dim_ = 2;
//    environment_info_.actions_min_value_ = {-0.5, -0.5};
//    environment_info_.actions_max_value_ = {0.5, 0.5};
    environment_info_.actions_min_value_ = {-1.0, -1.0};
    environment_info_.actions_max_value_ = {1.0, 1.0};
//    environment_info_.actions_min_value_ = {-0.7, -0.7};
//    environment_info_.actions_max_value_ = {0.7, 0.7};
    if(environment_info_.name_ == "house")
    {
        environment_info_.max_pos_x_ = 15.0;
        environment_info_.max_pos_y_ = 15.0;
    }
    else if(environment_info_.name_ == "house_2")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "house_3")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_3b")
    {
        environment_info_.max_pos_x_ = 6.0;
        environment_info_.max_pos_y_ = 6.0;
    }
    else if(environment_info_.name_ == "house_3c")
    {
        if(TEST_MODE)
        {
            //In this case, despite the size of the env is 4x8 m
            //we use the same normalization factor as TRAIN ENV (house 3)
            environment_info_.max_pos_x_ = 8.0;
            environment_info_.max_pos_y_ = 8.0;
        }
        else
        {
            environment_info_.max_pos_x_ = 5.0;
            environment_info_.max_pos_y_ = 8.0;
        }
    }
    else if(environment_info_.name_ == "house_4")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_5")
    {
        environment_info_.max_pos_x_ = 5.0;
        environment_info_.max_pos_y_ = 5.0;
    }
    else if(environment_info_.name_ == "house_6")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "maze")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 18.0;
    }
    else if(environment_info_.name_ == "maze_3")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 8.5;
    }
    else if(environment_info_.name_ == "maze_4")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }


    laser_info_.max_virtual_range_ = 2.0; //Maximum range for defining the laser components in the STATE.
    laser_info_.max_real_range_ = 30.0;
    laser_info_.min_real_range_ = 0.10;
    laser_info_.num_ranges_ = 720; //720;
    laser_info_.angle_range_ = 270;
    laser_info_.sampling_factor_ = int(720/10.0); //72; //Sampling every 27 degrees
    //laser_info_.sampling_factor_ = 48; //Sampling every 18 degrees
    laser_info_.angle_sampling_factor_ = laser_info_.angle_range_ / (laser_info_.num_ranges_/laser_info_.sampling_factor_);

    if(TEST_MODE)
        laser_info_.min_range_reset_value_ = 0.3;
    else
        laser_info_.min_range_reset_value_ = 0.4;
    laser_info_.laser_state_normalization_factor_ = laser_info_.max_virtual_range_ - laser_info_.min_range_reset_value_;



    laser_image_info_.angles_ranges_.clear();
    laser_image_info_.laser_scans_image_size_ = cv::Size(200, 200);
    laser_image_info_.angle_ini_ = -45.0;
    laser_image_info_.angle_ini_rad_ = laser_image_info_.angle_ini_*M_PI/180;
    laser_image_info_.angle_increment_ = laser_info_.angle_range_ / laser_info_.num_ranges_;
    laser_image_info_.p_origin_ = cv::Point(laser_image_info_.laser_scans_image_size_.width/2.0,
                                            laser_image_info_.laser_scans_image_size_.height/2.0);
    for(int i=0;i<laser_info_.num_ranges_;i++)
    {
        float angle_i = (laser_image_info_.angle_ini_ + i*laser_image_info_.angle_increment_) * M_PI/180.0;
        float cos_angle_i = std::cos(angle_i);
        float sin_angle_i = std::sin(angle_i);
        laser_image_info_.angles_ranges_.push_back(angle_i);
        laser_image_info_.cos_angles_ranges_.push_back(cos_angle_i);
        laser_image_info_.sin_angles_ranges_.push_back(sin_angle_i);
    }

    laser_image_info_.laser_scans_image_res_ = laser_info_.max_virtual_range_ / (laser_image_info_.laser_scans_image_size_.width/2.0);
    laser_image_info_.laser_scans_image_ = cv::Mat(laser_image_info_.laser_scans_image_size_.height,
                                                  laser_image_info_.laser_scans_image_size_.width,
                                                  CV_8U, cv::Scalar(255));
    laser_image_info_.obstacles_boundary_image_ = laser_image_info_.laser_scans_image_.clone();



    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_);
    else
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_) + 1;


    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0, 0.0);
    uav_target_current_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);
    uav_target_previous_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);


    min_distance_from_laser_flag_ = false;
    min_distance_target_reached_flag_ = false;
    if(TEST_MODE)
        min_distance_to_target_thresh_ = 0.3;
    else
        min_distance_to_target_thresh_ = 0.1;
    virtual_ranges_offset_ = 0.0;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;
    std::cout<<"pitch_roll_max_value_ : "<<pitch_roll_max_value_<<std::endl;

    uav_initial_reset_position_ = cv::Point3f(0.0, 0.0, 0.0);
    slam_out_uav_position_ = cv::Point3f(0.0, 0.0, 0.0);


    // Init circular buffer
    filtered_derivative_wcb_x_.setTimeParameters( 0.005, 0.005, 0.200, 1.0, 40.0);
    filtered_derivative_wcb_y_.setTimeParameters( 0.005, 0.005, 0.200, 1.0, 40.0);
    filtered_derivative_wcb_x_.reset();
    filtered_derivative_wcb_y_.reset();

}


RlEnvironmentLaserBasedNavigation::~RlEnvironmentLaserBasedNavigation()
{
    f_data_recorder.close();
}

void RlEnvironmentLaserBasedNavigation::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: LASER-BASED REACTIVE NAVIGATION Environment" << std::endl;


    std::string mav_name;
    ros::param::get("~mav_name", mav_name);
    if(mav_name.length() == 0)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }


    int drone_swarm_number = -1;
    ros::param::get("~drone_swarm_number", drone_swarm_number);
    if(drone_swarm_number == -1)
    {
        ROS_ERROR("FATAL: Namespace Id not found");
        return;
    }

    UAV_NAME = mav_name + std::to_string(drone_swarm_number);


    std::string drone_namespace;
    ros::param::get("~drone_namespace", drone_namespace);
    if(drone_namespace.length() == 0)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }
    //UAV_NAME = drone_namespace;
    std::cout<<"+++++++++++++ --------------- UAV_NAME: "<<UAV_NAME<<std::endl;

    int drone_id_int = -1;
    ros::param::get("~droneId", drone_id_int);
    if(drone_id_int == -1)
    {
        ROS_ERROR("FATAL: Namespace Id not found");
        return;
    }

    // Init subscribers
    std::string param_string;


    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentLaserBasedNavigation::PoseVelocityCallback, this);


    ros::param::get("~esimtated_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    //drone_estimated_pose_subs_ = n.subscribe(param_string, 1, &RlEnvironmentLaserBasedNavigation::DroneEstimatedPoseCallback, this);
    drone_estimated_pose_subs_ = n.subscribe("/" + drone_namespace + "/" + param_string, 1,
                                             &RlEnvironmentLaserBasedNavigation::DroneEstimatedPoseCallback, this);



    ros::param::get("~laser_scan", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_laser_scan_subs_ = n.subscribe("/" + mav_name + std::to_string(drone_swarm_number) + "/" + param_string, 1,
                                         &RlEnvironmentLaserBasedNavigation::LaserScanCallback, this);

    std::cout<<"+++++++++++++ --------------- Laser Scan topic: "<<"/" + mav_name + std::to_string(drone_swarm_number) + "/" + param_string<<std::endl;

    ros::param::get("~slam_out_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_slam_out_pose_subs_ = n.subscribe("/" + drone_namespace + "/" + param_string, 1,
                                         &RlEnvironmentLaserBasedNavigation::SlamOutPoseCallback, this);




    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/" + drone_namespace + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/" + drone_namespace + "/" + param_string, 1000);


    ros::param::get("~hector_mapping_reset", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    //hector_mapping_reset_pub_ = n.advertise<std_msgs::String>(param_string, 1000);
    hector_mapping_reset_pub_ = n.advertise<std_msgs::String>("/" + drone_namespace + "/" + param_string, 1000);





    // Init services
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_set_model_state_srv_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~set_link_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_set_link_state_srv_ = n.serviceClient<gazebo_msgs::SetLinkState>(param_string);


    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/" + drone_namespace + "/" + param_string);


    // Set number of iterations of Gazebo
    if (ENABLE_PAUSED_SIMULATION)
    {
        this->data_->num_iterations = (unsigned int) environment_info_.num_iterations_;

        // Print
        std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;
    }



    ros::param::get("~configs_path", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    std::string configs_file_name = param_string;
    std::cout<<"++++++++++ CONFIGS PATH ++++++++++"<<std::endl<<configs_file_name<<std::endl;
    ReadConfigs(configs_file_name);

    // Reset environment
    if(test_goal_sequence_mode_flag_)
    {
        cv::Point3f target_pos;
        target_pos.x = goals_sequence_[0].x;
        target_pos.y = goals_sequence_[0].y;
        target_pos.z = goals_sequence_[0].z;
        SetTargetState(target_pos.x, target_pos.y, target_pos.z);

        gazebo_msgs::SetModelState model_msg_target;
        model_msg_target.request.model_state.model_name = TARGET_NAME;
        model_msg_target.request.model_state.pose.position.x = target_pos.x;
        model_msg_target.request.model_state.pose.position.y = target_pos.y;
        model_msg_target.request.model_state.pose.position.z = 0.0;
        if (gazebo_set_model_state_srv_.call(model_msg_target))
        {

        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
    //        return false;
        }
    }
    else
        Reset();
    //f_data_recorder.open("/media/carlos/DATA/TesisCVG/PAPERS/MIS_PAPERS/ObjectFollowing_IROS_2018/Experiments/speeds_pos_ground_truth_LowPass_40.txt");
}


bool RlEnvironmentLaserBasedNavigation::ReadConfigs(std::string &configFile)
{
    pugi::xml_document doc;

    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        std::cout << "ERROR: Could not load the file: " << result.description() << std::endl;
        return 0;
    }

    pugi::xml_node Configuration = doc.child("Laser_Based_Navigation_Config");
    std::string readingValue;


    // ******** FLAGS for VISUALIZATION purposes ********
    readingValue = Configuration.child_value("imshow_laser_state_scans");
    imshow_laser_state_scans_ = std::atoi(readingValue.c_str());
    std::cout<<"imshow_laser_state_scans_: "<<imshow_laser_state_scans_<<std::endl;

    readingValue = Configuration.child_value("imshow_laser_state_scans_and_goal");
    imshow_laser_state_scans_and_goal_ = std::atoi(readingValue.c_str());
    std::cout<<"imshow_laser_state_scans_and_goal_: "<<imshow_laser_state_scans_and_goal_<<std::endl;

    readingValue = Configuration.child_value("imshow_all_saturated_ranges");
    imshow_all_saturated_ranges_ = std::atoi(readingValue.c_str());
    std::cout<<"imshow_all_saturated_ranges_: "<<imshow_all_saturated_ranges_<<std::endl;

    readingValue = Configuration.child_value("imshow_obstacles_boundary");
    imshow_obstacles_boundary_ = std::atoi(readingValue.c_str());
    std::cout<<"imshow_obstacles_boundary_: "<<imshow_obstacles_boundary_<<std::endl;



    // ******** FLAGS for TESTING MODES ********
    readingValue = Configuration.child_value("test_goal_sequence_mode");
    test_goal_sequence_mode_flag_ = std::atoi(readingValue.c_str());
    std::cout<<"test_goal_sequence_mode_flag_: "<<test_goal_sequence_mode_flag_<<std::endl;




    readingValue = Configuration.child_value("min_distance_to_target_thresh");
    min_distance_to_target_thresh_ = atof(readingValue.c_str());
    std::cout<<"min_distance_to_target_thresh: "<<min_distance_to_target_thresh_<<std::endl;


    readingValue = Configuration.child_value("virtual_ranges_offset");
    virtual_ranges_offset_ = atof(readingValue.c_str());
    std::cout<<"virtual_ranges_offset_: "<<virtual_ranges_offset_<<std::endl;



    readingValue = Configuration.child("environment_info").child_value("name");
    environment_info_.name_ = readingValue.c_str();
    std::cout<<"environment_info_.name_: "<<environment_info_.name_<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("num_episode_steps");
    environment_info_.num_episode_steps_ = atoi(readingValue.c_str());
    std::cout<<"environment_info_.num_episode_steps_: "<<environment_info_.num_episode_steps_<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("actions_min_value");
    float actions_min_value = atof(readingValue.c_str());
    std::cout<<"actions_min_value: "<<actions_min_value<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("actions_max_value");
    float actions_max_value = atof(readingValue.c_str());
    std::cout<<"actions_max_value: "<<actions_max_value<<std::endl;
    environment_info_.actions_min_value_ = {actions_min_value, actions_min_value};
    environment_info_.actions_max_value_ = {actions_max_value, actions_max_value};


    readingValue = Configuration.child("environment_info").child_value("max_pos_x");
    environment_info_.max_pos_x_ = atof(readingValue.c_str());
    std::cout<<"environment_info_.max_pos_x_: "<<environment_info_.max_pos_x_<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("max_pos_y");
    environment_info_.max_pos_y_ = atof(readingValue.c_str());
    std::cout<<"environment_info_.max_pos_y_: "<<environment_info_.max_pos_y_<<std::endl;




    readingValue = Configuration.child("laser_info").child_value("max_virtual_range");
    laser_info_.max_virtual_range_ = atof(readingValue.c_str());
    std::cout<<"laser_info_.max_virtual_range_: "<<laser_info_.max_virtual_range_<<std::endl;

    readingValue = Configuration.child("laser_info").child_value("max_real_range");
    laser_info_.max_real_range_ = atof(readingValue.c_str());
    std::cout<<"laser_info_.max_real_range_: "<<laser_info_.max_real_range_<<std::endl;

    readingValue = Configuration.child("laser_info").child_value("min_real_range");
    laser_info_.min_real_range_ = atof(readingValue.c_str());
    std::cout<<"laser_info_.min_real_range_: "<<laser_info_.min_real_range_<<std::endl;

    readingValue = Configuration.child("laser_info").child_value("angle_range");
    laser_info_.angle_range_ = atof(readingValue.c_str());
    std::cout<<"laser_info_.angle_range_: "<<laser_info_.angle_range_<<std::endl;

    readingValue = Configuration.child("laser_info").child_value("num_ranges");
    laser_info_.num_ranges_ = atoi(readingValue.c_str());
    std::cout<<"laser_info_.num_ranges_: "<<laser_info_.num_ranges_<<std::endl;

    readingValue = Configuration.child("laser_info").child_value("min_range_reset_value");
    laser_info_.min_range_reset_value_ = atof(readingValue.c_str());
    std::cout<<"laser_info_.min_range_reset_value_: "<<laser_info_.min_range_reset_value_<<std::endl;



    pugi::xml_node node_goals = Configuration.child("ListOfGoals");
    std::cout<<"***** GOAL PARAMETERS *****"<<std::endl;
    for(pugi::xml_node node_goal = node_goals.first_child(); node_goal; node_goal = node_goal.next_sibling())
    {

        readingValue = node_goal.child_value("x");
        float x = atof(readingValue.c_str());
        readingValue = node_goal.child_value("y");
        float y = atof(readingValue.c_str());
        readingValue = node_goal.child_value("z");
        float z = atof(readingValue.c_str());

        goals_sequence_.push_back(cv::Point3f(x, y, z));
        num_goals_in_sequence_++;
    }
    std::cout<<"NUM GOALs in SEQUENCE:"<<num_goals_in_sequence_<<std::endl;
    std::cout<<"GOALs SEQUENCE:"<<std::endl;
    for(int i=0;i<goals_sequence_.size();i++)
        std::cout<<goals_sequence_[i]<<std::endl;



}



bool RlEnvironmentLaserBasedNavigation::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{
    //UpdateUavTargetPreviousRelativePosition();

    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);


    clock_t begin = clock();
    std::vector<float> current_state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        current_state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        current_state = ComputeNormalizedState();

    clock_t end = clock();
//    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
//    std::cout<<"*** ELAPSED TIME (ComputeNormalizedState) ***   "<<elapsed_secs<<std::endl;


    if(DEBUG_MODE_STATE)
    {
        std::cout<<"***** CURRENT STATE *****"<<std::endl;
        for(int i=0; i<current_state.size();i++)
        {
            if(i==0)
                std::cout<<"[";
            if(i==current_state.size()-1)
                std::cout<<current_state[i]<<"]";
            else
                std::cout<<current_state[i]<<" , ";
        }
        std::cout<<std::endl;
    }

    float shaping = 0.0;
    if(!TEST_MODE)
    {

        float position_attr_reward_term = 0.0f;
        float position_repul_reward_term = 0.0f;
        float velocity_reward_term = 0.0f;
        float action_reward_term = 0.0f;

        position_attr_reward_term = - 100 * std::sqrt(std::pow(current_state[0], 2) + std::pow(current_state[1], 2));

        if(COMPUTE_REPULSIVE_POTENTIAL_FIELD)
        {

            clock_t begin = clock();

            if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
                position_repul_reward_term = ComputeRepulsivePotentialFieldBasedOnImage();
            else
            {
                std::vector<float> vector_of_norm_laser_measurements;
                vector_of_norm_laser_measurements.insert(vector_of_norm_laser_measurements.end(), current_state.begin() + 4, current_state.end());
                position_repul_reward_term = ComputeRepulsivePotentialField(vector_of_norm_laser_measurements);
            }
            clock_t end = clock();
    //        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    //        std::cout<<"*** ELAPSED TIME (ComputeRepulsivePotentialFieldBasedOnImage) ***   "<<elapsed_secs<<std::endl;

            if(DEBUG_POTENTIAL_FIELDS)
            {
                std::cout<<"Attraction Potential Field: "<<position_attr_reward_term<<std::endl;
                std::cout<<"Repulsion Potential Field: "<<position_repul_reward_term<<std::endl;
            }
        }

        //velocity_reward_term = - 10 * std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2));
        //float shaping = position_attr_reward_term + velocity_reward_term;
        if(COMPUTE_REPULSIVE_POTENTIAL_FIELD)
            shaping = position_attr_reward_term + position_repul_reward_term;
        else
            shaping = position_attr_reward_term;

    }
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;


    if(min_distance_target_reached_flag_)
    {
        cont_ind_goal_in_sequence_++;
        if(test_goal_sequence_mode_flag_ && (cont_ind_goal_in_sequence_ < num_goals_in_sequence_))
        {
            min_distance_target_reached_flag_ = false;
            cv::Point3f target_pos;
            target_pos.x = goals_sequence_[cont_ind_goal_in_sequence_].x;
            target_pos.y = goals_sequence_[cont_ind_goal_in_sequence_].y;
            target_pos.z = goals_sequence_[cont_ind_goal_in_sequence_].z;
            SetTargetState(target_pos.x, target_pos.y, target_pos.z);
            std::cout<<"target_pos: "<<target_pos<<std::endl;

            gazebo_msgs::SetModelState model_msg_target;
            model_msg_target.request.model_state.model_name = TARGET_NAME;
            model_msg_target.request.model_state.pose.position.x = target_pos.x;
            model_msg_target.request.model_state.pose.position.y = target_pos.y;
            model_msg_target.request.model_state.pose.position.z = 0.0;
            if (gazebo_set_model_state_srv_.call(model_msg_target))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
        //        return false;
            }
        }
        else
        {
            float position_attr_reward_term_exp = std::sqrt(std::pow(current_state[0], 2) + std::pow(current_state[1], 2));
            float velocity_reward_term_exp = std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2));
            float action_reward_term_exp = std::sqrt(std::pow(request.action[0]/environment_info_.actions_max_value_[0], 2) +
                    std::pow(request.action[1]/environment_info_.actions_max_value_[1], 2));

            float exponential_reward = 10.0/std::exp(2*(position_attr_reward_term_exp + velocity_reward_term_exp));
            response.reward = reward + exponential_reward;
            response.terminal_state = true;
            reset_env_ = true;
            min_distance_target_reached_flag_ = false;

    //        response.reward = 105;
    //        response.terminal_state = true;
    //        reset_env_ = true;
    //        min_distance_target_reached_flag_ = false;
        }

    }
    else if (min_distance_from_laser_flag_)
    {
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
        min_distance_from_laser_flag_ = false;
    }
    else
        response.terminal_state = false;

    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();


    if(DEBUG_SHAPING_REWARD)
    {
        std::cout<<std::endl<<"****** SHPAING - REWARD ******"<<std::endl;
        std::cout<<"Shaping: "<<shaping<<std::endl;
        std::cout<<"Reward: "<<response.reward<<std::endl<<std::endl;
    }


    // Read next state
    std::vector<float> next_state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        next_state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        next_state = ComputeNormalizedState();
    response.obs_real = next_state;


    // Successful return
    return true;
}

bool RlEnvironmentLaserBasedNavigation::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality Reactive Navigation service called" << std::endl;

    // Action dimensionality
    response.action_dim = environment_info_.actions_dim_;


    // Action max
    response.action_max = environment_info_.actions_max_value_;

    // Action min
    response.action_min = environment_info_.actions_min_value_;

    // States dimensionality
    response.state_dim_lowdim = environment_info_.state_dim_low_dim_;


    // Number of iterations
    response.num_iterations = environment_info_.num_episode_steps_;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentLaserBasedNavigation::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv Reactive Navigation service called" << std::endl;


    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
    {
        if(!test_goal_sequence_mode_flag_)
            Reset();
    }


    std::vector<float> state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        state = ComputeNormalizedState();
    response.state = state;
    //UpdateUavTargetPreviousRelativePosition();


    return true;
}

bool RlEnvironmentLaserBasedNavigation::Reset()
{
    // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav;
    float roll, pitch, yaw;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav, roll, pitch, yaw);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);


    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = kUav_Altitude_;
    uav_pose_ref_pub_.publish(altitude_msg);


    // Spawn model to RANDOM locations (UAV and Target)
    cv::Point2f uav_rand_pos, target_rand_pos;
    if(environment_info_.name_ == "house")
        GenerateRandomPositionsHouse(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_2")
        GenerateRandomPositionsHouse2(uav_rand_pos, target_rand_pos);
    else if((environment_info_.name_ == "house_3") || (environment_info_.name_ == "house_3b") || (environment_info_.name_ == "house_3c"))
        GenerateRandomPositionsHouse3(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_4")
        GenerateRandomPositionsHouse4(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_5")
        GenerateRandomPositionsHouse5(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_6")
        GenerateRandomPositionsHouse6(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze")
        GenerateRandomPositionsMaze(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze_3")
        GenerateRandomPositionsMaze3(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze_4")
        GenerateRandomPositionsMaze4(uav_rand_pos, target_rand_pos);

    gazebo_msgs::SetModelState model_msg_uav;
    model_msg_uav.request.model_state.model_name = UAV_NAME;
    model_msg_uav.request.model_state.pose.position.x = uav_rand_pos.x;
    model_msg_uav.request.model_state.pose.position.y = uav_rand_pos.y;
    model_msg_uav.request.model_state.pose.position.z = kUav_Altitude_;
    if (gazebo_set_model_state_srv_.call(model_msg_uav))
    {

    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    gazebo_msgs::SetModelState model_msg_target;
    model_msg_target.request.model_state.model_name = TARGET_NAME;
    model_msg_target.request.model_state.pose.position.x = target_rand_pos.x;
    model_msg_target.request.model_state.pose.position.y = target_rand_pos.y;
    model_msg_target.request.model_state.pose.position.z = 0.0;
    if (gazebo_set_model_state_srv_.call(model_msg_target))
    {

    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }


    if((environment_info_.name_ == "house_3") || (environment_info_.name_ == "house_3b"))
    {
        if(MOVING_OBSTACLE_FOR_TRAINING)
        {
            float min_distance_to_obstacle = 1.8;
            cv::Point3f uav_target_relative_position;
            uav_target_relative_position.x = target_rand_pos.x - uav_rand_pos.x;
            uav_target_relative_position.y = target_rand_pos.y - uav_rand_pos.y;
            float distance_uav_to_target = std::sqrt(std::pow(uav_target_relative_position.x, 2) + std::pow(uav_target_relative_position.y, 2));

            cv::Point3f obstacle_cylinder_pos;
            if(distance_uav_to_target > min_distance_to_obstacle)
            {
                obstacle_cylinder_pos.x = uav_rand_pos.x + uav_target_relative_position.x/2.0;
                obstacle_cylinder_pos.y = uav_rand_pos.y + uav_target_relative_position.y/2.0;
                obstacle_cylinder_pos.z = 0.0;
            }
            else
            {
                float distance_uav_to_obs = 0.0;
                float distance_target_to_obs = 0.0;
                float max_pos = (environment_info_.max_pos_x_ / 2.0) - 1;
                float lower_lim = -max_pos;
                float upper_lim = max_pos;
                do
                {
                    float rand_pos_x = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim - lower_lim)));
                    float rand_pos_y = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim - lower_lim)));

                    obstacle_cylinder_pos.x = rand_pos_x;
                    obstacle_cylinder_pos.y = rand_pos_y;
                    obstacle_cylinder_pos.z = 0.0;

                    cv::Point3f uav_obs_relative_position;
                    uav_obs_relative_position.x = obstacle_cylinder_pos.x - uav_rand_pos.x;
                    uav_obs_relative_position.y = obstacle_cylinder_pos.y - uav_rand_pos.y;
                    distance_uav_to_obs = std::sqrt(std::pow(uav_obs_relative_position.x, 2) +
                                                          std::pow(uav_obs_relative_position.y, 2));

                    cv::Point3f target_obs_relative_position;
                    target_obs_relative_position.x = obstacle_cylinder_pos.x - target_rand_pos.x;
                    target_obs_relative_position.y = obstacle_cylinder_pos.y - target_rand_pos.y;
                    distance_uav_to_obs = std::sqrt(std::pow(target_obs_relative_position.x, 2) +
                                                          std::pow(target_obs_relative_position.y, 2));
                }while((distance_uav_to_obs < min_distance_to_obstacle) && (distance_target_to_obs < min_distance_to_obstacle));
            }
            gazebo_msgs::SetModelState model_msg_obstacle_cylinder;
            model_msg_obstacle_cylinder.request.model_state.model_name = "obstacle_cylinder_big";
            model_msg_obstacle_cylinder.request.model_state.pose.position.x = obstacle_cylinder_pos.x;
            model_msg_obstacle_cylinder.request.model_state.pose.position.y = obstacle_cylinder_pos.y;
            model_msg_obstacle_cylinder.request.model_state.pose.position.z = obstacle_cylinder_pos.z;
            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
        //        return false;
            }
        }
    }



    if(environment_info_.name_ == "house_4")
    {
        int num_obstacles = 2;
        std::vector<cv::Point2f> obstacles_position = GenerateRandomPositionForObstacles(num_obstacles);
        std::cout<<"obstacles_position.size(): "<<obstacles_position.size()<<std::endl;
        gazebo_msgs::SetLinkState link_obstacles_msg;
        std::string link_0_name = "house_4::link_0";
        link_obstacles_msg.request.link_state.link_name = link_0_name;
        link_obstacles_msg.request.link_state.pose.position.x = obstacles_position[0].x;
        link_obstacles_msg.request.link_state.pose.position.y = obstacles_position[0].y;
        if (gazebo_set_link_state_srv_.call(link_obstacles_msg))
        {

        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set link state");
    //        return false;
        }

        std::string link_1_name = "house_4::link_1";
        link_obstacles_msg.request.link_state.link_name = link_1_name;
        link_obstacles_msg.request.link_state.pose.position.x = obstacles_position[1].x;
        link_obstacles_msg.request.link_state.pose.position.y = obstacles_position[1].y;
        if (gazebo_set_link_state_srv_.call(link_obstacles_msg))
        {

        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set link state");
    //        return false;
        }
    }


    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting UAV pos at x: " << uav_rand_pos.x << " and y: " << uav_rand_pos.y << std::endl;
    std::cout << "RL_ENV_INFO: reseting TARGET pos at x: " << target_rand_pos.x << " and y: " << target_rand_pos.y << std::endl;
    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg))
    {
        std_msgs::String reset_msg;
        reset_msg.data = "reset";
        hector_mapping_reset_pub_.publish(reset_msg);

        uav_initial_reset_position_.x = uav_rand_pos.x;
        uav_initial_reset_position_.y = uav_rand_pos.y;
        uav_initial_reset_position_.z = kUav_Altitude_;

        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        return false;
    }

}

void RlEnvironmentLaserBasedNavigation::SlamOutPoseCallback(const geometry_msgs::PoseStamped &msg)
{
    slam_out_uav_position_.x = msg.pose.position.x + uav_initial_reset_position_.x;
    slam_out_uav_position_.y = msg.pose.position.y + uav_initial_reset_position_.y;
    slam_out_uav_position_.z = msg.pose.position.z + uav_initial_reset_position_.z;


    //std::cout<<"SLAM OUT POSE (msg)"<<std::endl;
    //std::cout<<"[x,y,z]: "<<"["<<slam_out_uav_position_.x<<" , "<<slam_out_uav_position_.y<<" , "<<slam_out_uav_position_.z<<"]"<<std::endl;
}

void RlEnvironmentLaserBasedNavigation::LaserScanCallback(const sensor_msgs::LaserScan &msg)
{
    laser_info_.laser_ranges_.clear();
    laser_info_.laser_ranges_.insert(laser_info_.laser_ranges_.end(), msg.ranges.begin(), msg.ranges.end());

//    if ( std::any_of(laser_info_.laser_ranges_.begin(), laser_info_.laser_ranges_.end(), [this](float i){return i<laser_info_.min_range_reset_value_;}) )
//    {
//        min_distance_from_laser_flag_ = true;
//        std::cout<<"MIN DISTANCE reached!"<<std::endl;
//    }


}



void RlEnvironmentLaserBasedNavigation::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    // Get position of the uav in the vector
    cv::Point3f target_position;
    cv::Point3f uav_position, uav_linear_vel;
    cv::Point3f uav_previous_pos, uav_previous_linear_vel;
    uav_previous_pos.x = uav_state_.pos_x_;uav_previous_pos.y = uav_state_.pos_y_;uav_previous_pos.z = uav_state_.pos_z_;
    uav_previous_linear_vel.x = uav_state_.speed_x_;uav_previous_linear_vel.y = uav_state_.speed_y_;uav_previous_linear_vel.z = uav_state_.speed_z_;

    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            uav_position.x = msg->pose[i].position.x;
            uav_position.y = msg->pose[i].position.y;
            uav_position.z = msg->pose[i].position.z;

            uav_linear_vel.x = msg->twist[i].linear.x;
            uav_linear_vel.y = msg->twist[i].linear.y;
            uav_linear_vel.z = msg->twist[i].linear.z;
            break;
        }

        else if(msg->name[i].compare(TARGET_NAME) == 0)
        {
            target_position.x = msg->pose[i].position.x;
            target_position.y = msg->pose[i].position.y;
            target_position.z = msg->pose[i].position.z;
        }
    }

    if(TEST_MODE && environment_info_.name_ == "house_3c")
    {
        if((uav_position.x > -1.4 && uav_position.x < -1.36) || (uav_position.x > -0.3 && uav_position.x < -0.25))
        {
            gazebo_msgs::SetModelState model_msg_obstacle_cylinder;
            model_msg_obstacle_cylinder.request.model_state.model_name = "moving_obstacle_squared1";
            model_msg_obstacle_cylinder.request.model_state.pose.position.x = uav_position.x + 1.0;
            model_msg_obstacle_cylinder.request.model_state.pose.position.y = uav_position.y;
            model_msg_obstacle_cylinder.request.model_state.pose.position.z = 0.0;
            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
            }
        }
    }
    else if(TEST_MODE && environment_info_.name_ == "house_3")
    {
        if((uav_position.x > -2.2 && uav_position.x < -2.195))
        {
            float lower_lim_y = -0.3;
            float upper_lim_y = 0.3;
            float rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

            gazebo_msgs::SetModelState model_msg_obstacle_cylinder1;
            model_msg_obstacle_cylinder1.request.model_state.model_name = "moving_obstacle_squared_big1";
            model_msg_obstacle_cylinder1.request.model_state.pose.position.x = uav_position.x + 1.2;
            model_msg_obstacle_cylinder1.request.model_state.pose.position.y = uav_position.y + rand_pos_y;
            model_msg_obstacle_cylinder1.request.model_state.pose.position.z = 0.0;
            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder1))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
            }


            lower_lim_y = -1.0;
            upper_lim_y = 1.0;
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

            gazebo_msgs::SetModelState model_msg_obstacle_cylinder2;
            model_msg_obstacle_cylinder2.request.model_state.model_name = "moving_obstacle_squared_big2";
            model_msg_obstacle_cylinder2.request.model_state.pose.position.x = 0.75;
            if(rand_pos_y > 0)
                model_msg_obstacle_cylinder2.request.model_state.pose.position.y = 1.8;
            else
                model_msg_obstacle_cylinder2.request.model_state.pose.position.y = -1.8;
            model_msg_obstacle_cylinder2.request.model_state.pose.position.z = 0.0;
            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder2))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
            }
        }

    }

    if(DEBUG_UAV_TARGET_POSITON)
    {
        std::cout<<"UAV position: "<<"x: "<<uav_position.x<<" ; "<<"y: "<<uav_position.y<<" ; "<<"area_num: "<<area_uav_num_<<std::endl;
        std::cout<<"TARGET position: "<<"x: "<<target_position.x<<" ; "<<"y: "<<target_position.y<<" ; "<<"area_num: "<<area_target_num_<<std::endl;
    }


    // Set uav state
    SetUavState(uav_position.x, uav_position.y, uav_position.z, uav_linear_vel.x, uav_linear_vel.y, uav_linear_vel.z);
    SetUavPreviousState(uav_previous_pos.x, uav_previous_pos.y, uav_previous_pos.z,
                        uav_previous_linear_vel.x, uav_previous_linear_vel.y, uav_previous_linear_vel.z);
    SetTargetState(target_position.x, target_position.y, target_position.z);


}

void RlEnvironmentLaserBasedNavigation::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose_  = msg;
    //yaw pitch roll
//    uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
//    uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
//    uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

//    uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
//    uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
//    uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;

    return;
}


void RlEnvironmentLaserBasedNavigation::SetUavState(const float x, const float y, const float z,
                                                  const float dx, const float dy, const float dz)
{
    uav_mutex_.lock();
    uav_state_.pos_x_ = x;
    uav_state_.pos_y_ = y;
    uav_state_.pos_z_ = z;
    uav_state_.speed_x_ = dx;
    uav_state_.speed_y_ = dy;
    uav_state_.speed_z_ = dz;
    uav_mutex_.unlock();
}

void RlEnvironmentLaserBasedNavigation::SetUavPreviousState(const float x, const float y, const float z,
                                                  const float dx, const float dy, const float dz)
{
    uav_mutex_.lock();
    uav_state_.prev_pos_x_ = x;
    uav_state_.prev_pos_y_ = y;
    uav_state_.prev_pos_z_ = z;
    uav_state_.prev_speed_x_ = dx;
    uav_state_.prev_speed_y_ = dy;
    uav_state_.prev_speed_z_ = dz;
    uav_mutex_.unlock();
}


void RlEnvironmentLaserBasedNavigation::SetTargetState(const float x, const float y, const float z)
{
    target_mutex_.lock();
    target_state_.pos_x_ = x;
    target_state_.pos_y_ = y;
    target_state_.pos_z_ = z;
    target_mutex_.unlock();
}

void RlEnvironmentLaserBasedNavigation::GetUavState(float &x, float &y, float &z,
                                                  float &dx, float &dy, float &dz,
                                                  float &roll, float &pitch, float &yaw)
{
    uav_mutex_.lock();
    x = uav_state_.pos_x_;
    y = uav_state_.pos_y_;
    z = uav_state_.pos_z_;
    dx = uav_state_.speed_x_;
    dy = uav_state_.speed_y_;
    dz = uav_state_.speed_z_;
    roll = uav_state_.roll_;
    pitch = uav_state_.pitch_;
    yaw = uav_state_.yaw_;
    uav_mutex_.unlock();
}

void RlEnvironmentLaserBasedNavigation::GetTargetState(float &x, float &y, float &z)
{
    target_mutex_.lock();
    x = target_state_.pos_x_;
    y = target_state_.pos_y_;
    z = target_state_.pos_z_;
    target_mutex_.unlock();
}


float RlEnvironmentLaserBasedNavigation::L2Norm(const std::vector<float> &vec)
{
    float accum = 0.0;
    for(int i=0;i<vec.size();i++)
    {
        accum += vec[i] * vec[i];
    }

    return sqrt(accum);
}


float RlEnvironmentLaserBasedNavigation::ComputeGainForRepulsivePotentialField(const int gain_type, const float max_beta)
{
    float beta = 0.0;
    float d_influence_exp_beta = 0.75*laser_info_.max_virtual_range_;

    switch(gain_type)
    {
        case 1: //STEPPED GAIN
            if(distance_to_target_ > laser_info_.max_virtual_range_/2.0)
                beta = max_beta;
            else
                beta = 50;
            break;
        case 2: //EXPONENTIAL GAIN
            beta = max_beta/std::exp(4.0*(laser_info_.max_virtual_range_ - distance_to_target_));
            break;
        case 3: //HYBRID GAIN (CONSTANT and EXPONENTIAL when d_target < d_influence)
            if(distance_to_target_ > d_influence_exp_beta)
                beta = max_beta;
            else
                beta = max_beta/std::exp(4.0*(d_influence_exp_beta - distance_to_target_));
            break;
        default:
            beta = max_beta;

    }

    return beta;
}


float RlEnvironmentLaserBasedNavigation::ComputeRepulsivePotentialField(const std::vector<float> &vec)
{
    //This function is used to compute the repulsion component field regarding the laser measurements
    //vec is the vector containing the normalized distances to the surrounding obstacles

    float beta = ComputeGainForRepulsivePotentialField(3, 400.0);

    std::vector<float> vec_ones(vec);
    std::fill(vec_ones.begin(), vec_ones.end(), 1.0);

    float d_actual = L2Norm(vec);
    float d_max = L2Norm(vec_ones);

    //std::cout<<"distance to target: "<<distance_to_target_<<std::endl;
    //std::cout<<"d_actual: "<<d_actual<<std::endl;
    //std::cout<<"d_max: "<<d_max<<std::endl;

    float u_rep = (1.0/d_actual - 1.0/d_max);
    return -beta * u_rep;
}

float RlEnvironmentLaserBasedNavigation::ComputeRepulsivePotentialFieldBasedOnImage()
{
    std::vector<int> closest_ranges_to_obstacle_indices = ComputeMinLaserRangesToObstacles();
//    std::cout<<"closest_ranges_to_obstacle_indices: "<<std::endl;
//    for(int i=0;i<closest_ranges_to_obstacle_indices.size();i++)
//        std::cout<<closest_ranges_to_obstacle_indices[i]<<" , ";
//    std::cout<<std::endl;

    float beta = ComputeGainForRepulsivePotentialField(3, 2.0);

    std::vector<float> min_laser_ranges_norm = GetMinLaserRangesNorm();
    float u_rep = 0.0;
    float d_max = 1.0;
    float k = 0.04;
    for(int i=0;i<closest_ranges_to_obstacle_indices.size();i++)
    {
        int num_sector = closest_ranges_to_obstacle_indices[i];
        u_rep += (1.0/(k + min_laser_ranges_norm[num_sector]) - 1.0/(k + d_max));
    }

    return -beta * u_rep;
}


void RlEnvironmentLaserBasedNavigation::UpdateUavTargetPreviousRelativePosition()
{
    uav_target_previous_relative_position_.x = uav_target_current_relative_position_.x;
    uav_target_previous_relative_position_.y = uav_target_current_relative_position_.y;
    uav_target_previous_relative_position_.z = uav_target_current_relative_position_.z;
}

std::vector<float> RlEnvironmentLaserBasedNavigation::ComputeNormalizedState()
{
    normalized_state_.clear();


    uav_target_current_relative_position_.x = target_state_.pos_x_ - uav_state_.pos_x_;
    uav_target_current_relative_position_.y = target_state_.pos_y_ - uav_state_.pos_y_ ;
    uav_target_current_relative_position_.z = target_state_.pos_z_ - uav_state_.pos_z_;
    distance_to_target_ = std::sqrt(std::pow(uav_target_current_relative_position_.x, 2)
                                         + std::pow(uav_target_current_relative_position_.y, 2));
    if(distance_to_target_ < min_distance_to_target_thresh_)
        min_distance_target_reached_flag_ = true;

    float x_norm = uav_target_current_relative_position_.x / environment_info_.max_pos_x_;
    float y_norm = uav_target_current_relative_position_.y / environment_info_.max_pos_y_;

    float dx_norm = 0.0;
    float dy_norm = 0.0;
    if(STATE_BASED_ON_UAV_POS_AND_SPEED)
    {
        dx_norm = uav_state_.speed_x_;
        dy_norm = uav_state_.speed_y_;
    }
    else if(STATE_BASED_ON_UAV_POS_AND_DIFF_PREVIOUS_POSE)
    {
        dx_norm = (uav_target_current_relative_position_.x - uav_target_previous_relative_position_.x) / environment_info_.max_pos_x_;
        dy_norm = (uav_target_current_relative_position_.y - uav_target_previous_relative_position_.y) / environment_info_.max_pos_y_;
    }


    normalized_state_.push_back(x_norm);
    normalized_state_.push_back(y_norm);
    normalized_state_.push_back(dx_norm);
    normalized_state_.push_back(dy_norm);



    laser_ranges_state_.clear();
    laser_ranges_state_norm_.clear();
    //std::vector<float> laser_ranges_state;
    //std::vector<float> laser_ranges_state_norm;
    float laser_state_normalization_factor_ = laser_info_.max_virtual_range_ - laser_info_.min_range_reset_value_;
    for(int i=0;i<laser_info_.laser_ranges_.size();i++)
    {
        if((i%laser_info_.sampling_factor_ == 0) || (i == laser_info_.laser_ranges_.size()-1))
        {
            if(std::isfinite(laser_info_.laser_ranges_[i]))
            {
                if(laser_info_.laser_ranges_[i] > laser_info_.max_virtual_range_)
                {
                    laser_ranges_state_.push_back(laser_info_.max_virtual_range_);
                    laser_ranges_state_norm_.push_back(1.0);
                }
                else
                {
                    laser_ranges_state_.push_back(laser_info_.laser_ranges_[i]);
                    //laser_ranges_state_norm_.push_back(laser_info_.laser_ranges_[i]/laser_info_.max_virtual_range_);
                    float laser_range_wrt_min_range = laser_info_.laser_ranges_[i] - laser_info_.min_range_reset_value_;
                    laser_ranges_state_norm_.push_back(laser_range_wrt_min_range/laser_state_normalization_factor_);
                }
            }
            else
            {
                laser_ranges_state_.push_back(laser_info_.max_virtual_range_);
                laser_ranges_state_norm_.push_back(1.0);
            }

            if(laser_info_.laser_ranges_[i] < laser_info_.min_range_reset_value_)
            {
                min_distance_from_laser_flag_ = true;
                std::cout<<"MIN DISTANCE reached! ("<<laser_info_.laser_ranges_[i]<<")"<<std::endl;
            }
        }
//        if(laser_info_.laser_ranges_[i] < laser_info_.min_range_reset_value_)
//        {
//            min_distance_from_laser_flag_ = true;
//            std::cout<<"MIN DISTANCE reached! ("<<laser_info_.laser_ranges_[i]<<")"<<std::endl;
//        }
    }
//    for(int i=0;i<laser_ranges_state_norm_.size();i++)
//    {
//        laser_ranges_state_norm_[i] = 1.0 - laser_ranges_state_norm_[i];
//    }


    if(DEBUG_LASER_MEASUREMENTS)
    {
        std::cout<<"----- laser_ranges_state -----"<<std::endl;
        std::cout<<"size: "<<laser_ranges_state_norm_.size()<<std::endl;
//        for(int i=0;i<laser_ranges_state_.size();i++)
//            std::cout<<laser_ranges_state_[i]<<" , ";
//        std::cout<<std::endl;
        for(int i=0;i<laser_ranges_state_norm_.size();i++)
            std::cout<<laser_ranges_state_norm_[i]<<" , ";
        std::cout<<std::endl;
    }


    DrawResults();



    normalized_state_.insert(normalized_state_.end(), laser_ranges_state_norm_.begin(), laser_ranges_state_norm_.end());

    //Ensure that all the components in the NORMALIZED STATE vector are within the range [-1.0, 1.0]
    for(int i=0;i<normalized_state_.size();i++)
    {
        if(normalized_state_[i] > 1.0)
            normalized_state_[i] = 1.0;
        else if(normalized_state_[i] < -1.0)
            normalized_state_[i] = -1.0;
    }

    return normalized_state_;

}

std::vector<float> RlEnvironmentLaserBasedNavigation::ComputeNormalizedStateBasedOnGroupOfLaserRanges()
{
    normalized_state_.clear();


    uav_target_previous_relative_position_.x = uav_target_current_relative_position_.x;
    uav_target_previous_relative_position_.y = uav_target_current_relative_position_.y;
    uav_target_previous_relative_position_.z = uav_target_current_relative_position_.z;


    cv::Point3f uav_pos = cv::Point3f(0.0, 0.0, 0.0);
    cv::Point3f uav_speed = cv::Point3f(0.0, 0.0, 0.0);
    if(STATE_BASED_ON_GROUND_TRUTH_DATA)
    {
        uav_pos.x = uav_state_.pos_x_;
        uav_pos.y = uav_state_.pos_y_;
        uav_pos.z = uav_state_.pos_z_;
    }
    else
    {
        uav_pos.x = slam_out_uav_position_.x;
        uav_pos.y = slam_out_uav_position_.y;
        uav_pos.z = slam_out_uav_position_.z;
    }


    uav_target_current_relative_position_.x = target_state_.pos_x_ - uav_pos.x;
    uav_target_current_relative_position_.y = target_state_.pos_y_ - uav_pos.y;
    uav_target_current_relative_position_.z = target_state_.pos_z_ - uav_pos.z;
    distance_to_target_ = std::sqrt(std::pow(uav_target_current_relative_position_.x, 2)
                                         + std::pow(uav_target_current_relative_position_.y, 2));
    if(distance_to_target_ < min_distance_to_target_thresh_)
        min_distance_target_reached_flag_ = true;

    float x_norm = uav_target_current_relative_position_.x / environment_info_.max_pos_x_;
    float y_norm = uav_target_current_relative_position_.y / environment_info_.max_pos_y_;


    if(STATE_BASED_ON_GROUND_TRUTH_DATA)
    {
        uav_speed.x = uav_state_.speed_x_;
        uav_speed.y = uav_state_.speed_y_;
        uav_speed.z = uav_state_.speed_z_;
    }
    else
    {
        //Compute the velocity of the UAV
        ros::Time current_timestamp = ros::Time::now();

        double x_raw_t = uav_pos.x;
        double y_raw_t = uav_pos.y;

        time_t tv_sec; suseconds_t tv_usec;

        tv_sec  = current_timestamp.sec;
        tv_usec = current_timestamp.nsec / 1000.0;
        filtered_derivative_wcb_x_.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_y_.setInput( y_raw_t, tv_sec, tv_usec);


        double x_t, dx_t;
        double y_t, dy_t;
        filtered_derivative_wcb_x_.getOutput( x_t,  dx_t);
        filtered_derivative_wcb_y_.getOutput( y_t,  dy_t);
        uav_speed.x = dx_t;
        uav_speed.y = dy_t;
    }


    float dx_norm = 0.0;
    float dy_norm = 0.0;
    if(STATE_BASED_ON_UAV_POS_AND_SPEED)
    {
        dx_norm = uav_speed.x;
        dy_norm = uav_speed.y;
    }
    else if(STATE_BASED_ON_UAV_POS_AND_DIFF_PREVIOUS_POSE)
    {
        dx_norm = (uav_target_current_relative_position_.x - uav_target_previous_relative_position_.x) / environment_info_.max_pos_x_;
        dy_norm = (uav_target_current_relative_position_.y - uav_target_previous_relative_position_.y) / environment_info_.max_pos_y_;
    }


    normalized_state_.push_back(x_norm);
    normalized_state_.push_back(y_norm);
    normalized_state_.push_back(dx_norm);
    normalized_state_.push_back(dy_norm);


    //f_data_recorder<<uav_state_.pos_x_<<" ; "<<uav_state_.pos_y_<<" ; "<<uav_state_.speed_x_<<" ; "<<uav_state_.speed_y_<<" ; "<<
                  //uav_pos.x<<" ; "<<uav_pos.y<<" ; "<<uav_speed.x<<" ; "<<uav_speed.y<<std::endl;



    laser_ranges_state_.clear();
    laser_ranges_state_norm_.clear();
    laser_info_.min_laser_ranges_norm_.clear();

    std::vector<float> laser_ranges_saturated;
    laser_ranges_saturated.reserve(laser_info_.laser_ranges_.size());
    std::transform(laser_info_.laser_ranges_.begin(), laser_info_.laser_ranges_.end(),
                   std::back_inserter(laser_ranges_saturated), [this](float a){ return this->SaturateLaserRanges(a); });

    std::vector<float> laser_ranges_saturated_norm;
    laser_ranges_saturated_norm.reserve(laser_info_.laser_ranges_.size());
    std::transform(laser_ranges_saturated.begin(), laser_ranges_saturated.end(),
                   std::back_inserter(laser_ranges_saturated_norm), [this](float a){ return this->NormalizeSaturatedLaserRanges(a); });


    int num_circular_sectors = laser_info_.num_ranges_ / laser_info_.sampling_factor_;
    float accum_ranges_measurements = 0.0;
    float accum_ranges_measurements_norm = 0.0;
    for(int i=0;i<num_circular_sectors;i++)
    {
        int begin_ind = i*laser_info_.sampling_factor_;
        int end_ind = (i + 1) * laser_info_.sampling_factor_;

        accum_ranges_measurements = std::accumulate(laser_ranges_saturated.begin() + begin_ind,
                                                    laser_ranges_saturated.begin() + end_ind, 0.0);
        laser_ranges_state_.push_back(accum_ranges_measurements/laser_info_.sampling_factor_);

        accum_ranges_measurements_norm = std::accumulate(laser_ranges_saturated_norm.begin() + begin_ind,
                                                    laser_ranges_saturated_norm.begin() + end_ind, 0.0);
        laser_ranges_state_norm_.push_back(accum_ranges_measurements_norm/laser_info_.sampling_factor_);

        laser_info_.min_laser_ranges_norm_.push_back(*std::min_element(laser_ranges_saturated_norm.begin() + begin_ind,
                                                                       laser_ranges_saturated_norm.begin() + end_ind));
    }

    normalized_state_.insert(normalized_state_.end(), laser_ranges_state_norm_.begin(), laser_ranges_state_norm_.end());


    if ( std::any_of(laser_info_.laser_ranges_.begin(), laser_info_.laser_ranges_.end(), [this](float i){return i<laser_info_.min_range_reset_value_;}) )
    {
        min_distance_from_laser_flag_ = true;
        std::cout<<"MIN DISTANCE reached!"<<std::endl;
    }


    //********** Code for drawing ALL the RANGES of the laser en each sector of the state **********//
    if(imshow_all_saturated_ranges_)
    {
        std::vector<float> all_saturated_ranges;
        all_saturated_ranges.reserve(laser_info_.laser_ranges_.size());
        for(int i=0;i<laser_info_.laser_ranges_.size();i++)
        {
            if(std::isfinite(laser_info_.laser_ranges_[i]))
            {
                if(laser_info_.laser_ranges_[i] > laser_info_.max_virtual_range_)
                    all_saturated_ranges.push_back(laser_info_.max_virtual_range_);
                else
                    all_saturated_ranges.push_back(laser_info_.laser_ranges_[i]);
            }
            else
                all_saturated_ranges.push_back(laser_info_.max_virtual_range_);
        }


        cv::Mat I_all_saturated_ranges = laser_image_info_.laser_scans_image_.clone();
        cv::cvtColor(I_all_saturated_ranges, I_all_saturated_ranges, CV_GRAY2BGR);
        for(int i=0;i<all_saturated_ranges.size();i++)
        {
            float delta_pos_proyec_module = all_saturated_ranges[i]/laser_image_info_.laser_scans_image_res_;
            cv::Point2f delta_pos_proyec;
            delta_pos_proyec.x = delta_pos_proyec_module * laser_image_info_.cos_angles_ranges_[i];
            delta_pos_proyec.y = -delta_pos_proyec_module * laser_image_info_.sin_angles_ranges_[i];

            int x_im = laser_image_info_.laser_scans_image_.cols/2 + delta_pos_proyec.x;
            int y_im = laser_image_info_.laser_scans_image_.rows/2 + delta_pos_proyec.y;

            cv::Point p_fin = cv::Point(x_im, y_im);
            cv::line(I_all_saturated_ranges, laser_image_info_.p_origin_, p_fin, cv::Scalar(0), 1);
            if((i%laser_info_.sampling_factor_ == 0) || (i == all_saturated_ranges.size()-1))
                cv::line(I_all_saturated_ranges, laser_image_info_.p_origin_, p_fin, cv::Scalar(255, 0, 0), 2);
        }

        cv::imshow("All saturated ranges", I_all_saturated_ranges);
        cv::waitKey(1);
    }




    DrawResults();

    //Ensure that all the components in the NORMALIZED STATE vector are within [-1.0, 1.0]
    for(int i=0;i<normalized_state_.size();i++)
    {
        if(normalized_state_[i] > 1.0)
            normalized_state_[i] = 1.0;
        else if(normalized_state_[i] < -1.0)
            normalized_state_[i] = -1.0;
    }

    return normalized_state_;


}


std::vector<int> RlEnvironmentLaserBasedNavigation::ComputeMinLaserRangesToObstacles()
{
    cv::Mat I_obstacles_boundary = laser_image_info_.obstacles_boundary_image_.clone();
    int circle_radius = 5;
    for(int i=0;i<laser_info_.laser_ranges_.size();i++)
    {
        float delta_pos_proyec_module = laser_info_.laser_ranges_[i]/laser_image_info_.laser_scans_image_res_;
        cv::Point2f delta_pos_proyec;
        delta_pos_proyec.x = delta_pos_proyec_module * laser_image_info_.cos_angles_ranges_[i];
        delta_pos_proyec.y = -delta_pos_proyec_module * laser_image_info_.sin_angles_ranges_[i];

        int x_im = laser_image_info_.laser_scans_image_.cols/2 + delta_pos_proyec.x;
        int y_im = laser_image_info_.laser_scans_image_.rows/2 + delta_pos_proyec.y;

        cv::Point p_range_i = cv::Point(x_im, y_im);
        cv::circle(I_obstacles_boundary, p_range_i, circle_radius, cv::Scalar(0), -1);
    }


    //Find contours in image in order to compute the number of obstacles
    std::vector<std::vector<cv::Point> > contours;
    cv::Mat I_obstacles_vector_rgb = I_obstacles_boundary.clone();
    cv::bitwise_not(I_obstacles_vector_rgb, I_obstacles_vector_rgb);
    cv::Mat I_for_contours = I_obstacles_vector_rgb.clone();
    cv::findContours(I_for_contours, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    //Remove those obstacles that are very small (only consider obstacles > than a predefined area)
    std::vector<std::vector<cv::Point> >::iterator itc = contours.begin();
    itc = contours.begin();
    while(itc!=contours.end())
    {
        cv::Rect box = cv::boundingRect(cv::Mat(*itc));
        if(box.area() < 400)
            itc = contours.erase(itc);
        else
            ++itc;
    }

//    cv::cvtColor(I_obstacles_vector_rgb, I_obstacles_vector_rgb, CV_GRAY2RGB);
//    cv::drawContours(I_obstacles_vector_rgb, contours, -1, cv::Scalar(255, 0, 255), 2);
//    cv::imshow("Obstacles Contour", I_obstacles_vector_rgb);

    //Compute the closest Point of the obstacle contour to the UAV
    std::vector<cv::Point> closest_obstacle_points_to_uav;
    std::vector<cv::Point> closest_obstacle_distances_to_uav;
    for(int i=0;i<contours.size();i++)
    {
        int ind_nearest_obs = 0;
        float min_ditance_to_uav = FLT_MAX;
        for(int j=0;j<contours[i].size();j++)
        {
            cv::Point d_obs_near;
            d_obs_near.x = contours[i][j].x - laser_image_info_.p_origin_.x;
            d_obs_near.y = contours[i][j].y - laser_image_info_.p_origin_.y;
            float distance_to_uav = std::pow(d_obs_near.x, 2) + std::pow(d_obs_near.y, 2);
            if(distance_to_uav < min_ditance_to_uav)
            {
                min_ditance_to_uav = distance_to_uav;
                ind_nearest_obs = j;
            }
        }
        //cv::Point closest_obstacle_point_to_uav = contours[i][ind_nearest_obs];
        cv::Point d_obs_nearest;
        d_obs_nearest.x = contours[i][ind_nearest_obs].x - laser_image_info_.p_origin_.x;
        d_obs_nearest.y = contours[i][ind_nearest_obs].y - laser_image_info_.p_origin_.y;

        closest_obstacle_distances_to_uav.push_back(d_obs_nearest);
        closest_obstacle_points_to_uav.push_back(contours[i][ind_nearest_obs]);
    }



    std::vector<int> closest_ranges_to_obstacle_indices;
    for(int i=0;i<closest_obstacle_distances_to_uav.size();i++)
    {
        //  y
        //  ^
        //  |
        //  |
        //  ------> x
        float x = closest_obstacle_distances_to_uav[i].x;
        float y = -closest_obstacle_distances_to_uav[i].y;

        //Refer every point (x, y) to a frame of reference ROTATED angle_ini (NOTE: [x_trans, y_trans]' = R'*[x, y]')
        float x_trans = x*std::cos(-laser_image_info_.angle_ini_rad_) - y*std::sin(-laser_image_info_.angle_ini_rad_);
        float y_trans = x*std::sin(-laser_image_info_.angle_ini_rad_) + y*std::cos(-laser_image_info_.angle_ini_rad_);
        double angle = atan2(-y_trans, -x_trans)/M_PI*180 + 180;

        int range_index = static_cast<int>(angle / laser_image_info_.angle_increment_);
        if(range_index >= laser_info_.num_ranges_)
            range_index = laser_info_.num_ranges_ - 1;

        int circular_sector_index = static_cast<int>(range_index / laser_info_.sampling_factor_);

        closest_ranges_to_obstacle_indices.push_back(circular_sector_index);

//        std::cout<<" ANGLE (atan2) closest_obstacle_distances_to_uav[i]: "<<angle<<std::endl;
//        std::cout<<" INDEX closest_obstacle_distances_to_uav[i]: "<<range_index<<std::endl<<std::endl;
//        std::cout<<" INDEX circular_sector_index[i]: "<<circular_sector_index<<std::endl;

    }

    if(imshow_obstacles_boundary_)
    {
        cv::Mat I_closest_obstacle_points_to_uav = I_obstacles_boundary.clone();
        cv::cvtColor(I_closest_obstacle_points_to_uav, I_closest_obstacle_points_to_uav, CV_GRAY2BGR);
        for(int i=0;i<closest_obstacle_points_to_uav.size();i++)
            DrawArrowedLine(I_closest_obstacle_points_to_uav, laser_image_info_.p_origin_, closest_obstacle_points_to_uav[i], cv::Scalar(0,0,255), 2, 8, 0, 0.3);
        cv::imshow("Min Distance to Obstacles", I_closest_obstacle_points_to_uav);
        cv::waitKey(1);
    }

    return closest_ranges_to_obstacle_indices;

}




void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsMaze(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 5;

    int min_target_area_num = 1;
    int max_target_area_num = 5;

    //First Randomly select one of the 4 possible areas

    std::srand (static_cast <unsigned> (time(0)));

    float lower_lim = 2.5;
    float upper_lim = 5.5;
    float r3 = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
    std::cout<<std::endl<<"++ Random Number ++"<<std::endl<<r3<<std::endl;

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;

    switch(area_uav_num)
    {
        case 1:
            uav_pose.x = 5.0;
            uav_pose.y = 5.5;
            break;
        case 2:
            uav_pose.x = 5.0;
            uav_pose.y = -5.5;
            break;
        case 3:
            uav_pose.x = -4.0;
            uav_pose.y = 0.0;
            break;
        case 4:
            uav_pose.x = 0.5;
            uav_pose.y = -0.5;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            target_pose.x = 5.0;
            target_pose.y = -7.0;
            break;
        case 2:
            target_pose.x = 5.0;
            target_pose.y = 7.0;
            break;
        case 3:
            target_pose.x = 0.0;
            target_pose.y = 0.0;
            break;
        case 4:
            target_pose.x = 5.0;
            target_pose.y = -5.0;
            break;
        default:
            target_pose.x = 5.0;
            target_pose.y = -5.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}

void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsMaze3(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 3;

    int min_target_area_num = 1;
    int max_target_area_num = 4;

    //First Randomly select one of the 4 possible areas

    srand(time(NULL));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;


    float lower_lim_x = 0.0;
    float upper_lim_x = 0.0;
    float lower_lim_y = 0.0;
    float upper_lim_y = 0.0;
    float rand_pos_x = 0.0;
    float rand_pos_y = 0.0;
    switch(area_uav_num)
    {
        case 1:
            lower_lim_x = -1.0;
            upper_lim_x = -5.5;
            lower_lim_y = -0.75;
            upper_lim_y = 0.75;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            uav_pose.x = rand_pos_x;
            uav_pose.y = rand_pos_y;
            break;
        case 2:
            lower_lim_x = 0.2;
            upper_lim_x = 4.0;
            lower_lim_y = -0.25;
            upper_lim_y = 0.25;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            uav_pose.x = rand_pos_x;
            uav_pose.y = rand_pos_y;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            lower_lim_x = -1.0;
            upper_lim_x = -5.5;
            lower_lim_y = -0.75;
            upper_lim_y = 0.75;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            target_pose.x = rand_pos_x;
            target_pose.y = rand_pos_y;
            break;
        case 2:
            lower_lim_x = 0.5;
            upper_lim_x = 5.0;
            lower_lim_y = -0.25;
            upper_lim_y = 0.25;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            target_pose.x = rand_pos_x;
            target_pose.y = rand_pos_y;
            break;
        case 3:
            lower_lim_y = -3.2;
            upper_lim_y = 3.2;
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            target_pose.x = 5.7;
            target_pose.y = rand_pos_y;
            break;
        default:
            target_pose.x = 6.0;
            target_pose.y = 0.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}

void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsMaze4(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 5;

    int min_target_area_num = 1;
    int max_target_area_num = 5;

    //First Randomly select one of the 4 possible areas

    srand(time(NULL));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;

    switch(area_uav_num)
    {
        case 1:
            uav_pose.x = -3.5;
            uav_pose.y = 3.0;
            break;
        case 2:
            uav_pose.x = -3.75;
            uav_pose.y = 2.9;
            break;
        case 3:
            uav_pose.x = -3.8;
            uav_pose.y = -2;
            break;
        case 4:
            uav_pose.x = -0.5;
            uav_pose.y = -0.5;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            target_pose.x = 3.75;
            target_pose.y = 3.0;
            break;
        case 2:
            target_pose.x = 3.55;
            target_pose.y = 2.9;
            break;
        case 3:
            target_pose.x = 0.0;
            target_pose.y = 2.8;
            break;
        case 4:
            target_pose.x = 3.55;
            target_pose.y = -2.5;
            break;
        default:
            target_pose.x = 6.0;
            target_pose.y = 0.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}


void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsHouse(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 7;

    int min_target_area_num = 1;
    int max_target_area_num = 6;

    //First Randomly select one of the 8 possible areas
    std::srand (static_cast <unsigned> (time(0)));


    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;

    target_pose.x = 0.0;
    target_pose.y = 0.0;

    float lower_lim = 0.0;
    float upper_lim = 0.0;
    float rand_pos = 0.0;
    switch(area_uav_num)
    {
        case 1:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = -5.55;
            uav_pose.y = rand_pos;
            break;
        case 2:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = rand_pos;
            uav_pose.y = 5.5;
            break;
        case 3:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = 5.6;
            uav_pose.y = rand_pos;
            break;
        case 4:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = rand_pos;
            uav_pose.y = -5.45;
            break;
        case 5:
            lower_lim = -1.5;
            upper_lim = 1.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = 0.4;
            uav_pose.y = rand_pos;
            break;
        case 6:
            lower_lim = -1.0;
            upper_lim = 1.0;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = rand_pos;
            uav_pose.y = 0.0;
            break;
        default:
            uav_pose.x = 0.5;
            uav_pose.y = 0.5;
            break;
    }


    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}


void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsHouse2(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 6;

    int min_target_area_num = 1;
    int max_target_area_num = 9;

    //First Randomly select one of the 8 possible areas
    srand(time(NULL));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;


    switch(area_uav_num)
    {
        case 1:
            uav_pose.x = -0.5;
            uav_pose.y = 0.0;
            break;
        case 2:
            uav_pose.x = 0.0;
            uav_pose.y = 4.0;
            break;
        case 3:
            uav_pose.x = 4.0;
            uav_pose.y = 0.5;
            break;
        case 4:
            uav_pose.x = -3.5;
            uav_pose.y = -3.0;
            break;
        case 5:
            uav_pose.x = -3.5;
            uav_pose.y = 1.0;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            target_pose.x = -1.5;
            target_pose.y = 3.5;
            break;
        case 2:
            target_pose.x = 3.5;
            target_pose.y = 3.5;
            break;
        case 3:
            target_pose.x = 3.5;
            target_pose.y = -0.5;
            break;
        case 4:
            target_pose.x = 3.0;
            target_pose.y = -4.0;
            break;
        case 5:
            target_pose.x = 0.5;
            target_pose.y = -3.5;
            break;
        case 6:
            target_pose.x = -3.5;
            target_pose.y = -3.0;
            break;
        case 7:
            target_pose.x = -4.0;
            target_pose.y = 3.5;
            break;
        case 8:
            target_pose.x = 0.5;
            target_pose.y = 0.5;
            break;
        default:
            target_pose.x = 5.0;
            target_pose.y = -5.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}

void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsHouse3(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{

    std::srand (static_cast <unsigned> (time(0)));

    float max_pos_x = (environment_info_.max_pos_x_ / 2.0) - 1;
    float max_pos_y = (environment_info_.max_pos_y_ / 2.0) - 1;


    if(TEST_MODE)
    {
         //*********** FOR TESTING PURPOSES ***********
        float lower_lim_x = -max_pos_x;
        float upper_lim_x = max_pos_x;
        float lower_lim_y = -1.0;
        float upper_lim_y = 1.0;
        float rand_pos_x = -3.0;
        float rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        uav_pose.x = rand_pos_x;
        uav_pose.y = rand_pos_y;

        lower_lim_x = -max_pos_x;
        upper_lim_x = max_pos_x;
        lower_lim_y = -1.0;
        upper_lim_y = 1.0;
        if(environment_info_.name_ == "house_3c")
            lower_lim_y = 0.0;
        rand_pos_x = 2.0;
        rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        target_pose.x = rand_pos_x;
        target_pose.y = rand_pos_y;
    }
    else
    {
        //*********** FOR TRAINING PURPOSES ***********
        float lower_lim_x = -max_pos_x;
        float upper_lim_x = max_pos_x;
        float lower_lim_y = -max_pos_y;
        float upper_lim_y = max_pos_y;
        float rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
        float rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        uav_pose.x = rand_pos_x;
        uav_pose.y = rand_pos_y;

        lower_lim_x = -max_pos_x;
        upper_lim_x = max_pos_x;
        lower_lim_y = -max_pos_y;
        upper_lim_y = max_pos_y;
        rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
        rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        target_pose.x = rand_pos_x;
        target_pose.y = rand_pos_y;
    }


    area_uav_num_ = 1;
    area_target_num_ = 1;
}

void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsHouse4(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{

    int min_uav_area_num = 1;
    int max_uav_area_num = 7;

    int min_target_area_num = 1;
    int max_target_area_num = 7;


    std::srand (static_cast <unsigned> (time(0)));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;


    float lower_lim_x = 0.0;
    float upper_lim_x = 0.0;
    float lower_lim_y = 0.0;
    float upper_lim_y = 0.0;
    float rand_pos_x = 0.0;
    float rand_pos_y = 0.0;

    float l_lim = 0.8;
    float up_lim = 2.3;
    float max_lim = 3.2;
    switch(area_uav_num)
    {
        case 1:
            lower_lim_x = -l_lim; upper_lim_x = l_lim;
            lower_lim_y = -l_lim; upper_lim_y = l_lim;
            break;
        case 2:
            lower_lim_x = -l_lim; upper_lim_x = l_lim;
            lower_lim_y = -l_lim; upper_lim_y = l_lim;
            break;
        case 3:
            lower_lim_x = -max_lim; upper_lim_x = max_lim;
            lower_lim_y = up_lim; upper_lim_y = max_lim;
            break;
        case 4:
            lower_lim_x = up_lim; upper_lim_x = max_lim;
            lower_lim_y = -max_lim; upper_lim_y = max_lim;
            break;
        case 5:
            lower_lim_x = -max_lim; upper_lim_x = max_lim;
            lower_lim_y = -up_lim; upper_lim_y = -max_lim;
            break;
        case 6:
            lower_lim_x = -up_lim; upper_lim_x = -max_lim;
            lower_lim_y = -max_lim; upper_lim_y = max_lim;
            break;
        default:
            lower_lim_x = -0.5; upper_lim_x = 0.5;
            lower_lim_y = -0.5; upper_lim_y = 0.5;
            break;
    }
    rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
    uav_pose.x = rand_pos_x;
    uav_pose.y = rand_pos_y;

    switch(area_target_num)
    {
        case 1:
            lower_lim_x = -l_lim; upper_lim_x = l_lim;
            lower_lim_y = -2.0; upper_lim_y = 2.0;
            break;
        case 2:
            lower_lim_x = -l_lim; upper_lim_x = l_lim;
            lower_lim_y = -2.0; upper_lim_y = 2.0;
            break;
        case 3:
            lower_lim_x = -max_lim; upper_lim_x = max_lim;
            lower_lim_y = up_lim; upper_lim_y = max_lim;
            break;
        case 4:
            lower_lim_x = up_lim; upper_lim_x = max_lim;
            lower_lim_y = -max_lim; upper_lim_y = max_lim;
            break;
        case 5:
            lower_lim_x = -max_lim; upper_lim_x = max_lim;
            lower_lim_y = -up_lim; upper_lim_y = -max_lim;
            break;
        case 6:
            lower_lim_x = -up_lim; upper_lim_x = -max_lim;
            lower_lim_y = -max_lim; upper_lim_y = max_lim;
            break;
        default:
            lower_lim_x = -0.5; upper_lim_x = 0.5;
            lower_lim_y = -0.5; upper_lim_y = 0.5;
            break;
    }
    rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
    target_pose.x = rand_pos_x;
    target_pose.y = rand_pos_y;

    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}


void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsHouse5(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{

    int min_uav_area_num = 1;
    int max_uav_area_num = 6;

    int min_target_area_num = 1;
    int max_target_area_num = 6;


    std::srand (static_cast <unsigned> (time(0)));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;


    float lower_lim_x = 0.0;
    float upper_lim_x = 0.0;
    float lower_lim_y = 0.0;
    float upper_lim_y = 0.0;
    float rand_pos_x = 0.0;
    float rand_pos_y = 0.0;

    float l_lim = 1.0;
    float up_lim = 2.4;
    switch(area_uav_num)
    {
        case 1:
            lower_lim_x = -0.7; upper_lim_x = 0.7;
            lower_lim_y = -1.0; upper_lim_y = 1.0;
            break;
        case 2:
            lower_lim_x = -up_lim; upper_lim_x = -l_lim;
            lower_lim_y = l_lim; upper_lim_y = up_lim;
            break;
        case 3:
            lower_lim_x = l_lim; upper_lim_x = up_lim;
            lower_lim_y = l_lim; upper_lim_y = up_lim;
            break;
        case 4:
            lower_lim_x = l_lim; upper_lim_x = up_lim;
            lower_lim_y = -up_lim; upper_lim_y = -l_lim;
            break;
        case 5:
            lower_lim_x = -up_lim; upper_lim_x = -l_lim;
            lower_lim_y = -up_lim; upper_lim_y = -l_lim;
            break;
        default:
            lower_lim_x = -0.5; upper_lim_x = 0.5;
            lower_lim_y = -0.5; upper_lim_y = 0.5;
            break;
    }
    rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
    uav_pose.x = rand_pos_x;
    uav_pose.y = rand_pos_y;

    switch(area_target_num)
    {
        case 1:
            lower_lim_x = -0.7; upper_lim_x = 0.7;
            lower_lim_y = -1.0; upper_lim_y = 1.0;
            break;
        case 2:
            lower_lim_x = -up_lim; upper_lim_x = -l_lim;
            lower_lim_y = l_lim; upper_lim_y = up_lim;
            break;
        case 3:
            lower_lim_x = l_lim; upper_lim_x = up_lim;
            lower_lim_y = l_lim; upper_lim_y = up_lim;
            break;
        case 4:
            lower_lim_x = l_lim; upper_lim_x = up_lim;
            lower_lim_y = -up_lim; upper_lim_y = -l_lim;
            break;
        case 5:
            lower_lim_x = -up_lim; upper_lim_x = -l_lim;
            lower_lim_y = -up_lim; upper_lim_y = -l_lim;
            break;
        default:
            lower_lim_x = -0.5; upper_lim_x = 0.5;
            lower_lim_y = -0.5; upper_lim_y = 0.5;
    }
    rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
    target_pose.x = rand_pos_x;
    target_pose.y = rand_pos_y;

    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}

void RlEnvironmentLaserBasedNavigation::GenerateRandomPositionsHouse6(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{

    int min_uav_area_num = 1;
    int max_uav_area_num = 7;

    int min_target_area_num = 1;
    int max_target_area_num = 7;


    std::srand (static_cast <unsigned> (time(0)));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;


    float lower_lim_x = 0.0;
    float upper_lim_x = 0.0;
    float lower_lim_y = 0.0;
    float upper_lim_y = 0.0;
    float rand_pos_x = 0.0;
    float rand_pos_y = 0.0;

    switch(area_uav_num)
    {
        case 1:
            lower_lim_x = -4.0; upper_lim_x = 0.0;
            lower_lim_y = -1.5; upper_lim_y = 1.5;
            break;
        case 2:
            lower_lim_x = -0.5; upper_lim_x = 2.5;
            lower_lim_y = 2.0; upper_lim_y = 4.0;
            break;
        case 3:
            lower_lim_x = 3.0; upper_lim_x = 4.0;
            lower_lim_y = -4.0; upper_lim_y = 4.0;
            break;
        case 4:
            lower_lim_x = 0.5; upper_lim_x = 2.5;
            lower_lim_y = -4.0; upper_lim_y = -1.0;
            break;
        case 5:
            lower_lim_x = -4.0; upper_lim_x = -2.5;
            lower_lim_y = -4.0; upper_lim_y = -1.0;
            break;
        case 6:
            lower_lim_x = -4.0; upper_lim_x = -1.5;
            lower_lim_y = 3.5; upper_lim_y = 4.0;
            break;
        default:
            lower_lim_x = -0.5; upper_lim_x = 0.5;
            lower_lim_y = -0.5; upper_lim_y = 0.5;
            break;
    }
    rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
    uav_pose.x = rand_pos_x;
    uav_pose.y = rand_pos_y;

    switch(area_target_num)
    {
        case 1:
            lower_lim_x = -4.0; upper_lim_x = 0.0;
            lower_lim_y = -1.2; upper_lim_y = 1.2;
            break;
        case 2:
            lower_lim_x = -0.5; upper_lim_x = 3.0;
            lower_lim_y = 2.0; upper_lim_y = 4.0;
            break;
        case 3:
            lower_lim_x = 3.2; upper_lim_x = 4.0;
            lower_lim_y = -4.0; upper_lim_y = 4.0;
            break;
        case 4:
            lower_lim_x = 0.75; upper_lim_x = 3.0;
            lower_lim_y = -4.0; upper_lim_y = -1.0;
            break;
        case 5:
            lower_lim_x = -4.0; upper_lim_x = -2.5;
            lower_lim_y = -4.0; upper_lim_y = -1.0;
            break;
        case 6:
            lower_lim_x = -4.0; upper_lim_x = -0.5;
            lower_lim_y = 3.5; upper_lim_y = 4.0;
            break;
        default:
            lower_lim_x = -0.5; upper_lim_x = 0.5;
            lower_lim_y = -0.5; upper_lim_y = 0.5;
            break;
    }
    rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
    target_pose.x = rand_pos_x;
    target_pose.y = rand_pos_y;

    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}


std::vector<cv::Point2f> RlEnvironmentLaserBasedNavigation::GenerateRandomPositionForObstacles(const int num_obstacles)
{
    int min_area_num = 1;
    int max_area_num = 5;


    std::srand (static_cast <unsigned> (time(0)));


    std::vector<cv::Point2f> obstacles_rand_pos;
    std::vector<int> obstacles_area_num;

    float lower_lim_x = 0.0;
    float upper_lim_x = 0.0;
    float lower_lim_y = 0.0;
    float upper_lim_y = 0.0;
    float rand_pos_x = 0.0;
    float rand_pos_y = 0.0;

    float l_lim = 1.5;
    float up_lim = 2.0;
    for(int i=0;i<num_obstacles;i++)
    {
        int area_num = min_area_num + rand() % (max_area_num - min_area_num);
        if(i>0)
        {
            do
            {
                area_num = min_area_num + rand() % (max_area_num - min_area_num);
            }while(area_num == obstacles_area_num[0]);
        }
        obstacles_area_num.push_back(area_num);
        std::cout<<"Random (OBSTACLE) area number generated: "<<area_num<<std::endl;


        switch(area_num)
        {
            case 1:
                lower_lim_x = -up_lim; upper_lim_x = up_lim;
                lower_lim_y = l_lim; upper_lim_y = up_lim;
                break;
            case 2:
                lower_lim_x = l_lim; upper_lim_x = up_lim;
                lower_lim_y = -up_lim; upper_lim_x = up_lim;
                break;
            case 3:
                lower_lim_x = -up_lim; upper_lim_x = up_lim;
                lower_lim_y = -l_lim; upper_lim_y = -up_lim;
                break;
            case 4:
                lower_lim_x = -l_lim; upper_lim_y = -up_lim;
                lower_lim_y = -up_lim; upper_lim_x = up_lim;
                break;
            default:
                lower_lim_x = -up_lim; upper_lim_x = up_lim;
                lower_lim_y = l_lim; upper_lim_y = up_lim;
                break;
        }
        rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
        rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

        cv::Point2f random_obstale_point;
        random_obstale_point.x = rand_pos_x;
        random_obstale_point.y = rand_pos_y;
        obstacles_rand_pos.push_back(random_obstale_point);
    }

    return obstacles_rand_pos;
}





//********************* DRAWING FUNCTIONS *********************//
void RlEnvironmentLaserBasedNavigation::DrawResults()
{
    if(imshow_laser_state_scans_)
    {
        cv::Mat I_laser_scans = laser_image_info_.laser_scans_image_.clone();
        for(int i=0;i<laser_ranges_state_.size();i++)
        {
            float angle = (laser_image_info_.angle_ini_ + laser_info_.angle_sampling_factor_/2.0 + i*laser_info_.angle_sampling_factor_) * M_PI/180.0;
            float delta_pos_proyec_module = laser_ranges_state_[i]/laser_image_info_.laser_scans_image_res_;
            cv::Point2f delta_pos_proyec;
            delta_pos_proyec.x = delta_pos_proyec_module * std::cos(angle);
            delta_pos_proyec.y = -delta_pos_proyec_module * std::sin(angle);

            int x_im = laser_image_info_.laser_scans_image_.cols/2 + delta_pos_proyec.x;
            int y_im = laser_image_info_.laser_scans_image_.rows/2 + delta_pos_proyec.y;

            cv::Point p_fin = cv::Point(x_im, y_im);
            cv::line(I_laser_scans, laser_image_info_.p_origin_, p_fin, cv::Scalar(0), 2);

        }


        cv::Mat I_laser_scans_with_min_range = I_laser_scans.clone();
        cv::cvtColor(I_laser_scans_with_min_range, I_laser_scans_with_min_range, CV_GRAY2BGR);

        if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        {
            std::vector<float> circular_sectors (laser_ranges_state_.size() + 1);
            std::fill(circular_sectors.begin(), circular_sectors.end(), laser_info_.max_virtual_range_);

            for(int i=0;i<circular_sectors.size();i++)
            {
                float angle = (laser_image_info_.angle_ini_ + i*laser_info_.angle_sampling_factor_) * M_PI/180.0;
                float delta_pos_proyec_module = circular_sectors[i]/laser_image_info_.laser_scans_image_res_;
                cv::Point2f delta_pos_proyec;
                delta_pos_proyec.x = delta_pos_proyec_module * std::cos(angle);
                delta_pos_proyec.y = -delta_pos_proyec_module * std::sin(angle);

                int x_im = laser_image_info_.laser_scans_image_.cols/2 + delta_pos_proyec.x;
                int y_im = laser_image_info_.laser_scans_image_.rows/2 + delta_pos_proyec.y;

                cv::Point p_fin = cv::Point(x_im, y_im);
                cv::line(I_laser_scans_with_min_range, laser_image_info_.p_origin_, p_fin, cv::Scalar(255, 0, 0), 1);

            }
        }

        cv::Point min_range_scans_circle_center = cv::Point(laser_image_info_.laser_scans_image_.cols/2, laser_image_info_.laser_scans_image_.cols/2);
        int  min_range_scans_circle_radius = laser_info_.min_range_reset_value_ / laser_image_info_.laser_scans_image_res_;
        cv::circle(I_laser_scans_with_min_range, min_range_scans_circle_center, min_range_scans_circle_radius, cv::Scalar(0,0,255), 2);

        if(imshow_laser_state_scans_and_goal_)
        {
            float delta_x_proyec = uav_target_current_relative_position_.x/laser_image_info_.laser_scans_image_res_;
            float delta_y_proyec = uav_target_current_relative_position_.y/laser_image_info_.laser_scans_image_res_;

            //Transform the proyected deltas (these deltas are referred to the frame of reference of the costmap) to image coordinates
            int x_im = laser_image_info_.laser_scans_image_.cols/2 - delta_y_proyec;
            int y_im = laser_image_info_.laser_scans_image_.rows/2 - delta_x_proyec;


            cv::Point p_fin = cv::Point(x_im, y_im);
            cv::line(I_laser_scans_with_min_range, laser_image_info_.p_origin_, p_fin, cv::Scalar(0, 255, 0), 2);
            cv::circle(I_laser_scans_with_min_range, p_fin, 5, cv::Scalar(0, 255, 0), -1);
        }


        cv::imshow("Laser Scans", I_laser_scans_with_min_range);
        cv::waitKey(1);
    }

//    if(imshow_obstacles_boundary_)
//    {
//        //CODE for drawing OBSTACLES CONVERTED to CIRCLES
////        cv::Mat I_obstacles_boundary2 = laser_image_info_.obstacles_boundary_image_.clone();
////        for(int i=0;i<laser_info_.laser_ranges_.size();i++)
////        {
////            float delta_pos_proyec_module = laser_info_.laser_ranges_[i]/laser_image_info_.laser_scans_image_res_;
////            cv::Point2f delta_pos_proyec;
////            delta_pos_proyec.x = delta_pos_proyec_module * laser_image_info_.cos_angles_ranges_[i];
////            delta_pos_proyec.y = -delta_pos_proyec_module * laser_image_info_.sin_angles_ranges_[i];

////            int x_im = laser_image_info_.laser_scans_image_.cols/2 + delta_pos_proyec.x;
////            int y_im = laser_image_info_.laser_scans_image_.rows/2 + delta_pos_proyec.y;

////            cv::Point p_range_i = cv::Point(x_im, y_im);
////            cv::circle(I_obstacles_boundary2, p_range_i, 2, cv::Scalar(0), -1);
////        }
////        cv::imshow("Obstacles Boundary2", I_obstacles_boundary2);

//        cv::imshow("Min Distance to Obstacles", I_closest_obstacle_points_to_uav);
//        //cv::imshow("Obstacles Boundary", I_obstacles_boundary);
//        cv::waitKey(1);
//    }


}



float RlEnvironmentLaserBasedNavigation::SaturateLaserRanges(float range)
{
    if(std::isfinite(range))
    {
        if(range > laser_info_.max_virtual_range_)
            range = laser_info_.max_virtual_range_;
    }
    else
        range = laser_info_.max_virtual_range_;

    return range;
}

float RlEnvironmentLaserBasedNavigation::NormalizeSaturatedLaserRanges(float range)
{
    return (range - virtual_ranges_offset_ - laser_info_.min_range_reset_value_)/laser_info_.laser_state_normalization_factor_;
}




void RlEnvironmentLaserBasedNavigation::DrawArrowedLine(cv::Mat& img, cv::Point pt1, cv::Point pt2, const cv::Scalar& color,
           int thickness, int line_type, int shift, double tipLength)
{

    const double tipSize = 50 * tipLength; // Factor to normalize the size of the tip depending on the length of the arrow

    cv::line(img, pt1, pt2, color, thickness, line_type, shift);

    const double angle = atan2( (double) pt1.y - pt2.y, (double) pt1.x - pt2.x );

    cv::Point p(cvRound(pt2.x + tipSize * cos(angle + CV_PI / 4)),
        cvRound(pt2.y + tipSize * sin(angle + CV_PI / 4)));
    cv::line(img, p, pt2, color, thickness, line_type, shift);

    p.x = cvRound(pt2.x + tipSize * cos(angle - CV_PI / 4));
    p.y = cvRound(pt2.y + tipSize * sin(angle - CV_PI / 4));
    cv::line(img, p, pt2, color, thickness, line_type, shift);
}






