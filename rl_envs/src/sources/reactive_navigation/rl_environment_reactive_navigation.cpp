#include "rl_environment_reactive_navigation.h"

#define DEBUG_MODE_STATE 0
#define DEBUG_UAV_TARGET_POSITON 0
#define DEBUG_DISTANCE_TO_OBSTACLE_CALCULATION 0
#define DEBUG_SHAPING_REWARD 0

#define IMSHOW_COSTMAP 0
#define IMSHOW_COSTMAP_WITH_OBSTACLE_VECTOR 1

#define STATE_BASED_ON_HOG 0
#define STATE_BASED_ON_OBSTACLE_VECTOR 1
#define STATE_BASED_ON_COSTMAP_IMAGE 0

#define TEST_MODE 0



RlEnvironmentReactiveNavigation::RlEnvironmentReactiveNavigation()
{
    local_costmap_info_.width_ = 5.0;
    local_costmap_info_.height_ = 5.0;
    local_costmap_info_.res_ = 0.05;
    local_costmap_info_.image_width_ = local_costmap_info_.width_ / local_costmap_info_.res_;
    local_costmap_info_.image_height_ = local_costmap_info_.height_ / local_costmap_info_.res_;

    environment_info_.name_ = "house_3";
    environment_info_.num_episode_steps_ = 400;
    environment_info_.num_iterations_ = 4;
    environment_info_.actions_dim_ = 2;
    environment_info_.actions_max_value_ = 0.5;
    environment_info_.actions_min_value_ = -0.5;
    if(environment_info_.name_ == "house")
    {
        environment_info_.max_pos_x_ = 15.0;
        environment_info_.max_pos_y_ = 15.0;
    }
    else if(environment_info_.name_ == "house_2")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "house_3")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "maze")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 18.0;
    }
    else if(environment_info_.name_ == "maze_3")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 8.5;
    }
    else if(environment_info_.name_ == "maze_4")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    if(STATE_BASED_ON_HOG)
        environment_info_.state_dim_low_dim_ = 76; //36*2 HOG descriptor on image of 32x32 + 4 dim of euclidean distance and linear velocity
    else
    {
        if(STATE_BASED_ON_OBSTACLE_VECTOR)
            environment_info_.state_dim_low_dim_ = 6;
        else
            environment_info_.state_dim_low_dim_ = 4;
    }


    image_based_state_.image_size_ = cv::Size(64, 64);
    image_based_state_.num_channels_ = 1;
    image_based_state_.state_dim_ = 3;
    win_size_ = cv::Size(32,32);
    cell_size_ = cv::Size(16,16);
    block_size_ = cv::Size(32,32);
    block_stride_ = cv::Size(32,32);
    num_histogram_bins_ = 9;

    hog_.winSize = win_size_;
    hog_.blockSize = block_size_;
    hog_.blockStride = block_stride_;
    hog_.cellSize = cell_size_;
    hog_.nbins = num_histogram_bins_;

    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0, 0.0);
    image_based_state_.state_.clear();
    for(int i=0;i<image_based_state_.state_dim_;i++)
        image_based_state_.state_.push_back(cv::Mat::zeros(100, 100, CV_8U));
    current_costmap_image_ = cv::Mat::zeros(local_costmap_info_.image_height_, local_costmap_info_.image_width_, CV_8U);
    prev_costmap_image_ = cv::Mat::zeros(local_costmap_info_.image_height_, local_costmap_info_.image_width_, CV_8U);

    min_distance_from_laser_flag_ = false;
    d_obs_norm_ = cv::Point2f(0.0, 0.0);

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;
    std::cout<<"pitch_roll_max_value_ : "<<pitch_roll_max_value_<<std::endl;

}


RlEnvironmentReactiveNavigation::~RlEnvironmentReactiveNavigation()
{
    f_data_recorder.close();
}

void RlEnvironmentReactiveNavigation::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: REACTIVE NAVIGATION Environment" << std::endl;

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;

    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentReactiveNavigation::PoseVelocityCallback, this);


    ros::param::get("~esimtated_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_estimated_pose_subs_ = n.subscribe(param_string, 1, &RlEnvironmentReactiveNavigation::DroneEstimatedPoseCallback, this);

    ros::param::get("~costmap", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_local_costmap_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
                                            &RlEnvironmentReactiveNavigation::CostmapCallback, this);

    ros::param::get("~laser_scan", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_laser_scan_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
                                         &RlEnvironmentReactiveNavigation::LaserScanCallback, this);




    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~hector_slam_reset", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    hector_slam_reset_pub_ = n.advertise<std_msgs::String>(param_string, 1000);




    // Init services
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    ros::param::get("~env_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    rl_environment_state_pub_ = n.advertise<std_msgs::Float32MultiArray>(param_string, 1, true);


    // Set number of iterations of Gazebo
    if (ENABLE_PAUSED_SIMULATION)
    {
        this->data_->num_iterations = (unsigned int) environment_info_.num_iterations_;

        // Print
        std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;
    }

    // Init recording experiment
//    exp_rec_.Open(n);

    // Reset environment
    Reset();
    //f_data_recorder.open("/home/carlos/pruebas_Matlab/speeds_pos_in_image_with_ground_truth3.txt");
}

bool RlEnvironmentReactiveNavigation::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{
    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    std::vector<float> current_state = GetNormalizedState();

    if(DEBUG_MODE_STATE)
    {
        std::cout<<"***** CURRENT STATE *****"<<std::endl;
        for(int i=0; i<current_state.size();i++)
        {
            if(i==0)
                std::cout<<"[";
            if(i==current_state.size()-1)
                std::cout<<current_state[i]<<"]";
            else
                std::cout<<current_state[i]<<" , ";
        }
        std::cout<<std::endl;
    }

    // Print info
//    std::cout << "RL_ENV_DEBUG: UAV pose x " << x << " y " << y << std::endl;

    // Compute reward
//    float shaping = - 100 * std::sqrt(pow(current_state[0], 2) + pow(current_state[1], 2))
//              - 10 * std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2)) -
//            std::sqrt(std::pow(request.action[0]/environment_info_.actions_max_value_, 2) + std::pow(request.action[1]/environment_info_.actions_max_value_, 2));


    float shaping = 0.0;
    if(d_obs_norm_.x == 0.0 && d_obs_norm_.y == 0.0) //If there is NO obstacle in the trajectory UAV-Target
    {
        //std::cout<<"1 termed shaping"<<std::endl;
        shaping = - 150 * std::sqrt(std::pow(current_state[0], 2) + std::pow(current_state[1], 2))
                  - 10 * std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2));
    }
    else
    {
        //std::cout<<"2 termed shaping"<<std::endl;
        float shaping_based_on_3d_positions = - 150 * std::sqrt(std::pow(current_state[0], 2) + std::pow(current_state[1], 2))
                  - 10 * std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2));

        float shaping_based_on_image = -10.0/std::sqrt(std::pow(d_obs_norm_.x, 2) + std::pow(d_obs_norm_.y, 2));

        shaping = shaping_based_on_3d_positions + shaping_based_on_image;
    }
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;

    if(DEBUG_SHAPING_REWARD)
    {
        std::cout<<std::endl<<"****** SHPAING - REWARD ******"<<std::endl;
        std::cout<<"Shaping: "<<shaping<<std::endl;
        std::cout<<"Reward: "<<reward<<std::endl<<std::endl;
    }

    if (min_distance_from_laser_flag_)
    {
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
        min_distance_from_laser_flag_ = false;
    }
    else
        response.terminal_state = false;

    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();


    // Read next state
    std::vector<float> next_state = GetNormalizedState();
    response.obs_real = next_state;

    if(STATE_BASED_ON_COSTMAP_IMAGE)
    {
        if(image_based_state_.state_.size() == image_based_state_.state_dim_)
        {
            std::vector<sensor_msgs::Image> image_vector_msg;
            for(int i=0;i<image_based_state_.state_.size();i++)
            {
                cv_bridge::CvImage cv_bridge_obj;
                cv_bridge_obj.header.stamp = ros::Time::now();
                cv_bridge_obj.encoding = sensor_msgs::image_encodings::MONO8;
                cv::Mat costmap_image_resized_for_CNN;
                cv::resize(image_based_state_.state_[i], costmap_image_resized_for_CNN, image_based_state_.image_size_, 0, 0, cv::INTER_CUBIC);
                cv_bridge_obj.image = costmap_image_resized_for_CNN;


                sensor_msgs::Image image_msg;
                cv_bridge_obj.toImageMsg(image_msg);
                image_vector_msg.push_back(image_msg);
            }
            response.img = image_vector_msg;
        }
    }


    // Successful return
    return true;
}

bool RlEnvironmentReactiveNavigation::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality Reactive Navigation service called" << std::endl;

    // Action dimensionality
    response.action_dim = environment_info_.actions_dim_;


    // Action max
    std::vector<float> actions_max = {environment_info_.actions_max_value_, environment_info_.actions_max_value_};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {environment_info_.actions_min_value_, environment_info_.actions_min_value_};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = environment_info_.state_dim_low_dim_;

    if(STATE_BASED_ON_COSTMAP_IMAGE)
    {
        std::vector<u_int64_t> state_img_dim = {static_cast<u_int64_t>(image_based_state_.image_size_.width), static_cast<u_int64_t>(image_based_state_.image_size_.height),
                                               static_cast<u_int64_t>(image_based_state_.state_dim_)};
        response.state_dim_img = state_img_dim;
    }

    // Number of iterations
    response.num_iterations = environment_info_.num_episode_steps_;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentReactiveNavigation::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv Reactive Navigation service called" << std::endl;


    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
//    if (exp_rec_.getRecording())
//        long_iteration_ = true;

    std::vector<float> state = GetNormalizedState();
    response.state = state;

    if(STATE_BASED_ON_COSTMAP_IMAGE)
    {
        if(image_based_state_.state_.size() == image_based_state_.state_dim_)
        {
            std::vector<sensor_msgs::Image> image_vector_msg;
            for(int i=0;i<image_based_state_.state_.size();i++)
            {
                cv_bridge::CvImage cv_bridge_obj;
                cv_bridge_obj.header.stamp = ros::Time::now();
                cv_bridge_obj.encoding = sensor_msgs::image_encodings::MONO8;
                cv::Mat costmap_image_resized_for_CNN;
                cv::resize(image_based_state_.state_[i], costmap_image_resized_for_CNN, image_based_state_.image_size_, 0, 0, cv::INTER_CUBIC);
                cv_bridge_obj.image = costmap_image_resized_for_CNN;


                sensor_msgs::Image image_msg;
                cv_bridge_obj.toImageMsg(image_msg);
                image_vector_msg.push_back(image_msg);
            }
            response.img_state = image_vector_msg;
        }
    }

    return true;
}

bool RlEnvironmentReactiveNavigation::Reset()
{
    // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav;
    float roll, pitch, yaw;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav, roll, pitch, yaw);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);


    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = kUav_Altitude_;
    uav_pose_ref_pub_.publish(altitude_msg);


    // Spawn model to RANDOM locations (UAV and Target)
    cv::Point2f uav_rand_pos, target_rand_pos;
    if(environment_info_.name_ == "house")
        GenerateRandomPositionsHouse(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_2")
        GenerateRandomPositionsHouse2(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_3")
        GenerateRandomPositionsHouse3(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze")
        GenerateRandomPositionsMaze(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze_3")
        GenerateRandomPositionsMaze3(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze_4")
        GenerateRandomPositionsMaze4(uav_rand_pos, target_rand_pos);

    gazebo_msgs::SetModelState model_msg_uav;
    model_msg_uav.request.model_state.model_name = UAV_NAME;
    model_msg_uav.request.model_state.pose.position.x = uav_rand_pos.x;
    model_msg_uav.request.model_state.pose.position.y = uav_rand_pos.y;
    model_msg_uav.request.model_state.pose.position.z = kUav_Altitude_;

    gazebo_msgs::SetModelState model_msg_target;
    model_msg_target.request.model_state.model_name = TARGET_NAME;
    model_msg_target.request.model_state.pose.position.x = target_rand_pos.x;
    model_msg_target.request.model_state.pose.position.y = target_rand_pos.y;
    model_msg_target.request.model_state.pose.position.z = 0.0;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting UAV pos at x: " << uav_rand_pos.x << " and y: " << uav_rand_pos.y << std::endl;
    std::cout << "RL_ENV_INFO: reseting TARGET pos at x: " << target_rand_pos.x << " and y: " << target_rand_pos.y << std::endl;

    if (gazebo_client_.call(model_msg_uav))
    {

    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    if(!(environment_info_.name_ == "house"))
    {
        if (gazebo_client_.call(model_msg_target))
        {

        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
    //        return false;
        }
    }

    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg))
    {
        ResetHectorSlam();
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        return false;
    }

}



void RlEnvironmentReactiveNavigation::LaserScanCallback(const sensor_msgs::LaserScan &msg)
{
    float range_offset = 0.5;
    for(int i=0;i<msg.ranges.size();i++)
    {
        if(msg.ranges[i] < range_offset)
        {
            min_distance_from_laser_flag_ = true;
            std::cout<<"MIN DISTANCE reached! ("<<msg.ranges[i]<<")"<<std::endl;
            break;
        }
    }
}

void RlEnvironmentReactiveNavigation::CostmapCallback(const nav_msgs::OccupancyGrid &msg)
{    
    prev_costmap_image_ = current_costmap_image_.clone();

    grid_map::GridMap costmap;
    grid_map::GridMapRosConverter::fromOccupancyGrid(msg, "elevation", costmap);
    grid_map::GridMapCvConverter::toImage<unsigned char, 1>(costmap, "elevation", CV_8UC1, 100, 0, current_costmap_image_);

    if(image_based_state_.list_state_.size() < image_based_state_.state_dim_)
    {
        image_based_state_.list_state_.push_back(current_costmap_image_);
    }
    else
    {
        image_based_state_.list_state_.pop_front();
        image_based_state_.list_state_.push_back(current_costmap_image_);
    }

//    std::cout<<"current_costmap_image_ (width): "<<current_costmap_image_.cols<<std::endl;
//    std::cout<<"current_costmap_image_ (height): "<<current_costmap_image_.rows<<std::endl;
//    std::cout<<"current_costmap_image_ (type): "<<current_costmap_image_.type()<<std::endl;
    image_based_state_.state_.clear();
    image_based_state_.state_.insert(image_based_state_.state_.end(), image_based_state_.list_state_.begin(), image_based_state_.list_state_.end());

    cv::Mat costmap_diff_image = current_costmap_image_ - prev_costmap_image_;



    cv::Mat costmap_image_resized;
    cv::Mat costmap_diff_image_resized;
    cv::resize(current_costmap_image_, costmap_image_resized, cv::Size(win_size_.width, win_size_.height), 0, 0, cv::INTER_CUBIC);
    cv::resize(costmap_diff_image, costmap_diff_image_resized, cv::Size(win_size_.width, win_size_.height), 0, 0, cv::INTER_CUBIC);
    cv::resize(current_costmap_image_, local_costmap_double_res_, cv::Size(current_costmap_image_.cols*2,
                                                                           current_costmap_image_.rows*2), 0, 0, cv::INTER_CUBIC);

    if(IMSHOW_COSTMAP)
    {
        cv::imshow("Occupancy Grid TO cv::Mat", current_costmap_image_);
        cv::imshow("costmap_image_resized", costmap_image_resized);
        cv::imshow("costmap_diff_image", costmap_diff_image);
        cv::imshow("local costmap DOUBLE res", local_costmap_double_res_);
        cv::waitKey(1);
    }

    std::vector<float> hog_descriptor_costmap;
    hog_.compute(costmap_image_resized, hog_descriptor_costmap);
    std::vector<float> hog_descriptor_costmap_diff;
    hog_.compute(costmap_diff_image_resized, hog_descriptor_costmap_diff);


    hog_descriptors_.clear();
    hog_descriptors_.push_back(hog_descriptor_costmap);
    hog_descriptors_.push_back(hog_descriptor_costmap_diff);


    if(STATE_BASED_ON_HOG)
        ComputeNormalizedState();

    ComputeNormalizedState();

//    std::cout<<"hog_descriptors_: "<<hog_descriptors_.size()<<std::endl;
//    std::cout<<"HOG[0] Size: "<<hog_descriptors_[0].size()<<std::endl;
//    for(int i=0;i<hog_descriptors_[0].size();++i)
//        std::cout<<hog_descriptors_[0][i]<<" , ";
//    std::cout<<std::endl<<"HOG[1] Size: "<<hog_descriptors_[1].size()<<std::endl;
//    for(int i=0;i<hog_descriptors_[1].size();++i)
//        std::cout<<hog_descriptors_[1][i]<<" , ";
//    std::cout<<std::endl<<std::endl;

}


void RlEnvironmentReactiveNavigation::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    // Get position of the uav in the vector
    cv::Point3f target_position;
    cv::Point3f uav_position, uav_linear_vel;
    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            uav_position.x = msg->pose[i].position.x;
            uav_position.y = msg->pose[i].position.y;
            uav_position.z = msg->pose[i].position.z;

            uav_linear_vel.x = msg->twist[i].linear.x;
            uav_linear_vel.y = msg->twist[i].linear.y;
            uav_linear_vel.z = msg->twist[i].linear.z;
            break;
        }

        else if(msg->name[i].compare(TARGET_NAME) == 0)
        {
            target_position.x = msg->pose[i].position.x;
            target_position.y = msg->pose[i].position.y;
            target_position.z = msg->pose[i].position.z;
        }
    }

    if(DEBUG_UAV_TARGET_POSITON)
    {
        std::cout<<"UAV position: "<<"x: "<<uav_position.x<<" ; "<<"y: "<<uav_position.y<<" ; "<<"area_num: "<<area_uav_num_<<std::endl;
        std::cout<<"TARGET position: "<<"x: "<<target_position.x<<" ; "<<"y: "<<target_position.y<<" ; "<<"area_num: "<<area_target_num_<<std::endl;
    }


    // Set uav state
    SetUavState(uav_position.x, uav_position.y, uav_position.z, uav_linear_vel.x, uav_linear_vel.y, uav_linear_vel.z);
    SetTargetState(target_position.x, target_position.y, target_position.z);

//    if(!STATE_BASED_ON_HOG)
//        ComputeNormalizedState();
}

void RlEnvironmentReactiveNavigation::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose_  = msg;
    //yaw pitch roll
//    uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
//    uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
//    uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

//    uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
//    uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
//    uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;

    return;
}


void RlEnvironmentReactiveNavigation::SetUavState(const float x, const float y, const float z,
                                                  const float dx, const float dy, const float dz)
{
    uav_mutex_.lock();
    uav_state_.pos_x_ = x;
    uav_state_.pos_y_ = y;
    uav_state_.pos_z_ = z;
    uav_state_.speed_x_ = dx;
    uav_state_.speed_y_ = dy;
    uav_state_.speed_z_ = dz;
    uav_mutex_.unlock();
}

void RlEnvironmentReactiveNavigation::SetTargetState(const float x, const float y, const float z)
{
    target_mutex_.lock();
    target_state_.pos_x_ = x;
    target_state_.pos_y_ = y;
    target_state_.pos_z_ = z;
    target_mutex_.unlock();
}

void RlEnvironmentReactiveNavigation::GetUavState(float &x, float &y, float &z,
                                                  float &dx, float &dy, float &dz,
                                                  float &roll, float &pitch, float &yaw)
{
    uav_mutex_.lock();
    x = uav_state_.pos_x_;
    y = uav_state_.pos_y_;
    z = uav_state_.pos_z_;
    dx = uav_state_.speed_x_;
    dy = uav_state_.speed_y_;
    dz = uav_state_.speed_z_;
    roll = uav_state_.roll_;
    pitch = uav_state_.pitch_;
    yaw = uav_state_.yaw_;
    uav_mutex_.unlock();
}

void RlEnvironmentReactiveNavigation::GetTargetState(float &x, float &y, float &z)
{
    target_mutex_.lock();
    x = target_state_.pos_x_;
    y = target_state_.pos_y_;
    z = target_state_.pos_z_;
    target_mutex_.unlock();
}



std::vector<float> RlEnvironmentReactiveNavigation::ComputeNormalizedState()
{
    normalized_state_.clear();


    cv::Point3f uav_target_relative_position;
    uav_target_relative_position.x = target_state_.pos_x_ - uav_state_.pos_x_;
    uav_target_relative_position.y = target_state_.pos_y_ - uav_state_.pos_y_ ;
    uav_target_relative_position.z = target_state_.pos_z_ - uav_state_.pos_z_;

    float x_norm = uav_target_relative_position.x / environment_info_.max_pos_x_;
    float y_norm = uav_target_relative_position.y / environment_info_.max_pos_y_;
    float z_norm = uav_target_relative_position.z;
    float dx_norm = uav_state_.speed_x_;
    float dy_norm = uav_state_.speed_y_;


    normalized_state_.push_back(x_norm);
    normalized_state_.push_back(y_norm);
    normalized_state_.push_back(dx_norm);
    normalized_state_.push_back(dy_norm);
    if(STATE_BASED_ON_HOG)
    {
        for(int i=0;i<hog_descriptors_.size();i++)
            normalized_state_.insert(normalized_state_.end(), hog_descriptors_[i].begin(), hog_descriptors_[i].end());
    }
    //std::cout<<"Normalized State Size: "<<normalized_state_.size()<<std::endl;
    ComputeDistanceToObstacleInTrajectory();
    
    if(STATE_BASED_ON_OBSTACLE_VECTOR)
    {
        normalized_state_.push_back(d_obs_norm_.x);
        normalized_state_.push_back(d_obs_norm_.y);
    }
    

    if(TEST_MODE)
    {
        std_msgs::Float32MultiArray state_msg;

        // set up dimensions
        state_msg.layout.dim.push_back(std_msgs::MultiArrayDimension());
        state_msg.layout.dim[0].size = normalized_state_.size();
        state_msg.layout.dim[0].stride = 1;
        state_msg.layout.dim[0].label = "x"; // or whatever name you typically use to index vec1
        state_msg.data.insert(state_msg.data.end(), normalized_state_.begin(), normalized_state_.end());
        rl_environment_state_pub_.publish(state_msg);
    }

    return normalized_state_;

}


void RlEnvironmentReactiveNavigation::ComputeDistanceToObstacleInTrajectory()
{
    d_obs_norm_.x = 0.0; d_obs_norm_.y = 0.0;
    cv::Point3f delta_pos;
    delta_pos.x = target_state_.pos_x_ - uav_state_.pos_x_;
    delta_pos.y = target_state_.pos_y_ - uav_state_.pos_y_ ;
    delta_pos.z = target_state_.pos_z_ - uav_state_.pos_z_;

    float delta_x_proyec = delta_pos.x/local_costmap_info_.res_;
    float delta_y_proyec = delta_pos.y/local_costmap_info_.res_;

    //Transform the proyected deltas (these deltas are referred to the frame of reference of the costmap) to image coordinates
    int x_im = local_costmap_info_.image_width_/2 - delta_y_proyec;
    int y_im = local_costmap_info_.image_height_/2 - delta_x_proyec;



    cv::Mat I_obstacle_vector = cv::Mat(local_costmap_info_.image_height_, local_costmap_info_.image_width_, CV_8U, cv::Scalar(255));
    cv::Point p_origin = cv::Point(local_costmap_info_.image_width_/2, local_costmap_info_.image_height_/2);
    cv::Point p_fin = cv::Point(x_im, y_im);
    cv::line(I_obstacle_vector, p_origin, p_fin, cv::Scalar(0), 1);

    cv::Mat I_NOT_obstacle_vector;
    cv::Mat I_NOT_current_costmap = current_costmap_image_.clone();
    cv::bitwise_not(I_obstacle_vector, I_NOT_obstacle_vector);
    cv::bitwise_not(I_NOT_current_costmap, I_NOT_current_costmap);
    cv::Mat I_AND_costmap_obstacle_vector;
    cv::bitwise_and(I_NOT_current_costmap, I_NOT_obstacle_vector, I_AND_costmap_obstacle_vector);

    cv::Mat I_obstacle_vector_rgb = current_costmap_image_.clone();
    cv::cvtColor(I_obstacle_vector_rgb, I_obstacle_vector_rgb, CV_GRAY2RGB);
    std::vector<std::vector<cv::Point> > contours;
    cv::Mat I_for_contours = I_AND_costmap_obstacle_vector.clone();
    cv::findContours(I_for_contours, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    cv::Point d_obs = cv::Point(0,0);
    cv::Point d_near_obs = cv::Point(0,0);
    if(contours.size())
    {
//        cv::Rect obstacle_contour_rect = cv::boundingRect(contours[0]);
//        //cv::rectangle(I_obstacle_vector_rgb, obstacle_contour_rect, cv::Scalar(255,0,0),2);

//        cv::Point obstacle_contour_rect_center = cv::Point(obstacle_contour_rect.tl().x + obstacle_contour_rect.width/2,
//                                                           obstacle_contour_rect.tl().y + obstacle_contour_rect.height/2);
//        d_obs.x = p_origin.x - obstacle_contour_rect_center.x;
//        d_obs.y = p_origin.y - obstacle_contour_rect_center.y;

//        d_obs_norm_.x = static_cast<float>(d_obs.x)/(local_costmap_info_.image_width_/2.0);
//        d_obs_norm_.y = static_cast<float>(d_obs.y)/(local_costmap_info_.image_height_/2.0);

//        arrowedLine(I_obstacle_vector_rgb, p_origin, obstacle_contour_rect_center, cv::Scalar(0,255,0), 2, 8, 0, 0.3);



        //Compute the closest Point of the obstacle contour to the UAV
        int ind_nearest_obs = 0;
        float min_ditance_to_uav = 9999;
        for(int i=0;i<contours[0].size();i++)
        {
            cv::Point d_obs_near;
            d_obs_near.x = contours[0][i].x - p_origin.x;
            d_obs_near.y = contours[0][i].y - p_origin.y;
            float distance_to_uav = std::pow(d_obs_near.x, 2) + std::pow(d_obs_near.y, 2);
            if(distance_to_uav < min_ditance_to_uav)
            {
                min_ditance_to_uav = distance_to_uav;
                ind_nearest_obs = i;
            }
        }
        cv::Point closest_obstacle_point_to_uav = contours[0][ind_nearest_obs];
        d_near_obs.x =  closest_obstacle_point_to_uav.x - p_origin.x;
        d_near_obs.y =  closest_obstacle_point_to_uav.y - p_origin.y;
        d_obs_norm_.x = static_cast<float>(d_near_obs.x)/(local_costmap_info_.image_width_/2.0);
        d_obs_norm_.y = static_cast<float>(d_near_obs.y)/(local_costmap_info_.image_height_/2.0);
        arrowedLine(I_obstacle_vector_rgb, p_origin, closest_obstacle_point_to_uav, cv::Scalar(0,0,255), 2, 8, 0, 0.3);

    }
    //cv::drawContours(I_obstacle_vector_rgb, contours, -1, cv::Scalar(255, 0, 255), 1);


    //Draw distance vector (target-UAV) in color
    //arrowedLine(I_obstacle_vector_rgb, p_origin, p_fin, cv::Scalar(0,0,255), 2, 8, 0, 0.3);


    float d_obs_mod = std::sqrt(std::pow(static_cast<float>(d_obs.x),2) + std::pow(static_cast<float>(d_obs.y),2));
    if(DEBUG_DISTANCE_TO_OBSTACLE_CALCULATION)
    {
        std::cout<<std::endl<<"delta_pos.x: "<<delta_pos.x<<std::endl;
        std::cout<<"delta_pos.y: "<<delta_pos.y<<std::endl;
        std::cout<<"delta_x_proyec: "<<delta_x_proyec<<std::endl;
        std::cout<<"delta_y_proyec: "<<delta_y_proyec<<std::endl;
        std::cout<<"local_costmap_info_.image_width_: "<<local_costmap_info_.image_width_<<std::endl;
        std::cout<<"local_costmap_info_.image_height_: "<<local_costmap_info_.image_height_<<std::endl;
        std::cout<<"x_im: "<<x_im<<std::endl;
        std::cout<<"y_im: "<<y_im<<std::endl;
        std::cout<<"d_obs (x,y): "<<d_obs.x<<" , "<<d_obs.y<<std::endl;
        std::cout<<"d_obs (mod): "<<d_obs_mod<<std::endl<<std::endl;
        std::cout<<"d_obs_norm_ (x,y): "<<d_obs_norm_.x<<" , "<<d_obs_norm_.y<<std::endl;
    }


    if(IMSHOW_COSTMAP_WITH_OBSTACLE_VECTOR)
    {
        cv::imshow("I_obstacle_vector", I_obstacle_vector);
        cv::imshow("I_obstacle_vector_rgb", I_obstacle_vector_rgb);
        cv::imshow("I_AND_costmap_obstacle_vector", I_AND_costmap_obstacle_vector);
        cv::waitKey(1);
    }


}


void RlEnvironmentReactiveNavigation::ResetHectorSlam()
{
//    geometry_msgs::PoseWithCovarianceStamped hector_slam_pose;

//    hector_slam_pose.header.stamp = ros::Time::now();
//    hector_slam_pose.header.frame_id       = "map";
//    hector_slam_pose.pose.pose.position.x    = last_drone_estimated_GMRwrtGFF_pose_.x;
//    hector_slam_pose.pose.pose.position.y    = last_drone_estimated_GMRwrtGFF_pose_.y;
//    hector_slam_pose.pose.pose.position.z    = last_drone_estimated_GMRwrtGFF_pose_.z;

//    hector_slam_reset_pub_.publish(hector_slam_pose);

    std_msgs::String reset_msg;
    reset_msg.data = "reset";

    hector_slam_reset_pub_.publish(reset_msg);
}

void RlEnvironmentReactiveNavigation::DrawResults()
{

}

void RlEnvironmentReactiveNavigation::arrowedLine(cv::Mat& img, cv::Point pt1, cv::Point pt2, const cv::Scalar& color,
           int thickness, int line_type, int shift, double tipLength)
{

    const double tipSize = cv::norm(pt1-pt2)*tipLength; // Factor to normalize the size of the tip depending on the length of the arrow

    cv::line(img, pt1, pt2, color, thickness, line_type, shift);

    const double angle = atan2( (double) pt1.y - pt2.y, (double) pt1.x - pt2.x );

    cv::Point p(cvRound(pt2.x + tipSize * cos(angle + CV_PI / 4)),
        cvRound(pt2.y + tipSize * sin(angle + CV_PI / 4)));
    cv::line(img, p, pt2, color, thickness, line_type, shift);

    p.x = cvRound(pt2.x + tipSize * cos(angle - CV_PI / 4));
    p.y = cvRound(pt2.y + tipSize * sin(angle - CV_PI / 4));
    cv::line(img, p, pt2, color, thickness, line_type, shift);
}



void RlEnvironmentReactiveNavigation::GenerateRandomPositionsMaze(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 5;

    int min_target_area_num = 1;
    int max_target_area_num = 5;

    //First Randomly select one of the 4 possible areas

    std::srand (static_cast <unsigned> (time(0)));

    float lower_lim = 2.5;
    float upper_lim = 5.5;
    float r3 = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
    std::cout<<std::endl<<"++ Random Number ++"<<std::endl<<r3<<std::endl;

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;

    switch(area_uav_num)
    {
        case 1:
            uav_pose.x = 5.0;
            uav_pose.y = 5.5;
            break;
        case 2:
            uav_pose.x = 5.0;
            uav_pose.y = -5.5;
            break;
        case 3:
            uav_pose.x = -4.0;
            uav_pose.y = 0.0;
            break;
        case 4:
            uav_pose.x = 0.5;
            uav_pose.y = -0.5;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            target_pose.x = 5.0;
            target_pose.y = -7.0;
            break;
        case 2:
            target_pose.x = 5.0;
            target_pose.y = 7.0;
            break;
        case 3:
            target_pose.x = 0.0;
            target_pose.y = 0.0;
            break;
        case 4:
            target_pose.x = 5.0;
            target_pose.y = -5.0;
            break;
        default:
            target_pose.x = 5.0;
            target_pose.y = -5.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}

void RlEnvironmentReactiveNavigation::GenerateRandomPositionsMaze3(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 5;

    int min_target_area_num = 1;
    int max_target_area_num = 4;

    //First Randomly select one of the 4 possible areas

    srand(time(NULL));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;

    switch(area_uav_num)
    {
        case 1:
            uav_pose.x = -5.0;
            uav_pose.y = 0.55;
            break;
        case 2:
            uav_pose.x = -3.0;
            uav_pose.y = -0.5;
            break;
        case 3:
            uav_pose.x = 0.0;
            uav_pose.y = 0.2;
            break;
        case 4:
            uav_pose.x = 3.0;
            uav_pose.y = -0.1;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            target_pose.x = 5.7;
            target_pose.y = 3.1;
            break;
        case 2:
            target_pose.x = 5.5;
            target_pose.y = 0.3;
            break;
        case 3:
            target_pose.x = 5.6;
            target_pose.y = -3.1;
            break;
        default:
            target_pose.x = 6.0;
            target_pose.y = 0.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}

void RlEnvironmentReactiveNavigation::GenerateRandomPositionsMaze4(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 5;

    int min_target_area_num = 1;
    int max_target_area_num = 5;

    //First Randomly select one of the 4 possible areas

    srand(time(NULL));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;

    switch(area_uav_num)
    {
        case 1:
            uav_pose.x = -3.5;
            uav_pose.y = 3.0;
            break;
        case 2:
            uav_pose.x = -3.75;
            uav_pose.y = 2.9;
            break;
        case 3:
            uav_pose.x = -3.8;
            uav_pose.y = -2;
            break;
        case 4:
            uav_pose.x = -0.5;
            uav_pose.y = -0.5;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            target_pose.x = 3.75;
            target_pose.y = 3.0;
            break;
        case 2:
            target_pose.x = 3.55;
            target_pose.y = 2.9;
            break;
        case 3:
            target_pose.x = 0.0;
            target_pose.y = 2.8;
            break;
        case 4:
            target_pose.x = 3.55;
            target_pose.y = -2.5;
            break;
        default:
            target_pose.x = 6.0;
            target_pose.y = 0.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}


void RlEnvironmentReactiveNavigation::GenerateRandomPositionsHouse(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 7;

    int min_target_area_num = 1;
    int max_target_area_num = 6;

    //First Randomly select one of the 8 possible areas
    std::srand (static_cast <unsigned> (time(0)));


    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;

    target_pose.x = 0.0;
    target_pose.y = 0.0;

    float lower_lim = 0.0;
    float upper_lim = 0.0;
    float rand_pos = 0.0;
    switch(area_uav_num)
    {
        case 1:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = -5.55;
            uav_pose.y = rand_pos;
            break;
        case 2:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = rand_pos;
            uav_pose.y = 5.5;
            break;
        case 3:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = 5.6;
            uav_pose.y = rand_pos;
            break;
        case 4:
            lower_lim = -4.5;
            upper_lim = 4.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = rand_pos;
            uav_pose.y = -5.45;
            break;
        case 5:
            lower_lim = -1.5;
            upper_lim = 1.5;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = 0.4;
            uav_pose.y = rand_pos;
            break;
        case 6:
            lower_lim = -1.0;
            upper_lim = 1.0;
            rand_pos = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim-lower_lim)));
            uav_pose.x = rand_pos;
            uav_pose.y = 0.0;
            break;
        default:
            uav_pose.x = 0.5;
            uav_pose.y = 0.5;
            break;
    }


    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}


void RlEnvironmentReactiveNavigation::GenerateRandomPositionsHouse2(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 6;

    int min_target_area_num = 1;
    int max_target_area_num = 9;

    //First Randomly select one of the 8 possible areas
    srand(time(NULL));

    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    int area_target_num = min_target_area_num + rand() % (max_target_area_num - min_target_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;
    std::cout<<"Random (TARGET) area number generated: "<<area_target_num<<std::endl;


    switch(area_uav_num)
    {
        case 1:
            uav_pose.x = -0.5;
            uav_pose.y = 0.0;
            break;
        case 2:
            uav_pose.x = 0.0;
            uav_pose.y = 4.0;
            break;
        case 3:
            uav_pose.x = 4.0;
            uav_pose.y = 0.5;
            break;
        case 4:
            uav_pose.x = -3.5;
            uav_pose.y = -3.0;
            break;
        case 5:
            uav_pose.x = -3.5;
            uav_pose.y = 1.0;
            break;
        default:
            uav_pose.x = 0.0;
            uav_pose.y = 0.0;
            break;
    }

    switch(area_target_num)
    {
        case 1:
            target_pose.x = -1.5;
            target_pose.y = 3.5;
            break;
        case 2:
            target_pose.x = 3.5;
            target_pose.y = 3.5;
            break;
        case 3:
            target_pose.x = 3.5;
            target_pose.y = -0.5;
            break;
        case 4:
            target_pose.x = 3.0;
            target_pose.y = -4.0;
            break;
        case 5:
            target_pose.x = 0.5;
            target_pose.y = -3.5;
            break;
        case 6:
            target_pose.x = -3.5;
            target_pose.y = -3.0;
            break;
        case 7:
            target_pose.x = -4.0;
            target_pose.y = 3.5;
            break;
        case 8:
            target_pose.x = 0.5;
            target_pose.y = 0.5;
            break;
        default:
            target_pose.x = 5.0;
            target_pose.y = -5.0;
            break;
    }
    area_uav_num_ = area_uav_num;
    area_target_num_ = area_target_num;
}

void RlEnvironmentReactiveNavigation::GenerateRandomPositionsHouse3(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{
    int min_uav_area_num = 1;
    int max_uav_area_num = 7;


    //First Randomly select one of the 8 possible areas
    std::srand (static_cast <unsigned> (time(0)));


    int area_uav_num = min_uav_area_num + rand() % (max_uav_area_num - min_uav_area_num);
    std::cout<<"Random (UAV) area number generated: "<<area_uav_num<<std::endl;

    target_pose.x = 0.0;
    target_pose.y = 0.0;

    float lower_lim_x = 0.0;
    float upper_lim_x = 0.0;
    float lower_lim_y = 0.0;
    float upper_lim_y = 0.0;
    float rand_pos_x = 0.0;
    float rand_pos_y = 0.0;
    switch(area_uav_num)
    {
        case 1:
            lower_lim_y = -2.5;
            upper_lim_y = 2.5;
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            uav_pose.x = -2.55;
            uav_pose.y = rand_pos_y;
            break;
        case 2:
            lower_lim_x = -2.5;
            upper_lim_x = 2.5;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            uav_pose.x = rand_pos_x;
            uav_pose.y = 2.75;
            break;
        case 3:
            lower_lim_y = -2.5;
            upper_lim_y = 2.5;
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));;
            uav_pose.x = 2.8;
            uav_pose.y = rand_pos_y;
            break;
        case 4:
            lower_lim_x = -2.5;
            upper_lim_x = 2.5;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            uav_pose.x = rand_pos_x;
            uav_pose.y = -3.0;
            break;
        case 5:
            lower_lim_x = -1.0;
            upper_lim_x = 1.0;
            lower_lim_y = 0.4;
            upper_lim_y = -1.25;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            uav_pose.x = rand_pos_x;
            uav_pose.y = rand_pos_y;
            break;
        case 6:
            lower_lim_x = -1.0;
            upper_lim_x = 1.0;
            lower_lim_y = 0.4;
            upper_lim_y = -1.25;
            rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
            uav_pose.x = rand_pos_x;
            uav_pose.y = rand_pos_y;
            break;
        default:
            uav_pose.x = 0.5;
            uav_pose.y = 0.5;
            break;
    }


    area_uav_num_ = area_uav_num;
    area_target_num_ = 1;
}







