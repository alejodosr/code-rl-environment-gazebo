#include "rl_environment_laser_based_navigation_with_RPdY.h"

#define DEBUG_MODE_STATE 0
#define DEBUG_UAV_TARGET_POSITON 0
#define DEBUG_SHAPING_REWARD 0
#define DEBUG_LASER_MEASUREMENTS 0
#define DEBUG_POTENTIAL_FIELDS 0

#define STATE_BASED_ON_GROUP_OF_LASER_RANGES 1
#define STATE_BASED_ON_UAV_POS_AND_SPEED 1
#define STATE_BASED_ON_UAV_POS_AND_DIFF_PREVIOUS_POSE 0
#define COMPUTE_REPULSIVE_POTENTIAL_FIELD 1

#define IMSHOW_LASER_STATE_SCANS 1
#define IMSHOW_LASER_STATE_SCANS_AND_GOAL 1
#define IMSHOW_OBSTACLES_BOUNDARY 1
#define IMSHOW_ALL_SATURATED_RANGES 0


#define TEST_MODE 0



RlEnvironmentLaserBasedNavigationWithRPdY::RlEnvironmentLaserBasedNavigationWithRPdY()
{
    if(TEST_MODE)
        environment_info_.name_ = "house_3";
    else
        environment_info_.name_ = "house_6";
    environment_info_.num_episode_steps_ = 500;
    environment_info_.num_iterations_ = 4;
    environment_info_.actions_dim_ = 3;
    environment_info_.actions_min_value_ = {-0.5, -0.5, -0.5};
    environment_info_.actions_max_value_ = {0.5, 0.5, 0.5};
    if(environment_info_.name_ == "house")
    {
        environment_info_.max_pos_x_ = 15.0;
        environment_info_.max_pos_y_ = 15.0;
    }
    else if(environment_info_.name_ == "house_2")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "house_3")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_3b")
    {
        environment_info_.max_pos_x_ = 6.0;
        environment_info_.max_pos_y_ = 6.0;
    }
    else if(environment_info_.name_ == "house_4")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_5")
    {
        environment_info_.max_pos_x_ = 5.0;
        environment_info_.max_pos_y_ = 5.0;
    }
    else if(environment_info_.name_ == "house_6")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "maze")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 18.0;
    }
    else if(environment_info_.name_ == "maze_3")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 8.5;
    }
    else if(environment_info_.name_ == "maze_4")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }


    laser_info_.max_virtual_range_ = 2.0; //Maximum range for defining the laser components in the STATE.
    laser_info_.max_real_range_ = 30.0;
    laser_info_.min_real_range_ = 0.10;
    laser_info_.num_ranges_ = 720;
    laser_info_.angle_range_ = 270;
    laser_info_.sampling_factor_ = 72; //Sampling every 27 degrees
    //laser_info_.sampling_factor_ = 48; //Sampling every 18 degrees
    laser_info_.angle_sampling_factor_ = laser_info_.angle_range_ / (laser_info_.num_ranges_/laser_info_.sampling_factor_);
    laser_info_.min_range_reset_value_ = 0.4;



    laser_image_info_.angles_ranges_.clear();
    laser_image_info_.laser_scans_image_size_ = cv::Size(200, 200);
    laser_image_info_.angle_ini_ = -45.0;
    laser_image_info_.angle_ini_rad_ = laser_image_info_.angle_ini_*M_PI/180;
    laser_image_info_.angle_increment_ = laser_info_.angle_range_ / laser_info_.num_ranges_;
    laser_image_info_.p_origin_ = cv::Point(laser_image_info_.laser_scans_image_size_.width/2.0,
                                            laser_image_info_.laser_scans_image_size_.height/2.0);
    for(int i=0;i<laser_info_.num_ranges_;i++)
    {
        float angle_i = (laser_image_info_.angle_ini_ + i*laser_image_info_.angle_increment_) * M_PI/180.0;
        float cos_angle_i = std::cos(angle_i);
        float sin_angle_i = std::sin(angle_i);
        laser_image_info_.angles_ranges_.push_back(angle_i);
        laser_image_info_.cos_angles_ranges_.push_back(cos_angle_i);
        laser_image_info_.sin_angles_ranges_.push_back(sin_angle_i);
    }

    laser_image_info_.laser_scans_image_res_ = laser_info_.max_virtual_range_ / (laser_image_info_.laser_scans_image_size_.width/2.0);
    laser_image_info_.laser_scans_image_ = cv::Mat(laser_image_info_.laser_scans_image_size_.height,
                                                  laser_image_info_.laser_scans_image_size_.width,
                                                  CV_8U, cv::Scalar(255));
    laser_image_info_.obstacles_boundary_image_ = laser_image_info_.laser_scans_image_.clone();



    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
    {
        std::cout<<"STATE_BASED_ON_GROUP_OF_LASER_RANGES"<<std::endl;
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_);
    }
    else
    {
        std::cout<<"STATE BASED ON SAMPLED LASER_RANGES"<<std::endl;
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_) + 1;
    }


    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0, 0.0);
    uav_target_current_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);
    uav_target_previous_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);


    min_distance_from_laser_flag_ = false;
    min_distance_target_reached_flag_ = false;
    min_distance_to_target_thresh_ = 0.1;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;
    std::cout<<"pitch_roll_max_value_ : "<<pitch_roll_max_value_<<std::endl;

}


RlEnvironmentLaserBasedNavigationWithRPdY::~RlEnvironmentLaserBasedNavigationWithRPdY()
{
    f_data_recorder.close();
}

void RlEnvironmentLaserBasedNavigationWithRPdY::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: LASER-BASED REACTIVE NAVIGATION WITH ROLL-PITCH-YAW Environment" << std::endl;

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;

    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentLaserBasedNavigationWithRPdY::PoseVelocityCallback, this);


    ros::param::get("~esimtated_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_estimated_pose_subs_ = n.subscribe(param_string, 1, &RlEnvironmentLaserBasedNavigationWithRPdY::DroneEstimatedPoseCallback, this);



    ros::param::get("~laser_scan", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_laser_scan_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
                                         &RlEnvironmentLaserBasedNavigationWithRPdY::LaserScanCallback, this);




    // Init publishers
    ros::param::get("~roll_pitch", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    roll_pitch_ref_pub_ = n.advertise<droneMsgsROS::dronePitchRollCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~dYaw", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    dYaw_ref_pub_ = n.advertise<droneMsgsROS::droneDYawCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);


    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);



    // Init services
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_set_model_state_srv_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~set_link_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_set_link_state_srv_ = n.serviceClient<gazebo_msgs::SetLinkState>(param_string);


    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);


    // Set number of iterations of Gazebo
    if (ENABLE_PAUSED_SIMULATION)
    {
        this->data_->num_iterations = (unsigned int) environment_info_.num_iterations_;

        // Print
        std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;
    }

    // Init recording experiment
//    exp_rec_.Open(n);

    // Reset environment
    Reset();
    //f_data_recorder.open("/home/carlos/pruebas_Matlab/speeds_pos_in_image_with_ground_truth3.txt");
}

bool RlEnvironmentLaserBasedNavigationWithRPdY::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{
    // Send action
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    roll_pitch_msg.rollCmd = request.action[0];  // Roll
    roll_pitch_msg.pitchCmd = request.action[1];  // Pitch
    roll_pitch_ref_pub_.publish(roll_pitch_msg);

    droneMsgsROS::droneDYawCmd dyaw_msg;
//    dyaw_msg.dYawCmd = 0; // No dYaw
    dyaw_msg.dYawCmd = request.action[2];
    dYaw_ref_pub_.publish(dyaw_msg);


    std::vector<float> current_state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        current_state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        current_state = ComputeNormalizedState();


    if(DEBUG_MODE_STATE)
    {
        std::cout<<"***** CURRENT STATE *****"<<std::endl;
        std::cout<<"Size: "<<current_state.size()<<std::endl;
        for(int i=0; i<current_state.size();i++)
        {
            if(i==0)
                std::cout<<"[";
            if(i==current_state.size()-1)
                std::cout<<current_state[i]<<"]";
            else
                std::cout<<current_state[i]<<" , ";
        }
        std::cout<<std::endl;
    }

    // Print info
//    std::cout << "RL_ENV_DEBUG: UAV pose x " << x << " y " << y << std::endl;

    float position_attr_reward_term = 0.0f;
    float position_repul_reward_term = 0.0f;
    float velocity_reward_term = 0.0f;
    float action_reward_term = 0.0f;
    // Compute reward
//    float shaping = - 100 * std::sqrt(pow(current_state[0], 2) + pow(current_state[1], 2))
//              - 10 * std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2)) -
//            std::sqrt(std::pow(request.action[0]/environment_info_.actions_max_value_[0], 2) + std::pow(request.action[1]/environment_info_.actions_max_value_[1], 2));



    position_attr_reward_term = - 100 * std::sqrt(std::pow(current_state[0], 2) + std::pow(current_state[1], 2));

    if(COMPUTE_REPULSIVE_POTENTIAL_FIELD)
    {
        std::vector<float> vector_of_norm_laser_measurements;
        vector_of_norm_laser_measurements.insert(vector_of_norm_laser_measurements.end(), current_state.begin() + 4, current_state.end());
        //position_repul_reward_term = ComputeRepulsivePotentialField(vector_of_norm_laser_measurements);
        position_repul_reward_term = ComputeRepulsivePotentialFieldBasedOnImage();

        if(DEBUG_POTENTIAL_FIELDS)
        {
            std::cout<<"Attraction Potential Field: "<<position_attr_reward_term<<std::endl;
            std::cout<<"Repulsion Potential Field: "<<position_repul_reward_term<<std::endl;
        }
    }



    //velocity_reward_term = - 10 * std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2));
    //float shaping = position_attr_reward_term + velocity_reward_term;
    float shaping = 0.0;
    if(COMPUTE_REPULSIVE_POTENTIAL_FIELD)
        shaping = position_attr_reward_term + position_repul_reward_term;
    else
        shaping = position_attr_reward_term;

    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;


    if(min_distance_target_reached_flag_)
    {
        float position_attr_reward_term_exp = std::sqrt(std::pow(current_state[0], 2) + std::pow(current_state[1], 2));
        float velocity_reward_term_exp = std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2));
        float action_reward_term_exp = std::sqrt(std::pow(request.action[0]/environment_info_.actions_max_value_[0], 2) +
                std::pow(request.action[1]/environment_info_.actions_max_value_[1], 2));

        float exponential_reward = 10.0/std::exp(2*(position_attr_reward_term_exp + velocity_reward_term_exp));
        response.reward = reward + exponential_reward;
        response.terminal_state = true;
        reset_env_ = true;
        min_distance_target_reached_flag_ = false;

//        response.reward = 105;
//        response.terminal_state = true;
//        reset_env_ = true;
//        min_distance_target_reached_flag_ = false;
    }
    else if (min_distance_from_laser_flag_)
    {
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
        min_distance_from_laser_flag_ = false;
    }
    else
        response.terminal_state = false;

    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();


    if(DEBUG_SHAPING_REWARD)
    {
        std::cout<<std::endl<<"****** SHPAING - REWARD ******"<<std::endl;
        std::cout<<"Shaping: "<<shaping<<std::endl;
        std::cout<<"Reward: "<<response.reward<<std::endl<<std::endl;
    }


    // Read next state
    std::vector<float> next_state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        next_state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        next_state = ComputeNormalizedState();
    response.obs_real = next_state;
    UpdateUavTargetPreviousRelativePosition();


    // Successful return
    return true;
}


bool RlEnvironmentLaserBasedNavigationWithRPdY::Reset()
{
    // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav;
    float roll, pitch, yaw;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav, roll, pitch, yaw);

    // Send null action
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    roll_pitch_msg.rollCmd = 0;  // Roll
    roll_pitch_msg.pitchCmd = 0;  // Pitch
    roll_pitch_ref_pub_.publish(roll_pitch_msg);

    droneMsgsROS::droneDYawCmd dyaw_msg;
    dyaw_msg.dYawCmd = 0; // No dYaw
    dYaw_ref_pub_.publish(dyaw_msg);


    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = kUav_Altitude_;
    uav_pose_ref_pub_.publish(altitude_msg);


    // Spawn model to RANDOM locations (UAV and Target)
    cv::Point2f uav_rand_pos, target_rand_pos;
    if(environment_info_.name_ == "house")
        GenerateRandomPositionsHouse(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_2")
        GenerateRandomPositionsHouse2(uav_rand_pos, target_rand_pos);
    else if((environment_info_.name_ == "house_3") || (environment_info_.name_ == "house_3b"))
        GenerateRandomPositionsHouse3(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_4")
        GenerateRandomPositionsHouse4(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_5")
        GenerateRandomPositionsHouse5(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_6")
        GenerateRandomPositionsHouse6(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze")
        GenerateRandomPositionsMaze(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze_3")
        GenerateRandomPositionsMaze3(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "maze_4")
        GenerateRandomPositionsMaze4(uav_rand_pos, target_rand_pos);

    gazebo_msgs::SetModelState model_msg_uav;
    model_msg_uav.request.model_state.model_name = UAV_NAME;
    model_msg_uav.request.model_state.pose.position.x = uav_rand_pos.x;
    model_msg_uav.request.model_state.pose.position.y = uav_rand_pos.y;
    model_msg_uav.request.model_state.pose.position.z = kUav_Altitude_;
    if (gazebo_set_model_state_srv_.call(model_msg_uav))
    {

    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    gazebo_msgs::SetModelState model_msg_target;
    model_msg_target.request.model_state.model_name = TARGET_NAME;
    model_msg_target.request.model_state.pose.position.x = target_rand_pos.x;
    model_msg_target.request.model_state.pose.position.y = target_rand_pos.y;
    model_msg_target.request.model_state.pose.position.z = 0.0;
    if (gazebo_set_model_state_srv_.call(model_msg_target))
    {

    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }


    if((environment_info_.name_ == "house_3") || (environment_info_.name_ == "house_3b"))
    {
        float min_distance_to_obstacle = 1.8;
        cv::Point3f uav_target_relative_position;
        uav_target_relative_position.x = target_rand_pos.x - uav_rand_pos.x;
        uav_target_relative_position.y = target_rand_pos.y - uav_rand_pos.y;
        float distance_uav_to_target = std::sqrt(std::pow(uav_target_relative_position.x, 2) + std::pow(uav_target_relative_position.y, 2));

        cv::Point3f obstacle_cylinder_pos;
        if(distance_uav_to_target > min_distance_to_obstacle)
        {
            obstacle_cylinder_pos.x = uav_rand_pos.x + uav_target_relative_position.x/2.0;
            obstacle_cylinder_pos.y = uav_rand_pos.y + uav_target_relative_position.y/2.0;
            obstacle_cylinder_pos.z = 0.0;
        }
        else
        {
            float distance_uav_to_obs = 0.0;
            float distance_target_to_obs = 0.0;
            float max_pos = (environment_info_.max_pos_x_ / 2.0) - 1;
            float lower_lim = -max_pos;
            float upper_lim = max_pos;
            do
            {
                float rand_pos_x = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim - lower_lim)));
                float rand_pos_y = lower_lim + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim - lower_lim)));

                obstacle_cylinder_pos.x = rand_pos_x;
                obstacle_cylinder_pos.y = rand_pos_y;
                obstacle_cylinder_pos.z = 0.0;

                cv::Point3f uav_obs_relative_position;
                uav_obs_relative_position.x = obstacle_cylinder_pos.x - uav_rand_pos.x;
                uav_obs_relative_position.y = obstacle_cylinder_pos.y - uav_rand_pos.y;
                distance_uav_to_obs = std::sqrt(std::pow(uav_obs_relative_position.x, 2) +
                                                      std::pow(uav_obs_relative_position.y, 2));

                cv::Point3f target_obs_relative_position;
                target_obs_relative_position.x = obstacle_cylinder_pos.x - target_rand_pos.x;
                target_obs_relative_position.y = obstacle_cylinder_pos.y - target_rand_pos.y;
                distance_uav_to_obs = std::sqrt(std::pow(target_obs_relative_position.x, 2) +
                                                      std::pow(target_obs_relative_position.y, 2));
            }while((distance_uav_to_obs < min_distance_to_obstacle) && (distance_target_to_obs < min_distance_to_obstacle));
        }
        gazebo_msgs::SetModelState model_msg_obstacle_cylinder;
        model_msg_obstacle_cylinder.request.model_state.model_name = "obstacle_cylinder_big";
        model_msg_obstacle_cylinder.request.model_state.pose.position.x = obstacle_cylinder_pos.x;
        model_msg_obstacle_cylinder.request.model_state.pose.position.y = obstacle_cylinder_pos.y;
        model_msg_obstacle_cylinder.request.model_state.pose.position.z = obstacle_cylinder_pos.z;
        if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder))
        {

        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
    //        return false;
        }
    }



    if(environment_info_.name_ == "house_4")
    {
        int num_obstacles = 2;
        std::vector<cv::Point2f> obstacles_position = GenerateRandomPositionForObstacles(num_obstacles);
        std::cout<<"obstacles_position.size(): "<<obstacles_position.size()<<std::endl;
        gazebo_msgs::SetLinkState link_obstacles_msg;
        std::string link_0_name = "house_4::link_0";
        link_obstacles_msg.request.link_state.link_name = link_0_name;
        link_obstacles_msg.request.link_state.pose.position.x = obstacles_position[0].x;
        link_obstacles_msg.request.link_state.pose.position.y = obstacles_position[0].y;
        if (gazebo_set_link_state_srv_.call(link_obstacles_msg))
        {

        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set link state");
    //        return false;
        }

        std::string link_1_name = "house_4::link_1";
        link_obstacles_msg.request.link_state.link_name = link_1_name;
        link_obstacles_msg.request.link_state.pose.position.x = obstacles_position[1].x;
        link_obstacles_msg.request.link_state.pose.position.y = obstacles_position[1].y;
        if (gazebo_set_link_state_srv_.call(link_obstacles_msg))
        {

        }
        else{
            ROS_ERROR("RL_ENV_INFO: Failed to call set link state");
    //        return false;
        }
    }


    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting UAV pos at x: " << uav_rand_pos.x << " and y: " << uav_rand_pos.y << std::endl;
    std::cout << "RL_ENV_INFO: reseting TARGET pos at x: " << target_rand_pos.x << " and y: " << target_rand_pos.y << std::endl;
    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg))
    {
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        return false;
    }

}

void RlEnvironmentLaserBasedNavigationWithRPdY::LaserScanCallback(const sensor_msgs::LaserScan &msg)
{
    laser_info_.laser_ranges_.clear();
    laser_info_.laser_ranges_.insert(laser_info_.laser_ranges_.end(), msg.ranges.begin(), msg.ranges.end());

}



void RlEnvironmentLaserBasedNavigationWithRPdY::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    // Get position of the uav in the vector
    cv::Point3f target_position;
    cv::Point3f uav_position, uav_linear_vel;
    cv::Point3f uav_previous_pos, uav_previous_linear_vel;
    uav_previous_pos.x = uav_state_.pos_x_;uav_previous_pos.y = uav_state_.pos_y_;uav_previous_pos.z = uav_state_.pos_z_;
    uav_previous_linear_vel.x = uav_state_.speed_x_;uav_previous_linear_vel.y = uav_state_.speed_y_;uav_previous_linear_vel.z = uav_state_.speed_z_;

    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            uav_position.x = msg->pose[i].position.x;
            uav_position.y = msg->pose[i].position.y;
            uav_position.z = msg->pose[i].position.z;

            uav_linear_vel.x = msg->twist[i].linear.x;
            uav_linear_vel.y = msg->twist[i].linear.y;
            uav_linear_vel.z = msg->twist[i].linear.z;
            break;
        }

        else if(msg->name[i].compare(TARGET_NAME) == 0)
        {
            target_position.x = msg->pose[i].position.x;
            target_position.y = msg->pose[i].position.y;
            target_position.z = msg->pose[i].position.z;
        }
    }

    if(DEBUG_UAV_TARGET_POSITON)
    {
        std::cout<<"UAV position: "<<"x: "<<uav_position.x<<" ; "<<"y: "<<uav_position.y<<" ; "<<"area_num: "<<area_uav_num_<<std::endl;
        std::cout<<"TARGET position: "<<"x: "<<target_position.x<<" ; "<<"y: "<<target_position.y<<" ; "<<"area_num: "<<area_target_num_<<std::endl;
    }


    // Set uav state
    SetUavState(uav_position.x, uav_position.y, uav_position.z, uav_linear_vel.x, uav_linear_vel.y, uav_linear_vel.z);
    SetUavPreviousState(uav_previous_pos.x, uav_previous_pos.y, uav_previous_pos.z,
                        uav_previous_linear_vel.x, uav_previous_linear_vel.y, uav_previous_linear_vel.z);
    SetTargetState(target_position.x, target_position.y, target_position.z);


}

void RlEnvironmentLaserBasedNavigationWithRPdY::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose_  = msg;
    //yaw pitch roll
//    uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
//    uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
//    uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

//    uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
//    uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
//    uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;

    return;
}

