#include "rl_environment_laser_based_navigation_test.h"

#define DEBUG_MODE_STATE 0
#define DEBUG_UAV_TARGET_POSITON 0
#define DEBUG_SHAPING_REWARD 0
#define DEBUG_LASER_MEASUREMENTS 0
#define DEBUG_POTENTIAL_FIELDS 0

#define STATE_BASED_ON_GROUP_OF_LASER_RANGES 1
#define STATE_BASED_ON_UAV_POS_AND_SPEED 1
#define STATE_BASED_ON_UAV_POS_AND_DIFF_PREVIOUS_POSE 0
#define COMPUTE_REPULSIVE_POTENTIAL_FIELD 1

#define IMSHOW_LASER_STATE_SCANS 1
#define IMSHOW_LASER_STATE_SCANS_AND_GOAL 1
#define IMSHOW_OBSTACLES_BOUNDARY 1
#define IMSHOW_ALL_SATURATED_RANGES 0


#define TEST_MODE 1



RlEnvironmentLaserBasedNavigationTest::RlEnvironmentLaserBasedNavigationTest()
{
    if(TEST_MODE) //Use house_3 for a 8x8 house or house_3c for a 5x8 house
        environment_info_.name_ = "house_3c";
    else
        environment_info_.name_ = "house_6";
    environment_info_.num_episode_steps_ = 500;
    environment_info_.num_iterations_ = 4;
    environment_info_.actions_dim_ = 2;
//    environment_info_.actions_min_value_ = {-0.5, -0.5};
//    environment_info_.actions_max_value_ = {0.5, 0.5};
//    environment_info_.actions_min_value_ = {-1.0, -1.0};
//    environment_info_.actions_max_value_ = {1.0, 1.0};
    environment_info_.actions_min_value_ = {-0.7, -0.7};
    environment_info_.actions_max_value_ = {0.7, 0.7};
    if(environment_info_.name_ == "house")
    {
        environment_info_.max_pos_x_ = 15.0;
        environment_info_.max_pos_y_ = 15.0;
    }
    else if(environment_info_.name_ == "house_2")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "house_3")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_3b")
    {
        environment_info_.max_pos_x_ = 6.0;
        environment_info_.max_pos_y_ = 6.0;
    }
    else if(environment_info_.name_ == "house_3c")
    {
        if(TEST_MODE)
        {
            //In this case, despite the size of the env is 4x8 m
            //we use the same normalization factor as TRAIN ENV (house 3)
            environment_info_.max_pos_x_ = 8.0;
            environment_info_.max_pos_y_ = 8.0;
        }
        else
        {
            environment_info_.max_pos_x_ = 5.0;
            environment_info_.max_pos_y_ = 8.0;
        }
    }
    else if(environment_info_.name_ == "house_4")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_5")
    {
        environment_info_.max_pos_x_ = 5.0;
        environment_info_.max_pos_y_ = 5.0;
    }
    else if(environment_info_.name_ == "house_6")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "maze")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 18.0;
    }
    else if(environment_info_.name_ == "maze_3")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 8.5;
    }
    else if(environment_info_.name_ == "maze_4")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }


    laser_info_.max_virtual_range_ = 2.0; //Maximum range for defining the laser components in the STATE.
    laser_info_.max_real_range_ = 30.0;
    laser_info_.min_real_range_ = 0.10;
    laser_info_.num_ranges_ = 720;
    laser_info_.angle_range_ = 270;
    laser_info_.sampling_factor_ = 72; //Sampling every 27 degrees
    //laser_info_.sampling_factor_ = 90; //Sampling every 18 degrees
    laser_info_.angle_sampling_factor_ = laser_info_.angle_range_ / (laser_info_.num_ranges_/laser_info_.sampling_factor_);

    if(TEST_MODE)
        laser_info_.min_range_reset_value_ = 0.3;
    else
        laser_info_.min_range_reset_value_ = 0.4;
    laser_info_.laser_state_normalization_factor_ = laser_info_.max_virtual_range_ - laser_info_.min_range_reset_value_;



    laser_image_info_.angles_ranges_.clear();
    laser_image_info_.laser_scans_image_size_ = cv::Size(200, 200);
    laser_image_info_.angle_ini_ = -45.0;
    laser_image_info_.angle_ini_rad_ = laser_image_info_.angle_ini_*M_PI/180;
    laser_image_info_.angle_increment_ = laser_info_.angle_range_ / laser_info_.num_ranges_;
    laser_image_info_.p_origin_ = cv::Point(laser_image_info_.laser_scans_image_size_.width/2.0,
                                            laser_image_info_.laser_scans_image_size_.height/2.0);
    for(int i=0;i<laser_info_.num_ranges_;i++)
    {
        float angle_i = (laser_image_info_.angle_ini_ + i*laser_image_info_.angle_increment_) * M_PI/180.0;
        float cos_angle_i = std::cos(angle_i);
        float sin_angle_i = std::sin(angle_i);
        laser_image_info_.angles_ranges_.push_back(angle_i);
        laser_image_info_.cos_angles_ranges_.push_back(cos_angle_i);
        laser_image_info_.sin_angles_ranges_.push_back(sin_angle_i);
    }

    laser_image_info_.laser_scans_image_res_ = laser_info_.max_virtual_range_ / (laser_image_info_.laser_scans_image_size_.width/2.0);
    laser_image_info_.laser_scans_image_ = cv::Mat(laser_image_info_.laser_scans_image_size_.height,
                                                  laser_image_info_.laser_scans_image_size_.width,
                                                  CV_8U, cv::Scalar(255));
    laser_image_info_.obstacles_boundary_image_ = laser_image_info_.laser_scans_image_.clone();



    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_);
    else
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_) + 1;


    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0, 0.0);
    uav_target_current_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);
    uav_target_previous_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);


    min_distance_from_laser_flag_ = false;
    min_distance_target_reached_flag_ = false;
    if(TEST_MODE)
        min_distance_to_target_thresh_ = 0.2;
    else
        min_distance_to_target_thresh_ = 0.1;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;
    std::cout<<"pitch_roll_max_value_ : "<<pitch_roll_max_value_<<std::endl;

    uav_initial_reset_position_ = cv::Point3f(0.0, 0.0, 0.0);
    slam_out_uav_position_ = cv::Point3f(0.0, 0.0, 0.0);


    // Init circular buffer
    filtered_derivative_wcb_x_.setTimeParameters( 0.005, 0.005, 0.200, 1.0, 40.0);
    filtered_derivative_wcb_y_.setTimeParameters( 0.005, 0.005, 0.200, 1.0, 40.0);
    filtered_derivative_wcb_x_.reset();
    filtered_derivative_wcb_y_.reset();
}


RlEnvironmentLaserBasedNavigationTest::~RlEnvironmentLaserBasedNavigationTest()
{
    f_data_recorder.close();
}

void RlEnvironmentLaserBasedNavigationTest::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: LASER-BASED REACTIVE NAVIGATION TEST Environment" << std::endl;

//    int drone_namespace = -1;
//    ros::param::get("~droneId", drone_namespace);
//    if(drone_namespace == -1)
//    {
//        ROS_ERROR("FATAL: Namespace not found");
//        return;
//    }

//    // Init subscribers
//    std::string param_string;

//    ros::param::get("~model_states", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentLaserBasedNavigationTest::PoseVelocityCallback, this);


//    ros::param::get("~esimtated_pose", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    drone_estimated_pose_subs_ = n.subscribe(param_string, 1, &RlEnvironmentLaserBasedNavigationTest::DroneEstimatedPoseCallback, this);



//    ros::param::get("~laser_scan", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    drone_laser_scan_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
//                                         &RlEnvironmentLaserBasedNavigationTest::LaserScanCallback, this);

//    ros::param::get("~slam_out_pose", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    drone_slam_out_pose_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
//                                         &RlEnvironmentLaserBasedNavigationTest::SlamOutPoseCallback, this);




//    // Init publishers
//    ros::param::get("~speed_refs", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

//    ros::param::get("~position_refs", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);


//    ros::param::get("~hector_mapping_reset", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    hector_mapping_reset_pub_ = n.advertise<std_msgs::String>(param_string, 1000);

//    ros::param::get("~env_state", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    rl_environment_state_pub_ = n.advertise<std_msgs::Float32MultiArray>(param_string, 1, true);





//    // Init services
//    ros::param::get("~set_model_state", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    gazebo_set_model_state_srv_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

//    ros::param::get("~set_link_state", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    gazebo_set_link_state_srv_ = n.serviceClient<gazebo_msgs::SetLinkState>(param_string);


//    ros::param::get("~reset_estimator", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);


    //f_data_recorder.open("/media/carlos/DATA/TesisCVG/PAPERS/MIS_PAPERS/ObjectFollowing_IROS_2018/Experiments/speeds_pos_ground_truth_LowPass_40.txt");
}

bool RlEnvironmentLaserBasedNavigationTest::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{

    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);



    // set up dimensions
//    std_msgs::Float32MultiArray state_msg;
//    state_msg.layout.dim.push_back(std_msgs::MultiArrayDimension());
//    state_msg.layout.dim[0].size = current_state.size();
//    state_msg.layout.dim[0].stride = 1;
//    state_msg.layout.dim[0].label = "x"; // or whatever name you typically use to index vec1
//    state_msg.data.insert(state_msg.data.end(), current_state.begin(), current_state.end());
//    rl_environment_state_pub_.publish(state_msg);


    std::vector<float> current_state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();

    response.obs_real = current_state;
    response.reward = 0;
    response.terminal_state = true;
}


bool RlEnvironmentLaserBasedNavigationTest::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{
    std::vector<float> state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        state = ComputeNormalizedState();
    response.state = state;

    return true;
}



bool RlEnvironmentLaserBasedNavigationTest::Reset()
{

}

//void RlEnvironmentLaserBasedNavigationTest::LaserScanCallback(const sensor_msgs::LaserScan &msg)
//{
//    laser_info_.laser_ranges_.clear();
//    laser_info_.laser_ranges_.insert(laser_info_.laser_ranges_.end(), msg.ranges.begin(), msg.ranges.end());

//}



//void RlEnvironmentLaserBasedNavigationTest::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
//{
//    // Get position of the uav in the vector
//    cv::Point3f target_position;
//    cv::Point3f uav_position, uav_linear_vel;
//    cv::Point3f uav_previous_pos, uav_previous_linear_vel;
//    uav_previous_pos.x = uav_state_.pos_x_;uav_previous_pos.y = uav_state_.pos_y_;uav_previous_pos.z = uav_state_.pos_z_;
//    uav_previous_linear_vel.x = uav_state_.speed_x_;uav_previous_linear_vel.y = uav_state_.speed_y_;uav_previous_linear_vel.z = uav_state_.speed_z_;

//    for (int i=0; i<msg->name.size(); i++)
//    {
//        if (msg->name[i].compare(UAV_NAME) == 0)
//        {
//            uav_position.x = msg->pose[i].position.x;
//            uav_position.y = msg->pose[i].position.y;
//            uav_position.z = msg->pose[i].position.z;

//            uav_linear_vel.x = msg->twist[i].linear.x;
//            uav_linear_vel.y = msg->twist[i].linear.y;
//            uav_linear_vel.z = msg->twist[i].linear.z;
//            break;
//        }

//        else if(msg->name[i].compare(TARGET_NAME) == 0)
//        {
//            target_position.x = msg->pose[i].position.x;
//            target_position.y = msg->pose[i].position.y;
//            target_position.z = msg->pose[i].position.z;
//        }
//    }

//    if(TEST_MODE && environment_info_.name_ == "house_3c")
//    {
//        if((uav_position.x > -1.4 && uav_position.x < -1.36) || (uav_position.x > -0.3 && uav_position.x < -0.25))
//        {
//            gazebo_msgs::SetModelState model_msg_obstacle_cylinder;
//            model_msg_obstacle_cylinder.request.model_state.model_name = "moving_obstacle_squared1";
//            model_msg_obstacle_cylinder.request.model_state.pose.position.x = uav_position.x + 1.0;
//            model_msg_obstacle_cylinder.request.model_state.pose.position.y = uav_position.y;
//            model_msg_obstacle_cylinder.request.model_state.pose.position.z = 0.0;
//            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder))
//            {

//            }
//            else{
//                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//            }
//        }
//    }
//    else if(TEST_MODE && environment_info_.name_ == "house_3")
//    {
//        if((uav_position.x > -2.2 && uav_position.x < -2.195))
//        {
//            float lower_lim_y = -0.3;
//            float upper_lim_y = 0.3;
//            float rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

//            gazebo_msgs::SetModelState model_msg_obstacle_cylinder1;
//            model_msg_obstacle_cylinder1.request.model_state.model_name = "moving_obstacle_squared_big1";
//            model_msg_obstacle_cylinder1.request.model_state.pose.position.x = uav_position.x + 1.2;
//            model_msg_obstacle_cylinder1.request.model_state.pose.position.y = uav_position.y + rand_pos_y;
//            model_msg_obstacle_cylinder1.request.model_state.pose.position.z = 0.0;
//            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder1))
//            {

//            }
//            else{
//                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//            }


//            lower_lim_y = -1.0;
//            upper_lim_y = 1.0;
//            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

//            gazebo_msgs::SetModelState model_msg_obstacle_cylinder2;
//            model_msg_obstacle_cylinder2.request.model_state.model_name = "moving_obstacle_squared_big2";
//            model_msg_obstacle_cylinder2.request.model_state.pose.position.x = 0.75;
//            if(rand_pos_y > 0)
//                model_msg_obstacle_cylinder2.request.model_state.pose.position.y = 1.8;
//            else
//                model_msg_obstacle_cylinder2.request.model_state.pose.position.y = -1.8;
//            model_msg_obstacle_cylinder2.request.model_state.pose.position.z = 0.0;
//            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder2))
//            {

//            }
//            else{
//                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//            }
//        }

//    }

//    if(DEBUG_UAV_TARGET_POSITON)
//    {
//        std::cout<<"UAV position: "<<"x: "<<uav_position.x<<" ; "<<"y: "<<uav_position.y<<" ; "<<"area_num: "<<area_uav_num_<<std::endl;
//        std::cout<<"TARGET position: "<<"x: "<<target_position.x<<" ; "<<"y: "<<target_position.y<<" ; "<<"area_num: "<<area_target_num_<<std::endl;
//    }


//    // Set uav state
//    SetUavState(uav_position.x, uav_position.y, uav_position.z, uav_linear_vel.x, uav_linear_vel.y, uav_linear_vel.z);
//    SetUavPreviousState(uav_previous_pos.x, uav_previous_pos.y, uav_previous_pos.z,
//                        uav_previous_linear_vel.x, uav_previous_linear_vel.y, uav_previous_linear_vel.z);
//    SetTargetState(target_position.x, target_position.y, target_position.z);
//}

//void RlEnvironmentLaserBasedNavigationTest::SlamOutPoseCallback(const geometry_msgs::PoseStamped &msg)
//{
//    slam_out_uav_position_.x = msg.pose.position.x + uav_initial_reset_position_.x;
//    slam_out_uav_position_.y = msg.pose.position.y + uav_initial_reset_position_.y;
//    slam_out_uav_position_.z = msg.pose.position.z + uav_initial_reset_position_.z;


//    //std::cout<<"SLAM OUT POSE (msg)"<<std::endl;
//    //std::cout<<"[x,y,z]: "<<"["<<slam_out_uav_position_.x<<" , "<<slam_out_uav_position_.y<<" , "<<slam_out_uav_position_.z<<"]"<<std::endl;
//}

//void RlEnvironmentLaserBasedNavigationTest::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
//{
//    last_drone_estimated_GMRwrtGFF_pose_  = msg;
//    //yaw pitch roll
////    uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
////    uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
////    uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

////    uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
////    uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
////    uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;

//    return;
//}


