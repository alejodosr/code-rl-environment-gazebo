#include "rl_environment_laser_based_navigation_with_planner.h"

#define DEBUG_MODE_STATE 0
#define DEBUG_UAV_TARGET_POSITON 0
#define DEBUG_SHAPING_REWARD 0
#define DEBUG_LASER_MEASUREMENTS 0
#define DEBUG_POTENTIAL_FIELDS 0

#define STATE_BASED_ON_GROUP_OF_LASER_RANGES 1
#define STATE_BASED_ON_UAV_POS_AND_SPEED 1
#define STATE_BASED_ON_UAV_POS_AND_DIFF_PREVIOUS_POSE 0
#define COMPUTE_REPULSIVE_POTENTIAL_FIELD 1

#define IMSHOW_LASER_STATE_SCANS 1
#define IMSHOW_LASER_STATE_SCANS_AND_GOAL 1
#define IMSHOW_OBSTACLES_BOUNDARY 1
#define IMSHOW_ALL_SATURATED_RANGES 0


#define TEST_MODE 1



RlEnvironmentLaserBasedNavigationWithPlanner::RlEnvironmentLaserBasedNavigationWithPlanner()
{
    if(TEST_MODE)
        environment_info_.name_ = "house_3c";
    else
        environment_info_.name_ = "house_6";
    environment_info_.num_episode_steps_ = 500;
    environment_info_.num_iterations_ = 4;
    environment_info_.actions_dim_ = 3;
    environment_info_.actions_min_value_ = {-0.5, -0.5, -0.5};
    environment_info_.actions_max_value_ = {0.5, 0.5, 0.5};
    if(environment_info_.name_ == "house")
    {
        environment_info_.max_pos_x_ = 15.0;
        environment_info_.max_pos_y_ = 15.0;
    }
    else if(environment_info_.name_ == "house_2")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "house_3")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_3b")
    {
        environment_info_.max_pos_x_ = 6.0;
        environment_info_.max_pos_y_ = 6.0;
    }
    else if(environment_info_.name_ == "house_4")
    {
        environment_info_.max_pos_x_ = 8.0;
        environment_info_.max_pos_y_ = 8.0;
    }
    else if(environment_info_.name_ == "house_5")
    {
        environment_info_.max_pos_x_ = 5.0;
        environment_info_.max_pos_y_ = 5.0;
    }
    else if(environment_info_.name_ == "house_6")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }
    else if(environment_info_.name_ == "maze")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 18.0;
    }
    else if(environment_info_.name_ == "maze_3")
    {
        environment_info_.max_pos_x_ = 14.0;
        environment_info_.max_pos_y_ = 8.5;
    }
    else if(environment_info_.name_ == "maze_4")
    {
        environment_info_.max_pos_x_ = 10.0;
        environment_info_.max_pos_y_ = 10.0;
    }


    laser_info_.max_virtual_range_ = 2.0; //Maximum range for defining the laser components in the STATE.
    laser_info_.max_real_range_ = 30.0;
    laser_info_.min_real_range_ = 0.10;
    laser_info_.num_ranges_ = 720;
    laser_info_.angle_range_ = 270;
    laser_info_.sampling_factor_ = 72; //Sampling every 27 degrees
    //laser_info_.sampling_factor_ = 48; //Sampling every 18 degrees
    laser_info_.angle_sampling_factor_ = laser_info_.angle_range_ / (laser_info_.num_ranges_/laser_info_.sampling_factor_);
    laser_info_.min_range_reset_value_ = 0.4;



    laser_image_info_.angles_ranges_.clear();
    laser_image_info_.laser_scans_image_size_ = cv::Size(200, 200);
    laser_image_info_.angle_ini_ = -45.0;
    laser_image_info_.angle_ini_rad_ = laser_image_info_.angle_ini_*M_PI/180;
    laser_image_info_.angle_increment_ = laser_info_.angle_range_ / laser_info_.num_ranges_;
    laser_image_info_.p_origin_ = cv::Point(laser_image_info_.laser_scans_image_size_.width/2.0,
                                            laser_image_info_.laser_scans_image_size_.height/2.0);
    for(int i=0;i<laser_info_.num_ranges_;i++)
    {
        float angle_i = (laser_image_info_.angle_ini_ + i*laser_image_info_.angle_increment_) * M_PI/180.0;
        float cos_angle_i = std::cos(angle_i);
        float sin_angle_i = std::sin(angle_i);
        laser_image_info_.angles_ranges_.push_back(angle_i);
        laser_image_info_.cos_angles_ranges_.push_back(cos_angle_i);
        laser_image_info_.sin_angles_ranges_.push_back(sin_angle_i);
    }

    laser_image_info_.laser_scans_image_res_ = laser_info_.max_virtual_range_ / (laser_image_info_.laser_scans_image_size_.width/2.0);
    laser_image_info_.laser_scans_image_ = cv::Mat(laser_image_info_.laser_scans_image_size_.height,
                                                  laser_image_info_.laser_scans_image_size_.width,
                                                  CV_8U, cv::Scalar(255));
    laser_image_info_.obstacles_boundary_image_ = laser_image_info_.laser_scans_image_.clone();



    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
    {
        std::cout<<"STATE_BASED_ON_GROUP_OF_LASER_RANGES"<<std::endl;
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_);
    }
    else
    {
        std::cout<<"STATE BASED ON SAMPLED LASER_RANGES"<<std::endl;
        environment_info_.state_dim_low_dim_ = 4 + (laser_info_.num_ranges_ / laser_info_.sampling_factor_) + 1;
    }


    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0, 0.0);
    uav_target_current_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);
    uav_target_previous_relative_position_ = cv::Point3f(0.0, 0.0, 0.0);


    min_distance_from_laser_flag_ = false;
    min_distance_target_reached_flag_ = false;
    min_distance_to_target_thresh_ = 0.1;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;
    std::cout<<"pitch_roll_max_value_ : "<<pitch_roll_max_value_<<std::endl;

}


RlEnvironmentLaserBasedNavigationWithPlanner::~RlEnvironmentLaserBasedNavigationWithPlanner()
{
    f_data_recorder.close();
}

void RlEnvironmentLaserBasedNavigationWithPlanner::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: LASER-BASED REACTIVE NAVIGATION WITH PLANNER Environment" << std::endl;

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;

    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentLaserBasedNavigationWithPlanner::PoseVelocityCallback, this);


    ros::param::get("~esimtated_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_estimated_pose_subs_ = n.subscribe(param_string, 1, &RlEnvironmentLaserBasedNavigationWithPlanner::DroneEstimatedPoseCallback, this);



    ros::param::get("~laser_scan", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_laser_scan_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
                                         &RlEnvironmentLaserBasedNavigationWithPlanner::LaserScanCallback, this);


    ros::param::get("~slam_out_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_slam_out_pose_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
                                         &RlEnvironmentLaserBasedNavigationWithPlanner::SlamOutPoseCallback, this);



    ros::param::get("~cmd_vel", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_cmd_vel_subs_ = n.subscribe("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1,
                                         &RlEnvironmentLaserBasedNavigationWithPlanner::CmdVelCallback, this);




    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);


    ros::param::get("~hector_mapping_reset", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    hector_mapping_reset_pub_ = n.advertise<std_msgs::String>(param_string, 1);


    ros::param::get("~goal_to_planner", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    goal_to_planner_pub_ = n.advertise<move_base_msgs::MoveBaseActionGoal>(param_string,1);



    // Init services
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_set_model_state_srv_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~set_link_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_set_link_state_srv_ = n.serviceClient<gazebo_msgs::SetLinkState>(param_string);


    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);



    // Reset environment
    Reset();


    //f_data_recorder.open("/home/carlos/pruebas_Matlab/speeds_pos_in_image_with_ground_truth3.txt");
}

bool RlEnvironmentLaserBasedNavigationWithPlanner::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{
    // Send action

    std::vector<float> current_state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        current_state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        current_state = ComputeNormalizedState();


    if(DEBUG_MODE_STATE)
    {
        std::cout<<"***** CURRENT STATE *****"<<std::endl;
        std::cout<<"Size: "<<current_state.size()<<std::endl;
        for(int i=0; i<current_state.size();i++)
        {
            if(i==0)
                std::cout<<"[";
            if(i==current_state.size()-1)
                std::cout<<current_state[i]<<"]";
            else
                std::cout<<current_state[i]<<" , ";
        }
        std::cout<<std::endl;
    }



    response.reward = 0.0;


    if(min_distance_target_reached_flag_)
    {
        response.reward = 0.0;
        response.terminal_state = true;
        reset_env_ = true;
        min_distance_target_reached_flag_ = false;

    }
    else if (min_distance_from_laser_flag_)
    {
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
        min_distance_from_laser_flag_ = false;
    }
    else
        response.terminal_state = false;

    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();



    // Read next state
    std::vector<float> next_state;
    if(STATE_BASED_ON_GROUP_OF_LASER_RANGES)
        next_state = ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    else
        next_state = ComputeNormalizedState();
    response.obs_real = next_state;
    UpdateUavTargetPreviousRelativePosition();


    // Successful return
    return true;
}


bool RlEnvironmentLaserBasedNavigationWithPlanner::Reset()
{
   // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav;
    float roll, pitch, yaw;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, dz_uav, roll, pitch, yaw);



    // Spawn model to RANDOM locations (UAV and Target)
    cv::Point2f uav_rand_pos, target_rand_pos;
    if(environment_info_.name_ == "house")
        GenerateRandomPositionsHouse(uav_rand_pos, target_rand_pos);
    else if(environment_info_.name_ == "house_2")
        GenerateRandomPositionsHouse2(uav_rand_pos, target_rand_pos);
    else if((environment_info_.name_ == "house_3") || (environment_info_.name_ == "house_3b") || (environment_info_.name_ == "house_3c"))
        GenerateRandomPositionsHouse3(uav_rand_pos, target_rand_pos);


//    gazebo_msgs::SetModelState model_msg_uav;
//    model_msg_uav.request.model_state.model_name = UAV_NAME;
//    model_msg_uav.request.model_state.pose.position.x = uav_rand_pos.x;
//    model_msg_uav.request.model_state.pose.position.y = uav_rand_pos.y;
//    model_msg_uav.request.model_state.pose.position.z = kUav_Altitude_;
//    if (gazebo_set_model_state_srv_.call(model_msg_uav))
//    {

//    }
//    else{
//        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
////        return false;
//    }

//    gazebo_msgs::SetModelState model_msg_target;
//    model_msg_target.request.model_state.model_name = TARGET_NAME;
//    model_msg_target.request.model_state.pose.position.x = target_rand_pos.x;
//    model_msg_target.request.model_state.pose.position.y = target_rand_pos.y;
//    model_msg_target.request.model_state.pose.position.z = 0.0;
//    if (gazebo_set_model_state_srv_.call(model_msg_target))
//    {

//    }
//    else{
//        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
////        return false;
//    }



    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting UAV pos at x: " << uav_rand_pos.x << " and y: " << uav_rand_pos.y << std::endl;
    std::cout << "RL_ENV_INFO: reseting TARGET pos at x: " << target_rand_pos.x << " and y: " << target_rand_pos.y << std::endl;



//    //Send GOAL to move_base planner
//    move_base_msgs::MoveBaseActionGoal goal_msg;
//    goal_msg.goal.target_pose.header.stamp = ros::Time::now();
//    goal_msg.goal.target_pose.header.frame_id = "map";
//    goal_msg.goal.target_pose.pose.position.x = 6.2;
//    goal_msg.goal.target_pose.pose.position.y = 0.0;
//    goal_msg.goal.target_pose.pose.position.z = kUav_Altitude_;
//    goal_msg.goal.target_pose.pose.orientation.x = 0.0;
//    goal_msg.goal.target_pose.pose.orientation.y = 0.0;
//    goal_msg.goal.target_pose.pose.orientation.z = 0.0;
//    goal_msg.goal.target_pose.pose.orientation.w = 1.0;
//    goal_to_planner_pub_.publish(goal_msg);

//    std::cout<<"goal at: "<<"["<<goal_msg.goal.target_pose.pose.position.x<<" , "<<goal_msg.goal.target_pose.pose.position.y<<"]"<<std::endl;



    std_msgs::String reset_msg;
    reset_msg.data = "reset";
    hector_mapping_reset_pub_.publish(reset_msg);

    // Spawn model to origin
//    std_srvs::Empty estimator_msg;
//    if (estimator_client_.call(estimator_msg))
//    {
//        uav_initial_reset_position_.x = uav_rand_pos.x;
//        uav_initial_reset_position_.y = uav_rand_pos.y;
//        uav_initial_reset_position_.z = kUav_Altitude_;

//        std_msgs::String reset_msg;
//        reset_msg.data = "reset";
//        hector_mapping_reset_pub_.publish(reset_msg);

//        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
//        return true;
//    }
//    else{
//        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
//        return false;
//    }

}

void RlEnvironmentLaserBasedNavigationWithPlanner::LaserScanCallback(const sensor_msgs::LaserScan &msg)
{
    laser_info_.laser_ranges_.clear();
    laser_info_.laser_ranges_.insert(laser_info_.laser_ranges_.end(), msg.ranges.begin(), msg.ranges.end());

}


void RlEnvironmentLaserBasedNavigationWithPlanner::CmdVelCallback(const geometry_msgs::Twist &msg)
{
//    std::cout<<"linear velocity: "<<std::endl;
//    std::cout<<"[vx, vy]: "<<"["<<msg.linear.x<<" , "<<msg.linear.y<<"]"<<std::endl;

    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = msg.linear.x;
    action_msg.dy = msg.linear.y;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);
}

void RlEnvironmentLaserBasedNavigationWithPlanner::SlamOutPoseCallback(const geometry_msgs::PoseStamped &msg)
{
    slam_out_uav_position_.x = msg.pose.position.x + uav_initial_reset_position_.x;
    slam_out_uav_position_.y = msg.pose.position.y + uav_initial_reset_position_.y;
    slam_out_uav_position_.z = msg.pose.position.z + uav_initial_reset_position_.z;


    //std::cout<<"SLAM OUT POSE (msg)"<<std::endl;
    //std::cout<<"[x,y,z]: "<<"["<<slam_out_uav_position_.x<<" , "<<slam_out_uav_position_.y<<" , "<<slam_out_uav_position_.z<<"]"<<std::endl;
}




void RlEnvironmentLaserBasedNavigationWithPlanner::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    cv::Point3f target_position;
    cv::Point3f uav_position, uav_linear_vel;
    cv::Point3f uav_previous_pos, uav_previous_linear_vel;
    uav_previous_pos.x = uav_state_.pos_x_;uav_previous_pos.y = uav_state_.pos_y_;uav_previous_pos.z = uav_state_.pos_z_;
    uav_previous_linear_vel.x = uav_state_.speed_x_;uav_previous_linear_vel.y = uav_state_.speed_y_;uav_previous_linear_vel.z = uav_state_.speed_z_;

    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            uav_position.x = msg->pose[i].position.x;
            uav_position.y = msg->pose[i].position.y;
            uav_position.z = msg->pose[i].position.z;

            uav_linear_vel.x = msg->twist[i].linear.x;
            uav_linear_vel.y = msg->twist[i].linear.y;
            uav_linear_vel.z = msg->twist[i].linear.z;
            break;
        }

        else if(msg->name[i].compare(TARGET_NAME) == 0)
        {
            target_position.x = msg->pose[i].position.x;
            target_position.y = msg->pose[i].position.y;
            target_position.z = msg->pose[i].position.z;
        }

    }


    if(TEST_MODE && environment_info_.name_ == "house_3c")
    {
        if((uav_position.x > -1.4 && uav_position.x < -1.36) || (uav_position.x > -0.3 && uav_position.x < -0.25))
        {
            gazebo_msgs::SetModelState model_msg_obstacle_cylinder;
            model_msg_obstacle_cylinder.request.model_state.model_name = "moving_obstacle_squared1";
            model_msg_obstacle_cylinder.request.model_state.pose.position.x = uav_position.x + 1.0;
            model_msg_obstacle_cylinder.request.model_state.pose.position.y = uav_position.y;
            model_msg_obstacle_cylinder.request.model_state.pose.position.z = 0.0;
            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
            }
        }
    }
    else if(TEST_MODE && environment_info_.name_ == "house_3")
    {
        if((uav_position.x > -2.2 && uav_position.x < -2.195))
        {
            float lower_lim_y = -0.3;
            float upper_lim_y = 0.3;
            float rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

            gazebo_msgs::SetModelState model_msg_obstacle_cylinder1;
            model_msg_obstacle_cylinder1.request.model_state.model_name = "moving_obstacle_squared_big1";
            model_msg_obstacle_cylinder1.request.model_state.pose.position.x = uav_position.x + 1.2;
            model_msg_obstacle_cylinder1.request.model_state.pose.position.y = uav_position.y + rand_pos_y;
            model_msg_obstacle_cylinder1.request.model_state.pose.position.z = 0.0;
            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder1))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
            }


            lower_lim_y = -1.0;
            upper_lim_y = 1.0;
            rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

            gazebo_msgs::SetModelState model_msg_obstacle_cylinder2;
            model_msg_obstacle_cylinder2.request.model_state.model_name = "moving_obstacle_squared_big2";
            model_msg_obstacle_cylinder2.request.model_state.pose.position.x = 0.75;
            if(rand_pos_y > 0)
                model_msg_obstacle_cylinder2.request.model_state.pose.position.y = 1.8;
            else
                model_msg_obstacle_cylinder2.request.model_state.pose.position.y = -1.8;
            model_msg_obstacle_cylinder2.request.model_state.pose.position.z = 0.0;
            if (gazebo_set_model_state_srv_.call(model_msg_obstacle_cylinder2))
            {

            }
            else{
                ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
            }
        }

    }

    if(DEBUG_UAV_TARGET_POSITON)
    {
        std::cout<<"UAV position: "<<"x: "<<uav_position.x<<" ; "<<"y: "<<uav_position.y<<" ; "<<"area_num: "<<area_uav_num_<<std::endl;
        std::cout<<"TARGET position: "<<"x: "<<target_position.x<<" ; "<<"y: "<<target_position.y<<" ; "<<"area_num: "<<area_target_num_<<std::endl;
    }


    // Set uav state
    SetUavState(uav_position.x, uav_position.y, uav_position.z, uav_linear_vel.x, uav_linear_vel.y, uav_linear_vel.z);
    SetUavPreviousState(uav_previous_pos.x, uav_previous_pos.y, uav_previous_pos.z,
                        uav_previous_linear_vel.x, uav_previous_linear_vel.y, uav_previous_linear_vel.z);
    SetTargetState(target_position.x, target_position.y, target_position.z);



}

void RlEnvironmentLaserBasedNavigationWithPlanner::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose_  = msg;
    //yaw pitch roll
//    uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
//    uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
//    uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

//    uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
//    uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
//    uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;

    return;
}


void RlEnvironmentLaserBasedNavigationWithPlanner::GenerateRandomPositionsHouse3(cv::Point2f &uav_pose, cv::Point2f &target_pose)
{

    std::srand (static_cast <unsigned> (time(0)));

    float max_pos_x = (environment_info_.max_pos_x_ / 2.0) - 1;
    float max_pos_y = (environment_info_.max_pos_y_ / 2.0) - 1;


    if(TEST_MODE)
    {
         //*********** FOR TESTING PURPOSES ***********
        float lower_lim_x = -max_pos_x;
        float upper_lim_x = max_pos_x;
        float lower_lim_y = -1.0;
        float upper_lim_y = 1.0;
        float rand_pos_x = -3.0;
        float rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        uav_pose.x = rand_pos_x;
        uav_pose.y = rand_pos_y;

        lower_lim_x = -max_pos_x;
        upper_lim_x = max_pos_x;
        lower_lim_y = -1.0;
        upper_lim_y = 1.0;
        if(environment_info_.name_ == "house_3c")
            lower_lim_y = 0.0;
        rand_pos_x = 2.0;
        rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        target_pose.x = rand_pos_x;
        target_pose.y = rand_pos_y;
    }
    else
    {
        //*********** FOR TRAINING PURPOSES ***********
        float lower_lim_x = -max_pos_x;
        float upper_lim_x = max_pos_x;
        float lower_lim_y = -max_pos_y;
        float upper_lim_y = max_pos_y;
        float rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
        float rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        uav_pose.x = rand_pos_x;
        uav_pose.y = rand_pos_y;

        lower_lim_x = -max_pos_x;
        upper_lim_x = max_pos_x;
        lower_lim_y = -max_pos_y;
        upper_lim_y = max_pos_y;
        rand_pos_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
        rand_pos_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
        target_pose.x = rand_pos_x;
        target_pose.y = rand_pos_y;
    }


    area_uav_num_ = 1;
    area_target_num_ = 1;
}

