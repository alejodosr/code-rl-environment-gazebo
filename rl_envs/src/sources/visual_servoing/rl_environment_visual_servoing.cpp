#include "rl_environment_visual_servoing.h"

//RlEnvironmentVisualServoing::RlEnvironmentVisualServoing()
//{}


//RlEnvironmentVisualServoing::~RlEnvironmentVisualServoing()
//{}

bool RlEnvironmentVisualServoing::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response){
    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Is terminal state?
    float x, y, z, dx, dy;
    GetUavState(x, y, z, dx, dy);

    // Print info
//    std::cout << "RL_ENV_DEBUG: UAV pose x " << x << " y " << y << std::endl;

    // Compute reward
//    float reward = -(pow(x, 2) + pow(y, 2) + 0.1 * pow(dx, 2) + 0.1 * pow(dy, 2) + 0.001 * pow(request.action[0], 2) + 0.001 * pow(request.action[1], 2));
    float shaping = - 100 * sqrt(pow(x,2) + pow(y,2)) - 10 * sqrt(pow(dx, 2) + pow(dy, 2)) - sqrt(pow(request.action[0], 2) + pow(request.action[1], 2));
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;

    if ((fabs(x) > 1) || (fabs(y) > 1)){
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else
        response.terminal_state = false;

    // Wait for environment to render
    WaitForEnv();

    // Read next state
    std::vector<float> state_next;
    GetUavState(x, y, z, dx, dy);
    state_next.push_back(x);
    state_next.push_back(y);
    state_next.push_back(dx);
    state_next.push_back(dy);
    response.obs_real = state_next;

    // Successful return
    return true;
}

bool RlEnvironmentVisualServoing::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;

    // Action max
    std::vector<float> actions_max = {0.5, 0.5};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {-0.5, -0.5};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentVisualServoing::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response){
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv service called" << std::endl;

    // Enable reset
    reset_env_ = true;

    // Is it long iteration?
//    if (exp_rec_.getRecording())
//        long_iteration_ = true;

    std::vector<float> s;
    float x, y, z, dx, dy;
    GetUavState(x, y, z, dx, dy);
    s.push_back(x);
    s.push_back(y);
    s.push_back(dx);
    s.push_back(dy);
    response.state = s;

    return true;
}

void RlEnvironmentVisualServoing::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg){
    // Get position of the uav in the vector
    int i_position;
    for (int i=0; i<msg->name.size(); i++){
        if (msg->name[i].compare(UAV_NAME) == 0){
            i_position = i;
            break;
        }
    }

    // Set uav state
    SetUavState(msg->pose[i_position].position.x / (float) MAX_POSE_X, msg->pose[i_position].position.y / (float) MAX_POSE_Y, msg->pose[i_position].position.z, msg->twist[i_position].linear.x, msg->twist[i_position].linear.y);
}

void RlEnvironmentVisualServoing::InitChild(ros::NodeHandle n){

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;
    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_sub_ = n.subscribe(param_string, 10, &RlEnvironmentVisualServoing::PoseVelocityCallback, this);

    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
    this->data_->num_iterations = (unsigned int) NUM_ITERATIONS;

    // Print
    std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;

    // Init recording experiment
//    exp_rec_.Open(n);

    // Reset environment
    Reset();
}

bool RlEnvironmentVisualServoing::Reset(){
    // Check experiment recorder
//    if(exp_rec_.getRecording()){
//        exp_rec_.RecordExperiment();
//        exp_rec_.setRecording(false);
//    }

    // Get state
    float x, y, z, dx, dy;
    GetUavState(x, y, z, dx, dy);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Random initial point
    std::srand(time(NULL));
    int seed = std::rand() % 10;
    int r_x = ((-1 * (int) MAX_POSE_X * 100) + (std::rand() % ((int) MAX_POSE_X * 100)));
    int r_y = ((-1 * (int) MAX_POSE_Y * 100) + (std::rand() % ((int) MAX_POSE_Y * 100)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    float init_x = (float) r_x / 100.f;
    float init_y = (float) r_y / 100.f;

    // Spawn model to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = z;
//    model_msg.request.model_state.pose.orientation.x = 0;
//    model_msg.request.model_state.pose.orientation.y = 0;
//    model_msg.request.model_state.pose.orientation.z = 0;
//    model_msg.request.model_state.pose.orientation.w = 1;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << " and y " << init_y << std::endl;

    if (gazebo_client_.call(model_msg)){
//        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg)){
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        return false;
    }
}



