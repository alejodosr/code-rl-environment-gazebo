#include "rl_environment_position_based_visual_servoing.h"

#define DEBUG_MODE 0
#define IMSHOW_MODE 1

RlEnvironmentPositionBasedVisualServoing::RlEnvironmentPositionBasedVisualServoing()
{
    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0);

}


RlEnvironmentPositionBasedVisualServoing::~RlEnvironmentPositionBasedVisualServoing()
{

}

void RlEnvironmentPositionBasedVisualServoing::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: POSITION BASED VISUAL SERVOING Environment" << std::endl;

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;
    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentPositionBasedVisualServoing::PoseVelocityCallback, this);

    ros::param::get("~object_recognized", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    visual_servoing_measurement_subs_ = n.subscribe(param_string, 1, &RlEnvironmentPositionBasedVisualServoing::VisualServoingMeasurementCallback, this);

    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_altitude_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
    this->data_->num_iterations = (unsigned int) NUM_ITERATIONS;


//    // Init circular buffer
    filtered_derivative_wcb_x_.setTimeParameters( 0.0015, 0.0015, 0.150, 1.0, 30.000);
    filtered_derivative_wcb_y_.setTimeParameters( 0.0015, 0.0015, 0.150, 1.0, 30.000);

    filtered_derivative_wcb_x_.reset();
    filtered_derivative_wcb_y_.reset();

    // Print
    std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;

    // Init recording experiment
//    exp_rec_.Open(n);


    f_data_recorder.open("/home/carlos/pruebas_Matlab/PBVS_position_and_speeds_Tderiv_015.txt");
    // Reset environment
    Reset();
}

bool RlEnvironmentPositionBasedVisualServoing::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{
    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Is terminal state?
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);

//    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
//    altitude_msg.position_command.x = 0;
//    altitude_msg.position_command.y = 0;
//    altitude_msg.position_command.z = kUav_Altitude_;
//    uav_altitude_ref_pub_.publish(altitude_msg);

    std::vector<float> current_state = ComputeNormalizedState();

    // Compute reward
//    float shaping = - 100 * sqrt(pow(error_in_image_.x, 2) + pow(error_in_image_.y, 2)) -
//            10 * sqrt(pow(dx_uav, 2) + pow(dy_uav, 2)) - sqrt(pow(request.action[0], 2) + pow(request.action[1], 2));
    float shaping = - 100 * sqrt(pow(current_state[0], 2) + pow(current_state[1], 2)) -
            10 * sqrt(pow(current_state[2], 2) + pow(current_state[3], 2)) - sqrt(pow(request.action[0], 2) + pow(request.action[1], 2));
    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;


    if ((fabs(current_state[0]) > MAX_POSE_X) || (fabs(current_state[1]) > MAX_POSE_Y))
    {
        std::cout<<"--------------------------------------"<<std::endl;
        std::cout<<"----------- MINIMUM REWARD -----------"<<std::endl;
        std::cout<<"--------------------------------------"<<std::endl;
        response.reward = -100;
        response.terminal_state = true;
        reset_env_ = true;
    }
    else
        response.terminal_state = false;


    // Wait for environment to render
    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();

    // Read next state
    std::vector<float> next_state = ComputeNormalizedState();

    response.obs_real = next_state;

    // Successful return
    return true;
}

bool RlEnvironmentPositionBasedVisualServoing::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality ImageBasedVisualServoing service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;

    // Action max
    std::vector<float> actions_max = {0.5, 0.5};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {-0.5, -0.5};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentPositionBasedVisualServoing::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv ImageBasedVisualServoing service called" << std::endl;

    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
//    if (exp_rec_.getRecording())
//        long_iteration_ = true;

    std::vector<float> state = ComputeNormalizedState();
    response.state = state;

    return true;
}

bool RlEnvironmentPositionBasedVisualServoing::Reset()
{
    // Check experiment recorder
//    if(exp_rec_.getRecording()){
//        exp_rec_.RecordExperiment();
//        exp_rec_.setRecording(false);
//    }

    // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Random initial point
    std::srand(time(NULL));
    int seed = std::rand() % 10;
    int r_x = ((-1 * (int) MAX_POSE_X * 100) + (std::rand() % ((int) MAX_POSE_X * 100)));
    int r_y = ((-1 * (int) MAX_POSE_Y * 100) + (std::rand() % ((int) MAX_POSE_Y * 100)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    float init_x = (float) r_x / 100.f;
    float init_y = (float) r_y / 100.f;


    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = kUav_Altitude_;
    uav_altitude_ref_pub_.publish(altitude_msg);

    // Spawn model to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = kUav_Altitude_;
//    model_msg.request.model_state.pose.orientation.x = 0;
//    model_msg.request.model_state.pose.orientation.y = 0;
//    model_msg.request.model_state.pose.orientation.z = 0;
//    model_msg.request.model_state.pose.orientation.w = 1;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << " and y " << init_y << std::endl;

    if (gazebo_client_.call(model_msg)){
//        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg)){
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        return false;
    }
}


void RlEnvironmentPositionBasedVisualServoing::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    // Get position of the uav in the vector
    int i_position;
    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            i_position = i;
            break;
        }
    }

    uav_real_pos_x_ = msg->pose[i_position].position.x;
    uav_real_pos_y_ = msg->pose[i_position].position.y;
    uav_real_vel_x_ = msg->twist[i_position].linear.x;
    uav_real_vel_y_ = msg->twist[i_position].linear.y;
}


void RlEnvironmentPositionBasedVisualServoing::VisualServoingMeasurementCallback(const droneMsgsROS::visualObjectRecognized &msg)
{
    //Extract the Estimated Pose of the DETECTED OBJECT
    float recognition_confidence = msg.recognition_confidence;
    visualObjectRecognized_msg.object_id = msg.object_id;
    visualObjectRecognized_msg.object_name = msg.object_name;

//    visualObjectRecognized_msg.pose.position.x = msg.pose.position.x;
//    visualObjectRecognized_msg.pose.position.y = msg.pose.position.y;
//    visualObjectRecognized_msg.pose.position.z = msg.pose.position.z;

//    visualObjectRecognized_msg.pose.orientation.x = msg.pose.orientation.x;
//    visualObjectRecognized_msg.pose.orientation.y = msg.pose.orientation.y;
//    visualObjectRecognized_msg.pose.orientation.z = msg.pose.orientation.z;
//    visualObjectRecognized_msg.pose.orientation.w = msg.pose.orientation.w;

//    visualObjectRecognized_msg.rotRect.angle = msg.rotRect.angle;
//    visualObjectRecognized_msg.rotRect.center.x = msg.rotRect.center.x;
//    visualObjectRecognized_msg.rotRect.center.y = msg.rotRect.center.y;
//    visualObjectRecognized_msg.rotRect.size.width = msg.rotRect.size.width;
//    visualObjectRecognized_msg.rotRect.size.height = msg.rotRect.size.height;


    // Compute speeds from pose ground truth
    ros::Time current_timestamp = ros::Time::now();

    double x_raw_t = msg.pose.position.x;
    double y_raw_t = msg.pose.position.y;

    time_t tv_sec; suseconds_t tv_usec;
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb_x_.setInput( x_raw_t, tv_sec, tv_usec);
    filtered_derivative_wcb_y_.setInput( y_raw_t, tv_sec, tv_usec);


    double x_t, dx_t;
    double y_t, dy_t;
    filtered_derivative_wcb_x_.getOutput( x_t,  dx_t);
    filtered_derivative_wcb_y_.getOutput( y_t,  dy_t);

    if(dx_t > 1.0)
        dx_t = 1.0;
    else if(dx_t < -1.0)
        dx_t = -1.0;

    if(dy_t > 1.0)
        dy_t = 1.0;
    else if(dy_t < -1.0)
        dy_t = -1.0;


    f_data_recorder<<x_t<<" ; "<<y_t<<" ; "<<dx_t<<" ; "<<dy_t<<
                     " ; "<<uav_real_pos_x_<<" ; "<<uav_real_pos_y_<<"; "<<uav_real_vel_x_<<" ; "<<uav_real_vel_y_<<std::endl;
    SetUavState(x_raw_t, y_raw_t, msg.pose.position.z, dx_t, dy_t);


}

void RlEnvironmentPositionBasedVisualServoing::CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &msg)
{

    camera_info_.cx_ = msg->K.at(2);
    camera_info_.cy_ = msg->K.at(5);
    camera_info_.fx_ = msg->K.at(0);
    camera_info_.fy_ = msg->K.at(4);

    camera_info_.image_width_ = msg->width;
    camera_info_.image_height_ = msg->height;

}


void RlEnvironmentPositionBasedVisualServoing::CameraImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
        cv::Mat img = cv_bridge::toCvShare(msg, "bgr8")->image;
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
}


void RlEnvironmentPositionBasedVisualServoing::SetUavState(const float x, const float y, const float z, const float dx, const float dy)
{
    uav_mutex_.lock();
    uav_pose_x_ = x;
    uav_pose_y_ = y;
    uav_pose_z_ = z;
    uav_velocity_x_ = dx;
    uav_velocity_y_ = dy;
    uav_mutex_.unlock();
}

void RlEnvironmentPositionBasedVisualServoing::GetUavState(float &x, float &y, float &z, float &dx, float &dy)
{
    uav_mutex_.lock();
    x = uav_pose_x_;
    y = uav_pose_y_;
    z = uav_pose_z_;
    dx = uav_velocity_x_;
    dy = uav_velocity_y_;
    uav_mutex_.unlock();
}


std::vector<float> RlEnvironmentPositionBasedVisualServoing::ComputeNormalizedState()
{
    state_.clear();

    float x_relative, y_relative, z_relative, dx_relative, dy_relative;
    GetUavState(x_relative, y_relative, z_relative, dx_relative, dy_relative);

    x_relative /= static_cast<float>(MAX_POSE_X);
    y_relative /= static_cast<float>(MAX_POSE_Y);



    state_.push_back(x_relative);
    state_.push_back(y_relative);
    state_.push_back(dx_relative);
    state_.push_back(dy_relative);

    std::cout<<"State [x, y, dx, dy]: "<<"["<<state_[0]<<" , "<<state_[1]<<" , "
            <<state_[2]<<" , "<<state_[3]<<"]"<<std::endl;

    return state_;

}


