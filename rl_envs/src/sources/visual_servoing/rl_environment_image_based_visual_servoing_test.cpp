#include "rl_environment_image_based_visual_servoing_test.h"


#define DEBUG_MODE 0
#define IMSHOW_MODE 1
#define IMSHOW_STABILIZED_DETECTED_OBJECT 1

#define OBJECT_STATE_BASED_ON_ERROR_IN_POSITION_ONLY 0
#define OBJECT_STATE_BASED_ON_ROTATED_RECT 0
#define OBJECT_STATE_INCLUDING_ROLL_PITCH 0
#define OBJECT_STATE_BASED_ON_GROUND_TRUTH 0
#define OBJECT_STATE_BASED_ON_STABILIZED_DETECTION 1

#define ROLL_PITCH_RECTIFICATION 0
#define POSITION_CONTROL_MODE 0
#define ESTIMATE_SPEEDS_WITH_CIRCULAR_BUFFER 1
#define COMPUTE_NORM_VALUE_WRT_ROI_REFERENCE 0
#define USE_OWN_OBJECT_DETECTOR 1

#define UAV_POSE_FROM_EKF 0
#define ERRORS_ARRAY_LENGTH 200
#define MAX_ERROR_FOR_TARGET_LOCK 0.055


RlEnvironmentImageBasedVisualServoingTest::RlEnvironmentImageBasedVisualServoingTest()
{
    ResetValues();
}

void RlEnvironmentImageBasedVisualServoingTest::ResetValues()
{
    module_started_ = false;
    errors_array_.clear();
    state_.clear();
    normalized_state_.clear();
    object_in_image_info_.rotated_rect_points_.clear();
    object_in_image_info_.rotated_rect_points_rpy_compensated_.clear();

    object_in_image_info_.detected_roi_.angle = 0.0;
    object_in_image_info_.detected_roi_.center.x = 0.0;
    object_in_image_info_.detected_roi_.center.y = 0.0;
    object_in_image_info_.detected_roi_.size.width = 0.0;
    object_in_image_info_.detected_roi_.size.height = 0.0;

    object_in_image_info_.previous_detected_roi_.angle  = 0.0;
    object_in_image_info_.previous_detected_roi_.center.x = 0.0;
    object_in_image_info_.previous_detected_roi_.center.y = 0.0;
    object_in_image_info_.previous_detected_roi_.size.width = 0.0;
    object_in_image_info_.previous_detected_roi_.size.height = 0.0;


    object_in_image_info_.stabilized_detected_roi_.angle = 0.0;
    object_in_image_info_.stabilized_detected_roi_.center.x = 0.0;
    object_in_image_info_.stabilized_detected_roi_.center.y = 0.0;
    object_in_image_info_.stabilized_detected_roi_.size.width = 0.0;
    object_in_image_info_.stabilized_detected_roi_.size.height = 0.0;

    object_in_image_info_.stabilized_detected_roi_rect_ = cv::Rect(0,0,0,0);


    roi_reference_ = cv::Rect(0,0,0,0);
    roi_reference_from_ground_truth_ = cv::Rect(0,0,0,0);
    center_roi_ref_ = cv::Point2f(0,0);


    camera_info_.camera_info_available_flag_ = false;
    camera_info_.image_width_ = 640;
    camera_info_.image_height_ = 480;
    center_roi_ref_max_.x = camera_info_.image_width_/2.0;
    center_roi_ref_max_.y = camera_info_.image_height_/2.0;

    mean_errors_array_ = 0.0;
    error_in_image_.x = 0.0;
    error_in_image_.y = 0.0;
    error_in_image_norm_.x = 0.0;
    error_in_image_norm_.y = 0.0;
    image_boundaries_offset_ = 10;

    for(int i=0;i<4;i++)
    {
        object_in_image_info_.rotated_rect_points_.push_back(cv::Point2f(0, 0));
        object_in_image_info_.rotated_rect_points_rpy_compensated_.push_back(cv::Point2f(0, 0));
        object_in_image_info_.ground_truth_current_object_points_[i] = cv::Point2f(0,0);
    }

    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0);
    current_time_ = ros::Time::now();
    previous_time_ = ros::Time::now();


    object_in_world_info_.width_ = 0.25;
    object_in_world_info_.height_ = 0.3;
    object_in_world_info_.center_offset_.x = 0.0;
    object_in_world_info_.center_offset_.y = 0.0;
    object_in_world_info_.points_mat_[0] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[0].at<float>(0,0) = -object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.x;
    object_in_world_info_.points_mat_[0].at<float>(1,0) = -object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.y;
    object_in_world_info_.points_mat_[0].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    object_in_world_info_.points_mat_[1] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[1].at<float>(0,0) = object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.x;
    object_in_world_info_.points_mat_[1].at<float>(1,0) = -object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.y;
    object_in_world_info_.points_mat_[1].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    object_in_world_info_.points_mat_[2] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[2].at<float>(0,0) = object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.x;
    object_in_world_info_.points_mat_[2].at<float>(1,0) = object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.y;
    object_in_world_info_.points_mat_[2].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    object_in_world_info_.points_mat_[3] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[3].at<float>(0,0) = -object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.x;
    object_in_world_info_.points_mat_[3].at<float>(1,0) = object_in_world_info_.width_/2.0 + object_in_world_info_.center_offset_.y;
    object_in_world_info_.points_mat_[3].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    actions_max_value_ = 1.0;
    actions_min_value_ = -1.0;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;
    std::cout<<"pitch_roll_max_value_ : "<<pitch_roll_max_value_<<std::endl;

    // Init circular buffer
    filtered_derivative_wcb_x_.setTimeParameters( 0.015, 0.015, 0.150, 1.0, 30.000);
    filtered_derivative_wcb_y_.setTimeParameters( 0.015, 0.015, 0.150, 1.0, 30.000);
//    filtered_derivative_wcb_x_.setTimeParameters(0.005, 0.005, 0.2, 1.0, 100.0);
//    filtered_derivative_wcb_y_.setTimeParameters(0.005, 0.005, 0.2, 1.0, 100.0);

    filtered_derivative_wcb_x_.reset();
    filtered_derivative_wcb_y_.reset();



    rot_mat_z_90_ = cv::Mat(4, 4, CV_32FC1);
    rot_mat_z_90_.at<float>(0,0) = 0; rot_mat_z_90_.at<float>(0,1) = 1; rot_mat_z_90_.at<float>(0,2) = 0; rot_mat_z_90_.at<float>(0,3) = 0;
    rot_mat_z_90_.at<float>(1,0) = -1; rot_mat_z_90_.at<float>(1,1) = 0; rot_mat_z_90_.at<float>(1,2) = 0; rot_mat_z_90_.at<float>(1,3) = 0;
    rot_mat_z_90_.at<float>(2,0) = 0; rot_mat_z_90_.at<float>(2,1) = 0; rot_mat_z_90_.at<float>(2,2) = 1; rot_mat_z_90_.at<float>(2,3) = 0;
    rot_mat_z_90_.at<float>(3,0) = 0; rot_mat_z_90_.at<float>(3,1) = 0; rot_mat_z_90_.at<float>(3,2) = 0; rot_mat_z_90_.at<float>(3,3) = 1;

    rot_mat_x180_ = cv::Mat(4, 4, CV_32FC1);
    rot_mat_x180_.at<float>(0,0) = 1; rot_mat_x180_.at<float>(0,1) = 0; rot_mat_x180_.at<float>(0,2) = 0; rot_mat_x180_.at<float>(0,3) = 0;
    rot_mat_x180_.at<float>(1,0) = 0; rot_mat_x180_.at<float>(1,1) = -1; rot_mat_x180_.at<float>(1,2) = 0; rot_mat_x180_.at<float>(1,3) = 0;
    rot_mat_x180_.at<float>(2,0) = 0; rot_mat_x180_.at<float>(2,1) = 0; rot_mat_x180_.at<float>(2,2) = -1; rot_mat_x180_.at<float>(2,3) = 0;
    rot_mat_x180_.at<float>(3,0) = 0; rot_mat_x180_.at<float>(3,1) = 0; rot_mat_x180_.at<float>(3,2) = 0; rot_mat_x180_.at<float>(3,3) = 1;

    t_cam_world_ = cv::Mat::eye(4, 4, CV_32FC1);
    rot_mat_2d_yaw_ = cv::Mat::eye(2, 2, CV_32FC1);

    std::cout<<"Call Reset Values!"<<std::endl;
}



RlEnvironmentImageBasedVisualServoingTest::~RlEnvironmentImageBasedVisualServoingTest()
{
    f_data_recorder.close();
}

void RlEnvironmentImageBasedVisualServoingTest::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: IMAGE BASED VISUAL SERVOING (TEST) Environment" << std::endl;

    DroneModule::open(n, "RlEnvironmentGazeboRos");

    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Init subscribers
    std::string param_string;
    ros::param::get("~camera_topic", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoingTest::CameraImageCallback, this);

    ros::param::get("~camera_info_topic", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    camera_info_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoingTest::CameraInfoCallback, this);
//    camera_image_subs_ = n.subscribe("/bottom_cam/image_raw", 1, &RlEnvironmentImageBasedVisualServoingTest::CameraImageCallback, this);
//    camera_info_subs_ = n.subscribe("/bottom_cam/camera_info", 1, &RlEnvironmentImageBasedVisualServoingTest::CameraInfoCallback, this);

    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentImageBasedVisualServoingTest::PoseVelocityCallback, this);

    ros::param::get("~visual_servoing_reference", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    visual_servoing_measurement_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoingTest::VisualServoingMeasurementCallback, this);

    ros::param::get("~esimtated_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_estimated_pose_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoingTest::DroneEstimatedPoseCallback, this);

    ros::param::get("~control_mode", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    control_mode_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoingTest::ControlModeCallback, this);


    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
//    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone7/droneSpeedsRefs", 1000);
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~target_locked", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    visual_servoing_target_locked_pub_ = n.advertise<std_msgs::Bool>(param_string, 1, true);

    ros::param::get("~env_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    rl_environment_state_pub_ = n.advertise<std_msgs::Float32MultiArray>(param_string, 1, true);

    ros::param::get("~image_ref", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    rl_reference_bottom_image_pub_ = n.advertise<sensor_msgs::Image>(param_string, 1, this);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);


    // Init recording experiment
//    exp_rec_.Open(n);

    //f_data_recorder.open("/media/carlos/DATA/TesisCVG/PAPERS/MIS_PAPERS/FullyAutonomous_JINT2017/Experiments/Simulation/Reinforcement_Learning/TestData/Network1400_test_data_POSE.txt");
}

bool RlEnvironmentImageBasedVisualServoingTest::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{

}

bool RlEnvironmentImageBasedVisualServoingTest::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality ImageBasedVisualServoing service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;


    if(POSITION_CONTROL_MODE)
    {
        actions_max_value_ = 2.0;
        actions_min_value_ = -2.0;
    }
    else
    {
        if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
        {
            actions_max_value_ = 0.5;
            actions_min_value_ = -0.5;
        }
        else
        {
            actions_max_value_ = 0.5;
            actions_min_value_ = -0.5;
        }
    }

    // Action max
    std::vector<float> actions_max = {actions_max_value_, actions_max_value_};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {actions_min_value_, actions_min_value_};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentImageBasedVisualServoingTest::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{

}



bool RlEnvironmentImageBasedVisualServoingTest::Reset()
{

}


void RlEnvironmentImageBasedVisualServoingTest::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    if(!moduleStarted)
        return;

    cv::Point3f target_pose;
    cv::Point3f uav_pose;
    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            uav_pose.x = msg->pose[i].position.x;
            uav_pose.y = msg->pose[i].position.y;
            uav_pose.z = msg->pose[i].position.z;

            // Calculating Roll, Pitch, Yaw
            tf::Quaternion q(msg->pose[i].orientation.x, msg->pose[i].orientation.y, msg->pose[i].orientation.z, msg->pose[i].orientation.w);
            tf::Matrix3x3 m(q);

            //convert quaternion to euler angels
            double y, p, r;
            m.getEulerYPR(y, p, r);

            uav_state_.pos_x_ = uav_pose.x;
            uav_state_.pos_y_ = uav_pose.y;
            uav_state_.pos_z_ = uav_pose.z;

            uav_state_.yaw_ = y;
            uav_state_.pitch_ = p;
            uav_state_.roll_ = r;

            rot_mat_2d_yaw_.at<float>(0,0) = cos(uav_state_.yaw_); rot_mat_2d_yaw_.at<float>(0,1) = -sin(uav_state_.yaw_);
            rot_mat_2d_yaw_.at<float>(1,0) = sin(uav_state_.yaw_); rot_mat_2d_yaw_.at<float>(1,1) = cos(uav_state_.yaw_);

            break;
        }

        else if(msg->name[i].compare(MOVING_TARGET_NAME) == 0)
        {
            target_pose.x = msg->pose[i].position.x;
            target_pose.y = msg->pose[i].position.y;
            target_pose.z = msg->pose[i].position.z;
        }
    }

    if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
    {
        cv::Point3f uav_target_relative_pose;
        uav_target_relative_pose.x = uav_pose.x - target_pose.x;
        uav_target_relative_pose.y = uav_pose.y - target_pose.y;
        uav_target_relative_pose.z = uav_pose.z;
        ComputeGroundTruthObjectPositionInImage(uav_target_relative_pose.x, uav_target_relative_pose.y,
                                                uav_target_relative_pose.z);
    }

//    f_data_recorder<<uav_pose.x<<" ; "<<uav_pose.y<<" ; "<<uav_pose.z<<" ; "
//                  <<target_pose.x<<" ; "<<target_pose.y<<" ; "<<target_pose.z<<std::endl;


}

void RlEnvironmentImageBasedVisualServoingTest::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
    if(!moduleStarted)
        return;

    last_drone_estimated_GMRwrtGFF_pose_  = msg;
    //yaw pitch roll
    if(UAV_POSE_FROM_EKF)
    {
        uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
        uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
        uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

        uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
        uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
        uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;

        rot_mat_2d_yaw_.at<float>(0,0) = cos(uav_state_.yaw_); rot_mat_2d_yaw_.at<float>(0,1) = -sin(uav_state_.yaw_);
        rot_mat_2d_yaw_.at<float>(1,0) = sin(uav_state_.yaw_); rot_mat_2d_yaw_.at<float>(1,1) = cos(uav_state_.yaw_);
    }




    if(ROLL_PITCH_RECTIFICATION)
    {
        cv::Mat R = convertFromRPYtoRotationMatrix(uav_state_.yaw_, uav_state_.pitch_, uav_state_.roll_);
        camera_info_.R_matrix_ = R.clone();
    //    std::cout<<"R: "<<camera_info_.R_matrix_<<std::endl;
    }

    return;
}

void RlEnvironmentImageBasedVisualServoingTest::VisualServoingMeasurementCallback(const opencv_apps::RotatedRectStamped& msg)
{
    if(!moduleStarted)
        return;

    if(!USE_OWN_OBJECT_DETECTOR)
    {
    //Save the previous Detected Object ROI for computing Speeds
    object_in_image_info_.previous_detected_roi_.angle  = object_in_image_info_.detected_roi_.angle;
    object_in_image_info_.previous_detected_roi_.center.x = object_in_image_info_.detected_roi_.center.x;
    object_in_image_info_.previous_detected_roi_.center.y = object_in_image_info_.detected_roi_.center.y;
    object_in_image_info_.previous_detected_roi_.size.width = object_in_image_info_.detected_roi_.size.width;
    object_in_image_info_.previous_detected_roi_.size.height = object_in_image_info_.detected_roi_.size.height;


    //Update CURRENT detected ROI
    object_in_image_info_.detected_roi_.angle = msg.rect.angle;
    object_in_image_info_.detected_roi_.center.x = msg.rect.center.x;
    object_in_image_info_.detected_roi_.center.y = msg.rect.center.y;
    object_in_image_info_.detected_roi_.size.width = msg.rect.size.width;
    object_in_image_info_.detected_roi_.size.height = msg.rect.size.height;


    //Extract the 4 RotatedRect CORNERS and sort them and save into the object_in_image_info struct
    object_in_image_info_.rotated_rect_points_.clear();
    cv::Point2f corner_points[4];
    object_in_image_info_.detected_roi_.points(corner_points);
    std::vector<cv::Point2f> rotated_rect_points;
    for(int j = 0; j < 4; j++)
        rotated_rect_points.push_back(corner_points[j]);

    object_in_image_info_.rotated_rect_points_.insert(object_in_image_info_.rotated_rect_points_.begin(),
                                                      rotated_rect_points.begin(), rotated_rect_points.end());
    }


    if(ROLL_PITCH_RECTIFICATION)
    {
        ComputeCompensatedRotatedRectByRpy();
    }

    ComputeObjectPositionBasedOnStabilizedDetection();
    ComputeNormalizedObjectInImageState();
    ComputeDistanceToTargetLocked();


    if(DEBUG_MODE)
    {
        std::cout<<"Detected ROI [x, y, w, h]: "<<"["<<object_in_image_info_.detected_roi_.boundingRect().x<<" ; "<<
                object_in_image_info_.detected_roi_.boundingRect().y<<" ; "<<object_in_image_info_.detected_roi_.boundingRect().width<<" ; "<<
                object_in_image_info_.detected_roi_.boundingRect().height<<"]"<<std::endl;
    }

}


void RlEnvironmentImageBasedVisualServoingTest::CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &msg)
{
    if(!camera_info_.camera_info_available_flag_)
    {
        camera_info_.camera_info_available_flag_ = true;

        camera_info_.cx_ = msg->K.at(2);
        camera_info_.cy_ = msg->K.at(5);
        camera_info_.fx_ = msg->K.at(0);
        camera_info_.fy_ = msg->K.at(4);

        camera_info_.image_width_ = msg->width;
        camera_info_.image_height_ = msg->height;

        camera_info_.K_matrix_ = cv::Mat(3, 3, CV_32FC1);
        camera_info_.K_matrix_.at<float>(0,0) = camera_info_.fx_;
        camera_info_.K_matrix_.at<float>(0,1) = 0.0;
        camera_info_.K_matrix_.at<float>(0,2) = camera_info_.cx_;

        camera_info_.K_matrix_.at<float>(1,0) = 0.0;
        camera_info_.K_matrix_.at<float>(1,1) = camera_info_.fy_;
        camera_info_.K_matrix_.at<float>(1,2) = camera_info_.cy_;

        camera_info_.K_matrix_.at<float>(2,0) = 0.0;
        camera_info_.K_matrix_.at<float>(2,1) = 0.0;
        camera_info_.K_matrix_.at<float>(2,2) = 1.0;

        //std::cout<<"Camera Matrix: "<<std::endl<<camera_info_.K_matrix_<<std::endl<<std::endl;
        ComputeObjectReferenceInImage();
    }
}

void RlEnvironmentImageBasedVisualServoingTest::CameraImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    if(IMSHOW_MODE)
    {
        try
        {
            captured_image_.release();
            cv::Mat img = cv_bridge::toCvShare(msg, "bgr8")->image;
            captured_image_ = img.clone();
            cv_object_rl_reference_ = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            if(USE_OWN_OBJECT_DETECTOR)
                ComputeDetectedObjectInImageState(img.clone(), "Red");
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
        }
    }
}

void RlEnvironmentImageBasedVisualServoingTest::ControlModeCallback(const droneMsgsROS::droneTrajectoryControllerControlMode::ConstPtr &msg)
{
    if(!moduleStarted)
        return;

    switch (msg->command)
    {
    case droneMsgsROS::droneTrajectoryControllerControlMode::TRAJECTORY_CONTROL:
        last_received_control_mode_ = static_cast<int>(droneMsgsROS::droneTrajectoryControllerControlMode::TRAJECTORY_CONTROL);
        break;
    case droneMsgsROS::droneTrajectoryControllerControlMode::POSITION_CONTROL:
        last_received_control_mode_ = static_cast<int>(droneMsgsROS::droneTrajectoryControllerControlMode::POSITION_CONTROL);
        break;
    case droneMsgsROS::droneTrajectoryControllerControlMode::SPEED_CONTROL:
        last_received_control_mode_ = static_cast<int>(droneMsgsROS::droneTrajectoryControllerControlMode::SPEED_CONTROL);
        //std::cout<<"SPEED Control Mode!"<<std::endl;
        break;

    default:
        last_received_control_mode_ = static_cast<int>(droneMsgsROS::droneTrajectoryControllerControlMode::UNKNOWN_CONTROL_MODE);
        break;
    }
}


void RlEnvironmentImageBasedVisualServoingTest::SetUavState(const float x, const float y, const float z, const float dx, const float dy)
{
    uav_mutex_.lock();
    uav_state_.pos_x_ = x;
    uav_state_.pos_y_ = y;
    uav_state_.pos_z_ = z;
    uav_state_.speed_x_ = dx;
    uav_state_.speed_y_ = dy;
    uav_mutex_.unlock();
}

void RlEnvironmentImageBasedVisualServoingTest::GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float pitch)
{
    uav_mutex_.lock();
    x = uav_state_.pos_x_;
    y = uav_state_.pos_y_;
    z = uav_state_.pos_z_;
    dx = uav_state_.speed_x_;
    dy = uav_state_.speed_y_;
    roll = uav_state_.roll_;
    pitch = uav_state_.pitch_;
    uav_mutex_.unlock();
}

void RlEnvironmentImageBasedVisualServoingTest::GetObjectInImageState(cv::RotatedRect &previous_roi, cv::RotatedRect &current_roi)
{
    object_in_image_mutex_.lock();

    if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
    {
        previous_roi = object_in_image_info_.ground_truth_previous_detected_roi_;
        current_roi = object_in_image_info_.ground_truth_detected_roi_;
    }
    else if(OBJECT_STATE_BASED_ON_STABILIZED_DETECTION)
    {
        previous_roi = object_in_image_info_.stabilized_previous_detected_roi_;
        current_roi = object_in_image_info_.stabilized_detected_roi_;
    }
    else
    {
        previous_roi = object_in_image_info_.previous_detected_roi_;
        current_roi = object_in_image_info_.detected_roi_;
    }

    object_in_image_mutex_.unlock();
}


std::vector<float> RlEnvironmentImageBasedVisualServoingTest::ComputeNormalizedObjectInImageState()
{
    state_.clear();
    normalized_state_.clear();
    cv::RotatedRect previous_object_roi;
    cv::RotatedRect current_object_roi;
    GetObjectInImageState(previous_object_roi, current_object_roi);


    //Change for other reference position in Image.
    error_in_image_.x = (current_object_roi.center.x - center_roi_ref_.x);
    error_in_image_.y = (current_object_roi.center.y - center_roi_ref_.y);

    //Compute the 2D Transformation for removing the YAW
    cv::Mat error_in_image_mat = cv::Mat(2, 1, CV_32FC1);
    cv::Mat error_in_image_mat_transformed = cv::Mat(2, 1, CV_32FC1);
    error_in_image_mat.at<float>(0,0) = error_in_image_.x; error_in_image_mat.at<float>(1,0) = error_in_image_.y;
    error_in_image_mat_transformed = rot_mat_2d_yaw_.inv() * error_in_image_mat;
    error_in_image_.x = error_in_image_mat_transformed.at<float>(0,0);
    error_in_image_.y = error_in_image_mat_transformed.at<float>(1,0);
    error_in_image_norm_.x = error_in_image_.x/center_roi_ref_max_.x;
    error_in_image_norm_.y = error_in_image_.y/center_roi_ref_max_.y;


    //Compute the velocity of the Object in the Image plane
    cv::Point2f delta_pos_object_roi_center, object_roi_center_speed_norm;
    delta_pos_object_roi_center.x = current_object_roi.center.x - previous_object_roi.center.x;
    delta_pos_object_roi_center.y = current_object_roi.center.y - previous_object_roi.center.y;
    object_roi_center_speed_norm.x = delta_pos_object_roi_center.x/center_roi_ref_max_.x;
    object_roi_center_speed_norm.y = delta_pos_object_roi_center.y/center_roi_ref_max_.y;


    double x_t, dx_t;
    double y_t, dy_t;
    float upper_limit = 1.0;
    if(!OBJECT_STATE_BASED_ON_GROUND_TRUTH)
        upper_limit = 1.0;

    current_time_ = ros::Time::now();
    if(ESTIMATE_SPEEDS_WITH_CIRCULAR_BUFFER)
    {
        // Compute speeds from pose ground truth
        double x_raw_t = error_in_image_.x;
        double y_raw_t = error_in_image_.y;
//        std::cout<<"x_raw_t (Circ Buffer): "<<x_raw_t<<std::endl;
//        std::cout<<"y_raw_t (Circ Buffer): "<<y_raw_t<<std::endl;

        time_t tv_sec; suseconds_t tv_usec;
        tv_sec  = current_time_.sec;
        tv_usec = current_time_.nsec / 1000.0;
        filtered_derivative_wcb_x_.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_y_.setInput( y_raw_t, tv_sec, tv_usec);


        filtered_derivative_wcb_x_.getOutput( x_t,  dx_t);
        filtered_derivative_wcb_y_.getOutput( y_t,  dy_t);
//        if(delta_t>0)
//        {
//            std::cout<<"Estimated Speed (delta_pos_x): "<<delta_pos_object_roi_center.x/delta_t <<std::endl;
//            std::cout<<"Estimated Speed (delta_pos_y): "<<delta_pos_object_roi_center.y/delta_t<<std::endl;
//        }
//        std::cout<<"dx_t (Circ Buffer): "<<dx_t<<std::endl;
//        std::cout<<"dy_t (Circ Buffer): "<<dy_t<<std::endl;

        object_roi_center_speed_norm.x = dx_t/center_roi_ref_max_.x;
        object_roi_center_speed_norm.y = dy_t/center_roi_ref_max_.y;

        if(object_roi_center_speed_norm.x > upper_limit)
            object_roi_center_speed_norm.x = upper_limit;
        else if(object_roi_center_speed_norm.x < -upper_limit)
            object_roi_center_speed_norm.x = -upper_limit;

        if(object_roi_center_speed_norm.y > upper_limit)
            object_roi_center_speed_norm.y = upper_limit;
        else if(object_roi_center_speed_norm.y < -upper_limit)
            object_roi_center_speed_norm.y = -upper_limit;

        //f_data_recorder<<delta_pos_object_roi_center.x/delta_t<<" ; "<<delta_pos_object_roi_center.y/delta_t<<" ; "<<dx_t<<" ; "<<dy_t<<std::endl;
    }



    if(OBJECT_STATE_BASED_ON_ROTATED_RECT)
    {

        for(int i=0;i<object_in_image_info_.rotated_rect_points_.size();i++)
        {
            float delta_x = (object_in_image_info_.rotated_rect_points_[i].x - center_roi_ref_.x);
            float delta_y = (object_in_image_info_.rotated_rect_points_[i].y - center_roi_ref_.y);
            float alpha = std::atan2(delta_y, delta_x);

            //Substract the radius of the reference
            float delta_x_substracted = delta_x - radius_ref_ * std::cos(alpha);
            float delta_y_substracted = delta_y - radius_ref_ * std::sin(alpha);

//            std::cout<<"Distance point (radius substracted)["<<i<<"]: "<<delta_x_substracted<<std::endl;
//            std::cout<<"Distance point (radius substracted)["<<i<<"]: "<<delta_y_substracted<<std::endl;

            float delta_x_norm = delta_x_substracted/center_roi_ref_max_.x;
            float delta_y_norm = delta_y_substracted/center_roi_ref_max_.y;


            state_.push_back(delta_x);
            state_.push_back(delta_y);
            normalized_state_.push_back(delta_x_norm);
            normalized_state_.push_back(delta_y_norm);
        }
        //std::cout<<std::endl<<std::endl;
        state_.push_back(delta_pos_object_roi_center.x);
        state_.push_back(delta_pos_object_roi_center.y);
        normalized_state_.push_back(object_roi_center_speed_norm.x);
        normalized_state_.push_back(object_roi_center_speed_norm.y);
    }
    else
    {
        if(OBJECT_STATE_BASED_ON_ERROR_IN_POSITION_ONLY)
        {
            normalized_state_.push_back(error_in_image_norm_.x);
            normalized_state_.push_back(error_in_image_norm_.y);
        }
        else
        {
            normalized_state_.push_back(error_in_image_norm_.x);
            normalized_state_.push_back(error_in_image_norm_.y);
            normalized_state_.push_back(object_roi_center_speed_norm.x);
            normalized_state_.push_back(object_roi_center_speed_norm.y);
        }
//        f_data_recorder<<error_in_image_norm_.x<<" ; "<<error_in_image_norm_.y<<" ; "
//                      <<object_roi_center_speed_norm.x<<" ; "<<object_roi_center_speed_norm.y<<" ; "<<dx_t<<" ; "<<dy_t<<std::endl;
//        f_data_recorder<<error_in_image_norm_.x<<" ; "<<error_in_image_norm_.y<<" ; "
//                      <<object_roi_center_speed_norm.x<<" ; "<<object_roi_center_speed_norm.x.y<<" ; "<<dx_t<<" ; "<<dy_t<<std::endl;

    }

    if(OBJECT_STATE_INCLUDING_ROLL_PITCH)
    {
        // Get state
        float x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch;
        GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch);

        normalized_state_.push_back(roll/pitch_roll_max_value_);
        normalized_state_.push_back(roll/pitch_roll_max_value_);

    }



    if(DEBUG_MODE)
    {
        std::cout<<"++++ CURRENT STATE ++++"<<std::endl;
        for(int i=0;i<normalized_state_.size();i++)
        {
            if(i==0)
                std::cout<<"[";
            std::cout<<normalized_state_[i]<<" ; ";
            if(i == normalized_state_.size()-1)
                std::cout<<"]"<<std::endl;

        }
    }

    std_msgs::Float32MultiArray state_msg;

    // set up dimensions
    state_msg.layout.dim.push_back(std_msgs::MultiArrayDimension());
    state_msg.layout.dim[0].size = normalized_state_.size();
    state_msg.layout.dim[0].stride = 1;
    state_msg.layout.dim[0].label = "x"; // or whatever name you typically use to index vec1
    state_msg.data.insert(state_msg.data.end(), normalized_state_.begin(), normalized_state_.end());
    rl_environment_state_pub_.publish(state_msg);

    if(IMSHOW_MODE)
        DrawResults();

    return normalized_state_;
}


void RlEnvironmentImageBasedVisualServoingTest::ComputeDistanceToTargetLocked()
{

    std::vector<float> state = GetNormalizedState();

    errors_array_.push_back(std::sqrt(std::pow(state[0],2) + std::pow(state[1],2)));


    double mean_error = 9999;
    if(errors_array_.size()>=ERRORS_ARRAY_LENGTH)
    {
        mean_error = std::accumulate(errors_array_.begin(), errors_array_.end(), 0.0) / errors_array_.size();
        errors_array_.erase(errors_array_.begin());
        //std::cout<<"Mean Error for Target Locked: "<<mean_error<<std::endl;
    }

    if(mean_error <= MAX_ERROR_FOR_TARGET_LOCK)
    {
        std::cout<<"Target Locked!"<<std::endl;
        std_msgs::Bool msg;
        msg.data = true;
        visual_servoing_target_locked_pub_.publish(msg);
    }
    mean_errors_array_ = mean_error;

}




void RlEnvironmentImageBasedVisualServoingTest::ComputeObjectReferenceInImage()
{
    if(camera_info_.camera_info_available_flag_)
    {
        cv::Mat p1 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[0];
        cv::Mat p2 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[1];
        cv::Mat p3 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[2];
        cv::Mat p4 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[3];

        cv::Mat p1_image = p1/p1.at<float>(2,0);
        cv::Mat p2_image = p2/p2.at<float>(2,0);
        cv::Mat p3_image = p3/p3.at<float>(2,0);
        cv::Mat p4_image = p4/p4.at<float>(2,0);

        roi_reference_ = cv::Rect(cv::Point(p1_image.at<float>(0,0), p1_image.at<float>(1,0)),
                                  cv::Point(p3_image.at<float>(0,0), p3_image.at<float>(1,0)));

        cv::Point2f diagonal_ref;
        diagonal_ref.x = p3_image.at<float>(0,0) - p1_image.at<float>(0,0);
        diagonal_ref.y = p3_image.at<float>(1,0) - p1_image.at<float>(1,0);
        center_roi_ref_.x = p1_image.at<float>(0,0) + diagonal_ref.x/2;
        center_roi_ref_.y = p1_image.at<float>(1,0) + diagonal_ref.y/2;
        radius_ref_ = std::sqrt(std::pow(diagonal_ref.x, 2) + std::pow(diagonal_ref.y, 2))/2;

        if(COMPUTE_NORM_VALUE_WRT_ROI_REFERENCE)
        {
            if(center_roi_ref_.x >= (camera_info_.image_width_ - center_roi_ref_.x))
                center_roi_ref_max_.x = center_roi_ref_.x;
            else
                center_roi_ref_max_.x = camera_info_.image_width_ - center_roi_ref_.x;

            if(center_roi_ref_.y >= (camera_info_.image_height_ - center_roi_ref_.y))
                center_roi_ref_max_.y = center_roi_ref_.y;
            else
                center_roi_ref_max_.y = camera_info_.image_height_ - center_roi_ref_.y;
        }
        else
        {
            center_roi_ref_max_.x = camera_info_.image_width_/2.0;
            center_roi_ref_max_.y = camera_info_.image_height_/2.0;
        }

        std::cout<<"center_roi_ref_.x: "<<center_roi_ref_.x<<std::endl;
        std::cout<<"center_roi_ref_.y: "<<center_roi_ref_.y<<std::endl;

        std::cout<<"p1_image: "<<p1_image<<std::endl;
        std::cout<<"p2_image: "<<p2_image<<std::endl;
        std::cout<<"p3_image: "<<p3_image<<std::endl;
        std::cout<<"p4_image: "<<p4_image<<std::endl;
        std::cout<<"ROI Reference: "<<roi_reference_<<std::endl;

        std::cout<<"center_roi_ref_max_.x: "<<center_roi_ref_max_.x<<std::endl;
        std::cout<<"center_roi_ref_max_.y: "<<center_roi_ref_max_.y<<std::endl;


        speed_roi_ref_max_.x = center_roi_ref_max_.x * 30; //ShapeColor_ObjectDetector FRECUENCY
        speed_roi_ref_max_.y = center_roi_ref_max_.y * 30;
    }
}

void RlEnvironmentImageBasedVisualServoingTest::DrawResults()
{
    cv::Mat I_for_drawing = captured_image_.clone();
    if(object_in_image_info_.detected_roi_.boundingRect().area() > 0 && !captured_image_.empty())
    {
        cv::Point2f corner_points[4];
        object_in_image_info_.detected_roi_.points(corner_points);
        for(unsigned int j = 0; j < 4; j++)
        {
            cv::circle(I_for_drawing, corner_points[j], 3, cv::Scalar(0, 255, 255), 1);
            cv::line(I_for_drawing, corner_points[j], corner_points[(j+1)%4], cv::Scalar(0, 255, 0), 2, 8);
        }


        if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
        {
//            for(unsigned int i = 0; i < 4; i++)
//            {
//                cv::Point2f p_truth = object_in_image_info_.ground_truth_current_object_points_[i];
//                cv::Point2f p_truth2 = object_in_image_info_.ground_truth_current_object_points_[(i+1)%4];
//                cv::circle(I_for_drawing, p_truth, 3, cv::Scalar(0, 255, 255), 1);
//                cv::line(I_for_drawing, p_truth, p_truth2, cv::Scalar(255, 128, 255), 2, 8);
//            }
            cv::Point2f ground_truth_roi_center;
            float ground_truth_roi_width = object_in_image_info_.ground_truth_current_object_points_[1].x
                    - object_in_image_info_.ground_truth_current_object_points_[0].x;

            float ground_truth_roi_height = object_in_image_info_.ground_truth_current_object_points_[3].y
                    - object_in_image_info_.ground_truth_current_object_points_[0].y;
            ground_truth_roi_center.x = object_in_image_info_.ground_truth_current_object_points_[0].x
                    + ground_truth_roi_width/2.0;
            ground_truth_roi_center.y = object_in_image_info_.ground_truth_current_object_points_[0].y
                    + ground_truth_roi_height/2.0;

            int line_offset = 7;
            cv::Point line1_ini_gt_center = cv::Point(ground_truth_roi_center.x, ground_truth_roi_center.y - line_offset);
            cv::Point line1_end_gt_center = cv::Point(ground_truth_roi_center.x, ground_truth_roi_center.y + line_offset);
            cv::Point line2_ini_gt_center = cv::Point(ground_truth_roi_center.x - line_offset, ground_truth_roi_center.y);
            cv::Point line2_end_gt_center = cv::Point(ground_truth_roi_center.x + line_offset, ground_truth_roi_center.y);
            cv::line(I_for_drawing, line1_ini_gt_center, line1_end_gt_center, cv::Scalar(255, 255, 0), 2);
            cv::line(I_for_drawing, line2_ini_gt_center, line2_end_gt_center, cv::Scalar(255, 255, 0), 2);
            cv::Point2f ground_truth_corner_points[4];
            object_in_image_info_.ground_truth_detected_roi_.points(ground_truth_corner_points);
            for(int j = 0; j < 4; j++)
            {
                cv::circle(I_for_drawing, ground_truth_corner_points[j], 3, cv::Scalar(255, 255, 255), 1);
                cv::line(I_for_drawing, ground_truth_corner_points[j], ground_truth_corner_points[(j+1)%4], cv::Scalar(255, 255, 0), 2, 8);
            }
        }

        if(OBJECT_STATE_BASED_ON_ROTATED_RECT)
        {
            char corner_point_id[10];
            for(unsigned int j = 0; j < object_in_image_info_.rotated_rect_points_.size(); j++)
            {
                std::sprintf(corner_point_id, "%d", j);
                cv::putText(I_for_drawing, corner_point_id, object_in_image_info_.rotated_rect_points_[j],
                            cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(255, 255, 255), 2);
                cv::line(I_for_drawing,  cv::Point(center_roi_ref_.x, center_roi_ref_.y),
                         object_in_image_info_.rotated_rect_points_[j], cv::Scalar(0, 255, 255), 2, 8);
            }

        }
        else
        {
            cv::RotatedRect previous_object_roi;
            cv::RotatedRect current_object_roi;
            GetObjectInImageState(previous_object_roi, current_object_roi);
            cv::line(I_for_drawing,  cv::Point(center_roi_ref_.x, center_roi_ref_.y),
                     cv::Point(current_object_roi.center.x, current_object_roi.center.y), cv::Scalar(0, 255, 255), 2, 8);
        }


        if(IMSHOW_STABILIZED_DETECTED_OBJECT)
        {
            cv::rectangle(I_for_drawing, object_in_image_info_.stabilized_detected_roi_rect_, cv::Scalar(0, 255, 255), 2, 4);
            cv::Point line1_ini = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x, object_in_image_info_.stabilized_detected_roi_.center.y - 10);
            cv::Point line1_end = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x, object_in_image_info_.stabilized_detected_roi_.center.y + 10);
            cv::Point line2_ini = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x - 10, object_in_image_info_.stabilized_detected_roi_.center.y);
            cv::Point line2_end = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x + 10, object_in_image_info_.stabilized_detected_roi_.center.y);
            cv::line(I_for_drawing, line1_ini, line1_end, cv::Scalar(0, 255, 255), 2);
            cv::line(I_for_drawing, line2_ini, line2_end, cv::Scalar(0, 255, 255), 2);
        }



        //cv::rectangle(I_for_drawing, object_in_image_info_.detected_roi_.boundingRect(), cv::Scalar(0, 255, 255), 2, 4);
        if(!OBJECT_STATE_BASED_ON_GROUND_TRUTH)
        {
            cv::Point line1_ini = cv::Point(object_in_image_info_.detected_roi_.center.x, object_in_image_info_.detected_roi_.center.y - 10);
            cv::Point line1_end = cv::Point(object_in_image_info_.detected_roi_.center.x, object_in_image_info_.detected_roi_.center.y + 10);
            cv::Point line2_ini = cv::Point(object_in_image_info_.detected_roi_.center.x - 10, object_in_image_info_.detected_roi_.center.y);
            cv::Point line2_end = cv::Point(object_in_image_info_.detected_roi_.center.x + 10, object_in_image_info_.detected_roi_.center.y);
            cv::line(I_for_drawing, line1_ini, line1_end, cv::Scalar(0, 255, 0), 2);
            cv::line(I_for_drawing, line2_ini, line2_end, cv::Scalar(0, 255, 0), 2);
        }

        cv::rectangle(I_for_drawing, roi_reference_, cv::Scalar(255, 0, 0), 2);
        cv::circle(I_for_drawing, cv::Point(center_roi_ref_.x, center_roi_ref_.y), radius_ref_, cv::Scalar(255, 0, 0), 2);
        cv::Point line1_ini_ref = cv::Point(center_roi_ref_.x, center_roi_ref_.y - 10);
        cv::Point line1_end_ref = cv::Point(center_roi_ref_.x, center_roi_ref_.y + 10);
        cv::Point line2_ini_ref = cv::Point(center_roi_ref_.x - 10, center_roi_ref_.y);
        cv::Point line2_end_ref = cv::Point(center_roi_ref_.x + 10, center_roi_ref_.y);
        cv::line(I_for_drawing, line1_ini_ref, line1_end_ref, cv::Scalar(255, 0, 0), 2);
        cv::line(I_for_drawing, line2_ini_ref, line2_end_ref, cv::Scalar(255, 0, 0), 2);


        char error_in_image_text[10];
        if(mean_errors_array_ != 9999)
        {
            sprintf(error_in_image_text,"%.5f", mean_errors_array_);
            cv::putText(I_for_drawing, "mean_error: ", cv::Point(20, 50), cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 0, 0), 2);
            cv::putText(I_for_drawing, error_in_image_text, cv::Point(140, 50), cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 0, 0), 2);
        }


        if(mean_errors_array_ <= MAX_ERROR_FOR_TARGET_LOCK)
        {
            cv::putText(I_for_drawing, "Target Locked! ", cv::Point(20, 80), cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 0, 0), 2);
        }

        cv::Point p1(image_boundaries_offset_, image_boundaries_offset_);
        cv::Point p2(camera_info_.image_width_ - image_boundaries_offset_, camera_info_.image_height_ - image_boundaries_offset_);
        cv::rectangle(I_for_drawing, p1, p2, cv::Scalar(128, 0, 128), 2, 4); //To plot the boundaries for terminal state


        cv_object_rl_reference_->image = I_for_drawing;
        rl_reference_bottom_image_pub_.publish(cv_object_rl_reference_->toImageMsg());
        cv::imshow("Received Image From Environment", I_for_drawing);
        cv::waitKey(1);
    }
}

cv::Mat RlEnvironmentImageBasedVisualServoingTest::convertFromRPYtoRotationMatrix(const float alpha, const float beta, const float gamma)
{
    cv::Mat R = cv::Mat(3, 3, CV_32FC1);
    R.at<float>(0,0) = cos(alpha)*cos(beta);
    R.at<float>(0,1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
    R.at<float>(0,2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);

    R.at<float>(1,0) = sin(alpha)*cos(beta);
    R.at<float>(1,1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
    R.at<float>(1,2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);

    R.at<float>(2,0) = -sin(beta);
    R.at<float>(2,1) = cos(beta)*sin(gamma);
    R.at<float>(2,2) = cos(beta)*cos(gamma);

    return R;

}


void RlEnvironmentImageBasedVisualServoingTest::ComputeCompensatedRotatedRectByRpy()
{
    object_in_image_info_.rotated_rect_points_rpy_compensated_.clear();
    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);
    if(camera_info_.camera_info_available_flag_)
    {
//        std::cout<<std::endl<<"K: "<<camera_info_.K_matrix_<<std::endl;
//        std::cout<<std::endl<<"R: "<<camera_info_.R_matrix_<<std::endl;
        H = camera_info_.K_matrix_ * camera_info_.R_matrix_ * camera_info_.K_matrix_.inv();
//        std::cout<<"H: "<<H<<std::endl;

        for(int i = 0;i<object_in_image_info_.rotated_rect_points_.size();i++)
        {
            cv::Mat p_mat = cv::Mat(3, 1, CV_32FC1);
            cv::Mat p_mat_dst = cv::Mat(3, 1, CV_32FC1);


            p_mat.at<float>(0,0) = object_in_image_info_.rotated_rect_points_[i].x;
            p_mat.at<float>(1,0) = object_in_image_info_.rotated_rect_points_[i].y;
            p_mat.at<float>(2,0) = 1;

            p_mat_dst = H * p_mat;
//            std::cout<<"source points: "<<p_mat<<std::endl;
//            std::cout<<"dst points: "<<p_mat_dst<<std::endl;
            cv::Point2f p_dst = cv::Point2f(p_mat_dst.at<float>(0,0), p_mat_dst.at<float>(1,0));
            object_in_image_info_.rotated_rect_points_rpy_compensated_.push_back(p_dst);
        }
    }
}

void RlEnvironmentImageBasedVisualServoingTest::ComputeGroundTruthObjectPositionInImage(const float &x_uav, const float &y_uav, const float &z_uav)
{
    if(camera_info_.camera_info_available_flag_)
    {
        t_cam_world_.at<float>(0,3) = -x_uav; t_cam_world_.at<float>(1,3) = -y_uav; t_cam_world_.at<float>(2,3) = -z_uav;

//        std::cout<<"rot_mat_z_90_: "<<rot_mat_z_90_<<std::endl;
//        std::cout<<"rot_mat_x180_: "<<rot_mat_x180_<<std::endl;
//        std::cout<<"t_cam_world_: "<<t_cam_world_<<std::endl;

        cv::Mat T_cam_world = rot_mat_z_90_ * rot_mat_x180_ * t_cam_world_;


        object_in_world_info_.current_points_mat_[0] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[0].at<float>(0,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[0].at<float>(1,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[0].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[0].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[1] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[1].at<float>(0,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[1].at<float>(1,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[1].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[1].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[2] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[2].at<float>(0,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[2].at<float>(1,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[2].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[2].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[3] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[3].at<float>(0,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[3].at<float>(1,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[3].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[3].at<float>(3,0) = 1.0;


        //Points Referred to Camera frame of reference
        cv::Mat p1 = T_cam_world * object_in_world_info_.current_points_mat_[0]; p1.pop_back(1);
        cv::Mat p2 = T_cam_world * object_in_world_info_.current_points_mat_[1]; p2.pop_back(1);
        cv::Mat p3 = T_cam_world * object_in_world_info_.current_points_mat_[2]; p3.pop_back(1);
        cv::Mat p4 = T_cam_world * object_in_world_info_.current_points_mat_[3]; p4.pop_back(1);

//        std::cout<<"T_cam_world: "<<T_cam_world<<std::endl;
//        std::cout<<"p1: "<<p1<<std::endl;
//        std::cout<<"p2: "<<p2<<std::endl;
//        std::cout<<"p3: "<<p3<<std::endl;
//        std::cout<<"p4: "<<p4<<std::endl;


        p1 = camera_info_.K_matrix_ * p1;
        p2 = camera_info_.K_matrix_ * p2;
        p3 = camera_info_.K_matrix_ * p3;
        p4 = camera_info_.K_matrix_ * p4;

        cv::Mat p1_image = p1/p1.at<float>(2,0);
        cv::Mat p2_image = p2/p2.at<float>(2,0);
        cv::Mat p3_image = p3/p3.at<float>(2,0);
        cv::Mat p4_image = p4/p4.at<float>(2,0);

        std::vector<cv::Mat> p_image;
        p_image.push_back(p1_image);
        p_image.push_back(p2_image);
        p_image.push_back(p3_image);
        p_image.push_back(p4_image);

        for(int i=0;i<p_image.size();i++)
        {
            object_in_image_info_.ground_truth_current_object_points_[i].x = p_image[i].at<float>(0,0);
            object_in_image_info_.ground_truth_current_object_points_[i].y = p_image[i].at<float>(1,0);
        }


        object_in_image_info_.ground_truth_previous_detected_roi_ = object_in_image_info_.ground_truth_detected_roi_;
        float ground_truth_roi_width = object_in_image_info_.ground_truth_current_object_points_[1].x
                - object_in_image_info_.ground_truth_current_object_points_[0].x;

        float ground_truth_roi_height = object_in_image_info_.ground_truth_current_object_points_[3].y
                - object_in_image_info_.ground_truth_current_object_points_[0].y;

        cv::Point2f ground_truth_roi_center;
        ground_truth_roi_center.x = object_in_image_info_.ground_truth_current_object_points_[0].x
                + ground_truth_roi_width/2.0;
        ground_truth_roi_center.y = object_in_image_info_.ground_truth_current_object_points_[0].y
                + ground_truth_roi_height/2.0;

        object_in_image_info_.ground_truth_detected_roi_ = cv::RotatedRect(ground_truth_roi_center, cv::Size(ground_truth_roi_width, ground_truth_roi_height), 0.0);

    }

}


bool RlEnvironmentImageBasedVisualServoingTest::startVal()
{

    return DroneModule::startVal();
}

bool RlEnvironmentImageBasedVisualServoingTest::stopVal()
{
    ResetValues();
    return DroneModule::stopVal();
}


void RlEnvironmentImageBasedVisualServoingTest::ComputeObjectPositionBasedOnStabilizedDetection()
{
    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);
    if(camera_info_.camera_info_available_flag_)
    {
        float camera_yaw = -uav_state_.yaw_;
        float camera_pitch = -uav_state_.roll_;
        float camera_roll = -uav_state_.pitch_;
        cv::Mat R = ConvertFromYPRtoRotationMatrix(camera_yaw, camera_pitch, camera_roll);

        H = camera_info_.K_matrix_ * R * camera_info_.K_matrix_.inv();


        cv::Mat I_warped;
        cv::warpPerspective(captured_image_.clone(), I_warped, H, captured_image_.size());

        cv::RotatedRect rotated_rect_item;;
        cv::Rect rect_item;
        DetectObjectInImage(I_warped.clone(), "Red", rotated_rect_item, rect_item);


        object_in_image_info_.stabilized_detected_roi_rect_ = rect_item;

        object_in_image_info_.stabilized_previous_detected_roi_ = object_in_image_info_.stabilized_detected_roi_;
        object_in_image_info_.stabilized_detected_roi_ = rotated_rect_item;


//        cv::rectangle(I_warped, object_in_image_info_.stabilized_detected_roi_rect_, cv::Scalar(0, 255, 255), 2);
//        cv::imshow("I warped", I_warped);
//        cv::waitKey(1);

    }

}


void RlEnvironmentImageBasedVisualServoingTest::DetectObjectInImage(const cv::Mat &I, const std::string object_color,
                                                                            cv::RotatedRect &rotated_rect_item, cv::Rect &rect_item)
{
    cv::Mat3b I_hsv;
    cv::cvtColor(I, I_hsv, CV_BGR2HSV);
    cv::Mat1b mask1_hsv, mask2_hsv;


    cv::Mat I_binarized;
    if(object_color == "Red")
    {
        //Real
        cv::inRange(I_hsv, cv::Scalar(0, 100, 100), cv::Scalar(10, 255, 255), mask1_hsv);
        cv::inRange(I_hsv, cv::Scalar(170, 100, 100), cv::Scalar(180, 255, 255), mask2_hsv);

//        cv::inRange(I_hsv, cv::Scalar(0, 70, 20), cv::Scalar(10, 255, 255), mask1_hsv);
//        cv::inRange(I_hsv, cv::Scalar(170, 70, 20), cv::Scalar(180, 255, 255), mask2_hsv);
        I_binarized = mask1_hsv | mask2_hsv;
    }
    else if(object_color == "Blue")
    {
        //cv::inRange(I_hsv, cv::Scalar(90, 100, 50), cv::Scalar(140, 255, 255), I_mask_hsv); //Real
        cv::inRange(I_hsv, cv::Scalar(110, 70, 50), cv::Scalar(130, 255, 255), I_binarized); //Simulation
    }


    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(I_binarized.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);


    std::vector<std::vector<cv::Point> >::iterator itc = contours.begin();
    while(itc != contours.end())
    {
        cv::RotatedRect rot_box = cv::minAreaRect(cv::Mat(*itc));
        float rot_box_area = (float)rot_box.size.width * (float)rot_box.size.height;

        if(rot_box_area < 500)
            itc = contours.erase(itc);
        else
        {
            ++itc;
        }
    }

    if(contours.size())
    {
        rotated_rect_item = cv::minAreaRect(cv::Mat(contours[0]));
        rect_item = cv::boundingRect(cv::Mat(contours[0]));

    }

}

cv::Mat RlEnvironmentImageBasedVisualServoingTest::ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma)
{
    cv::Mat R = cv::Mat(3, 3, CV_32FC1);
    R.at<float>(0,0) = cos(alpha)*cos(beta);
    R.at<float>(0,1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
    R.at<float>(0,2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);

    R.at<float>(1,0) = sin(alpha)*cos(beta);
    R.at<float>(1,1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
    R.at<float>(1,2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);

    R.at<float>(2,0) = -sin(beta);
    R.at<float>(2,1) = cos(beta)*sin(gamma);
    R.at<float>(2,2) = cos(beta)*cos(gamma);

    return R;

}

void RlEnvironmentImageBasedVisualServoingTest::ComputeDetectedObjectInImageState(const cv::Mat &I, const std::string object_color)
{

    cv::RotatedRect rotated_rect_item;
    cv::Rect rect_item;
    DetectObjectInImage(I.clone(), object_color, rotated_rect_item, rect_item);

    if(rect_item.area()>0)
    {

        object_in_image_info_.detected_roi_rect_ = rect_item;
        //Save the previous Detected Object ROI for computing Speeds
        object_in_image_info_.previous_detected_roi_.angle  = object_in_image_info_.detected_roi_.angle;
        object_in_image_info_.previous_detected_roi_.center.x = object_in_image_info_.detected_roi_.center.x;
        object_in_image_info_.previous_detected_roi_.center.y = object_in_image_info_.detected_roi_.center.y;
        object_in_image_info_.previous_detected_roi_.size.width = object_in_image_info_.detected_roi_.size.width;
        object_in_image_info_.previous_detected_roi_.size.height = object_in_image_info_.detected_roi_.size.height;


        //Update CURRENT detected ROI
        object_in_image_info_.detected_roi_.angle = rotated_rect_item.angle;
        object_in_image_info_.detected_roi_.center.x = rotated_rect_item.center.x;
        object_in_image_info_.detected_roi_.center.y = rotated_rect_item.center.y;
        object_in_image_info_.detected_roi_.size.width = rotated_rect_item.size.width;
        object_in_image_info_.detected_roi_.size.height = rotated_rect_item.size.height;


        //Extract the 4 RotatedRect CORNERS and sort them and save into the object_in_image_info struct
        object_in_image_info_.rotated_rect_points_.clear();
        cv::Point2f corner_points[4];
        object_in_image_info_.detected_roi_.points(corner_points);
        std::vector<cv::Point2f> rotated_rect_points;
        for(int j = 0; j < 4; j++)
            rotated_rect_points.push_back(corner_points[j]);
        //std::sort(rotated_rect_points.data(), rotated_rect_points.data() + rotated_rect_points.size(), cmpVecs);
        object_in_image_info_.rotated_rect_points_.insert(object_in_image_info_.rotated_rect_points_.begin(),
                                                          rotated_rect_points.begin(), rotated_rect_points.end());

        object_in_image_info_.detected_roi_rect_points_.clear();
        object_in_image_info_.detected_roi_rect_points_.push_back(object_in_image_info_.detected_roi_rect_.tl());
        object_in_image_info_.detected_roi_rect_points_.push_back(cv::Point(object_in_image_info_.detected_roi_rect_.tl().x + object_in_image_info_.detected_roi_rect_.width,
                                                                            object_in_image_info_.detected_roi_rect_.tl().y));
        object_in_image_info_.detected_roi_rect_points_.push_back(object_in_image_info_.detected_roi_rect_.br());
        object_in_image_info_.detected_roi_rect_points_.push_back(cv::Point(object_in_image_info_.detected_roi_rect_.tl().x,
                                                                            object_in_image_info_.detected_roi_rect_.tl().y + object_in_image_info_.detected_roi_rect_.height));




        if(DEBUG_MODE)
        {
            std::cout<<"Detected ROI [x, y, w, h]: "<<"["<<object_in_image_info_.detected_roi_.boundingRect().x<<" ; "<<
                    object_in_image_info_.detected_roi_.boundingRect().y<<" ; "<<object_in_image_info_.detected_roi_.boundingRect().width<<" ; "<<
                    object_in_image_info_.detected_roi_.boundingRect().height<<"]"<<std::endl;
        }
    }

}




