#include "rl_environment_image_based_visual_servoing.h"


#define DEBUG_MODE 0
#define IMSHOW_MODE 1
#define OBJECT_STATE_BASED_ON_ERROR_IN_POSITION_ONLY 0
#define OBJECT_STATE_BASED_ON_ROTATED_RECT 0
#define OBJECT_STATE_INCLUDING_ROLL_PITCH 0
#define OBJECT_STATE_BASED_ON_GROUND_TRUTH 1
#define OBJECT_STATE_BASED_ON_STABILIZED_DETECTION 0

#define ROLL_PITCH_RECTIFICATION 0
#define POSITION_CONTROL_MODE 0
#define ESTIMATE_SPEEDS_WITH_CIRCULAR_BUFFER 1
#define COMPUTE_NORM_VALUE_WRT_ROI_REFERENCE 0
#define UAV_POSE_FROM_EKF 0

//For comparing based on the Manhattan (Taxicab distance) to the origin
inline bool cmpVecs(const cv::Point2f &lhs, const cv::Point2f &rhs)
{
    return (lhs.x + lhs.y) < (rhs.x + rhs.y);
}


RlEnvironmentImageBasedVisualServoing::RlEnvironmentImageBasedVisualServoing()
{
    camera_info_.camera_info_available_flag_ = false;
    camera_info_.image_width_ = 640;
    camera_info_.image_height_ = 480;

    error_in_image_.x = 0.0;
    error_in_image_.y = 0.0;
    error_in_image_norm_.x = 0.0;
    error_in_image_norm_.y = 0.0;
    image_boundaries_offset_ = 10;

    for(int i=0;i<4;i++)
    {
        object_in_image_info_.rotated_rect_points_.push_back(cv::Point2f(0, 0));
        object_in_image_info_.rotated_rect_points_rpy_compensated_.push_back(cv::Point2f(0, 0));
        object_in_image_info_.ground_truth_current_object_points_[i] = cv::Point2f(0,0);
    }

    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0);
    current_time_ = ros::Time::now();
    previous_time_ = ros::Time::now();


    object_in_world_info_.width_ = 0.25;
    object_in_world_info_.height_ = 0.3;
    object_in_world_info_.points_mat_[0] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[0].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[0].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[0].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    object_in_world_info_.points_mat_[1] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[1].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[1].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[1].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    object_in_world_info_.points_mat_[2] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[2].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[2].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[2].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    object_in_world_info_.points_mat_[3] = cv::Mat(3, 1, CV_32FC1);
    object_in_world_info_.points_mat_[3].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[3].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.points_mat_[3].at<float>(2,0) = kUav_Altitude_ - object_in_world_info_.height_;

    actions_max_value_ = 1.0;
    actions_min_value_ = -1.0;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;
    std::cout<<"pitch_roll_max_value_ : "<<pitch_roll_max_value_<<std::endl;

    // Init circular buffer
    filtered_derivative_wcb_x_.setTimeParameters( 0.015, 0.015, 0.150, 1.0, 30.000);
    filtered_derivative_wcb_y_.setTimeParameters( 0.015, 0.015, 0.150, 1.0, 30.000);

    filtered_derivative_wcb_x_.reset();
    filtered_derivative_wcb_y_.reset();



    rot_mat_z_90_ = cv::Mat(4, 4, CV_32FC1);
    rot_mat_z_90_.at<float>(0,0) = 0; rot_mat_z_90_.at<float>(0,1) = 1; rot_mat_z_90_.at<float>(0,2) = 0; rot_mat_z_90_.at<float>(0,3) = 0;
    rot_mat_z_90_.at<float>(1,0) = -1; rot_mat_z_90_.at<float>(1,1) = 0; rot_mat_z_90_.at<float>(1,2) = 0; rot_mat_z_90_.at<float>(1,3) = 0;
    rot_mat_z_90_.at<float>(2,0) = 0; rot_mat_z_90_.at<float>(2,1) = 0; rot_mat_z_90_.at<float>(2,2) = 1; rot_mat_z_90_.at<float>(2,3) = 0;
    rot_mat_z_90_.at<float>(3,0) = 0; rot_mat_z_90_.at<float>(3,1) = 0; rot_mat_z_90_.at<float>(3,2) = 0; rot_mat_z_90_.at<float>(3,3) = 1;

    rot_mat_x180_ = cv::Mat(4, 4, CV_32FC1);
    rot_mat_x180_.at<float>(0,0) = 1; rot_mat_x180_.at<float>(0,1) = 0; rot_mat_x180_.at<float>(0,2) = 0; rot_mat_x180_.at<float>(0,3) = 0;
    rot_mat_x180_.at<float>(1,0) = 0; rot_mat_x180_.at<float>(1,1) = -1; rot_mat_x180_.at<float>(1,2) = 0; rot_mat_x180_.at<float>(1,3) = 0;
    rot_mat_x180_.at<float>(2,0) = 0; rot_mat_x180_.at<float>(2,1) = 0; rot_mat_x180_.at<float>(2,2) = -1; rot_mat_x180_.at<float>(2,3) = 0;
    rot_mat_x180_.at<float>(3,0) = 0; rot_mat_x180_.at<float>(3,1) = 0; rot_mat_x180_.at<float>(3,2) = 0; rot_mat_x180_.at<float>(3,3) = 1;

    t_cam_world_ = cv::Mat::eye(4, 4, CV_32FC1);

}


RlEnvironmentImageBasedVisualServoing::~RlEnvironmentImageBasedVisualServoing()
{
    f_data_recorder.close();
}

void RlEnvironmentImageBasedVisualServoing::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: IMAGE BASED VISUAL SERVOING Environment" << std::endl;

    // Init subscribers

    // Read drone namespace
    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Read topics
    std::string param_string;
    ros::param::get("~camera_topic", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoing::CameraImageCallback, this);

    ros::param::get("~camera_info_topic", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    camera_info_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoing::CameraInfoCallback, this);

    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentImageBasedVisualServoing::PoseVelocityCallback, this);

    ros::param::get("~visual_servoing_reference", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    visual_servoing_measurement_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoing::VisualServoingMeasurementCallback, this);

    ros::param::get("~esimtated_pose", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    drone_estimated_pose_subs_ = n.subscribe(param_string, 1, &RlEnvironmentImageBasedVisualServoing::DroneEstimatedPoseCallback, this);

    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
    if (ENABLE_PAUSED_SIMULATION)
    {
        this->data_->num_iterations = (unsigned int) NUM_ITERATIONS;

        // Print
        std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;
    }

    // Init recording experiment
//    exp_rec_.Open(n);

    // Reset environment
    Reset();
    //f_data_recorder.open("/home/carlos/pruebas_Matlab/speeds_pos_in_image_with_ground_truth3.txt");
}

bool RlEnvironmentImageBasedVisualServoing::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response)
{
    // Send action
    droneMsgsROS::droneSpeeds action_msg;
    droneMsgsROS::dronePositionRefCommandStamped action_pos_msg;
    if(POSITION_CONTROL_MODE)
    {
        action_pos_msg.header.stamp = ros::Time::now();
        action_pos_msg.header.frame_id = "";
        action_pos_msg.header.seq = 0;
        action_pos_msg.position_command.x = request.action[0];
        action_pos_msg.position_command.y = request.action[1];
        action_pos_msg.position_command.z = kUav_Altitude_;
        uav_pose_ref_pub_.publish(action_pos_msg);
    }
    else
    {
        action_msg.dx = request.action[0];
        action_msg.dy = request.action[1];
        action_msg.dz = 0;
        uav_speed_ref_pub_.publish(action_msg);
    }



    std::vector<float> current_state = GetNormalizedState();


    float position_reward_term = 0.0f;
    if(current_state.size()>=4)
    {
        for(int i=0;i<current_state.size()-2;i++)
        {
            if((i==0) || (i%2==0))
            {
                if(OBJECT_STATE_BASED_ON_ROTATED_RECT)
                {
                    float point_to_reference_distance = std::sqrt(pow(current_state[i], 2) + pow(current_state[i+1], 2));
                    //std::cout<<"Distance point["<<i<<"]: "<<point_to_reference_distance<<std::endl;
                    position_reward_term += point_to_reference_distance;
                }
                else
                    position_reward_term += std::sqrt(pow(current_state[i], 2) + pow(current_state[i+1], 2));
            }
        }
    }
    else if(current_state.size() == 2)
    {
        position_reward_term = std::sqrt(pow(current_state[0], 2) + pow(current_state[1], 2));
    }
    //std::cout<<std::endl<<std::endl;


    //std::cout<<"Received Action: "<<request.action[0]<<" ; "<<request.action[1]<<std::endl;

    // ********************************************************************
    // ************************** Compute reward **************************
    // ********************************************************************
//    float shaping = - 100 * sqrt(pow(current_state[0], 2) + pow(current_state[1], 2)) -
//            10 * sqrt(pow(current_state[2], 2) + pow(current_state[3], 2)) - sqrt(pow(request.action[0], 2) + pow(request.action[1], 2));
    float shaping = 0.0;
    if(current_state.size()>=4)
    {
        //std::cout<<"current_state.size() >= 4"<<std::endl;
        shaping = - 100 * position_reward_term - 10 * std::sqrt(std::pow(current_state[2], 2) + std::pow(current_state[3], 2)) -
                std::sqrt(std::pow(request.action[0]/actions_max_value_, 2) + std::pow(request.action[1]/actions_max_value_, 2));
    }
    else if(current_state.size() == 2)
    {
        //std::cout<<"current_state.size() == 2"<<std::endl;
        shaping = - 150 * position_reward_term -
                2 *std::sqrt(std::pow(request.action[0]/actions_max_value_, 2) + std::pow(request.action[1]/actions_max_value_, 2));
    }


    float reward = shaping - prev_shaping_;
    prev_shaping_ = shaping;
    response.reward = reward;


//    std::cout<<"Shaping: "<<shaping<<std::endl;
//    std::cout<<"Reward: "<<reward<<std::endl;


    if(IMSHOW_MODE)
    {
        DrawResults();
    }


    //Check if the Detected ROI is out of the IMAGE BOUNDARIES
    //std::cout<<"Detected ROI Center [x,y]: "<<"["<<object_in_image_info_.detected_roi_.center.x<<" , "<<object_in_image_info_.detected_roi_.center.y<<"]"<<std::endl;

    if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
    {
        if(object_in_image_info_.ground_truth_detected_roi_.boundingRect().tl().x < image_boundaries_offset_ || object_in_image_info_.ground_truth_detected_roi_.boundingRect().tl().y < image_boundaries_offset_ ||
                object_in_image_info_.ground_truth_detected_roi_.boundingRect().br().x > (camera_info_.image_width_ - image_boundaries_offset_) ||
                object_in_image_info_.ground_truth_detected_roi_.boundingRect().br().y > (camera_info_.image_height_ - image_boundaries_offset_))
        {
            std::cout<<"--------------------------------------"<<std::endl;
            std::cout<<"----------- MINIMUM REWARD -----------"<<std::endl;
            std::cout<<"--------------------------------------"<<std::endl;
            response.reward = -100;
            response.terminal_state = true;
            reset_env_ = true;
        }
        else
            response.terminal_state = false;
    }
    else
    {
        if(object_in_image_info_.detected_roi_.boundingRect().tl().x < image_boundaries_offset_ || object_in_image_info_.detected_roi_.boundingRect().tl().y < image_boundaries_offset_ ||
                object_in_image_info_.detected_roi_.boundingRect().br().x > (camera_info_.image_width_ - image_boundaries_offset_) ||
                object_in_image_info_.detected_roi_.boundingRect().br().y > (camera_info_.image_height_ - image_boundaries_offset_))
        {
            std::cout<<"--------------------------------------"<<std::endl;
            std::cout<<"----------- MINIMUM REWARD -----------"<<std::endl;
            std::cout<<"--------------------------------------"<<std::endl;
            response.reward = -100;
            response.terminal_state = true;
            reset_env_ = true;
        }
        else
            response.terminal_state = false;
    }


    // Wait for environment to render
    if (ENABLE_PAUSED_SIMULATION)
        WaitForEnv();

    // Read next state
    std::vector<float> next_state = GetNormalizedState();
    std::vector<float> response_state;
    if(OBJECT_STATE_BASED_ON_ERROR_IN_POSITION_ONLY)
    {
        response_state.push_back(next_state[0]);
        response_state.push_back(next_state[1]);
    }
    else
        response_state = next_state;
    response.obs_real = response_state;

    // Successful return
    return true;
}

bool RlEnvironmentImageBasedVisualServoing::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality ImageBasedVisualServoing service called" << std::endl;

    // Action dimensionality
    response.action_dim = ACTIONS_DIM;


    if(POSITION_CONTROL_MODE)
    {
        actions_max_value_ = 2.0;
        actions_min_value_ = -2.0;
    }
    else
    {
        actions_max_value_ = 0.5;
        actions_min_value_ = -0.5;
    }

    // Action max
    std::vector<float> actions_max = {actions_max_value_, actions_max_value_};
    response.action_max = actions_max;

    // Action min
    std::vector<float> actions_min = {actions_min_value_, actions_min_value_};
    response.action_min = actions_min;

    // States dimensionality
    response.state_dim_lowdim = STATE_DIM_LOW_DIM;

    // Number of iterations
    response.num_iterations = NUM_EPISODE_ITERATIONS;


    // Service succesfully executed
    return true;
}

bool RlEnvironmentImageBasedVisualServoing::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv ImageBasedVisualServoing service called" << std::endl;


    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
//    if (exp_rec_.getRecording())
//        long_iteration_ = true;

    std::vector<float> state = GetNormalizedState();
    std::vector<float> response_state;
    if(OBJECT_STATE_BASED_ON_ERROR_IN_POSITION_ONLY)
    {
        response_state.push_back(state[0]);
        response_state.push_back(state[1]);
    }
    else
        response_state = state;

    response.state = response_state;

    return true;
}

bool RlEnvironmentImageBasedVisualServoing::Reset()
{
    // Check experiment recorder
//    if(exp_rec_.getRecording()){
//        exp_rec_.RecordExperiment();
//        exp_rec_.setRecording(false);
//    }

    // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    // Random initial point
    std::srand(time(NULL));
    int seed = std::rand() % 10;
    int r_x = ((-1 * (int) MAX_POSE_X * 100) + (std::rand() % ((int) MAX_POSE_X * 100)));
    int r_y = ((-1 * (int) MAX_POSE_Y * 100) + (std::rand() % ((int) MAX_POSE_Y * 100)));
    if (seed < 3){
        r_x *= -1;
        r_y *= -1;
    }
    else if(seed < 6){
        r_x *= -1;
        r_y *= 1;
    }
    else if(seed < 8){
        r_x *= 1;
        r_y *= -1;
    }
    else{
        r_x *= 1;
        r_y *= 1;
    }

    float init_x = (float) r_x / 100.f;
    float init_y = (float) r_y / 100.f;


    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = kUav_Altitude_;
    uav_pose_ref_pub_.publish(altitude_msg);

    // Spawn model to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = kUav_Altitude_;
//    model_msg.request.model_state.pose.orientation.x = 0;
//    model_msg.request.model_state.pose.orientation.y = 0;
//    model_msg.request.model_state.pose.orientation.z = 0;
//    model_msg.request.model_state.pose.orientation.w = 1;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << " and y " << init_y << std::endl;

    if (gazebo_client_.call(model_msg)){
//        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg)){
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        return false;
    }
}


void RlEnvironmentImageBasedVisualServoing::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    // Get position of the uav in the vector
    int i_position;
    cv::Point3f uav_pose;
    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            i_position = i;

            uav_pose.x = msg->pose[i].position.x;
            uav_pose.y = msg->pose[i].position.y;
            uav_pose.z = msg->pose[i].position.z;

            // Calculating Roll, Pitch, Yaw
            tf::Quaternion q(msg->pose[i].orientation.x, msg->pose[i].orientation.y, msg->pose[i].orientation.z, msg->pose[i].orientation.w);
            tf::Matrix3x3 m(q);

            //convert quaternion to euler angels
            double y, p, r;
            m.getEulerYPR(y, p, r);

            uav_state_.pos_x_ = uav_pose.x;
            uav_state_.pos_y_ = uav_pose.y;
            uav_state_.pos_z_ = uav_pose.z;

            uav_state_.yaw_ = y;
            uav_state_.pitch_ = p;
            uav_state_.roll_ = r;
            break;
        }
    }

    if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
    {
        float uav_real_pos_x = msg->pose[i_position].position.x;
        float uav_real_pos_y = msg->pose[i_position].position.y;
        float uav_real_pos_z = msg->pose[i_position].position.z;
        //std::cout<<"uav_real_pos [x,y]: "<<uav_real_pos_x<<" ; "<<uav_real_pos_y<<std::endl;

        ComputeGroundTruthObjectPositionInImage(uav_real_pos_x, uav_real_pos_y, uav_real_pos_z);
    }
}

void RlEnvironmentImageBasedVisualServoing::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose_  = msg;
    //yaw pitch roll
    if(UAV_POSE_FROM_EKF)
    {
        uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
        uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
        uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

        uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
        uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
        uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;
    }

    if(ROLL_PITCH_RECTIFICATION)
    {
        cv::Mat R = convertFromRPYtoRotationMatrix(uav_state_.yaw_, uav_state_.pitch_, uav_state_.roll_);
        camera_info_.R_matrix_ = R.clone();
    //    std::cout<<"R: "<<camera_info_.R_matrix_<<std::endl;
    }

    return;
}

void RlEnvironmentImageBasedVisualServoing::VisualServoingMeasurementCallback(const opencv_apps::RotatedRectStamped& msg)
{
    //Save the previous Detected Object ROI for computing Speeds
    object_in_image_info_.previous_detected_roi_.angle  = object_in_image_info_.detected_roi_.angle;
    object_in_image_info_.previous_detected_roi_.center.x = object_in_image_info_.detected_roi_.center.x;
    object_in_image_info_.previous_detected_roi_.center.y = object_in_image_info_.detected_roi_.center.y;
    object_in_image_info_.previous_detected_roi_.size.width = object_in_image_info_.detected_roi_.size.width;
    object_in_image_info_.previous_detected_roi_.size.height = object_in_image_info_.detected_roi_.size.height;


    //Update CURRENT detected ROI
    object_in_image_info_.detected_roi_.angle = msg.rect.angle;
    object_in_image_info_.detected_roi_.center.x = msg.rect.center.x;
    object_in_image_info_.detected_roi_.center.y = msg.rect.center.y;
    object_in_image_info_.detected_roi_.size.width = msg.rect.size.width;
    object_in_image_info_.detected_roi_.size.height = msg.rect.size.height;


    //Extract the 4 RotatedRect CORNERS and sort them and save into the object_in_image_info struct
    object_in_image_info_.rotated_rect_points_.clear();
    cv::Point2f corner_points[4];
    object_in_image_info_.detected_roi_.points(corner_points);
    std::vector<cv::Point2f> rotated_rect_points;
    for(int j = 0; j < 4; j++)
        rotated_rect_points.push_back(corner_points[j]);
    //std::sort(rotated_rect_points.data(), rotated_rect_points.data() + rotated_rect_points.size(), cmpVecs);
    object_in_image_info_.rotated_rect_points_.insert(object_in_image_info_.rotated_rect_points_.begin(),
                                                      rotated_rect_points.begin(), rotated_rect_points.end());


    if(ROLL_PITCH_RECTIFICATION)
    {
        ComputeCompensatedRotatedRectByRpy();
    }

    ComputeObjectPositionBasedOnStabilizedDetection();
    ComputeNormalizedObjectInImageState();


    if(DEBUG_MODE)
    {
        std::cout<<"Detected ROI [x, y, w, h]: "<<"["<<object_in_image_info_.detected_roi_.boundingRect().x<<" ; "<<
                object_in_image_info_.detected_roi_.boundingRect().y<<" ; "<<object_in_image_info_.detected_roi_.boundingRect().width<<" ; "<<
                object_in_image_info_.detected_roi_.boundingRect().height<<"]"<<std::endl;
    }

}


void RlEnvironmentImageBasedVisualServoing::CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &msg)
{
    if(!camera_info_.camera_info_available_flag_)
    {
        camera_info_.camera_info_available_flag_ = true;

        camera_info_.cx_ = msg->K.at(2);
        camera_info_.cy_ = msg->K.at(5);
        camera_info_.fx_ = msg->K.at(0);
        camera_info_.fy_ = msg->K.at(4);

        camera_info_.image_width_ = msg->width;
        camera_info_.image_height_ = msg->height;

        camera_info_.K_matrix_ = cv::Mat(3, 3, CV_32FC1);
        camera_info_.K_matrix_.at<float>(0,0) = camera_info_.fx_;
        camera_info_.K_matrix_.at<float>(0,1) = 0.0;
        camera_info_.K_matrix_.at<float>(0,2) = camera_info_.cx_;

        camera_info_.K_matrix_.at<float>(1,0) = 0.0;
        camera_info_.K_matrix_.at<float>(1,1) = camera_info_.fy_;
        camera_info_.K_matrix_.at<float>(1,2) = camera_info_.cy_;

        camera_info_.K_matrix_.at<float>(2,0) = 0.0;
        camera_info_.K_matrix_.at<float>(2,1) = 0.0;
        camera_info_.K_matrix_.at<float>(2,2) = 1.0;

        //std::cout<<"Camera Matrix: "<<std::endl<<camera_info_.K_matrix_<<std::endl<<std::endl;
        ComputeObjectReferenceInImage();
    }
}

void RlEnvironmentImageBasedVisualServoing::CameraImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
        captured_image_.release();
        cv::Mat img = cv_bridge::toCvShare(msg, "bgr8")->image;
        captured_image_ = img.clone();
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
}


void RlEnvironmentImageBasedVisualServoing::SetUavState(const float x, const float y, const float z, const float dx, const float dy)
{
    uav_mutex_.lock();
    uav_state_.pos_x_ = x;
    uav_state_.pos_y_ = y;
    uav_state_.pos_z_ = z;
    uav_state_.speed_x_ = dx;
    uav_state_.speed_y_ = dy;
    uav_mutex_.unlock();
}

void RlEnvironmentImageBasedVisualServoing::GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float &pitch)
{
    uav_mutex_.lock();
    x = uav_state_.pos_x_;
    y = uav_state_.pos_y_;
    z = uav_state_.pos_z_;
    dx = uav_state_.speed_x_;
    dy = uav_state_.speed_y_;
    roll = uav_state_.roll_;
    pitch = uav_state_.pitch_;
    uav_mutex_.unlock();
}

void RlEnvironmentImageBasedVisualServoing::GetObjectInImageState(cv::RotatedRect &previous_roi, cv::RotatedRect &current_roi)
{
    object_in_image_mutex_.lock();

    if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
    {
        previous_roi = object_in_image_info_.ground_truth_previous_detected_roi_;
        current_roi = object_in_image_info_.ground_truth_detected_roi_;
    }
    else if(OBJECT_STATE_BASED_ON_STABILIZED_DETECTION)
    {
        previous_roi = object_in_image_info_.stabilized_previous_detected_roi_;
        current_roi = object_in_image_info_.stabilized_detected_roi_;
    }
    else
    {
        previous_roi = object_in_image_info_.previous_detected_roi_;
        current_roi = object_in_image_info_.detected_roi_;
    }

    object_in_image_mutex_.unlock();
}


std::vector<float> RlEnvironmentImageBasedVisualServoing::ComputeNormalizedObjectInImageState()
{
    state_.clear();
    normalized_state_.clear();
    cv::RotatedRect previous_object_roi;
    cv::RotatedRect current_object_roi;
    GetObjectInImageState(previous_object_roi, current_object_roi);


    //Change for other reference position in Image.
    error_in_image_.x = (current_object_roi.center.x - center_roi_ref_.x);
    error_in_image_.y = (current_object_roi.center.y - center_roi_ref_.y);
    error_in_image_norm_.x = (current_object_roi.center.x - center_roi_ref_.x)/center_roi_ref_max_.x;
    error_in_image_norm_.y = (current_object_roi.center.y - center_roi_ref_.y)/center_roi_ref_max_.y;

    //Compute the velocity of the Object in the Image plane
    current_time_ = ros::Time::now();
    double delta_t = (current_time_ - previous_time_).toSec();
    previous_time_ = current_time_;
    cv::Point2f delta_pos_object_roi_center, object_roi_center_speed_norm;
//    delta_pos_object_roi_center.x = (current_object_roi.center.x - previous_object_roi.center.x)/delta_t;
//    delta_pos_object_roi_center.y = (current_object_roi.center.y - previous_object_roi.center.y)/delta_t;
//    object_roi_center_speed_norm.x = delta_pos_object_roi_center.x/speed_roi_ref_max_.x;
//    object_roi_center_speed_norm.y = delta_pos_object_roi_center.y/speed_roi_ref_max_.y;
    delta_pos_object_roi_center.x = current_object_roi.center.x - previous_object_roi.center.x;
    delta_pos_object_roi_center.y = current_object_roi.center.y - previous_object_roi.center.y;
    object_roi_center_speed_norm.x = delta_pos_object_roi_center.x/center_roi_ref_max_.x;
    object_roi_center_speed_norm.y = delta_pos_object_roi_center.y/center_roi_ref_max_.y;


    double x_t, dx_t;
    double y_t, dy_t;
    if(ESTIMATE_SPEEDS_WITH_CIRCULAR_BUFFER)
    {
        // Compute speeds from pose ground truth
        double x_raw_t = error_in_image_.x;
        double y_raw_t = error_in_image_.y;

        time_t tv_sec; suseconds_t tv_usec;
        tv_sec  = current_time_.sec;
        tv_usec = current_time_.nsec / 1000.0;
        filtered_derivative_wcb_x_.setInput( x_raw_t, tv_sec, tv_usec);
        filtered_derivative_wcb_y_.setInput( y_raw_t, tv_sec, tv_usec);


        filtered_derivative_wcb_x_.getOutput( x_t,  dx_t);
        filtered_derivative_wcb_y_.getOutput( y_t,  dy_t);
//        if(delta_t>0)
//        {
//            std::cout<<"Estimated Speed (delta_pos_x): "<<delta_pos_object_roi_center.x/delta_t <<std::endl;
//            std::cout<<"Estimated Speed (delta_pos_y): "<<delta_pos_object_roi_center.y/delta_t<<std::endl;
//        }
//        std::cout<<"dx_t (Circ Buffer): "<<dx_t<<std::endl;
//        std::cout<<"dy_t (Circ Buffer): "<<dy_t<<std::endl;

        object_roi_center_speed_norm.x = dx_t/center_roi_ref_max_.x;
        object_roi_center_speed_norm.y = dy_t/center_roi_ref_max_.y;

        if(object_roi_center_speed_norm.x > 1.0)
            object_roi_center_speed_norm.x = 1.0;
        else if(object_roi_center_speed_norm.x < -1.0)
            object_roi_center_speed_norm.x = -1.0;

        if(object_roi_center_speed_norm.y > 1.0)
            object_roi_center_speed_norm.y = 1.0;
        else if(object_roi_center_speed_norm.y < -1.0)
            object_roi_center_speed_norm.y = -1.0;

        //f_data_recorder<<delta_pos_object_roi_center.x/delta_t<<" ; "<<delta_pos_object_roi_center.y/delta_t<<" ; "<<dx_t<<" ; "<<dy_t<<std::endl;
    }



    if(OBJECT_STATE_BASED_ON_ROTATED_RECT)
    {

        for(int i=0;i<object_in_image_info_.rotated_rect_points_.size();i++)
        {
            float delta_x = (object_in_image_info_.rotated_rect_points_[i].x - center_roi_ref_.x);
            float delta_y = (object_in_image_info_.rotated_rect_points_[i].y - center_roi_ref_.y);
            float alpha = std::atan2(delta_y, delta_x);

            //Substract the radius of the reference
            float delta_x_substracted = delta_x - radius_ref_ * std::cos(alpha);
            float delta_y_substracted = delta_y - radius_ref_ * std::sin(alpha);

//            std::cout<<"Distance point (radius substracted)["<<i<<"]: "<<delta_x_substracted<<std::endl;
//            std::cout<<"Distance point (radius substracted)["<<i<<"]: "<<delta_y_substracted<<std::endl;

            float delta_x_norm = delta_x_substracted/center_roi_ref_max_.x;
            float delta_y_norm = delta_y_substracted/center_roi_ref_max_.y;


            state_.push_back(delta_x);
            state_.push_back(delta_y);
            normalized_state_.push_back(delta_x_norm);
            normalized_state_.push_back(delta_y_norm);
        }
        //std::cout<<std::endl<<std::endl;
        state_.push_back(delta_pos_object_roi_center.x);
        state_.push_back(delta_pos_object_roi_center.y);
        normalized_state_.push_back(object_roi_center_speed_norm.x);
        normalized_state_.push_back(object_roi_center_speed_norm.y);
    }
    else
    {
        if(OBJECT_STATE_BASED_ON_ERROR_IN_POSITION_ONLY)
        {
            normalized_state_.push_back(error_in_image_norm_.x);
            normalized_state_.push_back(error_in_image_norm_.y);
        }
        else
        {
            normalized_state_.push_back(error_in_image_norm_.x);
            normalized_state_.push_back(error_in_image_norm_.y);
            normalized_state_.push_back(object_roi_center_speed_norm.x);
            normalized_state_.push_back(object_roi_center_speed_norm.y);
        }

//        normalized_state_.push_back(error_in_image_norm_.x);
//        normalized_state_.push_back(error_in_image_norm_.y);
//        normalized_state_.push_back(object_roi_center_speed_norm.x);
//        normalized_state_.push_back(object_roi_center_speed_norm.y);


//        f_data_recorder<<error_in_image_norm_.x<<" ; "<<error_in_image_norm_.y<<" ; "<<x_t/center_roi_ref_max_.x<<" ; "<<y_t/center_roi_ref_max_.y<<" ; "
//                      <<object_roi_center_speed_norm.x<<" ; "<<object_roi_center_speed_norm.y<<std::endl;

    }

    if(OBJECT_STATE_INCLUDING_ROLL_PITCH)
    {
        // Get state
        float x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch;
        GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch);

        normalized_state_.push_back(roll/pitch_roll_max_value_);
        normalized_state_.push_back(pitch/pitch_roll_max_value_);

    }



    if(DEBUG_MODE)
    {
        std::cout<<"++++ CURRENT STATE ++++"<<std::endl;
        for(int i=0;i<normalized_state_.size();i++)
        {
            if(i==0)
                std::cout<<"[";
            std::cout<<normalized_state_[i]<<" ; ";
            if(i == normalized_state_.size()-1)
                std::cout<<"]"<<std::endl;

        }
    }

    return normalized_state_;

}




void RlEnvironmentImageBasedVisualServoing::ComputeObjectReferenceInImage()
{
    if(camera_info_.camera_info_available_flag_)
    {
        cv::Mat p1 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[0];
        cv::Mat p2 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[1];
        cv::Mat p3 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[2];
        cv::Mat p4 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[3];

        cv::Mat p1_image = p1/p1.at<float>(2,0);
        cv::Mat p2_image = p2/p2.at<float>(2,0);
        cv::Mat p3_image = p3/p3.at<float>(2,0);
        cv::Mat p4_image = p4/p4.at<float>(2,0);

        roi_reference_ = cv::Rect(cv::Point(p1_image.at<float>(0,0), p1_image.at<float>(1,0)),
                                  cv::Point(p3_image.at<float>(0,0), p3_image.at<float>(1,0)));

        cv::Point2f diagonal_ref;
        diagonal_ref.x = p3_image.at<float>(0,0) - p1_image.at<float>(0,0);
        diagonal_ref.y = p3_image.at<float>(1,0) - p1_image.at<float>(1,0);
        center_roi_ref_.x = p1_image.at<float>(0,0) + diagonal_ref.x/2;
        center_roi_ref_.y = p1_image.at<float>(1,0) + diagonal_ref.y/2;
        radius_ref_ = std::sqrt(std::pow(diagonal_ref.x, 2) + std::pow(diagonal_ref.y, 2))/2;

        if(COMPUTE_NORM_VALUE_WRT_ROI_REFERENCE)
        {
            if(center_roi_ref_.x >= (camera_info_.image_width_ - center_roi_ref_.x))
                center_roi_ref_max_.x = center_roi_ref_.x;
            else
                center_roi_ref_max_.x = camera_info_.image_width_ - center_roi_ref_.x;

            if(center_roi_ref_.y >= (camera_info_.image_height_ - center_roi_ref_.y))
                center_roi_ref_max_.y = center_roi_ref_.y;
            else
                center_roi_ref_max_.y = camera_info_.image_height_ - center_roi_ref_.y;
        }
        else
        {
            center_roi_ref_max_.x = camera_info_.image_width_/2.0;
            center_roi_ref_max_.y = camera_info_.image_height_/2.0;
        }

        std::cout<<"center_roi_ref_.x: "<<center_roi_ref_.x<<std::endl;
        std::cout<<"center_roi_ref_.y: "<<center_roi_ref_.y<<std::endl;

        std::cout<<"p1_image: "<<p1_image<<std::endl;
        std::cout<<"p2_image: "<<p2_image<<std::endl;
        std::cout<<"p3_image: "<<p3_image<<std::endl;
        std::cout<<"p4_image: "<<p4_image<<std::endl;
        std::cout<<"ROI Reference: "<<roi_reference_<<std::endl;

        std::cout<<"center_roi_ref_max_.x: "<<center_roi_ref_max_.x<<std::endl;
        std::cout<<"center_roi_ref_max_.y: "<<center_roi_ref_max_.y<<std::endl;


        speed_roi_ref_max_.x = center_roi_ref_max_.x * 30; //ShapeColor_ObjectDetector FRECUENCY
        speed_roi_ref_max_.y = center_roi_ref_max_.y * 30;
    }
}

void RlEnvironmentImageBasedVisualServoing::DrawResults()
{
    if(object_in_image_info_.detected_roi_.boundingRect().area() > 0 && !captured_image_.empty())
    {
        cv::Point2f corner_points[4];
        object_in_image_info_.detected_roi_.points(corner_points);
        for(unsigned int j = 0; j < 4; j++)
        {
            cv::circle(captured_image_, corner_points[j], 3, cv::Scalar(0, 255, 255), 1);
            cv::line(captured_image_, corner_points[j], corner_points[(j+1)%4], cv::Scalar(0, 255, 0), 2, 8);
        }

        if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
        {
//            for(unsigned int i = 0; i < 4; i++)
//            {
//                cv::Point2f p_truth = object_in_image_info_.ground_truth_current_object_points_[i];
//                cv::Point2f p_truth2 = object_in_image_info_.ground_truth_current_object_points_[(i+1)%4];
//                cv::circle(captured_image_, p_truth, 3, cv::Scalar(0, 255, 255), 1);
//                cv::line(captured_image_, p_truth, p_truth2, cv::Scalar(255, 128, 255), 2, 8);
//            }
            cv::Point2f ground_truth_corner_points[4];
            object_in_image_info_.ground_truth_detected_roi_.points(ground_truth_corner_points);
            for(int j = 0; j < 4; j++)
            {
                cv::circle(captured_image_, ground_truth_corner_points[j], 3, cv::Scalar(255, 255, 255), 1);
                cv::line(captured_image_, ground_truth_corner_points[j], ground_truth_corner_points[(j+1)%4], cv::Scalar(255, 255, 0), 2, 8);
            }
        }

        if(OBJECT_STATE_BASED_ON_ROTATED_RECT)
        {
            char corner_point_id[10];
            for(unsigned int j = 0; j < object_in_image_info_.rotated_rect_points_.size(); j++)
            {
                std::sprintf(corner_point_id, "%d", j);
                cv::putText(captured_image_, corner_point_id, object_in_image_info_.rotated_rect_points_[j],
                            cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(255, 255, 255), 2);
                cv::line(captured_image_,  cv::Point(center_roi_ref_.x, center_roi_ref_.y),
                         object_in_image_info_.rotated_rect_points_[j], cv::Scalar(0, 255, 255), 2, 8);
            }

        }
        else
        {
            cv::RotatedRect previous_object_roi;
            cv::RotatedRect current_object_roi;
            GetObjectInImageState(previous_object_roi, current_object_roi);
            cv::line(captured_image_,  cv::Point(center_roi_ref_.x, center_roi_ref_.y),
                     cv::Point(current_object_roi.center.x, current_object_roi.center.y), cv::Scalar(0, 255, 255), 2, 8);
        }

        if(ROLL_PITCH_RECTIFICATION)
        {
            for(int j=0;j < object_in_image_info_.rotated_rect_points_rpy_compensated_.size();j++)
            {
                cv::circle(captured_image_, object_in_image_info_.rotated_rect_points_rpy_compensated_[j], 3, cv::Scalar(0, 255, 255), 1);
                cv::line(captured_image_, object_in_image_info_.rotated_rect_points_rpy_compensated_[j],
                         object_in_image_info_.rotated_rect_points_rpy_compensated_[(j+1)%4], cv::Scalar(0, 0, 255), 2, 8);
            }

        }


        //cv::rectangle(captured_image_, object_in_image_info_.detected_roi_.boundingRect(), cv::Scalar(0, 255, 255), 2, 4);
        cv::Point line1_ini = cv::Point(object_in_image_info_.detected_roi_.center.x, object_in_image_info_.detected_roi_.center.y - 10);
        cv::Point line1_end = cv::Point(object_in_image_info_.detected_roi_.center.x, object_in_image_info_.detected_roi_.center.y + 10);
        cv::Point line2_ini = cv::Point(object_in_image_info_.detected_roi_.center.x - 10, object_in_image_info_.detected_roi_.center.y);
        cv::Point line2_end = cv::Point(object_in_image_info_.detected_roi_.center.x + 10, object_in_image_info_.detected_roi_.center.y);
        cv::line(captured_image_, line1_ini, line1_end, cv::Scalar(0, 255, 0), 2);
        cv::line(captured_image_, line2_ini, line2_end, cv::Scalar(0, 255, 0), 2);

        cv::rectangle(captured_image_, roi_reference_, cv::Scalar(255, 0, 0), 2);
        cv::circle(captured_image_, cv::Point(center_roi_ref_.x, center_roi_ref_.y), radius_ref_, cv::Scalar(255, 0, 0), 2);
        cv::Point line1_ini_ref = cv::Point(center_roi_ref_.x, center_roi_ref_.y - 10);
        cv::Point line1_end_ref = cv::Point(center_roi_ref_.x, center_roi_ref_.y + 10);
        cv::Point line2_ini_ref = cv::Point(center_roi_ref_.x - 10, center_roi_ref_.y);
        cv::Point line2_end_ref = cv::Point(center_roi_ref_.x + 10, center_roi_ref_.y);
        cv::line(captured_image_, line1_ini_ref, line1_end_ref, cv::Scalar(255, 0, 0), 2);
        cv::line(captured_image_, line2_ini_ref, line2_end_ref, cv::Scalar(255, 0, 0), 2);


        cv::Point p1(image_boundaries_offset_, image_boundaries_offset_);
        cv::Point p2(camera_info_.image_width_ - image_boundaries_offset_, camera_info_.image_height_ - image_boundaries_offset_);
        cv::rectangle(captured_image_, p1, p2, cv::Scalar(255, 0, 255), 1, 4);


        cv::imshow("Received Image From Environment", captured_image_);
        cv::waitKey(1);
    }
}

cv::Mat RlEnvironmentImageBasedVisualServoing::convertFromRPYtoRotationMatrix(const float alpha, const float beta, const float gamma)
{
    cv::Mat R = cv::Mat(3, 3, CV_32FC1);
    R.at<float>(0,0) = cos(alpha)*cos(beta);
    R.at<float>(0,1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
    R.at<float>(0,2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);

    R.at<float>(1,0) = sin(alpha)*cos(beta);
    R.at<float>(1,1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
    R.at<float>(1,2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);

    R.at<float>(2,0) = -sin(beta);
    R.at<float>(2,1) = cos(beta)*sin(gamma);
    R.at<float>(2,2) = cos(beta)*cos(gamma);

    return R;

}


void RlEnvironmentImageBasedVisualServoing::ComputeCompensatedRotatedRectByRpy()
{
    object_in_image_info_.rotated_rect_points_rpy_compensated_.clear();
    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);
    if(camera_info_.camera_info_available_flag_)
    {
//        std::cout<<std::endl<<"K: "<<camera_info_.K_matrix_<<std::endl;
//        std::cout<<std::endl<<"R: "<<camera_info_.R_matrix_<<std::endl;
        H = camera_info_.K_matrix_ * camera_info_.R_matrix_ * camera_info_.K_matrix_.inv();
//        std::cout<<"H: "<<H<<std::endl;

        for(int i = 0;i<object_in_image_info_.rotated_rect_points_.size();i++)
        {
            cv::Mat p_mat = cv::Mat(3, 1, CV_32FC1);
            cv::Mat p_mat_dst = cv::Mat(3, 1, CV_32FC1);


            p_mat.at<float>(0,0) = object_in_image_info_.rotated_rect_points_[i].x;
            p_mat.at<float>(1,0) = object_in_image_info_.rotated_rect_points_[i].y;
            p_mat.at<float>(2,0) = 1;

            p_mat_dst = H * p_mat;
//            std::cout<<"source points: "<<p_mat<<std::endl;
//            std::cout<<"dst points: "<<p_mat_dst<<std::endl;
            cv::Point2f p_dst = cv::Point2f(p_mat_dst.at<float>(0,0), p_mat_dst.at<float>(1,0));
            object_in_image_info_.rotated_rect_points_rpy_compensated_.push_back(p_dst);
        }
    }
}

void RlEnvironmentImageBasedVisualServoing::ComputeGroundTruthObjectPositionInImage(const float &x_uav, const float &y_uav, const float &z_uav)
{
    if(camera_info_.camera_info_available_flag_)
    {
        t_cam_world_.at<float>(0,3) = -x_uav; t_cam_world_.at<float>(1,3) = -y_uav; t_cam_world_.at<float>(2,3) = -z_uav;

//        std::cout<<"rot_mat_z_90_: "<<rot_mat_z_90_<<std::endl;
//        std::cout<<"rot_mat_x180_: "<<rot_mat_x180_<<std::endl;
//        std::cout<<"t_cam_world_: "<<t_cam_world_<<std::endl;

        cv::Mat T_cam_world = rot_mat_z_90_ * rot_mat_x180_ * t_cam_world_;


        object_in_world_info_.current_points_mat_[0] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[0].at<float>(0,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[0].at<float>(1,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[0].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[0].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[1] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[1].at<float>(0,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[1].at<float>(1,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[1].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[1].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[2] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[2].at<float>(0,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[2].at<float>(1,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[2].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[2].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[3] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[3].at<float>(0,0) = -object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[3].at<float>(1,0) = object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[3].at<float>(2,0) = object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[3].at<float>(3,0) = 1.0;


        //Points Referred to Camera frame of reference
        cv::Mat p1 = T_cam_world * object_in_world_info_.current_points_mat_[0]; p1.pop_back(1);
        cv::Mat p2 = T_cam_world * object_in_world_info_.current_points_mat_[1]; p2.pop_back(1);
        cv::Mat p3 = T_cam_world * object_in_world_info_.current_points_mat_[2]; p3.pop_back(1);
        cv::Mat p4 = T_cam_world * object_in_world_info_.current_points_mat_[3]; p4.pop_back(1);

//        std::cout<<"T_cam_world: "<<T_cam_world<<std::endl;
//        std::cout<<"p1: "<<p1<<std::endl;
//        std::cout<<"p2: "<<p2<<std::endl;
//        std::cout<<"p3: "<<p3<<std::endl;
//        std::cout<<"p4: "<<p4<<std::endl;


        p1 = camera_info_.K_matrix_ * p1;
        p2 = camera_info_.K_matrix_ * p2;
        p3 = camera_info_.K_matrix_ * p3;
        p4 = camera_info_.K_matrix_ * p4;

        cv::Mat p1_image = p1/p1.at<float>(2,0);
        cv::Mat p2_image = p2/p2.at<float>(2,0);
        cv::Mat p3_image = p3/p3.at<float>(2,0);
        cv::Mat p4_image = p4/p4.at<float>(2,0);

        std::vector<cv::Mat> p_image;
        p_image.push_back(p1_image);
        p_image.push_back(p2_image);
        p_image.push_back(p3_image);
        p_image.push_back(p4_image);

        for(int i=0;i<p_image.size();i++)
        {
            object_in_image_info_.ground_truth_current_object_points_[i].x = p_image[i].at<float>(0,0);
            object_in_image_info_.ground_truth_current_object_points_[i].y = p_image[i].at<float>(1,0);
        }


        object_in_image_info_.ground_truth_previous_detected_roi_ = object_in_image_info_.ground_truth_detected_roi_;
        cv::Point2f ground_truth_roi_center;
        float ground_truth_roi_width = object_in_image_info_.ground_truth_current_object_points_[1].x
                - object_in_image_info_.ground_truth_current_object_points_[0].x;

        float ground_truth_roi_height = object_in_image_info_.ground_truth_current_object_points_[3].y
                - object_in_image_info_.ground_truth_current_object_points_[0].y;

        ground_truth_roi_center.x = object_in_image_info_.ground_truth_current_object_points_[0].x
                + ground_truth_roi_width/2.0;
        ground_truth_roi_center.y = object_in_image_info_.ground_truth_current_object_points_[0].y
                + ground_truth_roi_height/2.0;

        object_in_image_info_.ground_truth_detected_roi_ = cv::RotatedRect(ground_truth_roi_center, cv::Size(ground_truth_roi_width, ground_truth_roi_height), 0.0);

    }

}


void RlEnvironmentImageBasedVisualServoing::ComputeObjectPositionBasedOnStabilizedDetection()
{
    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);
    if(camera_info_.camera_info_available_flag_)
    {
        float camera_yaw = -uav_state_.yaw_;
        float camera_pitch = -uav_state_.roll_;
        float camera_roll = -uav_state_.pitch_;
        cv::Mat R = ConvertFromYPRtoRotationMatrix(camera_yaw, camera_pitch, camera_roll);

        H = camera_info_.K_matrix_ * R * camera_info_.K_matrix_.inv();


        cv::Mat I_warped;
        cv::warpPerspective(captured_image_.clone(), I_warped, H, captured_image_.size());

        cv::RotatedRect rotated_rect_item;;
        cv::Rect rect_item;
        DetectObjectInImage(I_warped.clone(), "Red", rotated_rect_item, rect_item);


        object_in_image_info_.stabilized_detected_roi_rect_ = rect_item;

        object_in_image_info_.stabilized_previous_detected_roi_ = object_in_image_info_.stabilized_detected_roi_;
        object_in_image_info_.stabilized_detected_roi_ = rotated_rect_item;


        cv::rectangle(I_warped, object_in_image_info_.stabilized_detected_roi_rect_, cv::Scalar(0, 255, 255), 2);
        cv::imshow("I warped", I_warped);
        cv::waitKey(1);

    }

}


void RlEnvironmentImageBasedVisualServoing::DetectObjectInImage(const cv::Mat &I, const std::string object_color,
                                                                            cv::RotatedRect &rotated_rect_item, cv::Rect &rect_item)
{
    cv::Mat3b I_hsv;
    cv::cvtColor(I, I_hsv, CV_BGR2HSV);
    cv::Mat1b mask1_hsv, mask2_hsv;


    cv::Mat I_binarized;
    if(object_color == "Red")
    {
        //Real
        cv::inRange(I_hsv, cv::Scalar(0, 100, 100), cv::Scalar(10, 255, 255), mask1_hsv);
        cv::inRange(I_hsv, cv::Scalar(170, 100, 100), cv::Scalar(180, 255, 255), mask2_hsv);

//        cv::inRange(I_hsv, cv::Scalar(0, 70, 20), cv::Scalar(10, 255, 255), mask1_hsv);
//        cv::inRange(I_hsv, cv::Scalar(170, 70, 20), cv::Scalar(180, 255, 255), mask2_hsv);
        I_binarized = mask1_hsv | mask2_hsv;
    }
    else if(object_color == "Blue")
    {
        //cv::inRange(I_hsv, cv::Scalar(90, 100, 50), cv::Scalar(140, 255, 255), I_mask_hsv); //Real
        cv::inRange(I_hsv, cv::Scalar(110, 70, 50), cv::Scalar(130, 255, 255), I_binarized); //Simulation
    }


    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(I_binarized.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);


    std::vector<std::vector<cv::Point> >::iterator itc = contours.begin();
    while(itc != contours.end())
    {
        cv::RotatedRect rot_box = cv::minAreaRect(cv::Mat(*itc));
        float rot_box_area = (float)rot_box.size.width * (float)rot_box.size.height;

        if(rot_box_area < 500)
            itc = contours.erase(itc);
        else
        {
            ++itc;
        }
    }

    if(contours.size())
    {
        rotated_rect_item = cv::minAreaRect(cv::Mat(contours[0]));
        rect_item = cv::boundingRect(cv::Mat(contours[0]));

    }

}

cv::Mat RlEnvironmentImageBasedVisualServoing::ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma)
{
    cv::Mat R = cv::Mat(3, 3, CV_32FC1);
    R.at<float>(0,0) = cos(alpha)*cos(beta);
    R.at<float>(0,1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
    R.at<float>(0,2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);

    R.at<float>(1,0) = sin(alpha)*cos(beta);
    R.at<float>(1,1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
    R.at<float>(1,2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);

    R.at<float>(2,0) = -sin(beta);
    R.at<float>(2,1) = cos(beta)*sin(gamma);
    R.at<float>(2,2) = cos(beta)*cos(gamma);

    return R;

}

