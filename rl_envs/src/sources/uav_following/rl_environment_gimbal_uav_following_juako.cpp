#include "rl_environment_gimbal_uav_following_juako.h"

#define OBJECT_BASED_GROUND_TRUTH_GIMBAL
//#define STABILIZE_YAW
//#define SHOW_IMAGES

/// TO BE FILLED: INIT VARIABLES AND STATES
RlEnvironmentUavGimbalFollowingJuako::RlEnvironmentUavGimbalFollowingJuako()
{
    kUav_Altitude_ = 1.2;
    kUav_max_Altitude_ = 4.7;
    kUav_min_Altitude_ = 0.7;

    environment_info_.num_episode_steps_ = 5000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 2;

    environment_info_.actions_max_value_ = {0.5, 0.5};
    environment_info_.actions_min_value_ = {-0.5, -0.5};

    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;

    environment_info_.state_dim_low_dim_ = 4;

    camera_info_.camera_info_available_flag_ = false;
    camera_info_.image_width_ = RESOLUTION_VIRTUAL_CAMERA_X;
    camera_info_.image_height_ = RESOLUTION_VIRTUAL_CAMERA_Y;
    camera_info_.uav_camera_offset_ = cv::Point3f(0.2, 0.0, -0.05); //Camera offset (x, y, z) (see Hummingbird_xacro.base)

    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0);
    current_time_ = ros::Time::now();
    previous_time_ = ros::Time::now();

    object_in_world_info_.width_ = 0.4; //0.3 for the cylinder on top of the Firefly
    object_in_world_info_.height_ = 0.05;

    //**** Object points w.r.t. OBJECT frame of reference ****//
    // Upper side
    object_in_world_info_.current_points_mat_[0] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[0].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[0].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[0].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[0].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[1] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[1].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[1].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[1].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[1].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[2] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[2].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[2].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[2].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[2].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[3] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[3].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[3].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[3].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[3].at<float>(3,0) = 1.0;

    // Lower side
    object_in_world_info_.current_points_mat_[4] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[4].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[4].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[4].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[4].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[5] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[5].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[5].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[5].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[5].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[6] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[6].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[6].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[6].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[6].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[7] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[7].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[7].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[7].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[7].at<float>(3,0) = 1.0;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;

    gimbal_state_.roll_ = 0.0f;
    gimbal_state_.pitch_ = 0.0f;
    gimbal_state_.yaw_ = 0.0f;

    previous_time_gimbal_ = clock();

}

RlEnvironmentUavGimbalFollowingJuako::~RlEnvironmentUavGimbalFollowingJuako()
{
    f_data_recorder.close();
}

void RlEnvironmentUavGimbalFollowingJuako::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: IMAGE BASED VISUAL SERVOING WITH ALTITUDE Environment" << std::endl;

    // Init subscribers

    // Read drone namespace
    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Read topics
    std::string param_string;
    ros::param::get("~camera_topic", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    //camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingJuako::CameraImageCallback, this);
    camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingJuako::CameraFishEyeCallback, this);


    ros::param::get("~camera_info_topic", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    camera_info_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingJuako::CameraInfoCallback, this);

    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentUavGimbalFollowingJuako::PoseVelocityCallback, this);

    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);


    ros::param::get("~dAltitude", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    daltitude_ref_pub_ = n.advertise<droneMsgsROS::droneDAltitudeCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
    if (ENABLE_PAUSED_SIMULATION)
    {
        this->data_->num_iterations = (unsigned int) environment_info_.num_iterations_;

        // Print
        std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;
    }


    ros::param::get("~configs_path", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    std::string configs_file_name = param_string;
    //std::cout<<"++++++++++ CONFIGS PATH ++++++++++"<<std::endl<<configs_file_name<<std::endl;
    //ReadConfigs(configs_file_name);

    this->rl_env_step_action_srv = n.advertiseService("/rl_env_step_action_srv", &RlEnvironmentUavGimbalFollowingJuako::StepAction, this);

    this->rl_env_step_state_srv = n.advertiseService("/rl_env_step_state_srv", &RlEnvironmentUavGimbalFollowingJuako::StepState, this);

    // Init member variables
    v = {1, 2, 3, 4};
    angular_position_gimbal_ = {0.0f,-0.7f};

    angular_position_gimbal_controller_ = {0.0f, 0.0f};
    angular_position_gimbal_controller_MAX_ = {0.61, 0.785};
    angular_position_gimbal_controller_MIN_ = {-0.61, -0.785};

    // Reset environment
    Reset();
    //f_data_recorder.open("/media/carlos/DATA/TesisCVG/PAPERS/MIS_PAPERS/ObjectFollowing_IROS_2018/Experiments/Real_Flights/Moving_Stick/RL_IBVS_Leader_Follower_positions.txt");
}

bool RlEnvironmentUavGimbalFollowingJuako::ReadConfigs(std::string &configFile)
{
    pugi::xml_document doc;

    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        std::cout << "ERROR: Could not load the file: " << result.description() << std::endl;
        return 0;
    }

    pugi::xml_node Configuration = doc.child("Image_Based_Visual_Servoing_Config");
    std::string readingValue;

    readingValue = Configuration.child_value("Uav_Altitude");
    kUav_Altitude_ = atof(readingValue.c_str());
    std::cout<<"kUav_Altitude_: "<<kUav_Altitude_<<std::endl;

    readingValue = Configuration.child_value("Uav_max_Altitude");
    kUav_max_Altitude_ = atof(readingValue.c_str());
    std::cout<<"kUav_max_Altitude_: "<<kUav_max_Altitude_<<std::endl;

    readingValue = Configuration.child_value("Uav_min_Altitude");
    kUav_min_Altitude_ = atof(readingValue.c_str());
    std::cout<<"kUav_min_Altitude_: "<<kUav_min_Altitude_<<std::endl;

    //******** Environment INFO ********
    readingValue = Configuration.child("environment_info").child_value("num_episode_steps");
    environment_info_.num_episode_steps_ = atoi(readingValue.c_str());
    std::cout<<"environment_info_.num_episode_steps_: "<<environment_info_.num_episode_steps_<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("actions_min_value");
    float actions_min_value = atof(readingValue.c_str());
    std::cout<<"actions_min_value: "<<actions_min_value<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("actions_max_value");
    float actions_max_value = atof(readingValue.c_str());
    std::cout<<"actions_max_value: "<<actions_max_value<<std::endl;
    environment_info_.actions_min_value_ = {actions_min_value, actions_min_value, actions_min_value};
    environment_info_.actions_max_value_ = {actions_max_value, actions_max_value, actions_max_value};

    readingValue = Configuration.child("environment_info").child_value("max_pos_x");
    environment_info_.max_pos_x_ = atof(readingValue.c_str());
    std::cout<<"environment_info_.max_pos_x_: "<<environment_info_.max_pos_x_<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("max_pos_y");
    environment_info_.max_pos_y_ = atof(readingValue.c_str());
    std::cout<<"environment_info_.max_pos_y_: "<<environment_info_.max_pos_y_<<std::endl;

    //******** Camera INFO ********
    readingValue = Configuration.child("camera_info").child_value("image_width");
    camera_info_.image_width_ = atoi(readingValue.c_str());
    std::cout<<"camera_info_.image_width_: "<<camera_info_.image_width_<<std::endl;

    readingValue = Configuration.child("camera_info").child_value("image_height");
    camera_info_.image_height_ = atof(readingValue.c_str());
    std::cout<<"camera_info_.image_height_: "<<camera_info_.image_height_<<std::endl;

    readingValue = Configuration.child("camera_info").child("camera_offset").child_value("x");
    camera_info_.uav_camera_offset_.x = atof(readingValue.c_str());
    readingValue = Configuration.child("camera_info").child("camera_offset").child_value("y");
    camera_info_.uav_camera_offset_.y = atof(readingValue.c_str());
    readingValue = Configuration.child("camera_info").child("camera_offset").child_value("z");
    camera_info_.uav_camera_offset_.z = atof(readingValue.c_str());
    std::cout<<"camera_info_.uav_camera_offset_: "<<camera_info_.uav_camera_offset_<<std::endl;

    //******** Object in WORLD INFO ********
    readingValue = Configuration.child("object_in_world_info").child_value("width");
    object_in_world_info_.width_ = atof(readingValue.c_str());
    std::cout<<"object_in_world_info_.width_: "<<object_in_world_info_.width_<<std::endl;

    readingValue = Configuration.child("object_in_world_info").child_value("height");
    object_in_world_info_.height_ = atof(readingValue.c_str());
    std::cout<<"object_in_world_info_.height_: "<<object_in_world_info_.height_<<std::endl;
}

/// THIS FUNCTION IS BEING DEPRECATED
bool RlEnvironmentUavGimbalFollowingJuako::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response){
//  cv::Rect previous_roi, current_roi;
//  GetTargetROI(previous_roi, current_roi);
}

/// TO BE FILLED: GENERATE THE NORMALIZED STATE
void RlEnvironmentUavGimbalFollowingJuako::GetNormalizedState(std::vector<float> &normalized_state){
  // Get ROI
  cv::Rect previous_roi, current_roi;
  GetTargetROI(previous_roi, current_roi);

  // Fill vector
  normalized_state.push_back(current_roi.x / (RESOLUTION_VIRTUAL_CAMERA_X));
  normalized_state.push_back(current_roi.y / (RESOLUTION_VIRTUAL_CAMERA_Y));
  normalized_state.push_back(current_roi.width / (RESOLUTION_VIRTUAL_CAMERA_X));
  normalized_state.push_back(current_roi.height / (RESOLUTION_VIRTUAL_CAMERA_Y));
}

/// TO BE FILLED: ACTION TO SEND TO THE ENVIRONMENT
// Sends the action to Gazebo environment
bool RlEnvironmentUavGimbalFollowingJuako::StepAction(rl_srvs::AgentActionSrv::Request &request, rl_srvs::AgentActionSrv::Response &response){
  // Send velocity commands
  droneMsgsROS::droneSpeeds action_msg;
  action_msg.dx = request.action[0];
  action_msg.dy = request.action[1];
  action_msg.dz = 0;
  uav_speed_ref_pub_.publish(action_msg);
    return true;
}

/// TO BE FILLED: READ THE STATE, COMPUTE THE REWARD AND SEND IT BACK TO THE AGENT
// Reads from Gazebo environment and send the state back to the agent
bool RlEnvironmentUavGimbalFollowingJuako::StepState(rl_srvs::AgentStateSrv::Request &request, rl_srvs::AgentStateSrv::Response &response){
  // Create state
  std::vector<float> response_state;
  GetNormalizedState(response_state);

  response.obs_real = response_state;
  response.reward = 0;  // Dummy reward
  response.terminal_state = false;

    // Successful return
    return true;
}

bool RlEnvironmentUavGimbalFollowingJuako::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality ImageBasedVisualServoing service called" << std::endl;

    // Action dimensionality
    response.action_dim = environment_info_.actions_dim_;

    // Action max
    response.action_max = environment_info_.actions_max_value_;

    // Action min
    response.action_min = environment_info_.actions_min_value_;

    // States dimensionality
    response.state_dim_lowdim = environment_info_.state_dim_low_dim_;

    // Number of steps per episode
    response.num_iterations = environment_info_.num_episode_steps_;

    response.state_min = std::vector<float>(environment_info_.state_dim_low_dim_, MIN_STATE_);
    response.state_max = std::vector<float>(environment_info_.state_dim_low_dim_, MAX_STATE_);

    // Service succesfully executed
    return true;
}

bool RlEnvironmentUavGimbalFollowingJuako::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv ImageBasedVisualServoing service called" << std::endl;


    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
//    if (exp_rec_.getRecording())
//        long_iteration_ = true;

    cv::Rect previous_roi, current_roi;
    GetTargetROI(previous_roi, current_roi);

    // Create state
    std::vector<float> response_state;
    response_state.push_back(current_roi.x / (RESOLUTION_VIRTUAL_CAMERA_X));
    response_state.push_back(current_roi.y / (RESOLUTION_VIRTUAL_CAMERA_Y));
    response_state.push_back(current_roi.width / (RESOLUTION_VIRTUAL_CAMERA_X));
    response_state.push_back(current_roi.height/ (RESOLUTION_VIRTUAL_CAMERA_Y));

    response.state = response_state;

    return true;
}

/// TO BE FILLED: BEHAVIOR WHEN RESETTING THE EPISODE
bool RlEnvironmentUavGimbalFollowingJuako::Reset()
{
    // Check experiment recorder
//    if(exp_rec_.getRecording()){
//        exp_rec_.RecordExperiment();
//        exp_rec_.setRecording(false);
//    }
    // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch);

    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);

    // Random initial point
    std::srand (static_cast <unsigned> (time(0)));

    float lower_lim_x = -environment_info_.max_pos_x_;
    float upper_lim_x = environment_info_.max_pos_x_;
    float lower_lim_y = -environment_info_.max_pos_y_;
    float upper_lim_y = environment_info_.max_pos_y_;
    float init_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    float init_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));


    float lower_lim_z = kUav_Altitude_;
    float upper_lim_z = kUav_Altitude_ + 0.3;
    float init_altitude = lower_lim_z + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_z-lower_lim_z)));

    init_x = -0.4f;
    init_y = 0.0f;
    init_altitude = kUav_Altitude_;

    std::cout << "Reseting gimbal position..." << std::endl;

    // Reset gimbal position
    angular_position_gimbal_= {0.0f, -0.5f};

    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = kUav_Altitude_;
    uav_pose_ref_pub_.publish(altitude_msg);

    // Spawn model to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = init_altitude;
//    model_msg.request.model_state.pose.orientation.x = 0;
//    model_msg.request.model_state.pose.orientation.y = 0;
//    model_msg.request.model_state.pose.orientation.z = 0;
//    model_msg.request.model_state.pose.orientation.w = 1;


    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << "y: " << init_y << "z: "<<init_altitude<< std::endl;

    if (gazebo_client_.call(model_msg)){
//        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
//        return false;
    }

    // Spawn model to origin
    std_srvs::Empty estimator_msg;

    if (estimator_client_.call(estimator_msg)){
        ROS_INFO("RL_ENV_INFO: Reseting estimator..");
        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call estimator");
        return false;
    }
}

void RlEnvironmentUavGimbalFollowingJuako::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{
    cv::Point3f target_pose;
    cv::Point3f uav_pose;
    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            uav_pose.x = msg->pose[i].position.x;
            uav_pose.y = msg->pose[i].position.y;
            uav_pose.z = msg->pose[i].position.z;

            // Calculating Roll, Pitch, Yaw
            tf::Quaternion q(msg->pose[i].orientation.x, msg->pose[i].orientation.y, msg->pose[i].orientation.z, msg->pose[i].orientation.w);
            tf::Matrix3x3 m(q);

            //convert quaternion to euler angels
            double y, p, r;
            m.getEulerYPR(y, p, r);

            uav_state_.pos_x_ = uav_pose.x;
            uav_state_.pos_y_ = uav_pose.y;
            uav_state_.pos_z_ = uav_pose.z;

            uav_state_.yaw_ = y;
            uav_state_.pitch_ = p;
            uav_state_.roll_ = r;

        }
        else if(msg->name[i].compare(MOVING_TARGET_NAME) == 0)
        {
            target_pose.x = msg->pose[i].position.x;
            target_pose.y = msg->pose[i].position.y;
            target_pose.z = msg->pose[i].position.z;
        }
        else if(msg->name[i].compare(UAV_LEADER_NAME) == 0)
        {
            uav_leader_state_.pos_x_ = msg->pose[i].position.x;
            uav_leader_state_.pos_y_ = msg->pose[i].position.y;
            uav_leader_state_.pos_z_ = msg->pose[i].position.z;
        }
    }

#ifdef OBJECT_BASED_GROUND_TRUTH_GIMBAL
        //**** Object points w.r.t. WORLD frame of reference ****//
        // Upper side
        object_in_world_info_.current_points_mat_[0] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[0].at<float>(0,0) = target_pose.x + object_in_world_info_.width_ / 2.0;
        object_in_world_info_.current_points_mat_[0].at<float>(1,0) = target_pose.y + object_in_world_info_.width_ / 2.0;
        object_in_world_info_.current_points_mat_[0].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[0].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[1] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[1].at<float>(0,0) = target_pose.x + object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[1].at<float>(1,0) = target_pose.y - object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[1].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[1].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[2] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[2].at<float>(0,0) = target_pose.x - object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[2].at<float>(1,0) = target_pose.y - object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[2].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[2].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[3] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[3].at<float>(0,0) = target_pose.x - object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[3].at<float>(1,0) = target_pose.y + object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[3].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[3].at<float>(3,0) = 1.0;

        // Lower side
        object_in_world_info_.current_points_mat_[4] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[4].at<float>(0,0) = target_pose.x + object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[4].at<float>(1,0) = target_pose.y + object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[4].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[4].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[5] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[5].at<float>(0,0) = target_pose.x + object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[5].at<float>(1,0) = target_pose.y - object_in_world_info_.width_/2.0;
        object_in_world_info_.current_points_mat_[5].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[5].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[6] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[6].at<float>(0,0) = target_pose.x - object_in_world_info_.width_ / 2.0;
        object_in_world_info_.current_points_mat_[6].at<float>(1,0) = target_pose.y - object_in_world_info_.width_ / 2.0;
        object_in_world_info_.current_points_mat_[6].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[6].at<float>(3,0) = 1.0;

        object_in_world_info_.current_points_mat_[7] = cv::Mat(4, 1, CV_32FC1);
        object_in_world_info_.current_points_mat_[7].at<float>(0,0) = target_pose.x - object_in_world_info_.width_/ 2.0;
        object_in_world_info_.current_points_mat_[7].at<float>(1,0) = target_pose.y + object_in_world_info_.width_/ 2.0;
        object_in_world_info_.current_points_mat_[7].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
        object_in_world_info_.current_points_mat_[7].at<float>(3,0) = 1.0;

        cv::Point3f uav_target_relative_pose;
        uav_target_relative_pose.x = uav_pose.x - target_pose.x;
        uav_target_relative_pose.y = uav_pose.y - target_pose.y;
        uav_target_relative_pose.z = uav_pose.z - target_pose.z;

        ComputeGroundTruthObjectPositionInImageFishEye(uav_pose.x, uav_pose.y, uav_pose.z);
#endif
}

void RlEnvironmentUavGimbalFollowingJuako::CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &msg)
{
    if(!camera_info_.camera_info_available_flag_)
    {
        std::cout<<"Camera Info Callback"<<std::endl;
        camera_info_.camera_info_available_flag_ = true;

        camera_info_.cx_ = msg->K.at(2);
        camera_info_.cy_ = msg->K.at(5);
        camera_info_.fx_ = msg->K.at(0);
        camera_info_.fy_ = msg->K.at(4);

        camera_info_.image_width_ = RESOLUTION_VIRTUAL_CAMERA_X;
        camera_info_.image_height_ = RESOLUTION_VIRTUAL_CAMERA_Y;

        camera_info_.K_matrix_ = cv::Mat(3, 3, CV_32FC1);
        camera_info_.K_matrix_.at<float>(0,0) = camera_info_.fx_;
        camera_info_.K_matrix_.at<float>(0,1) = 0.0;
        camera_info_.K_matrix_.at<float>(0,2) = RESOLUTION_VIRTUAL_CAMERA_X/2 + 0.5;

        camera_info_.K_matrix_.at<float>(1,0) = 0.0;
        camera_info_.K_matrix_.at<float>(1,1) = camera_info_.fy_;
        camera_info_.K_matrix_.at<float>(1,2) = RESOLUTION_VIRTUAL_CAMERA_Y/2 + 0.5;

        camera_info_.K_matrix_.at<float>(2,0) = 0.0;
        camera_info_.K_matrix_.at<float>(2,1) = 0.0;
        camera_info_.K_matrix_.at<float>(2,2) = 1.0;

        K_matrix_fish_eye_ = cv::Mat(3, 3, CV_32FC1);
        K_matrix_fish_eye_.at<float>(0,0) = camera_info_.fx_;
        K_matrix_fish_eye_.at<float>(0,1) = 0.0;
        K_matrix_fish_eye_.at<float>(0,2) = camera_info_.cx_;

        K_matrix_fish_eye_.at<float>(1,0) = 0.0;
        K_matrix_fish_eye_.at<float>(1,1) = camera_info_.fy_;
        K_matrix_fish_eye_.at<float>(1,2) = camera_info_.cy_;

        K_matrix_fish_eye_.at<float>(2,0) = 0.0;
        K_matrix_fish_eye_.at<float>(2,1) = 0.0;
        K_matrix_fish_eye_.at<float>(2,2) = 1.0;


        std::cout<<"Camera Matrix: "<<std::endl<<camera_info_.K_matrix_<<std::endl<<std::endl;
    }
}

void RlEnvironmentUavGimbalFollowingJuako::CameraFishEyeCallback(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
    captured_image_.release();
    cv::Mat img = cv_bridge::toCvShare(msg, "bgr8")->image;


    captured_image_ = GetPictureGimbalAgent(img.clone());

  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
}

void RlEnvironmentUavGimbalFollowingJuako::SetUavState(const float x, const float y, const float z, const float dx, const float dy)
{
    uav_mutex_.lock();
    uav_state_.pos_x_ = x;
    uav_state_.pos_y_ = y;
    uav_state_.pos_z_ = z;
    uav_state_.speed_x_ = dx;
    uav_state_.speed_y_ = dy;
    uav_mutex_.unlock();
}

void RlEnvironmentUavGimbalFollowingJuako::GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float &pitch)
{
    uav_mutex_.lock();
    x = uav_state_.pos_x_;
    y = uav_state_.pos_y_;
    z = uav_state_.pos_z_;
    dx = uav_state_.speed_x_;
    dy = uav_state_.speed_y_;
    roll = uav_state_.roll_;
    pitch = uav_state_.pitch_;
    uav_mutex_.unlock();
}

cv::Mat RlEnvironmentUavGimbalFollowingJuako::ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma)
{
    cv::Mat R = cv::Mat(3, 3, CV_32FC1);
    R.at<float>(0,0) = cos(alpha)*cos(beta);
    R.at<float>(0,1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
    R.at<float>(0,2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);

    R.at<float>(1,0) = sin(alpha)*cos(beta);
    R.at<float>(1,1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
    R.at<float>(1,2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);

    R.at<float>(2,0) = -sin(beta);
    R.at<float>(2,1) = cos(beta)*sin(gamma);
    R.at<float>(2,2) = cos(beta)*cos(gamma);

    return R;

}

cv::Mat RlEnvironmentUavGimbalFollowingJuako::ComputeHomogeneousMatrixTransform(float px, float py, float pz, float yaw, float pitch, float roll)
{
    //Eigen::Matrix4d T_uav_world;
    cv::Mat R = ConvertFromYPRtoRotationMatrix(yaw, pitch, roll);

    cv::Mat T = cv::Mat::eye(4, 4, CV_32FC1);
    T.at<float>(0,0) = R.at<float>(0,0); T.at<float>(0,1) = R.at<float>(0,1); T.at<float>(0,2) = R.at<float>(0,2); T.at<float>(0,3) = px;
    T.at<float>(1,0) = R.at<float>(1,0); T.at<float>(1,1) = R.at<float>(1,1); T.at<float>(1,2) = R.at<float>(1,2); T.at<float>(1,3) = py;
    T.at<float>(2,0) = R.at<float>(2,0); T.at<float>(2,1) = R.at<float>(2,1); T.at<float>(2,2) = R.at<float>(2,2); T.at<float>(2,3) = pz;
    T.at<float>(3,0) = 0.0; T.at<float>(3,1) = 0.0; T.at<float>(3,2) = 0.0; T.at<float>(3,3) = 1.0;
    return T;
}

cv::Mat RlEnvironmentUavGimbalFollowingJuako::ComputeHomogeneousMatrixTransform(const float px, const float py, const float pz, const cv::Mat R)
{
    cv::Mat T = cv::Mat::eye(4, 4, CV_32FC1);
    T.at<float>(0,0) = R.at<float>(0,0); T.at<float>(0,1) = R.at<float>(0,1); T.at<float>(0,2) = R.at<float>(0,2); T.at<float>(0,3) = px;
    T.at<float>(1,0) = R.at<float>(1,0); T.at<float>(1,1) = R.at<float>(1,1); T.at<float>(1,2) = R.at<float>(1,2); T.at<float>(1,3) = py;
    T.at<float>(2,0) = R.at<float>(2,0); T.at<float>(2,1) = R.at<float>(2,1); T.at<float>(2,2) = R.at<float>(2,2); T.at<float>(2,3) = pz;
    T.at<float>(3,0) = 0.0; T.at<float>(3,1) = 0.0; T.at<float>(3,2) = 0.0; T.at<float>(3,3) = 1.0;
    return T;
}

cv::Mat RlEnvironmentUavGimbalFollowingJuako::StabilizeVirtualCamera(cv::Mat stabilizied_captured_image_){
    // From degrees to Pixels
    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);

    cv::Mat R_uav_cam = ConvertFromYPRtoRotationMatrix(M_PI/2, M_PI, 0);
    cv::Mat Rot_cam_gimbal = ConvertFromYPRtoRotationMatrix(0,  0, M_PI/2-0.558);
    cv::Mat Rotacion_uav_gimbal = R_uav_cam * Rot_cam_gimbal;

#ifdef STABILIZE_YAW
    cv::Mat Rotacion_wordl_uav = ConvertFromYPRtoRotationMatrix( uav_state_.yaw_, uav_state_.pitch_, uav_state_.roll_);
#else
    cv::Mat Rotacion_wordl_uav = ConvertFromYPRtoRotationMatrix( 0, uav_state_.pitch_, uav_state_.roll_);
#endif
    
    cv::Mat Rat = Rotacion_uav_gimbal.inv() * Rotacion_wordl_uav * Rotacion_uav_gimbal;
    cv::Mat StabilizedVirtualCamera_;

#ifdef SHOW_IMAGES
    cv::Mat ImShowNotStabilized_;
    resize(stabilizied_captured_image_, ImShowNotStabilized_, cv::Size(640, 380), 0, 0, cv::INTER_AREA); //
    cv::imshow("Sin estabilizar", ImShowNotStabilized_);
    cv::waitKey(1);
#endif

    if ((!stabilizied_captured_image_.empty()) && (!K_matrix_fish_eye_.empty())){
        H = K_matrix_fish_eye_ * Rat * K_matrix_fish_eye_.inv();
        // Stabilize image
        cv::warpPerspective(stabilizied_captured_image_, StabilizedVirtualCamera_, H, stabilizied_captured_image_.size());

#ifdef SHOW_IMAGES
        cv::Mat ImShowStabilized_;
        resize(StabilizedVirtualCamera_, ImShowStabilized_, cv::Size(640, 380), 0, 0, cv::INTER_AREA); //
        cv::imshow("Estabilizada", ImShowStabilized_);
        cv::waitKey(1);
#endif
    }
    else{
        StabilizedVirtualCamera_ = stabilizied_captured_image_.clone();
    }

    return StabilizedVirtualCamera_;
}

int RlEnvironmentUavGimbalFollowingJuako::AngularRotationToPixelDisplacement( const float HFOV, float angularDisplacement, const float resolutionAxis)
{ 
    float pixelDisplacement = (tan( angularDisplacement)* resolutionAxis)/(tan(HFOV/2)*2);
    return round(pixelDisplacement);
} 

std::vector<float> RlEnvironmentUavGimbalFollowingJuako::MoveGimbalPosition(std::vector<float> goalPosition){

    clock_t current_time_gimbal_ = clock();
    float elpasedTime = (float) double(previous_time_gimbal_ - current_time_gimbal_) /CLOCKS_PER_SEC;
    previous_time_gimbal_ = current_time_gimbal_;

    for (int i = 0; i < angular_position_gimbal_.size(); i++){
        if (angular_position_gimbal_[i] != goalPosition[i]){
                if(fabs(angular_position_gimbal_[i] - goalPosition[i]) < 0.05){
                    angular_position_gimbal_[i] = goalPosition[i];
                }
                else
                    if (angular_position_gimbal_[i] < goalPosition[i]){
                        angular_position_gimbal_[i] = angular_position_gimbal_[i] - elpasedTime * SPEED_MOVEMENT_GIMBAL_[i];
                    }
                    else
                        angular_position_gimbal_[i] = angular_position_gimbal_[i] + elpasedTime * SPEED_MOVEMENT_GIMBAL_[i];
            }
    }

    return angular_position_gimbal_;
}

cv::Mat RlEnvironmentUavGimbalFollowingJuako::GetPictureFromVirtualGimbal(cv::Mat fish_eye_picture_, std::vector<float> gimbal_position_){
    cv::Mat cropped_picture_;

    if (fish_eye_picture_.total() != 0){

        cv::Mat stabilized_fish_eye = StabilizeVirtualCamera(fish_eye_picture_);

        std::vector<float> angular_position_gimbal_vector = MoveGimbalPosition(gimbal_position_);
        float pixelPosition_X = AngularRotationToPixelDisplacement(HFOV_, angular_position_gimbal_vector[0], RESOLUTION_FISH_EYE_X-RESOLUTION_VIRTUAL_CAMERA_X);
        //Signo negativo porque en OpenCV esquina arriba izquierda es (0,0)
        float pixelPosition_Y = -AngularRotationToPixelDisplacement(VFOV_, angular_position_gimbal_vector[1], RESOLUTION_FISH_EYE_Y-RESOLUTION_VIRTUAL_CAMERA_Y);

        cv::Rect roi;
        roi.x = pixelPosition_X - RESOLUTION_VIRTUAL_CAMERA_X/2 + RESOLUTION_FISH_EYE_X/2;
        roi.y = pixelPosition_Y - RESOLUTION_VIRTUAL_CAMERA_Y/2 + RESOLUTION_FISH_EYE_Y/2;
        roi.width = RESOLUTION_VIRTUAL_CAMERA_X;
        roi.height = RESOLUTION_VIRTUAL_CAMERA_Y;

        roi_position_x = pixelPosition_X - RESOLUTION_VIRTUAL_CAMERA_X/2 + RESOLUTION_FISH_EYE_X/2; // USEFUL FOR ROI!! Theese variables are used to keep the position of the gimbal
        roi_position_y = pixelPosition_Y - RESOLUTION_VIRTUAL_CAMERA_Y/2 + RESOLUTION_FISH_EYE_Y/2;

        cropped_picture_ = stabilized_fish_eye(roi);

        cv::Mat cropped_picture_resize_;

        // Get current ROI
        cv::Rect previous_roi, current_roi;
        GetTargetROI(previous_roi, current_roi);
        resize(cropped_picture_, cropped_picture_resize_, cv::Size(640, 380), 0, 0, cv::INTER_AREA);
        cv::rectangle(cropped_picture_,  current_roi, cv::Scalar(255, 255, 0), 2, 4);
        cv::imshow("Final", cropped_picture_);
        cv::waitKey(1);
      
    } else{
        cropped_picture_ = fish_eye_picture_.clone();
    }
         
    return cropped_picture_;
}

void RlEnvironmentUavGimbalFollowingJuako::MoveGimbalAgent(std::vector<float> move_gimbal_){
    float MAX_STEP_X = 1;
    float MAX_STEP_Y = 1;
    std::cout<<"Acciones agente : "<< move_gimbal_[0]<< " " << move_gimbal_[1] << std::endl;

    angular_position_gimbal_controller_[0] = angular_position_gimbal_[0];
    angular_position_gimbal_controller_[1] = angular_position_gimbal_[1];


    angular_position_gimbal_controller_[0] = angular_position_gimbal_controller_[0] + MAX_STEP_X*move_gimbal_[0];
    angular_position_gimbal_controller_[1] = angular_position_gimbal_controller_[1] + MAX_STEP_Y*move_gimbal_[1];
    for (int j = 0; j < 3; j++){
        if (angular_position_gimbal_controller_[j] > angular_position_gimbal_controller_MAX_[j]){
            angular_position_gimbal_controller_[j] = angular_position_gimbal_controller_MAX_[j];
        }else if(angular_position_gimbal_controller_[j] < angular_position_gimbal_controller_MIN_[j]){
            angular_position_gimbal_controller_[j] = angular_position_gimbal_controller_MIN_[j];
        }
    }
}

cv::Mat RlEnvironmentUavGimbalFollowingJuako::GetPictureGimbalAgent(cv::Mat fish_eye_picture_){
    return GetPictureFromVirtualGimbal(fish_eye_picture_, angular_position_gimbal_controller_);
}

void RlEnvironmentUavGimbalFollowingJuako::ComputeGroundTruthObjectPositionInImageFishEye(const float &x_uav, const float &y_uav, const float &z_uav)
{
    if(camera_info_.camera_info_available_flag_)
    {
        // Transformations
#ifdef STABILIZE_YAW
        cv::Mat T_uav_world = ComputeHomogeneousMatrixTransform(-x_uav, -y_uav, -z_uav, 0, 0, 0);
#else
        cv::Mat T_uav_world =  ComputeHomogeneousMatrixTransform(0,0,0, -uav_state_.yaw_, 0, 0) * ComputeHomogeneousMatrixTransform(-x_uav, -y_uav, -z_uav, 0, 0, 0);
#endif
        cv::Mat T_cam_uav = ComputeHomogeneousMatrixTransform(0, 0, 0, ConvertFromYPRtoRotationMatrix(-M_PI/2, 0, M_PI));
        cv::Mat T_cam_gimbal = ComputeHomogeneousMatrixTransform(0, 0, 0, ConvertFromYPRtoRotationMatrix(0, 0, - (M_PI / 2 - 0.558)));

        // Destination = what_to_transform to reach origin
        cv::Mat T_cam_world = T_cam_gimbal * T_cam_uav * T_uav_world;

        //Points Referred to Camera frame of reference
        cv::Mat p1 = T_cam_world * object_in_world_info_.current_points_mat_[0]; p1.pop_back(1);
        cv::Mat p2 = T_cam_world * object_in_world_info_.current_points_mat_[1]; p2.pop_back(1);
        cv::Mat p3 = T_cam_world * object_in_world_info_.current_points_mat_[2]; p3.pop_back(1);
        cv::Mat p4 = T_cam_world * object_in_world_info_.current_points_mat_[3]; p4.pop_back(1);
        cv::Mat p5 = T_cam_world * object_in_world_info_.current_points_mat_[4]; p5.pop_back(1);
        cv::Mat p6 = T_cam_world * object_in_world_info_.current_points_mat_[5]; p6.pop_back(1);
        cv::Mat p7 = T_cam_world * object_in_world_info_.current_points_mat_[6]; p7.pop_back(1);
        cv::Mat p8 = T_cam_world * object_in_world_info_.current_points_mat_[7]; p8.pop_back(1);

        //Proyect the points Referred to Camera frame of reference into the image plane
        p1 = K_matrix_fish_eye_ * p1;
        p2 = K_matrix_fish_eye_ * p2;
        p3 = K_matrix_fish_eye_ * p3;
        p4 = K_matrix_fish_eye_ * p4;
        p5 = K_matrix_fish_eye_ * p5;
        p6 = K_matrix_fish_eye_ * p6;
        p7 = K_matrix_fish_eye_ * p7;
        p8 = K_matrix_fish_eye_ * p8;

        cv::Mat p1_image = p1/p1.at<float>(2,0);
        cv::Mat p2_image = p2/p2.at<float>(2,0);
        cv::Mat p3_image = p3/p3.at<float>(2,0);
        cv::Mat p4_image = p4/p4.at<float>(2,0);
        cv::Mat p5_image = p5/p5.at<float>(2,0);
        cv::Mat p6_image = p6/p6.at<float>(2,0);
        cv::Mat p7_image = p7/p7.at<float>(2,0);
        cv::Mat p8_image = p8/p8.at<float>(2,0);
        initial_points_.clear();
        initial_points_ = {p1_image, p2_image, p3_image, p4_image, p5_image, p6_image, p7_image, p8_image};

        // Search for maximum points
        float max_x = 0, max_y = 0;
        float min_x = 1E9, min_y = 1E9;
        for (int i = 0; i < initial_points_.size(); i++){
          if (initial_points_[i].at<float>(0,0) < min_x)
            min_x = initial_points_[i].at<float>(0,0);
          if (initial_points_[i].at<float>(1,0) < min_y)
            min_y = initial_points_[i].at<float>(1,0);
          if (initial_points_[i].at<float>(0,0) > max_x)
            max_x = initial_points_[i].at<float>(0,0);
          if (initial_points_[i].at<float>(1,0) > max_y)
            max_y = initial_points_[i].at<float>(1,0);
        }

        object_in_image_info_fish_eye_.ground_truth_detected_roi_rect_ = cv::Rect(cv::Point(p2_image.at<float>(0,0), p2_image.at<float>(1,0)),
                                                                                  cv::Point(p4_image.at<float>(0,0), p4_image.at<float>(1,0)));
        
        cv::Point2f roi_diag;
        roi_diag.x = p2_image.at<float>(0,0) - p1_image.at<float>(0,0);
        roi_diag.y = p2_image.at<float>(1,0) - p1_image.at<float>(1,0);
        object_in_image_info_fish_eye_.ground_truth_detected_roi_diameter_ = std::sqrt(std::pow(roi_diag.x, 2) + std::pow(roi_diag.y, 2));

        std::vector<cv::Mat> p_image;
        p_image.push_back(p1_image);
        p_image.push_back(p2_image);
        p_image.push_back(p3_image);
        p_image.push_back(p4_image);
        for(int i=0;i<p_image.size();i++)
        {
            object_in_image_info_fish_eye_.ground_truth_current_object_points_[i].x = p_image[i].at<float>(0,0);
            object_in_image_info_fish_eye_.ground_truth_current_object_points_[i].y = p_image[i].at<float>(1,0);
        }



        float ground_truth_roi_width= object_in_image_info_fish_eye_.ground_truth_current_object_points_[1].x
                - object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].x;

        float ground_truth_roi_height = object_in_image_info_fish_eye_.ground_truth_current_object_points_[1].y
                - object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].y;

        cv::Point2f ground_truth_roi_center;
        ground_truth_roi_center.x = object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].x
                + ground_truth_roi_width/2.0 - roi_position_x;
        ground_truth_roi_center.y = object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].y
                + ground_truth_roi_height/2.0 - roi_position_y;

        object_in_image_info_fish_eye_.gimbal_ground_truth_previous_detected_roi_rect_ = object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_;
        object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_ = cv::Rect(cv::Point(min_x - roi_position_x, min_y - roi_position_y),
                                                                         cv::Point(max_x - roi_position_x, max_y - roi_position_y));

    }
}

void RlEnvironmentUavGimbalFollowingJuako::GetTargetROI(cv::Rect &previous_roi, cv::Rect &current_roi){
  // Store in member variables
  previous_roi = object_in_image_info_fish_eye_.gimbal_ground_truth_previous_detected_roi_rect_;
  current_roi = object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_;
}

//void RlEnvironmentUavGimbalFollowingJuako::GetObjectInImageState(cv::RotatedRect &previous_roi, cv::RotatedRect &current_roi)
//{
//    std::cout << "I am being called" << std::endl;
//    object_in_image_mutex_.lock();

//    if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
//    {
//        previous_roi = object_in_image_info_.ground_truth_previous_detected_roi_;
//        current_roi = object_in_image_info_.ground_truth_detected_roi_;
//    }
//    else if(OBJECT_STATE_BASED_ON_STABILIZED_DETECTION)
//    {
//        previous_roi = object_in_image_info_.stabilized_previous_detected_roi_;
//        current_roi = object_in_image_info_.stabilized_detected_roi_;
//    }
//    else if(OBJECT_BASED_GROUND_TRUTH_GIMBAL)
//    {
//        previous_roi = object_in_image_info_.gimbal_ground_truth_previous_detected_roi_;
//        current_roi = object_in_image_info_.gimbal_ground_truth_detected_roi_;

//    }
//    else
//    {
//        previous_roi = object_in_image_info_.previous_detected_roi_;
//        current_roi = object_in_image_info_.detected_roi_;
//    }


//    object_in_image_mutex_.unlock();
//}

//std::vector<float> RlEnvironmentUavGimbalFollowingJuako::ComputeNormalizedObjectInImageState()
//{
//    state_.clear();
//    normalized_state_.clear();
//    cv::RotatedRect previous_object_roi;
//    cv::RotatedRect current_object_roi;
//    GetObjectInImageState(previous_object_roi, current_object_roi);


//    //Change for other reference position in Image.
//    error_in_image_.x = (current_object_roi.center.x - center_roi_ref_.x);
//    error_in_image_.y = (current_object_roi.center.y - center_roi_ref_.y);
//    float xy_distance_to_target = std::sqrt(std::pow(error_in_image_.x, 2) + std::pow(error_in_image_.y, 2));


////    std::cout<<"error_in_image_.x: "<<error_in_image_.x<<std::endl;
////    std::cout<<"error_in_image_.y: "<<error_in_image_.y<<std::endl;
////    std::cout<<"xy_distance_to_target: "<<xy_distance_to_target<<std::endl;
//    if(TEST_MODE)
//    {
//        error_in_image_norm_.x = (current_object_roi.center.x - (center_roi_ref_.x + offset_in_image_.x))/center_roi_ref_max_.x;
//        error_in_image_norm_.y = (current_object_roi.center.y - (center_roi_ref_.y + offset_in_image_.y))/center_roi_ref_max_.y;
//    }
//    else
//    {
//        error_in_image_norm_.x = (current_object_roi.center.x - center_roi_ref_.x)/center_roi_ref_max_.x;
//        error_in_image_norm_.y = (current_object_roi.center.y - center_roi_ref_.y)/center_roi_ref_max_.y;
//    }

//    //Compute the velocity of the Object in the Image plane
//    current_time_ = ros::Time::now();
//    double delta_t = (current_time_ - previous_time_).toSec();
//    previous_time_ = current_time_;
//    cv::Point2f delta_pos_object_roi_center, object_roi_center_speed_norm;
////    delta_pos_object_roi_center.x = (current_object_roi.center.x - previous_object_roi.center.x)/delta_t;
////    delta_pos_object_roi_center.y = (current_object_roi.center.y - previous_object_roi.center.y)/delta_t;
////    object_roi_center_speed_norm.x = delta_pos_object_roi_center.x/speed_roi_ref_max_.x;
////    object_roi_center_speed_norm.y = delta_pos_object_roi_center.y/speed_roi_ref_max_.y;
//    delta_pos_object_roi_center.x = current_object_roi.center.x - previous_object_roi.center.x;
//    delta_pos_object_roi_center.y = current_object_roi.center.y - previous_object_roi.center.y;
//    object_roi_center_speed_norm.x = delta_pos_object_roi_center.x/center_roi_ref_max_.x;
//    object_roi_center_speed_norm.y = delta_pos_object_roi_center.y/center_roi_ref_max_.y;


//    double x_t, dx_t;
//    double y_t, dy_t;
//    if(ESTIMATE_SPEEDS_WITH_CIRCULAR_BUFFER)
//    {
//        // Compute speeds from pose ground truth
//        double x_raw_t = error_in_image_.x;
//        double y_raw_t = error_in_image_.y;

//        time_t tv_sec; suseconds_t tv_usec;
//        tv_sec  = current_time_.sec;
//        tv_usec = current_time_.nsec / 1000.0;
//        filtered_derivative_wcb_x_.setInput( x_raw_t, tv_sec, tv_usec);
//        filtered_derivative_wcb_y_.setInput( y_raw_t, tv_sec, tv_usec);

//        filtered_derivative_wcb_x_.getOutput( x_t,  dx_t);
//        filtered_derivative_wcb_y_.getOutput( y_t,  dy_t);

//        object_roi_center_speed_norm.x = dx_t/center_roi_ref_max_.x;
//        object_roi_center_speed_norm.y = dy_t/center_roi_ref_max_.y;


//        //f_data_recorder<<delta_pos_object_roi_center.x/delta_t<<" ; "<<delta_pos_object_roi_center.y/delta_t<<" ; "<<dx_t<<" ; "<<dy_t<<std::endl;
//    }



//    if(OBJECT_STATE_BASED_ON_ROTATED_RECT)
//    {

//        for(int i=0;i<object_in_image_info_.rotated_rect_points_.size();i++)
//        {
//            float delta_x = (object_in_image_info_.rotated_rect_points_[i].x - center_roi_ref_.x);
//            float delta_y = (object_in_image_info_.rotated_rect_points_[i].y - center_roi_ref_.y);
//            float alpha = std::atan2(delta_y, delta_x);

//            //Substract the radius of the reference
//            float delta_x_substracted = delta_x - radius_ref_ * std::cos(alpha);
//            float delta_y_substracted = delta_y - radius_ref_ * std::sin(alpha);

////            std::cout<<"Distance point (radius substracted)["<<i<<"]: "<<delta_x_substracted<<std::endl;
////            std::cout<<"Distance point (radius substracted)["<<i<<"]: "<<delta_y_substracted<<std::endl;

//            float delta_x_norm = delta_x_substracted/center_roi_ref_max_.x;
//            float delta_y_norm = delta_y_substracted/center_roi_ref_max_.y;


//            state_.push_back(delta_x);
//            state_.push_back(delta_y);
//            normalized_state_.push_back(delta_x_norm);
//            normalized_state_.push_back(delta_y_norm);
//        }
//        //std::cout<<std::endl<<std::endl;
//        state_.push_back(delta_pos_object_roi_center.x);
//        state_.push_back(delta_pos_object_roi_center.y);
//        normalized_state_.push_back(object_roi_center_speed_norm.x);
//        normalized_state_.push_back(object_roi_center_speed_norm.y);
//    }
//    else
//    {
//        if(OBJECT_STATE_BASED_ON_ERROR_IN_POSITION_ONLY)
//        {
//            normalized_state_.push_back(error_in_image_norm_.x);
//            normalized_state_.push_back(error_in_image_norm_.y);
//        }
//        else
//        {
//            normalized_state_.push_back(error_in_image_norm_.x);
//            normalized_state_.push_back(error_in_image_norm_.y);
//            normalized_state_.push_back(object_roi_center_speed_norm.x);
//            normalized_state_.push_back(object_roi_center_speed_norm.y);
//        }

//        previous_error_in_diameter_ = current_error_in_diameter_;
//        if(OBJECT_STATE_BASED_ON_GROUND_TRUTH)
//        {
//            current_error_in_diameter_ = object_in_image_info_.ground_truth_detected_roi_diameter_ - object_in_image_info_.diameter_ref_;
//        }
//        else if(OBJECT_STATE_BASED_ON_STABILIZED_DETECTION)
//        {
//            current_error_in_diameter_ = object_in_image_info_.stabilized_detected_roi_diameter_ - object_in_image_info_.diameter_ref_;
//        }
//        else if(OBJECT_BASED_GROUND_TRUTH_GIMBAL)
//        {
////            current_error_in_diameter_ = object_in_image_info_.gimbal_ground_truth_detected_roi_diameter_ - object_in_image_info_.diameter_ref_;

//        }
//        else
//        {
//            current_error_in_diameter_ = object_in_image_info_.detected_roi_diameter_ - object_in_image_info_.diameter_ref_;
//        }


//        float current_error_in_diameter_norm = current_error_in_diameter_ / max_diameter_error_;
//        float delta_error_diameter = current_error_in_diameter_ - previous_error_in_diameter_;
//        float delta_error_diameter_norm = delta_error_diameter/max_diameter_error_;

//        if(DEBUG_ERROR_IN_DIAMETER)
//        {
//            std::cout<<"error_diameter: "<<current_error_in_diameter_<<std::endl;
//            std::cout<<"error_diameter_norm: "<<current_error_in_diameter_norm<<std::endl;
//            std::cout<<"delta_error_diameter: "<<delta_error_diameter<<std::endl;
//            std::cout<<"delta_error_diameter_norm: "<<delta_error_diameter_norm<<std::endl;
//        }
//        normalized_state_.push_back(current_error_in_diameter_norm);
//        normalized_state_.push_back(delta_error_diameter_norm);

//    }


//    //std::cout<<"current_error_in_diameter_: "<<current_error_in_diameter_<<std::endl;
//    //Check if the error IN PIXELS is less than a predefined threshold in order to give a POSITIVE REWARD
//    if((xy_distance_to_target < min_xy_distance_to_target_thresh_) && (std::abs(current_error_in_diameter_) < min_z_distance_to_target_thresh_))
//        min_distance_target_reached_flag_ = true;

//    if(OBJECT_STATE_INCLUDING_ROLL_PITCH)
//    {
//        // Get state
//        float x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch;
//        GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch);

//        normalized_state_.push_back(roll);
//        normalized_state_.push_back(pitch);

//    }


//    //Ensure that all the components in the NORMALIZED STATE vector are within [-1.0, 1.0]
//    for(int i=0;i<normalized_state_.size();i++)
//    {
//        if(normalized_state_[i] > 1.0)
//            normalized_state_[i] = 1.0;
//        else if(normalized_state_[i] < -1.0)
//            normalized_state_[i] = -1.0;
//    }


////    std_msgs::Float32MultiArray state_msg;
////    state_msg.layout.dim.push_back(std_msgs::MultiArrayDimension());
////    state_msg.layout.dim[0].size = normalized_state_.size();
////    state_msg.layout.dim[0].stride = 1;
////    state_msg.layout.dim[0].label = "x"; // or whatever name you typically use to index vec1
////    state_msg.data.insert(state_msg.data.end(), normalized_state_.begin(), normalized_state_.end());
////    rl_environment_state_pub_.publish(state_msg);

//    return normalized_state_;

//}

//void RlEnvironmentUavGimbalFollowingJuako::ComputeObjectReferenceInImage()
//{
//    if(camera_info_.camera_info_available_flag_)
//    {
//        //Computation of REFERENCE ROI in image based on the desirable UAV height
//        //This computation takes into account the projection of the object points using the
//        //INTRINSIC CAMERA PARAMETERS
//        cv::Mat p1 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[0];
//        cv::Mat p2 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[1];
//        cv::Mat p3 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[2];
//        cv::Mat p4 = camera_info_.K_matrix_ * object_in_world_info_.points_mat_[3];

//        cv::Mat p1_image = p1/p1.at<float>(2,0);
//        cv::Mat p2_image = p2/p2.at<float>(2,0);
//        cv::Mat p3_image = p3/p3.at<float>(2,0);
//        cv::Mat p4_image = p4/p4.at<float>(2,0);

//        roi_reference_ = cv::Rect(cv::Point(p1_image.at<float>(0,0), p1_image.at<float>(1,0)),
//                                  cv::Point(p3_image.at<float>(0,0), p3_image.at<float>(1,0)));
//        cv::Point2f diagonal_ref;
//        diagonal_ref.x = p3_image.at<float>(0,0) - p1_image.at<float>(0,0);
//        diagonal_ref.y = p3_image.at<float>(1,0) - p1_image.at<float>(1,0);
//        center_roi_ref_.x = p1_image.at<float>(0,0) + diagonal_ref.x/2;
//        center_roi_ref_.y = p1_image.at<float>(1,0) + diagonal_ref.y/2;
//        radius_ref_ = std::sqrt(std::pow(diagonal_ref.x, 2) + std::pow(diagonal_ref.y, 2))/2;
//        object_in_image_info_.diameter_ref_ = 2*radius_ref_;


//        //Computation of the maximum diameter that the object can have in the image based on the
//        //min height that the UAV can reach in the experiment
//        cv::Mat p1_min_height = camera_info_.K_matrix_ * object_in_world_info_.points_min_height_mat_[0];
//        cv::Mat p2_min_height = camera_info_.K_matrix_ * object_in_world_info_.points_min_height_mat_[1];
//        cv::Mat p3_min_height = camera_info_.K_matrix_ * object_in_world_info_.points_min_height_mat_[2];
//        cv::Mat p4_min_height = camera_info_.K_matrix_ * object_in_world_info_.points_min_height_mat_[3];

//        cv::Mat p1_image_min_height = p1_min_height/p1_min_height.at<float>(2,0);
//        cv::Mat p2_image_min_height = p2_min_height/p2_min_height.at<float>(2,0);
//        cv::Mat p3_image_min_height = p3_min_height/p3_min_height.at<float>(2,0);
//        cv::Mat p4_image_min_height = p4_min_height/p4_min_height.at<float>(2,0);

//        roi_reference_min_height_ = cv::Rect(cv::Point(p1_image_min_height.at<float>(0,0), p1_image_min_height.at<float>(1,0)),
//                                  cv::Point(p3_image_min_height.at<float>(0,0), p3_image_min_height.at<float>(1,0)));

//        cv::Point2f diagonal_roi_reference_min_height;
//        diagonal_roi_reference_min_height.x = p3_image_min_height.at<float>(0,0) - p1_image_min_height.at<float>(0,0);
//        diagonal_roi_reference_min_height.y = p3_image_min_height.at<float>(1,0) - p1_image_min_height.at<float>(1,0);
//        radius_min_height_ = std::sqrt(std::pow(diagonal_roi_reference_min_height.x, 2) + std::pow(diagonal_roi_reference_min_height.y, 2))/2;
//        object_in_image_info_.diameter_ref_min_height_ = 2*radius_min_height_;


//        max_diameter_error_ = object_in_image_info_.diameter_ref_min_height_ - object_in_image_info_.diameter_ref_;
//        std::cout<<"******* MAX DIAMETER ERROR *******"<<std::endl<<max_diameter_error_<<std::endl;

//        if(COMPUTE_NORM_VALUE_WRT_ROI_REFERENCE)
//        {
//            if(center_roi_ref_.x >= (camera_info_.image_width_ - center_roi_ref_.x))
//                center_roi_ref_max_.x = center_roi_ref_.x;
//            else
//                center_roi_ref_max_.x = camera_info_.image_width_ - center_roi_ref_.x;

//            if(center_roi_ref_.y >= (camera_info_.image_height_ - center_roi_ref_.y))
//                center_roi_ref_max_.y = center_roi_ref_.y;
//            else
//                center_roi_ref_max_.y = camera_info_.image_height_ - center_roi_ref_.y;
//        }
//        else
//        {
//            center_roi_ref_max_.x = camera_info_.image_width_/2.0;
//            center_roi_ref_max_.y = camera_info_.image_height_/2.0;
//        }

//        std::cout<<"center_roi_ref_.x: "<<center_roi_ref_.x<<std::endl;
//        std::cout<<"center_roi_ref_.y: "<<center_roi_ref_.y<<std::endl;

//        std::cout<<"p1_image: "<<p1_image<<std::endl;
//        std::cout<<"p2_image: "<<p2_image<<std::endl;
//        std::cout<<"p3_image: "<<p3_image<<std::endl;
//        std::cout<<"p4_image: "<<p4_image<<std::endl;
//        std::cout<<"ROI Reference: "<<roi_reference_<<std::endl;

//        std::cout<<"center_roi_ref_max_.x: "<<center_roi_ref_max_.x<<std::endl;
//        std::cout<<"center_roi_ref_max_.y: "<<center_roi_ref_max_.y<<std::endl;


//        speed_roi_ref_max_.x = center_roi_ref_max_.x * 30; //ShapeColor_ObjectDetector FRECUENCY
//        speed_roi_ref_max_.y = center_roi_ref_max_.y * 30;
//    }
//}

//void RlEnvironmentUavGimbalFollowingJuako::ComputeGroundTruthObjectPositionInImage(const float &x_uav, const float &y_uav, const float &z_uav)
//{
//    if(camera_info_.camera_info_available_flag_)
//    {
//        //Compute virtual WORLD -> UAV transformation as the UAV having NO ROTATION (and thus, NO rotation in CAMERA)
//        cv::Mat T_world_uav = ComputeHomogeneousMatrixTransform(x_uav, y_uav, z_uav, 0, 0, 0);
//        cv::Mat T_world_cam = T_world_uav * T_uav_cam_;
//        cv::Mat T_cam_world = T_world_cam.inv();


//        //Points Referred to Camera frame of reference
//        cv::Mat p1 = T_cam_world * object_in_world_info_.current_points_mat_[0]; p1.pop_back(1);
//        cv::Mat p2 = T_cam_world * object_in_world_info_.current_points_mat_[1]; p2.pop_back(1);
//        cv::Mat p3 = T_cam_world * object_in_world_info_.current_points_mat_[2]; p3.pop_back(1);
//        cv::Mat p4 = T_cam_world * object_in_world_info_.current_points_mat_[3]; p4.pop_back(1);
//        cv::Mat p5 = T_cam_world * object_in_world_info_.current_points_mat_[4]; p5.pop_back(1);
//        cv::Mat p6 = T_cam_world * object_in_world_info_.current_points_mat_[5]; p6.pop_back(1);
//        cv::Mat p7 = T_cam_world * object_in_world_info_.current_points_mat_[6]; p7.pop_back(1);
//        cv::Mat p8 = T_cam_world * object_in_world_info_.current_points_mat_[7]; p8.pop_back(1);


//        //Proyect the points Referred to Camera frame of reference into the image plane
//        p1 = camera_info_.K_matrix_ * p1;
//        p2 = camera_info_.K_matrix_ * p2;
//        p3 = camera_info_.K_matrix_ * p3;
//        p4 = camera_info_.K_matrix_ * p4;
//        p5 = camera_info_.K_matrix_ * p5;
//        p6 = camera_info_.K_matrix_ * p6;
//        p7 = camera_info_.K_matrix_ * p7;
//        p8 = camera_info_.K_matrix_ * p8;


//        cv::Mat p1_image = p1/p1.at<float>(2,0);
//        cv::Mat p2_image = p2/p2.at<float>(2,0);
//        cv::Mat p3_image = p3/p3.at<float>(2,0);
//        cv::Mat p4_image = p4/p4.at<float>(2,0);
//        cv::Mat p5_image = p5/p5.at<float>(2,0);
//        cv::Mat p6_image = p6/p6.at<float>(2,0);
//        cv::Mat p7_image = p7/p7.at<float>(2,0);
//        cv::Mat p8_image = p8/p8.at<float>(2,0);
//        std::vector<cv::Mat> initial_points = {p1_image, p2_image, p3_image, p4_image, p5_image, p6_image, p7_image, p8_image};

//        // Search for maximum points
//        float max_x = 0, max_y = 0;
//        float min_x = 1E9, min_y = 1E9;
//        for (int i = 0; i < initial_points.size(); i++){
//          if (initial_points[i].at<float>(0,0) < min_x)
//            min_x = initial_points[i].at<float>(0,0);
//          if (initial_points[i].at<float>(1,0) < min_y)
//            min_y = initial_points[i].at<float>(1,0);
//          if (initial_points[i].at<float>(0,0) > max_x)
//            max_x = initial_points[i].at<float>(0,0);
//          if (initial_points[i].at<float>(1,0) > max_y)
//            max_y = initial_points[i].at<float>(1,0);
//        }

//        object_in_image_info_.ground_truth_detected_roi_rect_ = cv::Rect(cv::Point(p3.at<float>(0,0), p3.at<float>(1,0)),
//                                                                         cv::Point(p1.at<float>(0,0), p1.at<float>(1,0)));
//        cv::Point2f roi_diag;
//        roi_diag.x = p3_image.at<float>(0,0) - p1_image.at<float>(0,0);
//        roi_diag.y = p3_image.at<float>(1,0) - p1_image.at<float>(1,0);
//        object_in_image_info_.ground_truth_detected_roi_diameter_ = std::sqrt(std::pow(roi_diag.x, 2) + std::pow(roi_diag.y, 2));

//        std::vector<cv::Mat> p_image;
//        p_image.push_back(p1_image);
//        p_image.push_back(p2_image);
//        p_image.push_back(p3_image);
//        p_image.push_back(p4_image);
//        for(int i=0;i<p_image.size();i++)
//        {
//            object_in_image_info_.ground_truth_current_object_points_[i].x = p_image[i].at<float>(0,0);
//            object_in_image_info_.ground_truth_current_object_points_[i].y = p_image[i].at<float>(1,0);
//        }



//        float ground_truth_roi_width = object_in_image_info_.ground_truth_current_object_points_[1].x
//                - object_in_image_info_.ground_truth_current_object_points_[0].x;

//        float ground_truth_roi_height = object_in_image_info_.ground_truth_current_object_points_[3].y
//                - object_in_image_info_.ground_truth_current_object_points_[0].y;

//        cv::Point2f ground_truth_roi_center;
//        ground_truth_roi_center.x = object_in_image_info_.ground_truth_current_object_points_[0].x
//                + ground_truth_roi_width/2.0;
//        ground_truth_roi_center.y = object_in_image_info_.ground_truth_current_object_points_[0].y
//                + ground_truth_roi_height/2.0;


//        object_in_image_info_.ground_truth_previous_detected_roi_ = object_in_image_info_.ground_truth_detected_roi_;
//        object_in_image_info_.ground_truth_detected_roi_ = cv::RotatedRect(ground_truth_roi_center,
//                                                                           cv::Size(ground_truth_roi_width, ground_truth_roi_height), 0.0);
//    }

//}

//void RlEnvironmentUavGimbalFollowingJuako::ComputeGroundTruthAndRealObjectPositionInImage(const float &x_uav, const float &y_uav, const float &z_uav)
//{
//    if(camera_info_.camera_info_available_flag_)
//    {
//        //Compute virtual WORLD -> UAV transformation as the UAV having NO ROTATION (and thus, NO rotation in CAMERA)
//        cv::Mat T_world_uav = ComputeHomogeneousMatrixTransform(x_uav, y_uav, z_uav, 0, 0, 0);
//        cv::Mat T_world_cam = T_world_uav * T_uav_cam_;
//        cv::Mat T_cam_world = T_world_cam.inv();

//        //Points Referred to Camera frame of reference
//        cv::Mat p1 = T_cam_world * object_in_world_info_.current_points_mat_[0]; p1.pop_back(1);
//        cv::Mat p2 = T_cam_world * object_in_world_info_.current_points_mat_[1]; p2.pop_back(1);
//        cv::Mat p3 = T_cam_world * object_in_world_info_.current_points_mat_[2]; p3.pop_back(1);
//        cv::Mat p4 = T_cam_world * object_in_world_info_.current_points_mat_[3]; p4.pop_back(1);
//        cv::Mat p5 = T_cam_world * object_in_world_info_.current_points_mat_[4]; p5.pop_back(1);
//        cv::Mat p6 = T_cam_world * object_in_world_info_.current_points_mat_[5]; p6.pop_back(1);
//        cv::Mat p7 = T_cam_world * object_in_world_info_.current_points_mat_[6]; p7.pop_back(1);
//        cv::Mat p8 = T_cam_world * object_in_world_info_.current_points_mat_[7]; p8.pop_back(1);



//        //Proyect the points Referred to Camera frame of reference into the image plane
//        p1 = camera_info_.K_matrix_ * p1;
//        p2 = camera_info_.K_matrix_ * p2;
//        p3 = camera_info_.K_matrix_ * p3;
//        p4 = camera_info_.K_matrix_ * p4;
//        p5 = camera_info_.K_matrix_ * p5;
//        p6 = camera_info_.K_matrix_ * p6;
//        p7 = camera_info_.K_matrix_ * p7;
//        p8 = camera_info_.K_matrix_ * p8;


//        cv::Mat p1_image = p1/p1.at<float>(2,0);
//        cv::Mat p2_image = p2/p2.at<float>(2,0);
//        cv::Mat p3_image = p3/p3.at<float>(2,0);
//        cv::Mat p4_image = p4/p4.at<float>(2,0);
//        cv::Mat p5_image = p5/p5.at<float>(2,0);
//        cv::Mat p6_image = p6/p6.at<float>(2,0);
//        cv::Mat p7_image = p7/p7.at<float>(2,0);
//        cv::Mat p8_image = p8/p8.at<float>(2,0);
//        std::vector<cv::Mat> initial_points = {p1_image, p2_image, p3_image, p4_image, p5_image, p6_image, p7_image, p8_image};

//        // Search for maximum points
//        float max_x = 0, max_y = 0;
//        float min_x = 1E9, min_y = 1E9;
//        for (int i = 0; i < initial_points.size(); i++){
//          if (initial_points[i].at<float>(0,0) < min_x)
//            min_x = initial_points[i].at<float>(0,0);
//          if (initial_points[i].at<float>(1,0) < min_y)
//            min_y = initial_points[i].at<float>(1,0);
//          if (initial_points[i].at<float>(0,0) > max_x)
//            max_x = initial_points[i].at<float>(0,0);
//          if (initial_points[i].at<float>(1,0) > max_y)
//            max_y = initial_points[i].at<float>(1,0);
//        }
//        cv::Rect truth_rect = cv::Rect(cv::Point(min_x, min_y),
//                                       cv::Point(max_x, max_y));


//        // *** TRANSFORMATION for seeing the object points projection in the ROTATED CAMERA
//        //Compute virtual WORLD -> UAV transformation as the UAV having NO ROTATION (and thus, NO rotation in CAMERA)
//        cv::Mat T_world_uav_rot = ComputeHomogeneousMatrixTransform(x_uav, y_uav, z_uav, uav_state_.yaw_, uav_state_.pitch_, uav_state_.roll_);
//        cv::Mat T_world_cam_rot = T_world_uav_rot * T_uav_cam_;
//        cv::Mat T_rotated_cam_world = T_world_cam_rot.inv();


//        //Points Referred to Camera frame of reference
//        cv::Mat p1_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[0]; p1.pop_back(1);
//        cv::Mat p2_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[1]; p2.pop_back(1);
//        cv::Mat p3_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[2]; p3.pop_back(1);
//        cv::Mat p4_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[3]; p4.pop_back(1);
//        cv::Mat p5_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[4]; p5.pop_back(1);
//        cv::Mat p6_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[5]; p6.pop_back(1);
//        cv::Mat p7_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[6]; p7.pop_back(1);
//        cv::Mat p8_rot = T_rotated_cam_world * object_in_world_info_.current_points_mat_[7]; p8.pop_back(1);


//        //Proyect the points Referred to Camera frame of reference into the image plane
//        p1_rot = camera_info_.K_matrix_ * p1_rot;
//        p2_rot = camera_info_.K_matrix_ * p2_rot;
//        p3_rot = camera_info_.K_matrix_ * p3_rot;
//        p4_rot = camera_info_.K_matrix_ * p4_rot;
//        p5_rot = camera_info_.K_matrix_ * p5_rot;
//        p6_rot = camera_info_.K_matrix_ * p6_rot;
//        p7_rot = camera_info_.K_matrix_ * p7_rot;
//        p8_rot = camera_info_.K_matrix_ * p8_rot;

//        cv::Mat p1_image_rot = p1_rot/p1_rot.at<float>(2,0);
//        cv::Mat p2_image_rot = p2_rot/p2_rot.at<float>(2,0);
//        cv::Mat p3_image_rot = p3_rot/p3_rot.at<float>(2,0);
//        cv::Mat p4_image_rot = p4_rot/p4_rot.at<float>(2,0);
//        cv::Mat p5_image_rot = p5_rot/p1_rot.at<float>(2,0);
//        cv::Mat p6_image_rot = p6_rot/p2_rot.at<float>(2,0);
//        cv::Mat p7_image_rot = p7_rot/p3_rot.at<float>(2,0);
//        cv::Mat p8_image_rot = p8_rot/p4_rot.at<float>(2,0);
//        initial_points.clear();
//        initial_points = {p1_image_rot, p2_image_rot, p3_image_rot, p4_image_rot, p5_image_rot, p6_image_rot, p7_image_rot, p8_image_rot};

//        // Search for maximum points
//        max_x = 0; max_y = 0;
//        min_x = 1E9; min_y = 1E9;
//        for (int i = 0; i < initial_points.size(); i++){
//          if (initial_points[i].at<float>(0,0) < min_x)
//            min_x = initial_points[i].at<float>(0,0);
//          if (initial_points[i].at<float>(1,0) < min_y)
//            min_y = initial_points[i].at<float>(1,0);
//          if (initial_points[i].at<float>(0,0) > max_x)
//            max_x = initial_points[i].at<float>(0,0);
//          if (initial_points[i].at<float>(1,0) > max_y)
//            max_y = initial_points[i].at<float>(1,0);
//        }

//        cv::Rect truth_rect_rot = cv::Rect(cv::Point(min_x, min_y),
//                                           cv::Point(max_x, max_y));


//        cv::Mat I_truth_rects = captured_image_.clone();

//        cv::line(I_truth_rects, cv::Point(p1_image.at<float>(0,0), p1_image.at<float>(1,0)),
//                                cv::Point(p2_image.at<float>(0,0), p2_image.at<float>(1,0)), cv::Scalar(255, 255, 0), 2, 8);
//        cv::line(I_truth_rects, cv::Point(p2_image.at<float>(0,0), p2_image.at<float>(1,0)),
//                                cv::Point(p3_image.at<float>(0,0), p3_image.at<float>(1,0)), cv::Scalar(255, 255, 0), 2, 8);
//        cv::line(I_truth_rects, cv::Point(p3_image.at<float>(0,0), p3_image.at<float>(1,0)),
//                                cv::Point(p4_image.at<float>(0,0), p4_image.at<float>(1,0)), cv::Scalar(255, 255, 0), 2, 8);
//        cv::line(I_truth_rects, cv::Point(p4_image.at<float>(0,0), p4_image.at<float>(1,0)),
//                                cv::Point(p1_image.at<float>(0,0), p1_image.at<float>(1,0)), cv::Scalar(255, 255, 0), 2, 8);


//        cv::line(I_truth_rects, cv::Point(p1_image_rot.at<float>(0,0), p1_image_rot.at<float>(1,0)),
//                                cv::Point(p2_image_rot.at<float>(0,0), p2_image_rot.at<float>(1,0)), cv::Scalar(0, 255, 255), 2, 8);
//        cv::line(I_truth_rects, cv::Point(p2_image_rot.at<float>(0,0), p2_image_rot.at<float>(1,0)),
//                                cv::Point(p3_image_rot.at<float>(0,0), p3_image_rot.at<float>(1,0)), cv::Scalar(0, 255, 255), 2, 8);
//        cv::line(I_truth_rects, cv::Point(p3_image_rot.at<float>(0,0), p3_image_rot.at<float>(1,0)),
//                                cv::Point(p4_image_rot.at<float>(0,0), p4_image_rot.at<float>(1,0)), cv::Scalar(0, 255, 255), 2, 8);
//        cv::line(I_truth_rects, cv::Point(p4_image_rot.at<float>(0,0), p4_image_rot.at<float>(1,0)),
//                                cv::Point(p1_image_rot.at<float>(0,0), p1_image_rot.at<float>(1,0)), cv::Scalar(0, 255, 255), 2, 8);


//        //cv::rectangle(I_truth_rects, truth_rect, cv::Scalar(255, 255, 0), 2, 4);
//        //cv::rectangle(I_truth_rects, truth_rect_rot, cv::Scalar(0, 255, 255), 2, 4);
//        cv::imshow("TRUTH RECTS", I_truth_rects);
//        cv::waitKey(1);

//    }

//}

//void RlEnvironmentUavGimbalFollowingJuako::DrawResults()
//{
//    cv::Mat I_for_drawing = captured_image_.clone();
//    if(object_in_image_info_.detected_roi_.boundingRect().area() > 0 && !I_for_drawing.empty())
//    {
//        if(IMSHOW_GROUND_TRUTH_OBJECT)
//        {
////            cv::Point2f ground_truth_corner_points[4];
////            object_in_image_info_.ground_truth_detected_roi_.points(ground_truth_corner_points);
////            for(int j = 0; j < 4; j++)
////            {
////                cv::circle(I_for_drawing, ground_truth_corner_points[j], 3, cv::Scalar(255, 255, 255), 1);
////                cv::line(I_for_drawing, ground_truth_corner_points[j], ground_truth_corner_points[(j+1)%4], cv::Scalar(255, 255, 0), 2, 8);
////            }
//            cv::rectangle(I_for_drawing, object_in_image_info_.ground_truth_detected_roi_rect_, cv::Scalar(255, 255, 0), 2, 4);
//            cv::circle(I_for_drawing, cv::Point(object_in_image_info_.ground_truth_detected_roi_.center.x,
//                                                  object_in_image_info_.ground_truth_detected_roi_.center.y),
//                                                  object_in_image_info_.ground_truth_detected_roi_diameter_/2.0, cv::Scalar(255, 255, 0), 2);
//            cv::Point line1_ini = cv::Point(object_in_image_info_.ground_truth_detected_roi_.center.x, object_in_image_info_.ground_truth_detected_roi_.center.y - 10);
//            cv::Point line1_end = cv::Point(object_in_image_info_.ground_truth_detected_roi_.center.x, object_in_image_info_.ground_truth_detected_roi_.center.y + 10);
//            cv::Point line2_ini = cv::Point(object_in_image_info_.ground_truth_detected_roi_.center.x - 10, object_in_image_info_.ground_truth_detected_roi_.center.y);
//            cv::Point line2_end = cv::Point(object_in_image_info_.ground_truth_detected_roi_.center.x + 10, object_in_image_info_.ground_truth_detected_roi_.center.y);
////            cv::line(I_for_drawing, line1_ini, line1_end, cv::Scalar(255, 255, 0), 2);
////            cv::line(I_for_drawing, line2_ini, line2_end, cv::Scalar(255, 255, 0), 2);
//        }

//        if(IMSHOW_DETECTED_OBJECT)
//        {
//            for(int j = 0; j < object_in_image_info_.detected_roi_rect_points_.size(); j++)
//            {
//                cv::circle(I_for_drawing, object_in_image_info_.detected_roi_rect_points_[j], 3, cv::Scalar(0, 255, 255), 1);
//            }

//            cv::rectangle(I_for_drawing, object_in_image_info_.detected_roi_rect_, cv::Scalar(0, 255, 0), 2, 4);
//            cv::Point line1_ini = cv::Point(object_in_image_info_.detected_roi_.center.x, object_in_image_info_.detected_roi_.center.y - 10);
//            cv::Point line1_end = cv::Point(object_in_image_info_.detected_roi_.center.x, object_in_image_info_.detected_roi_.center.y + 10);
//            cv::Point line2_ini = cv::Point(object_in_image_info_.detected_roi_.center.x - 10, object_in_image_info_.detected_roi_.center.y);
//            cv::Point line2_end = cv::Point(object_in_image_info_.detected_roi_.center.x + 10, object_in_image_info_.detected_roi_.center.y);
//            cv::line(I_for_drawing, line1_ini, line1_end, cv::Scalar(0, 255, 0), 2);
//            cv::line(I_for_drawing, line2_ini, line2_end, cv::Scalar(0, 255, 0), 2);
////            cv::circle(I_for_drawing, cv::Point(object_in_image_info_.detected_roi_.center.x, object_in_image_info_.detected_roi_.center.y),
////                       object_in_image_info_.detected_roi_diameter_/2.0, cv::Scalar(0, 255, 0), 2);

//        }


//        if(IMSHOW_STABILIZED_DETECTED_OBJECT)
//        {
//            cv::rectangle(I_for_drawing, object_in_image_info_.stabilized_detected_roi_rect_, cv::Scalar(0, 255, 255), 2, 4);
//            cv::circle(I_for_drawing, cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x,
//                                                  object_in_image_info_.stabilized_detected_roi_.center.y),
//                                                  object_in_image_info_.stabilized_detected_roi_diameter_/2.0, cv::Scalar(0, 255, 255), 2);
//            cv::Point line1_ini = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x, object_in_image_info_.stabilized_detected_roi_.center.y - 10);
//            cv::Point line1_end = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x, object_in_image_info_.stabilized_detected_roi_.center.y + 10);
//            cv::Point line2_ini = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x - 10, object_in_image_info_.stabilized_detected_roi_.center.y);
//            cv::Point line2_end = cv::Point(object_in_image_info_.stabilized_detected_roi_.center.x + 10, object_in_image_info_.stabilized_detected_roi_.center.y);
//            cv::line(I_for_drawing, line1_ini, line1_end, cv::Scalar(0, 255, 255), 2);
//            cv::line(I_for_drawing, line2_ini, line2_end, cv::Scalar(0, 255, 255), 2);
//        }

//        if(OBJECT_STATE_BASED_ON_ROTATED_RECT)
//        {
//            char corner_point_id[10];
//            for(unsigned int j = 0; j < object_in_image_info_.rotated_rect_points_.size(); j++)
//            {
//                std::sprintf(corner_point_id, "%d", j);
//                cv::putText(I_for_drawing, corner_point_id, object_in_image_info_.rotated_rect_points_[j],
//                            cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(255, 255, 255), 2);
//                cv::line(I_for_drawing,  cv::Point(center_roi_ref_.x, center_roi_ref_.y),
//                         object_in_image_info_.rotated_rect_points_[j], cv::Scalar(0, 255, 255), 2, 8);
//            }

//        }
//        else
//        {
//            cv::RotatedRect previous_object_roi;
//            cv::RotatedRect current_object_roi;
//            GetObjectInImageState(previous_object_roi, current_object_roi);
//            cv::line(I_for_drawing,  cv::Point(center_roi_ref_.x, center_roi_ref_.y),
//                     cv::Point(current_object_roi.center.x, current_object_roi.center.y), cv::Scalar(0, 255, 255), 2, 8);
//        }


//        //**** Draw References in image ****
//        cv::rectangle(I_for_drawing, roi_reference_, cv::Scalar(255, 0, 0), 2);
//        cv::circle(I_for_drawing, cv::Point(center_roi_ref_.x, center_roi_ref_.y),
//                   object_in_image_info_.diameter_ref_/2.0, cv::Scalar(255, 0, 0), 2);
//        cv::Point line1_ini_ref = cv::Point(center_roi_ref_.x, center_roi_ref_.y - 10);
//        cv::Point line1_end_ref = cv::Point(center_roi_ref_.x, center_roi_ref_.y + 10);
//        cv::Point line2_ini_ref = cv::Point(center_roi_ref_.x - 10, center_roi_ref_.y);
//        cv::Point line2_end_ref = cv::Point(center_roi_ref_.x + 10, center_roi_ref_.y);
//        cv::line(I_for_drawing, line1_ini_ref, line1_end_ref, cv::Scalar(255, 0, 0), 2);
//        cv::line(I_for_drawing, line2_ini_ref, line2_end_ref, cv::Scalar(255, 0, 0), 2);

//        //**** Draw Maximum Circle and ROI based on the minimum height that the UAV can reach ****
//        cv::rectangle(I_for_drawing, roi_reference_min_height_, cv::Scalar(0, 0, 255), 2);
//        cv::circle(I_for_drawing, cv::Point(center_roi_ref_.x, center_roi_ref_.y),
//                   object_in_image_info_.diameter_ref_min_height_/2.0, cv::Scalar(0, 0, 255), 2);


//        cv::Point p1(image_boundaries_offset_, image_boundaries_offset_);
//        cv::Point p2(camera_info_.image_width_ - image_boundaries_offset_, camera_info_.image_height_ - image_boundaries_offset_);
//        cv::rectangle(I_for_drawing, p1, p2, cv::Scalar(255, 0, 255), 1, 4);


//        cv::imshow("Received Image From Environment", I_for_drawing);
//        cv::waitKey(1);

////        sensor_msgs::ImagePtr image_msg;
////        image_msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", I_for_drawing).toImageMsg();
////        rl_environment_image_state_pub_.publish(image_msg);


//    }
//}

//void RlEnvironmentUavGimbalFollowingJuako::CameraImageCallback(const sensor_msgs::ImageConstPtr& msg)
//{
//  try
//  {
//        captured_image_.release();
//        cv::Mat img = cv_bridge::toCvShare(msg, "bgr8")->image;
//        captured_image_ = img.clone();
//        if(USE_OWN_OBJECT_DETECTOR)
//            ComputeDetectedObjectInImageState(img.clone(), "Red");
//        //ComputeObjectPositionBasedOnStabilizedDetection();
//  }
//  catch (cv_bridge::Exception& e)
//  {
//    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
//  }
//}

//void RlEnvironmentUavGimbalFollowingJuako::DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
//{
//    last_drone_estimated_GMRwrtGFF_pose_  = msg;
//    //yaw pitch roll

//    if(UAV_POSE_FROM_EKF)
//    {
//        uav_state_.pos_x_ = last_drone_estimated_GMRwrtGFF_pose_.x;
//        uav_state_.pos_y_ = last_drone_estimated_GMRwrtGFF_pose_.y;
//        uav_state_.pos_z_ = last_drone_estimated_GMRwrtGFF_pose_.z;

//        uav_state_.yaw_ = last_drone_estimated_GMRwrtGFF_pose_.yaw;
//        uav_state_.pitch_ = last_drone_estimated_GMRwrtGFF_pose_.pitch;
//        uav_state_.roll_ = last_drone_estimated_GMRwrtGFF_pose_.roll;
//    }

//    return;
//}

//void RlEnvironmentUavGimbalFollowingJuako::VisualServoingMeasurementCallback(const opencv_apps::RotatedRectStamped& msg)
//{
//    if(!USE_OWN_OBJECT_DETECTOR)
//    {
//        //Save the previous Detected Object ROI for computing Speeds
//        object_in_image_info_.previous_detected_roi_.angle  = object_in_image_info_.detected_roi_.angle;
//        object_in_image_info_.previous_detected_roi_.center.x = object_in_image_info_.detected_roi_.center.x;
//        object_in_image_info_.previous_detected_roi_.center.y = object_in_image_info_.detected_roi_.center.y;
//        object_in_image_info_.previous_detected_roi_.size.width = object_in_image_info_.detected_roi_.size.width;
//        object_in_image_info_.previous_detected_roi_.size.height = object_in_image_info_.detected_roi_.size.height;


//        //Update CURRENT detected ROI
//        object_in_image_info_.detected_roi_.angle = msg.rect.angle;
//        object_in_image_info_.detected_roi_.center.x = msg.rect.center.x;
//        object_in_image_info_.detected_roi_.center.y = msg.rect.center.y;
//        object_in_image_info_.detected_roi_.size.width = msg.rect.size.width;
//        object_in_image_info_.detected_roi_.size.height = msg.rect.size.height;


//        //Extract the 4 RotatedRect CORNERS and sort them and save into the object_in_image_info struct
//        object_in_image_info_.rotated_rect_points_.clear();
//        cv::Point2f corner_points[4];
//        object_in_image_info_.detected_roi_.points(corner_points);
//        std::vector<cv::Point2f> rotated_rect_points;
//        for(int j = 0; j < 4; j++)
//            rotated_rect_points.push_back(corner_points[j]);
//        //std::sort(rotated_rect_points.data(), rotated_rect_points.data() + rotated_rect_points.size(), cmpVecs);
//        object_in_image_info_.rotated_rect_points_.insert(object_in_image_info_.rotated_rect_points_.begin(),
//                                                          rotated_rect_points.begin(), rotated_rect_points.end());

//        cv::Point2f roi_diag;
//        roi_diag.x = corner_points[0].x - corner_points[2].x;
//        roi_diag.y = corner_points[0].y - corner_points[2].y;
//        object_in_image_info_.detected_roi_diameter_ = std::sqrt(std::pow(roi_diag.x, 2) + std::pow(roi_diag.y, 2));



//        if(DEBUG_MODE)
//        {
//            std::cout<<"Detected ROI [x, y, w, h]: "<<"["<<object_in_image_info_.detected_roi_.boundingRect().x<<" ; "<<
//                    object_in_image_info_.detected_roi_.boundingRect().y<<" ; "<<object_in_image_info_.detected_roi_.boundingRect().width<<" ; "<<
//                    object_in_image_info_.detected_roi_.boundingRect().height<<"]"<<std::endl;
//        }
//    }

//}

//void RlEnvironmentUavGimbalFollowingJuako::ComputeDetectedObjectInImageState(const cv::Mat &I, const std::string object_color)
//{

//    cv::RotatedRect rotated_rect_item;;
//    cv::Rect rect_item;
//    DetectObjectInImage(I.clone(), object_color, rotated_rect_item, rect_item);

//    if(rect_item.area()>0)
//    {

//        object_in_image_info_.detected_roi_rect_ = rect_item;
//        //Save the previous Detected Object ROI for computing Speeds
//        object_in_image_info_.previous_detected_roi_.angle  = object_in_image_info_.detected_roi_.angle;
//        object_in_image_info_.previous_detected_roi_.center.x = object_in_image_info_.detected_roi_.center.x;
//        object_in_image_info_.previous_detected_roi_.center.y = object_in_image_info_.detected_roi_.center.y;
//        object_in_image_info_.previous_detected_roi_.size.width = object_in_image_info_.detected_roi_.size.width;
//        object_in_image_info_.previous_detected_roi_.size.height = object_in_image_info_.detected_roi_.size.height;


//        //Update CURRENT detected ROI
//        object_in_image_info_.detected_roi_.angle = rotated_rect_item.angle;
//        object_in_image_info_.detected_roi_.center.x = rotated_rect_item.center.x;
//        object_in_image_info_.detected_roi_.center.y = rotated_rect_item.center.y;
//        object_in_image_info_.detected_roi_.size.width = rotated_rect_item.size.width;
//        object_in_image_info_.detected_roi_.size.height = rotated_rect_item.size.height;


//        //Extract the 4 RotatedRect CORNERS and sort them and save into the object_in_image_info struct
//        object_in_image_info_.rotated_rect_points_.clear();
//        cv::Point2f corner_points[4];
//        object_in_image_info_.detected_roi_.points(corner_points);
//        std::vector<cv::Point2f> rotated_rect_points;
//        for(int j = 0; j < 4; j++)
//            rotated_rect_points.push_back(corner_points[j]);
//        //std::sort(rotated_rect_points.data(), rotated_rect_points.data() + rotated_rect_points.size(), cmpVecs);
//        object_in_image_info_.rotated_rect_points_.insert(object_in_image_info_.rotated_rect_points_.begin(),
//                                                          rotated_rect_points.begin(), rotated_rect_points.end());

//        object_in_image_info_.detected_roi_rect_points_.clear();
//        object_in_image_info_.detected_roi_rect_points_.push_back(object_in_image_info_.detected_roi_rect_.tl());
//        object_in_image_info_.detected_roi_rect_points_.push_back(cv::Point(object_in_image_info_.detected_roi_rect_.tl().x + object_in_image_info_.detected_roi_rect_.width,
//                                                                            object_in_image_info_.detected_roi_rect_.tl().y));
//        object_in_image_info_.detected_roi_rect_points_.push_back(object_in_image_info_.detected_roi_rect_.br());
//        object_in_image_info_.detected_roi_rect_points_.push_back(cv::Point(object_in_image_info_.detected_roi_rect_.tl().x,
//                                                                            object_in_image_info_.detected_roi_rect_.tl().y + object_in_image_info_.detected_roi_rect_.height));

//        cv::Point2f roi_diag;
//        roi_diag.x = corner_points[0].x - corner_points[2].x;
//        roi_diag.y = corner_points[0].y - corner_points[2].y;
//        object_in_image_info_.detected_roi_diameter_ = std::sqrt(std::pow(roi_diag.x, 2) + std::pow(roi_diag.y, 2));



//        if(DEBUG_MODE)
//        {
//            std::cout<<"Detected ROI [x, y, w, h]: "<<"["<<object_in_image_info_.detected_roi_.boundingRect().x<<" ; "<<
//                    object_in_image_info_.detected_roi_.boundingRect().y<<" ; "<<object_in_image_info_.detected_roi_.boundingRect().width<<" ; "<<
//                    object_in_image_info_.detected_roi_.boundingRect().height<<"]"<<std::endl;
//        }
//    }

//}

//For comparing based on the Manhattan (Taxicab distance) to the origin
//inline bool cmpVecs(const cv::Point2f &lhs, const cv::Point2f &rhs)
//{
//    return (lhs.x + lhs.y) < (rhs.x + rhs.y);
//}

//void RlEnvironmentUavGimbalFollowingJuako::ComputeCompensatedRotatedRectByRpy()
//{
//    object_in_image_info_.rotated_rect_points_rpy_compensated_.clear();
//    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);
//    if(camera_info_.camera_info_available_flag_)
//    {
////        std::cout<<std::endl<<"K: "<<camera_info_.K_matrix_<<std::endl;
////        std::cout<<"yaw: "<<uav_state_.yaw_<<", "<<"pitch: "<<uav_state_.pitch_<<" , "<<"roll: "<<uav_state_.roll_<<std::endl;
////        std::cout<<std::endl<<"R: "<<camera_info_.R_matrix_<<std::endl;



//        float camera_yaw = -uav_state_.yaw_;
//        float camera_pitch = -uav_state_.roll_;
//        float camera_roll = -uav_state_.pitch_;
//        cv::Mat R = ConvertFromYPRtoRotationMatrix(camera_yaw, camera_pitch, camera_roll);

//        H = camera_info_.K_matrix_ * R * camera_info_.K_matrix_.inv();

//        cv::Mat H_affine = cv::Mat::ones(2, 3, CV_32FC1);
//        H_affine.at<float>(0,0) = H.at<float>(0,0); H_affine.at<float>(0,1) = H.at<float>(0,1); H_affine.at<float>(0,2) = 0.0;
//        H_affine.at<float>(1,0) = H.at<float>(1,0); H_affine.at<float>(1,1) = H.at<float>(1,1); H_affine.at<float>(1,2) = 0.0;

//        //std::cout<<"H: "<<H<<std::endl;
//        //std::cout<<"H_affine: "<<H_affine<<std::endl;




//        std::vector<cv::Point2f> p_image_stabilized_rect(4);
//        std::vector<cv::Point2f> p_image_detected_rect(4);

//        cv::Rect detected_rect = object_in_image_info_.detected_roi_rect_;
//        p_image_detected_rect[0] = cv::Point2f(detected_rect.tl().x, detected_rect.tl().y);
//        p_image_detected_rect[1] = cv::Point2f(detected_rect.tl().x + detected_rect.width, detected_rect.tl().y);
//        p_image_detected_rect[2] = cv::Point2f(detected_rect.tl().x + detected_rect.width, detected_rect.tl().y + detected_rect.height);
//        p_image_detected_rect[3] = cv::Point2f(detected_rect.tl().x, detected_rect.tl().y + detected_rect.height);



//        cv::perspectiveTransform(p_image_detected_rect, p_image_stabilized_rect, H);


//        cv::Mat I_warped;
//        cv::warpPerspective(captured_image_.clone(), I_warped, H, captured_image_.size());

//        cv::RotatedRect rotated_rect_item;;
//        cv::Rect rect_item;
//        DetectObjectInImage(I_warped.clone(), "Red", rotated_rect_item, rect_item);
//        object_in_image_info_.stabilized_detected_roi_rect_ = rect_item;

////        object_in_image_info_.stabilized_detected_roi_rect_ = cv::Rect(cv::Point(p_image_stabilized_rect[0].x, p_image_stabilized_rect[0].y),
////                                          cv::Point(p_image_stabilized_rect[2].x, p_image_stabilized_rect[2].y));


//        for(int j = 0; j < 4; j++)
//        {
//            cv::line(I_warped, p_image_stabilized_rect[j], p_image_stabilized_rect[(j+1)%4], cv::Scalar(0, 0, 255), 2, 8);
//        }
//        cv::rectangle(I_warped, object_in_image_info_.stabilized_detected_roi_rect_, cv::Scalar(0, 255, 255), 2);
//        cv::imshow("I warped", I_warped);
//        cv::waitKey(1);



//    }
//}

//void RlEnvironmentUavGimbalFollowingJuako::ComputeObjectPositionBasedOnStabilizedDetection()
//{
//    object_in_image_info_.rotated_rect_points_rpy_compensated_.clear();
//    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);
//    if(camera_info_.camera_info_available_flag_)
//    {
//        float camera_yaw = -uav_state_.yaw_;
//        float camera_pitch = -uav_state_.roll_;
//        float camera_roll = -uav_state_.pitch_;
//        cv::Mat R = ConvertFromYPRtoRotationMatrix(camera_yaw, camera_pitch, camera_roll);

//        H = camera_info_.K_matrix_ * R * camera_info_.K_matrix_.inv();


//        cv::Mat I_warped;
//        cv::warpPerspective(captured_image_.clone(), I_warped, H, captured_image_.size());

//        cv::RotatedRect rotated_rect_item;;
//        cv::Rect rect_item;
//        DetectObjectInImage(I_warped.clone(), "Red", rotated_rect_item, rect_item);


//        object_in_image_info_.stabilized_detected_roi_rect_ = rect_item;
//        object_in_image_info_.stabilized_detected_roi_diameter_ = std::sqrt(std::pow(rect_item.width, 2) + std::pow(rect_item.height, 2));

//        object_in_image_info_.stabilized_previous_detected_roi_ = object_in_image_info_.stabilized_detected_roi_;
//        object_in_image_info_.stabilized_detected_roi_ = rotated_rect_item;


//        cv::rectangle(I_warped, object_in_image_info_.stabilized_detected_roi_rect_, cv::Scalar(0, 255, 255), 2);
//        cv::imshow("I warped", I_warped);
//        cv::waitKey(1);

//    }
//}

//void RlEnvironmentUavGimbalFollowingJuako::DetectObjectInImage(const cv::Mat &I, const std::string object_color,
//                                                                            cv::RotatedRect &rotated_rect_item, cv::Rect &rect_item)
//{
//    cv::Mat3b I_hsv;
//    cv::cvtColor(I, I_hsv, CV_BGR2HSV);
//    cv::Mat1b mask1_hsv, mask2_hsv;


//    cv::Mat I_binarized;
//    if(object_color == "Red")
//    {
//        //Real
//        cv::inRange(I_hsv, cv::Scalar(0, 100, 100), cv::Scalar(10, 255, 255), mask1_hsv);
//        cv::inRange(I_hsv, cv::Scalar(170, 100, 100), cv::Scalar(180, 255, 255), mask2_hsv);

////        cv::inRange(I_hsv, cv::Scalar(0, 70, 20), cv::Scalar(10, 255, 255), mask1_hsv);
////        cv::inRange(I_hsv, cv::Scalar(170, 70, 20), cv::Scalar(180, 255, 255), mask2_hsv);
//        I_binarized = mask1_hsv | mask2_hsv;
//    }
//    else if(object_color == "Blue")
//    {
//        //cv::inRange(I_hsv, cv::Scalar(90, 100, 50), cv::Scalar(140, 255, 255), I_mask_hsv); //Real
//        cv::inRange(I_hsv, cv::Scalar(110, 70, 50), cv::Scalar(130, 255, 255), I_binarized); //Simulation
//    }


//    std::vector<std::vector<cv::Point> > contours;
//    cv::findContours(I_binarized.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);


//    std::vector<std::vector<cv::Point> >::iterator itc = contours.begin();
//    while(itc != contours.end())
//    {
//        cv::RotatedRect rot_box = cv::minAreaRect(cv::Mat(*itc));
//        float rot_box_area = (float)rot_box.size.width * (float)rot_box.size.height;

//        if(rot_box_area < 500)
//            itc = contours.erase(itc);
//        else
//        {
//            ++itc;
//        }
//    }

//    if(contours.size())
//    {
//        rotated_rect_item = cv::minAreaRect(cv::Mat(contours[0]));
//        rect_item = cv::boundingRect(cv::Mat(contours[0]));

//    }

//}
