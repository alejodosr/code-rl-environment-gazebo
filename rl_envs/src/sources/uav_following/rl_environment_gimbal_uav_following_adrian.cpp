#include "rl_environment_gimbal_uav_following_adrian.h"

/// TO BE FILLED: INIT VARIABLES AND STATES
RlEnvironmentUavGimbalFollowingAdrian::RlEnvironmentUavGimbalFollowingAdrian()
{


    // Reward
    prev_shaping_ = 0.0;

    // Target roi
    target_roi_.x = (RESOLUTION_VIRTUAL_CAMERA_X / 2) - (TARGET_ROI_WIDTH / 2);
    target_roi_.y = (RESOLUTION_VIRTUAL_CAMERA_Y / 2) - (TARGET_ROI_HEIGHT / 2);
    target_roi_.width = TARGET_ROI_WIDTH;
    target_roi_.height = TARGET_ROI_HEIGHT;

    kUav_Altitude_ = 5.8;
    kUav_max_Altitude_ = 4.7;
    kUav_min_Altitude_ = 0.7;

#ifdef TRAINING_A
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 2;
    environment_info_.actions_max_value_ = {0.5, 0.5};
    environment_info_.actions_min_value_ = {-0.5, -0.5};
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 6;
#endif

#ifdef TRAINING_B
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 2;
#ifdef SIGMOID
    environment_info_.actions_max_value_ = {0.5, 0.5};
    environment_info_.actions_min_value_ = {-0.5, -0.5};
#else
    environment_info_.actions_max_value_ = {0.12, 0.12};
    environment_info_.actions_min_value_ = {-0.12, -0.12};
#endif
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 6;
#endif

#ifdef TRAINING_B_diff
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 3;
    environment_info_.actions_max_value_ = {0.5, 0.5, 0.25};
    environment_info_.actions_min_value_ = {-0.5,-0.5, -0.25};
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 8;
#endif

#ifdef TRAINING_C
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 3;
    environment_info_.actions_max_value_ = {0.5, 0.5, 0.25};
    environment_info_.actions_min_value_ = {-0.5,-0.5, -0.25};
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 8;
#endif

#ifdef TRAINING_C_dA
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 4;
    environment_info_.actions_max_value_ = {2.0, 1.0, 0.2,0.5};
    environment_info_.actions_min_value_ = {-0.5,-0.5, -0.25,-0.5};
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 8;
#endif


#ifdef TRAINING_D
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 4;
#ifdef FAST
    environment_info_.actions_max_value_ = {2.0, 2.0, 2.0,1.0};
    environment_info_.actions_min_value_ = {-2.0, -2.0,-2.0,-1.0};
#else
    environment_info_.actions_max_value_ = {1.0, 1.0, 1.0,0.5};
    environment_info_.actions_min_value_ = {-1.0, -1.0,-1.0,-0.5};
#endif
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 8;
#endif

#ifdef TRAINING_D_GIMBAL
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 6;
#ifdef FAST
    environment_info_.actions_max_value_ = {2.0, 2.0, 2.0, 1.0, 0.5, 0.5};
    environment_info_.actions_min_value_ = {-2.0, -2.0,-2.0,-1.0, -0.5, -0.5};
#else
    environment_info_.actions_max_value_ = { 1.0, 1.0, 1.0,1.0, 0.5, 0.5};
    environment_info_.actions_min_value_ = {-1.0, -1.0,-1.0,-0.5, -0.5, -0.5};
#endif
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 10;
#endif

#ifdef TRAINING_E
    environment_info_.num_episode_steps_ = 1000;
    environment_info_.num_iterations_ = 5;
    environment_info_.actions_dim_ = 5;
    environment_info_.actions_max_value_ = { 0.4, 0.4, 0.5, 0.5, 0.5};
    environment_info_.actions_min_value_ = { -0.4, -0.4, -0.5, -0.5, -0.5};
    environment_info_.max_pos_x_ = 1.0;
    environment_info_.max_pos_y_ = 1.0;
    environment_info_.state_dim_low_dim_ = 10;
#endif

#ifdef LOG_DATA
    environment_info_.num_episode_steps_ = 1200;
#endif


    camera_info_.camera_info_available_flag_ = false;
    camera_info_.image_width_ = RESOLUTION_VIRTUAL_CAMERA_X;
    camera_info_.image_height_ = RESOLUTION_VIRTUAL_CAMERA_Y;
    camera_info_.uav_camera_offset_ = cv::Point3f(0.2, 0.0, -0.05); //Camera offset (x, y, z) (see Hummingbird_xacro.base)

    SetUavState(0.0, 0.0, kUav_Altitude_, 0.0, 0.0);

    object_in_world_info_.width_ = 0.4; //0.3 for the cylinder on top of the Firefly
    object_in_world_info_.height_ = 0.05;

    //**** Object points w.r.t. OBJECT frame of reference ****//
    // Upper side
    object_in_world_info_.current_points_mat_[0] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[0].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[0].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[0].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[0].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[1] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[1].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[1].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[1].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[1].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[2] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[2].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[2].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[2].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[2].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[3] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[3].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[3].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[3].at<float>(2,0) = object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[3].at<float>(3,0) = 1.0;

    // Lower side
    object_in_world_info_.current_points_mat_[4] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[4].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[4].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[4].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[4].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[5] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[5].at<float>(0,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[5].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[5].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[5].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[6] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[6].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[6].at<float>(1,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[6].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[6].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[7] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[7].at<float>(0,0) = -object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[7].at<float>(1,0) = object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[7].at<float>(2,0) = -object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[7].at<float>(3,0) = 1.0;

    //MAXIMUM ROLL and PITCH angles established from midlevel_autopilot.xml (configs)
    pitch_roll_max_value_ = 35.0 * M_PI/180.0;

    gimbal_state_.roll_ = 0.0f;
    gimbal_state_.pitch_ = 0.0f;
    gimbal_state_.yaw_ = 0.0f;


    angular_position_actual_gimbal_ = {0.0f,0.0f};
    angular_position_desired_gimbal_ = {0.0f, 0.0f};

//#if defined(REAL_FLIGHT_OWN_DETECTOR) || defined(REAL_FLIGHT)
//    geometry_msgs::Twist msg;
//    msg.angular.y = (angular_position_desired_gimbal_[1] * 180.0 / M_PI - OFFSET_GIMBAL_REAL_WRT_SIM); // In degrees and with transformed axes
//    msg.angular.z = angular_position_desired_gimbal_[0] * 180.0 / M_PI;
//    camera_control_pub_.publish(msg);
//#endif

    angular_position_desired_gimbal_MAX_ = {HFOV_/2, VFOV_/2};
    angular_position_desired_gimbal_MIN_ = {-HFOV_/2, -VFOV_/2};

    previous_time_gimbal_ = ros::Time::now();

#ifdef LOG_DATA
    outFile_.open ("/home/alejandro/temporal/ground_truth.csv", std::ios::out | std::ios::ate | std::ios::app);
    outFile_ << "uav_state_.pos_x_" << "," << "uav_state_.pos_y_" << "," << "uav_state_.pos_z_";
    outFile_ << "," <<"target_pose.x" << "," << "target_pose.y" << "," << "target_pose.z";
    outFile_ << "," << "error_x" << "," << "error_y" << "," << "current_roi.area()" << "," << "target_roi_.area()"<< std::endl;
    outFile_.close();
#endif
}

RlEnvironmentUavGimbalFollowingAdrian::~RlEnvironmentUavGimbalFollowingAdrian()
{
    f_data_recorder.close();
}

void RlEnvironmentUavGimbalFollowingAdrian::InitChild(ros::NodeHandle n)
{
    std::cout << "RL_ENV_INFO: IMAGE BASED VISUAL SERVOING WITH ALTITUDE Environment" << std::endl;

    // Init subscribers

    // Read drone namespace
    int drone_namespace = -1;
    ros::param::get("~droneId", drone_namespace);
    if(drone_namespace == -1)
    {
        ROS_ERROR("FATAL: Namespace not found");
        return;
    }

    // Read topics
    std::string param_string;
    ros::param::get("~camera_topic", param_string);
    std::cout << "~camera_topic" << param_string << std::endl;

    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    //camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::CameraImageCallback, this);
//    camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::CameraFishEyeCallback, this);
#if defined(REAL_FLIGHT_OWN_DETECTOR) || defined(REAL_FLIGHT)
    camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::CameraBebopREALCallbackNoGimbal, this);
#else
    camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::CameraFishEyeCallback, this);
#endif


    ros::param::get("~dYaw", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    dYaw_ref_pub_ = n.advertise<droneMsgsROS::droneDYawCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);


    // Init publishers
    ros::param::get("~roll_pitch", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    roll_pitch_ref_pub_ = n.advertise<droneMsgsROS::dronePitchRollCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~gimbal_control", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    camera_control_pub_ = n.advertise<geometry_msgs::Twist>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~camera_info_topic", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    camera_info_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::CameraInfoCallback, this);

#ifdef REAL_FLIGHT

    ros::param::get("~gimbal_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gimbal_state_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::GimbalStateREALCallback, this);

//    ros::param::get("~real_camera_topic", param_string);
//    if(param_string.length() == 0)
//    {
//        ROS_ERROR("FATAL: Topic not found");
//        return;
//    }
//    real_camera_image_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::CameraBebopREALCallback, this);

    ros::param::get("~std_bbs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    std_bbs_subs_ = n.subscribe(param_string, 1, &RlEnvironmentUavGimbalFollowingAdrian::StdBbsREALCallback, this);

    ros::param::get("~imu", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    real_imu_subs_ = n.subscribe("/drone4/odom", 10, &RlEnvironmentUavGimbalFollowingAdrian::RealImuCallback, this);
#endif

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_altitude_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~model_states", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_velocity_subs_ = n.subscribe(param_string, 10, &RlEnvironmentUavGimbalFollowingAdrian::PoseVelocityCallback, this);

    // Init publishers
    ros::param::get("~speed_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_speed_ref_pub_ = n.advertise<droneMsgsROS::droneSpeeds>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    ros::param::get("~position_refs", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    uav_pose_ref_pub_ = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);


    ros::param::get("~dAltitude", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    daltitude_ref_pub_ = n.advertise<droneMsgsROS::droneDAltitudeCmd>("/drone" + std::to_string(drone_namespace) + "/" + param_string, 1000);

    // Init service
    ros::param::get("~set_model_state", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    gazebo_client_ = n.serviceClient<gazebo_msgs::SetModelState>(param_string);

    ros::param::get("~reset_estimator", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    estimator_client_ = n.serviceClient<std_srvs::Empty>("/drone" + std::to_string(drone_namespace) + "/" + param_string);

    // Set number of iterations of Gazebo
    if (ENABLE_PAUSED_SIMULATION)
    {
        this->data_->num_iterations = (unsigned int) environment_info_.num_iterations_;

        // Print
        std::cout << "RL_ENV_INFO: for each step, Gazebo iterations are " << this->data_->num_iterations << " iterations" << std::endl;
    }


    ros::param::get("~configs_path", param_string);
    if(param_string.length() == 0)
    {
        ROS_ERROR("FATAL: Topic not found");
        return;
    }
    std::string configs_file_name = param_string;
    //std::cout<<"++++++++++ CONFIGS PATH ++++++++++"<<std::endl<<configs_file_name<<std::endl;
    //ReadConfigs(configs_file_name);

    this->rl_env_step_action_srv = n.advertiseService("rl_env_step_action_srv", &RlEnvironmentUavGimbalFollowingAdrian::StepAction, this);

    this->rl_env_step_state_srv = n.advertiseService("rl_env_step_state_srv", &RlEnvironmentUavGimbalFollowingAdrian::StepState, this);

    // Init member variables
    angular_position_actual_gimbal_ = {0.0f,0.0f};


#ifdef REAL_FLIGHT
    object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_ = cv::Rect(cv::Point(target_roi_.x, target_roi_.y),
                                                                                    cv::Point(target_roi_.width, target_roi_.height));
#else
    // Reset environment
    Reset();
    //f_data_recorder.open("/media/carlos/DATA/TesisCVG/PAPERS/MIS_PAPERS/ObjectFollowing_IROS_2018/Experiments/Real_Flights/Moving_Stick/RL_IBVS_Leader_Follower_positions.txt");
#endif

#ifdef EXTERNAL_YAW_CONTROLLER
    // Init yaw controller PID
    pid_yaw_.initPid(1.6,0.01,0.08,0.4,-0.4);

    // Init time
    last_time_ = ros::Time::now();
#endif

#ifdef ACTION_SMOOTHING
    pitch_ant_ = 0;
    roll_ant_ = 0;
#endif
}

bool RlEnvironmentUavGimbalFollowingAdrian::ReadConfigs(std::string &configFile)
{
    pugi::xml_document doc;

    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        std::cout << "ERROR: Could not load the file: " << result.description() << std::endl;
        return 0;
    }

    pugi::xml_node Configuration = doc.child("Image_Based_Visual_Servoing_Config");
    std::string readingValue;

    readingValue = Configuration.child_value("Uav_Altitude");
    kUav_Altitude_ = atof(readingValue.c_str());
    std::cout<<"kUav_Altitude_: "<<kUav_Altitude_<<std::endl;

    readingValue = Configuration.child_value("Uav_max_Altitude");
    kUav_max_Altitude_ = atof(readingValue.c_str());
    std::cout<<"kUav_max_Altitude_: "<<kUav_max_Altitude_<<std::endl;

    readingValue = Configuration.child_value("Uav_min_Altitude");
    kUav_min_Altitude_ = atof(readingValue.c_str());
    std::cout<<"kUav_min_Altitude_: "<<kUav_min_Altitude_<<std::endl;

    //******** Environment INFO ********
    readingValue = Configuration.child("environment_info").child_value("num_episode_steps");
    environment_info_.num_episode_steps_ = atoi(readingValue.c_str());
    std::cout<<"environment_info_.num_episode_steps_: "<<environment_info_.num_episode_steps_<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("actions_min_value");
    float actions_min_value = atof(readingValue.c_str());
    std::cout<<"actions_min_value: "<<actions_min_value<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("actions_max_value");
    float actions_max_value = atof(readingValue.c_str());
    std::cout<<"actions_max_value: "<<actions_max_value<<std::endl;
    environment_info_.actions_min_value_ = {actions_min_value, actions_min_value, actions_min_value};
    environment_info_.actions_max_value_ = {actions_max_value, actions_max_value, actions_max_value};

    readingValue = Configuration.child("environment_info").child_value("max_pos_x");
    environment_info_.max_pos_x_ = atof(readingValue.c_str());
    std::cout<<"environment_info_.max_pos_x_: "<<environment_info_.max_pos_x_<<std::endl;

    readingValue = Configuration.child("environment_info").child_value("max_pos_y");
    environment_info_.max_pos_y_ = atof(readingValue.c_str());
    std::cout<<"environment_info_.max_pos_y_: "<<environment_info_.max_pos_y_<<std::endl;

    //******** Camera INFO ********
    readingValue = Configuration.child("camera_info").child_value("image_width");
    camera_info_.image_width_ = atoi(readingValue.c_str());
    std::cout<<"camera_info_.image_width_: "<<camera_info_.image_width_<<std::endl;

    readingValue = Configuration.child("camera_info").child_value("image_height");
    camera_info_.image_height_ = atof(readingValue.c_str());
    std::cout<<"camera_info_.image_height_: "<<camera_info_.image_height_<<std::endl;

    readingValue = Configuration.child("camera_info").child("camera_offset").child_value("x");
    camera_info_.uav_camera_offset_.x = atof(readingValue.c_str());
    readingValue = Configuration.child("camera_info").child("camera_offset").child_value("y");
    camera_info_.uav_camera_offset_.y = atof(readingValue.c_str());
    readingValue = Configuration.child("camera_info").child("camera_offset").child_value("z");
    camera_info_.uav_camera_offset_.z = atof(readingValue.c_str());
    std::cout<<"camera_info_.uav_camera_offset_: "<<camera_info_.uav_camera_offset_<<std::endl;

    //******** Object in WORLD INFO ********
    readingValue = Configuration.child("object_in_world_info").child_value("width");
    object_in_world_info_.width_ = atof(readingValue.c_str());
    std::cout<<"object_in_world_info_.width_: "<<object_in_world_info_.width_<<std::endl;

    readingValue = Configuration.child("object_in_world_info").child_value("height");
    object_in_world_info_.height_ = atof(readingValue.c_str());
    std::cout<<"object_in_world_info_.height_: "<<object_in_world_info_.height_<<std::endl;
}

/// TO BE FILLED: GENERATE THE NORMALIZED STATE
void RlEnvironmentUavGimbalFollowingAdrian::GetNormalizedState(std::vector<float> &normalized_state){
    // Safe previous roi

#ifdef RED_DETECTOR
    if(prev_shaping_ == 0.0){
        object_in_image_info_fish_eye_.gimbal_red_detector_previous_roi_rect_ = object_in_image_info_fish_eye_.gimbal_red_detector_roi_rect_;
    }
#else
    if(prev_shaping_ == 0.0){
        object_in_image_info_fish_eye_.gimbal_ground_truth_previous_detected_roi_rect_ = object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_;
    }
#endif


    // Get ROI
    cv::Rect previous_roi, current_roi;
    GetTargetROI(previous_roi, current_roi);


    // Compute target roi normalized
    target_roi_normalized_.x = target_roi_.x / (RESOLUTION_VIRTUAL_CAMERA_X / 2);
    target_roi_normalized_.y = target_roi_.y / (RESOLUTION_VIRTUAL_CAMERA_Y / 2);
    target_roi_normalized_.width = target_roi_.width / (RESOLUTION_VIRTUAL_CAMERA_X / 2);
    target_roi_normalized_.height = target_roi_.height / (RESOLUTION_VIRTUAL_CAMERA_Y / 2);

    // Compute errors
    float current_center_x = current_roi.x + (current_roi.width / 2);
    float current_center_y = current_roi.y + (current_roi.height / 2);
    float target_center_x = target_roi_.x + (target_roi_.width / 2);
    float target_center_y = target_roi_.y + (target_roi_.height / 2);
    float error_x = (current_center_x - target_center_x);
    float error_y = (current_center_y - target_center_y);
    float error_area = 1.0 - ((float) current_roi.area() / (float) target_roi_.area());
    if (error_area < -1.0){    // In order to avoid lower than -1 in next step
        error_area = -1.0;
    }

    float DIFF_SCALER = 5.0;

    // Compute prev errors
    float prev_current_center_x = previous_roi.x + (previous_roi.width / 2);
    float prev_current_center_y = previous_roi.y + (previous_roi.height / 2);
    float prev_error_x = (prev_current_center_x - target_center_x);
    float prev_error_y = (prev_current_center_y - target_center_y);
    float prev_error_area = 1.0 - ((float) previous_roi.area() / (float) target_roi_.area());
    if (prev_error_area < -1.0){    // In order to avoid lower than -1 in next step
        prev_error_area = -1.0;
    }

    // Fill vector
    normalized_state.push_back(error_x / (RESOLUTION_VIRTUAL_CAMERA_X / 2));
    normalized_state.push_back(error_y / (RESOLUTION_VIRTUAL_CAMERA_Y / 2));
    normalized_state.push_back(error_area);
    normalized_state.push_back(DIFF_SCALER * (error_x - prev_error_x) / (RESOLUTION_VIRTUAL_CAMERA_X / 2));
    normalized_state.push_back(DIFF_SCALER * (error_y - prev_error_y) / (RESOLUTION_VIRTUAL_CAMERA_Y / 2));
    normalized_state.push_back((error_area - prev_error_area));
#if defined(TRAINING_B_diff)|| defined(TRAINING_C) || defined(TRAINING_C_dA) || defined(TRAINING_D) || defined(TRAINING_D_GIMBAL)  || defined(TRAINING_E)
    normalized_state.push_back(uav_state_.imu_pitch_/MAX_PITCH);
    normalized_state.push_back(uav_state_.imu_roll_/MAX_ROLL);
#endif

#ifdef LOG_DATA
    outFile_.open ("/home/alejandro/temporal/ground_truth.csv", std::ios::out | std::ios::ate | std::ios::app);
    outFile_ << uav_state_.pos_x_ << "," << uav_state_.pos_y_ << "," << uav_state_.pos_z_;
    outFile_ << "," <<target_pose.x << "," << target_pose.y << "," << target_pose.z;
    outFile_ << "," << error_x << "," << error_y << "," << current_roi.area() << "," << target_roi_.area()<< std::endl;
    outFile_.close();
#endif



#if defined (TRAINING_D_GIMBAL)
    normalized_state.push_back(angular_position_actual_gimbal_[0] / (angular_position_desired_gimbal_MAX_[0]));
    normalized_state.push_back(angular_position_actual_gimbal_[1] / (angular_position_desired_gimbal_MAX_[1]));
#endif

#if defined (TRAINING_E)
    normalized_state.push_back(angular_position_actual_gimbal_[0] / (angular_position_desired_gimbal_MAX_[0]));
    normalized_state.push_back(angular_position_actual_gimbal_[1] / (angular_position_desired_gimbal_MAX_[1]));
#endif
    for (auto i: normalized_state)
        std::cout << i << ' ';
    std::cout << std::endl;

    // Update previous ROI in image
#ifdef RED_DETECTOR
    object_in_image_info_fish_eye_.gimbal_red_detector_previous_roi_rect_ = object_in_image_info_fish_eye_.gimbal_red_detector_roi_rect_;
#else
    object_in_image_info_fish_eye_.gimbal_ground_truth_previous_detected_roi_rect_ = object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_;
#endif
}


float RlEnvironmentUavGimbalFollowingAdrian::sigmoid(const float x)
{
     float exp_value;
     float return_value;

     /*** Exponential calculation ***/
     exp_value = exp((double) -x);

     /*** Final sigmoid value ***/
     return_value = 1 / (1 + exp_value);

     return return_value;
}
/// THIS FUNCTION IS BEING DEPRECATED
bool RlEnvironmentUavGimbalFollowingAdrian::Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response){

    return true;
}

/// TO BE FILLED: ACTION TO SEND TO THE ENVIRONMENT
// Sends the action to Gazebo environment
bool RlEnvironmentUavGimbalFollowingAdrian::StepAction(rl_srvs::AgentActionSrv::Request &request, rl_srvs::AgentActionSrv::Response &response){


#ifdef ENABLE_ACTION_SCALING
   // Debug: Normlizing actions before it is implemented in the agent
    for (int i = 0; i < environment_info_.actions_max_value_.size(); i++){
        request.action[i] *= environment_info_.actions_max_value_[i];
    }
#endif

#ifdef ENABLE_ACTION_TRUNCATION
    // Debug: Truncating actions before it is implemented in the agent
     for (int i = 0; i < environment_info_.actions_max_value_.size(); i++){
         if (request.action[i] > environment_info_.actions_max_value_[i]){
             request.action[i] = environment_info_.actions_max_value_[i];
         }
         if (request.action[i] < environment_info_.actions_min_value_[i]){
             request.action[i] = environment_info_.actions_min_value_[i];
         }
     }
#endif

#ifdef TRAINING_A
    // Send velocity commands
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = request.action[0];
    action_msg.dy = request.action[1];
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);
#endif

#if defined(TRAINING_B)
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
#ifdef SIGMOID
    roll_pitch_msg.pitchCmd = sigmoid(request.action[0]) - 0.5;  // Pitch
    roll_pitch_msg.rollCmd = sigmoid(request.action[1]) - 0.5;  // Roll
#else
    roll_pitch_msg.pitchCmd = request.action[0];
    roll_pitch_msg.rollCmd = request.action[1];
#endif
    roll_pitch_ref_pub_.publish(roll_pitch_msg);


    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);
#endif

#ifdef TRAINING_B_diff
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    float pitch_cmd = uav_state_.pitch_ + sigmoid(request.action[0]) - 0.5;
    if (pitch_cmd > MAX_PITCH){
        pitch_cmd = MAX_PITCH;
    }
    else if(pitch_cmd < -MAX_PITCH){
        pitch_cmd = -MAX_PITCH;
    }
    float roll_cmd = uav_state_.roll_ + sigmoid(request.action[1]) - 0.5;
    if (roll_cmd > MAX_ROLL){
        roll_cmd = MAX_ROLL;
    }
    else if(roll_cmd < -MAX_ROLL){
        roll_cmd = -MAX_ROLL;
    }
    std::cout << "Pitch" <<pitch_cmd <<" "<< uav_state_.pitch_<<" "<< sigmoid(request.action[0]) - 0.5 << " Roll" << roll_cmd  <<" "<<uav_state_.pitch_<<" "<< sigmoid(request.action[1]) - 0.5 << std::endl;

    roll_pitch_msg.pitchCmd = pitch_cmd;  // Pitch
    roll_pitch_msg.rollCmd = roll_cmd;  // Roll
    roll_pitch_ref_pub_.publish(roll_pitch_msg);


    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);
#endif

#ifdef EXTERNAL_YAW_CONTROLLER
    // Yaw controller commands
    double commandYaw = pid_yaw_.computeCommand((uav_state_.yaw_ - TARGET_YAW) / M_PI, ros::Time::now() - last_time_);
    last_time_ = ros::Time::now();

    droneMsgsROS::droneDYawCmd dyaw_msg;
    dyaw_msg.dYawCmd = commandYaw;
    dYaw_ref_pub_.publish(dyaw_msg);

    // Send yaw command

#endif

#if defined(TRAINING_C) || defined(TRAINING_C_dA)
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    float pitch_cmd = uav_state_.imu_pitch_ + sigmoid(request.action[0]) - 0.5;
    if (pitch_cmd > MAX_PITCH){
        pitch_cmd = MAX_PITCH;
    }
    else if(pitch_cmd < -MAX_PITCH){
        pitch_cmd = -MAX_PITCH;
    }
    float roll_cmd = uav_state_.imu_roll_ + sigmoid(request.action[1]) - 0.5;
    if (roll_cmd > MAX_ROLL){
        roll_cmd = MAX_ROLL;
    }
    else if(roll_cmd < -MAX_ROLL){
        roll_cmd = -MAX_ROLL;
    }
    std::cout << "Pitch" <<pitch_cmd <<" "<< uav_state_.imu_pitch_<<" "<< sigmoid(request.action[0]) - 0.5 << " Roll" << roll_cmd  <<" "<<uav_state_.imu_pitch_<<" "<< sigmoid(request.action[1]) - 0.5 << std::endl;

    roll_pitch_msg.pitchCmd = pitch_cmd;  // Pitch
    roll_pitch_msg.rollCmd = roll_cmd;  // Roll
    roll_pitch_ref_pub_.publish(roll_pitch_msg);

    droneMsgsROS::droneDYawCmd dyaw_msg;
    dyaw_msg.dYawCmd = sigmoid(request.action[2]) - 0.5;
    dYaw_ref_pub_.publish(dyaw_msg);

    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
#ifdef TRAINING_C_dA
    daltitude_msg.dAltitudeCmd = sigmoid(request.action[3]) - 0.5;  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);
#else
    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);
#endif


    actions_.clear();
    actions_.push_back(request.action[0]);
    actions_.push_back(request.action[1]);
    actions_.push_back(request.action[3]);

#endif


#if defined(TRAINING_D) || defined(TRAINING_D_GIMBAL)
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    roll_pitch_msg.pitchCmd =(sigmoid(request.action[0]) - 0.5);  // Pitch
    roll_pitch_msg.rollCmd = (sigmoid(request.action[1]) - 0.5);
#ifdef NO_SIGMOID
    roll_pitch_msg.pitchCmd = 0.5*(request.action[0]* (sigmoid(environment_info_.actions_max_value_[0]) - 0.5)/environment_info_.actions_max_value_[0]) ;  // Pitch
    roll_pitch_msg.rollCmd = 0.5*(request.action[1]* (sigmoid(environment_info_.actions_max_value_[1]) - 0.5)/environment_info_.actions_max_value_[1]);
#endif// Roll
    roll_pitch_ref_pub_.publish(roll_pitch_msg);


    droneMsgsROS::droneDYawCmd dyaw_msg;
    dyaw_msg.dYawCmd = sigmoid(request.action[2]) - 0.5;
#ifdef NO_SIGMOID
    dyaw_msg.dYawCmd = request.action[2]* (sigmoid(environment_info_.actions_max_value_[2]) - 0.5)/environment_info_.actions_max_value_[2];  // Pitch
#endif// Roll
    dYaw_ref_pub_.publish(dyaw_msg);

    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0.5*request.action[3];  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);

#ifdef TRAINING_D_GIMBAL

#ifdef REAL_FLIGHT_OWN_DETECTOR
    // Move gimbal
    std::vector<float> move_gimbal = {request.action[4], request.action[5]};
    MoveGimbalAgentREAL(move_gimbal);
#else
    std::vector<float> move_gimbal = {request.action[4], request.action[5]};
    MoveGimbalAgent(move_gimbal);

#endif

#endif

    actions_.clear();
    actions_.push_back(request.action[0]);
    actions_.push_back(request.action[1]);
    actions_.push_back(request.action[2]);


#endif

#if defined(TRAINING_E)
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    roll_pitch_msg.pitchCmd = 0.5*request.action[0];  // Pitch
    roll_pitch_msg.rollCmd = 0.5*request.action[1];
    roll_pitch_ref_pub_.publish(roll_pitch_msg);


    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0.5*request.action[2];  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);

#ifdef TRAINING_E

#if defined(REAL_FLIGHT_OWN_DETECTOR) || defined(REAL_FLIGHT)
    // Move gimbal
    std::vector<float> move_gimbal = {request.action[3], request.action[4]};
    MoveGimbalAgentREAL(move_gimbal);
#else
    std::vector<float> move_gimbal = {request.action[3], request.action[4]};
    MoveGimbalAgent(move_gimbal);

#endif

#endif

    actions_.clear();
    actions_.push_back(request.action[0]);
    actions_.push_back(request.action[1]);
    actions_.push_back(request.action[2]);


#endif



    std::cout << "Actions (t):" << request.action[0]<<" " << request.action[1]<<" " << request.action[2]<<std::endl;

    // *************************************

    return true;
}

/// TO BE FILLED: READ THE STATE, COMPUTE THE REWARD AND SEND IT BACK TO THE AGENT
// Reads from Gazebo environment and send the state back to the agent
bool RlEnvironmentUavGimbalFollowingAdrian::StepState(rl_srvs::AgentStateSrv::Request &request, rl_srvs::AgentStateSrv::Response &response){
    // ************** STATE ***************
    // Create state
    std::vector<float> response_state;
    GetNormalizedState(response_state);

    // Init
    response.terminal_state = false;

    // Check if UAV is between image boundaries
    // Get ROI
    cv::Rect previous_roi, current_roi;
    GetTargetROI(previous_roi, current_roi);
#ifdef IMAGE_STATE
    cv::Mat StateImage = cv::Mat::zeros(RESOLUTION_VIRTUAL_CAMERA_Y/IMAGE_SCALE, RESOLUTION_VIRTUAL_CAMERA_X/IMAGE_SCALE, CV_8UC1);
    StateImage(object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_scaled_)=255;

    std::vector<sensor_msgs::Image> image_vector_msg;

    cv_bridge::CvImage cv_bridge_obj;
    cv_bridge_obj.header.stamp = ros::Time::now();
    cv_bridge_obj.encoding = sensor_msgs::image_encodings::MONO8;
    cv_bridge_obj.image = StateImage;

    sensor_msgs::Image image_msg;
    cv_bridge_obj.toImageMsg(image_msg);
    image_vector_msg.push_back(image_msg);

    response.img = image_vector_msg;
#endif
    response.obs_real = response_state;

#if defined(TRAINING_A) || defined(TRAINING_B) || defined(TRAINING_B_diff)
    // Reward calculation
    float shaping_center = - 100 * sqrtf(powf(response_state[0], 2) + powf(response_state[1], 2));
    float shaping_area = - 50 * sqrtf(powf(response_state[2], 2));
    float shaping_image_velocity = - 30 * sqrtf(powf(response_state[3], 2) + powf(response_state[4], 2));
//        float gimbal_position = - 65 * sqrtf(powf(response_state[6], 2) + powf(response_state[7], 2));
    float shaping = shaping_center + shaping_area + shaping_image_velocity;// + gimbal_position;
#ifdef PITCH_ROLL_SHAPING
    float shaping_pitch_roll = - 5 * sqrtf(powf(response_state[6], 2) + powf(response_state[7], 2));
     shaping = shaping +  shaping_pitch_roll;
#endif
    response.reward = shaping - prev_shaping_;
#endif

#if defined(TRAINING_C) || defined(TRAINING_C_dA)
    // Reward calculation
    float shaping_center = - 100 * sqrtf(powf(response_state[0], 2) + powf(response_state[1], 2));
    float shaping_area = - 50 * sqrtf(powf(response_state[2], 2));
    float shaping_image_velocity = - 30 * sqrtf(powf(response_state[3], 2) + powf(response_state[4], 2));
    float shaping_pitch_roll = - 5 * sqrtf(powf(response_state[6], 2) + powf(response_state[7], 2));
    float shaping_yaw = - 5 * sqrtf(powf(request.action[2], 2));
//    float shaping_dA = - 5 * sqrtf(powf(request.action[3], 2));
    float shaping = shaping_center + shaping_area + shaping_image_velocity +  shaping_pitch_roll+ shaping_yaw;// + gimbal_position;

    shaping = shaping;
    response.reward = shaping - prev_shaping_;
#endif


#if defined(TRAINING_D)
    // Reward calculation
    float shaping_center = - 100 * sqrtf(powf(response_state[0], 2) + powf(response_state[1], 2));
    float shaping_area = - 50 * sqrtf(powf(response_state[2], 2));
    float shaping_image_velocity = - 30 * sqrtf(powf(response_state[3], 2) + powf(response_state[4], 2));
    float shaping_pitch_roll = - 5 * sqrtf(powf(response_state[6], 2)+ powf(response_state[7], 2));
    float shaping = shaping_center + shaping_area + shaping_image_velocity +  shaping_pitch_roll;// + gimbal_position;

    shaping = shaping;
    response.reward = shaping - prev_shaping_;
#endif

#if defined(TRAINING_D_GIMBAL)
    // Reward calculation
    float shaping_center = - 100 * sqrtf(powf(response_state[0], 2) + powf(response_state[1], 2));
    float shaping_area = - 50 * sqrtf(powf(response_state[2], 2));
    float shaping_image_velocity = - 30 * sqrtf(powf(response_state[3], 2) + powf(response_state[4], 2));
    float shaping_pitch_roll = - 5 * sqrtf(powf(response_state[6], 2)+ powf(response_state[7], 2));
    float shaping_yaw = - 5 * sqrtf(powf(request.action[2], 2));
    float gimbal_position = - 65 * sqrtf(powf(response_state[8], 2) + powf(response_state[9], 2));
    float shaping = shaping_center + shaping_area + shaping_image_velocity +  shaping_pitch_roll + gimbal_position;

    shaping = shaping;
    response.reward = shaping - prev_shaping_;
#endif

#if defined(TRAINING_E)
    // Reward calculation
    float shaping_center = - 100 * sqrtf(powf(response_state[0], 2) + powf(response_state[1], 2));
    float shaping_area = - 50 * sqrtf(powf(response_state[2], 2));
    float shaping_image_velocity = - 30 * sqrtf(powf(response_state[3], 2) + powf(response_state[4], 2));
    float shaping_pitch_roll = - 10 * sqrtf(powf(response_state[6], 2)+ powf(response_state[7], 2));
    float gimbal_position = - 65 * sqrtf(powf(response_state[8], 2) + powf(response_state[9], 2));
    float shaping = shaping_center + shaping_area + shaping_image_velocity +  shaping_pitch_roll + gimbal_position;

    shaping = shaping;
    response.reward = shaping - prev_shaping_;
#endif




    // Initial state
    if (prev_shaping_ == 0.0){
        response.reward = 0.0;
    }

    prev_shaping_ = shaping;

//#ifndef REAL_FLIGHT_OWN_DETECTOR
//#ifdef RED_DETECTOR
//    if (bool_no_detection_){
//        // Reset the environment
//        Reset();

//        // Terminal state
//        bool_no_detection_ = false;
//        response.terminal_state = true;
//        response.reward = -100;
//    }
//#endif

//    if (current_roi.x < 0 || current_roi.y < 0
//            || (current_roi.x + current_roi.width) > RESOLUTION_VIRTUAL_CAMERA_X || (current_roi.y + current_roi.height)> RESOLUTION_VIRTUAL_CAMERA_Y || current_roi.area() < 350){
//        // Reset the environment
//        // Terminal state
//        response.terminal_state = true;
//        response.reward = -100;
//    }
//#endif


#if defined(TRAINING_A) || defined(TRAINING_B) ||  defined(TRAINING_B_diff)
    // Fill infos
    std_msgs::String str_msg;
    keys_.clear();
    values_.clear();
    str_msg.data = "shaping_center";
    keys_.push_back(str_msg);
    values_.push_back(shaping_center);
    str_msg.data = "shaping_area";
    keys_.push_back(str_msg);
    values_.push_back(shaping_area);
    str_msg.data = "shaping_image_velocity";
    keys_.push_back(str_msg);
    values_.push_back(shaping_image_velocity);
#ifdef PITCH_ROLL_SHAPING
    str_msg.data = "shaping_pitch_roll";
    keys_.push_back(str_msg);
    values_.push_back(shaping_pitch_roll);
#endif
//    str_msg.data = "gimbal_position";
//    keys_.push_back(str_msg);
//    values_.push_back(gimbal_position);



    response.extrakeys = keys_;
    response.extravalues = values_;
#endif

#if defined(TRAINING_C)||defined(TRAINING_C_dA)
    // Fill infos
    std_msgs::String str_msg;
    keys_.clear();
    values_.clear();
    str_msg.data = "shaping_center";
    keys_.push_back(str_msg);
    values_.push_back(shaping_center);
    str_msg.data = "shaping_area";
    keys_.push_back(str_msg);
    values_.push_back(shaping_area);
    str_msg.data = "shaping_image_velocity";
    keys_.push_back(str_msg);
    values_.push_back(shaping_image_velocity);
    str_msg.data = "shaping_pitch_roll";
    keys_.push_back(str_msg);
    values_.push_back(shaping_pitch_roll);
    str_msg.data = "shaping_yaw";
    keys_.push_back(str_msg);
    values_.push_back(shaping_yaw);



    response.extrakeys = keys_;
    response.extravalues = values_;
#endif

#if defined(TRAINING_D)
    // Fill infos
    std_msgs::String str_msg;
    keys_.clear();
    values_.clear();
    str_msg.data = "shaping_center";
    keys_.push_back(str_msg);
    values_.push_back(shaping_center);
    str_msg.data = "shaping_area";
    keys_.push_back(str_msg);
    values_.push_back(shaping_area);
    str_msg.data = "shaping_image_velocity";
    keys_.push_back(str_msg);
    values_.push_back(shaping_image_velocity);
    str_msg.data = "shaping_pitch_roll";
    keys_.push_back(str_msg);
    values_.push_back(shaping_pitch_roll);

    response.extrakeys = keys_;
    response.extravalues = values_;
#endif

#if defined(TRAINING_D_GIMBAL)
    // Fill infos
    std_msgs::String str_msg;
    keys_.clear();
    values_.clear();
    str_msg.data = "shaping_center";
    keys_.push_back(str_msg);
    values_.push_back(shaping_center);
    str_msg.data = "shaping_area";
    keys_.push_back(str_msg);
    values_.push_back(shaping_area);
    str_msg.data = "shaping_image_velocity";
    keys_.push_back(str_msg);
    values_.push_back(shaping_image_velocity);
    str_msg.data = "shaping_pitch_roll";
    keys_.push_back(str_msg);
    values_.push_back(shaping_pitch_roll);
    str_msg.data = "gimbal_position";
    keys_.push_back(str_msg);
    values_.push_back(gimbal_position);
    str_msg.data = "shaping_yaw";
    keys_.push_back(str_msg);
    values_.push_back(shaping_yaw);

    response.extrakeys = keys_;
    response.extravalues = values_;
#endif

#if defined(TRAINING_E)
    // Fill infos
    std_msgs::String str_msg;
    keys_.clear();
    values_.clear();
    str_msg.data = "shaping_center";
    keys_.push_back(str_msg);
    values_.push_back(shaping_center);
    str_msg.data = "shaping_area";
    keys_.push_back(str_msg);
    values_.push_back(shaping_area);
    str_msg.data = "shaping_image_velocity";
    keys_.push_back(str_msg);
    values_.push_back(shaping_image_velocity);
    str_msg.data = "shaping_pitch_roll";
    keys_.push_back(str_msg);
    values_.push_back(shaping_pitch_roll);
    str_msg.data = "gimbal_position";
    keys_.push_back(str_msg);
    values_.push_back(gimbal_position);


    response.extrakeys = keys_;
    response.extravalues = values_;
#endif
    std::cout << "Shaping (t): " << shaping << std::endl;
    std::cout << "Reward (t): " << response.reward << std::endl;
    // *************************************


    // Successful return
    return true;
}

void RlEnvironmentUavGimbalFollowingAdrian::PrintInfoInImage(std::vector<std_msgs::String> keys, std::vector<float> values){
    // Fill if empty
    if (values_max_.size() == 0){
        for (int i = 0; i < values_.size(); i++)
            values_max_.push_back((float) -1E8);
    }
    if (values_min_.size() == 0){
        for (int i = 0; i < values_.size(); i++)
            values_min_.push_back((float) 1E8);
    }

    // Print in image
    for(int i = 0; i < keys.size(); i++){
        // Print min
        if (values[i] < values_min_[i]){
            values_min_[i] = values[i];
        }
        std::ostringstream ss_min;
        ss_min.clear();
        ss_min << values_min_[i];
        cv::putText(cropped_picture_, ss_min.str(), cv::Point(20 + 200, 20 + i * 15), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0,143,143), 1);
        // Print instant
        cv::putText(cropped_picture_, keys[i].data, cv::Point(20, 20 + i * 15), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0,143,143), 1);
        std::ostringstream ss;
        ss.clear();
        ss << values[i];
        cv::putText(cropped_picture_, ss.str(), cv::Point(20 + 300, 20 + i * 15), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0,143,143), 1);
        // Print max
        if (values[i] > values_max_[i]){
            values_max_[i] = values[i];
        }
        std::ostringstream ss_max;
        ss_max.clear();
        ss_max << values_max_[i];
        cv::putText(cropped_picture_, ss_max.str(), cv::Point(20 + 400, 20 + i * 15), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0,143,143), 1);
    }
}

void RlEnvironmentUavGimbalFollowingAdrian::PrintAxesInImage(cv::Mat &src, std::vector<float> &axes){
  // Truncate
  for(int i = 0; i < axes.size(); i++){
    if (axes[i] > 1.0)
      axes[i] = 1.0;
    if (axes[i] < -1.0)
      axes[i] = -1.0;
  }

  // Display
  int ORIGIN_X = 530 - int(RESOLUTION_FISH_EYE_X / (2.0 * SCALE_OF_PREVIEW)) + 25;
  int ORIGIN_Y = 100;
  int RO_SIZE = 40;

  // Plot pitch (x axis)
  float p_coll = axes[0];
  cv::Scalar color(0, 0, int(255 * p_coll) + 255);

  cv::line(src, cv::Point(ORIGIN_X, ORIGIN_Y), cv::Point(ORIGIN_X, int(ORIGIN_Y - RO_SIZE * p_coll)), color, 5, cv::LINE_4);
  cv::putText(cropped_picture_, "P", cv::Point(ORIGIN_X - 5, ORIGIN_Y + RO_SIZE + 40), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(50,50,255), 1);

  // Plot roll (y axis)
  p_coll = axes[1];
  color = cv::Scalar(0, int(255 * p_coll) + 255, 0);
  ORIGIN_X += 40;

  cv::line(src, cv::Point(ORIGIN_X, ORIGIN_Y), cv::Point(ORIGIN_X, int(ORIGIN_Y - RO_SIZE * p_coll)), color, 5, cv::LINE_4);
  cv::putText(cropped_picture_, "R", cv::Point(ORIGIN_X - 5, ORIGIN_Y + RO_SIZE + 40), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(50,255,50), 1);

  // Plot altitude (z axis)
  p_coll = axes[2];
  color = cv::Scalar(int(255 * p_coll) + 255, 0, 0);
  ORIGIN_X += 40;

  cv::line(src, cv::Point(ORIGIN_X, ORIGIN_Y), cv::Point(ORIGIN_X, int(ORIGIN_Y - RO_SIZE * p_coll)), color, 5, cv::LINE_4);
  cv::putText(cropped_picture_, "Z", cv::Point(ORIGIN_X - 5, ORIGIN_Y + RO_SIZE + 40), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255,50,50), 1);

}


bool RlEnvironmentUavGimbalFollowingAdrian::EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: EnvDimensionality ImageBasedVisualServoing service called" << std::endl;

    // Action dimensionality
    response.action_dim = environment_info_.actions_dim_;

    // Action max
    response.action_max = environment_info_.actions_max_value_;

    // Action min
    response.action_min = environment_info_.actions_min_value_;

    // States dimensionality
    response.state_dim_lowdim = environment_info_.state_dim_low_dim_;

    // Number of steps per episode
    response.num_iterations = environment_info_.num_episode_steps_;

    response.state_min = std::vector<float>(environment_info_.state_dim_low_dim_, MIN_STATE_);
    response.state_max = std::vector<float>(environment_info_.state_dim_low_dim_, MAX_STATE_);

    // Service succesfully executed
    return true;
}

bool RlEnvironmentUavGimbalFollowingAdrian::ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response)
{
    // Print info
    std::cout << "RL_ENV_INFO: ResetSrv ImageBasedVisualServoing service called" << std::endl;


    // Enable reset
    if (ENABLE_PAUSED_SIMULATION)
        reset_env_ = true;
    else
        Reset();

    // Is it long iteration?
    //    if (exp_rec_.getRecording())
    //        long_iteration_ = true;

    cv::Rect previous_roi, current_roi;
    GetTargetROI(previous_roi, current_roi);

    std::vector<float> response_state;
    GetNormalizedState(response_state);

#ifdef IMAGE_STATE
    cv::Mat StateImage = cv::Mat::zeros(RESOLUTION_VIRTUAL_CAMERA_Y/IMAGE_SCALE, RESOLUTION_VIRTUAL_CAMERA_X/IMAGE_SCALE, CV_8UC1);
    StateImage(object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_scaled_)=255;

    std::vector<sensor_msgs::Image> image_vector_msg;

    cv_bridge::CvImage cv_bridge_obj;
    cv_bridge_obj.header.stamp = ros::Time::now();
    cv_bridge_obj.encoding = sensor_msgs::image_encodings::MONO8;
    cv_bridge_obj.image = StateImage;

    sensor_msgs::Image image_msg;
    cv_bridge_obj.toImageMsg(image_msg);
    image_vector_msg.push_back(image_msg);

    response.img_state = image_vector_msg;
#endif

    response.state = response_state;

    return true;
}

/// TO BE FILLED: BEHAVIOR WHEN RESETTING THE EPISODE
bool RlEnvironmentUavGimbalFollowingAdrian::Reset()
{
    // Check experiment recorder
    //    if(exp_rec_.getRecording()){
    //        exp_rec_.RecordExperiment();
    //        exp_rec_.setRecording(false);
    //    }
    // Get state
    float x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch;
    GetUavState(x_uav, y_uav, z_uav, dx_uav, dy_uav, roll, pitch);

    // Initialize prev_shaping
    prev_shaping_ = 0.0;

    // Reset info vectors
    values_max_.clear();
    values_min_.clear();

 #ifdef TRAINING_A
    // Send null action
    droneMsgsROS::droneSpeeds action_msg;
    action_msg.dx = 0;
    action_msg.dx = 0;
    action_msg.dz = 0;
    uav_speed_ref_pub_.publish(action_msg);

    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);
#endif

#if defined(TRAINING_B) || defined(TRAINING_C)|| defined(TRAINING_B_diff) || defined(TRAINING_C) || defined(TRAINING_C_dA) || defined(TRAINING_D) || defined(TRAINING_D_GIMBAL)|| defined(TRAINING_E)
    droneMsgsROS::dronePitchRollCmd roll_pitch_msg;
    roll_pitch_msg.pitchCmd = 0;  // Pitch
    roll_pitch_msg.rollCmd = 0;  // Roll
    roll_pitch_ref_pub_.publish(roll_pitch_msg);

    droneMsgsROS::droneDAltitudeCmd daltitude_msg;
    daltitude_msg.dAltitudeCmd = 0;  // dAltitude
    daltitude_ref_pub_.publish(daltitude_msg);

    angular_position_actual_gimbal_ = {0.0f,0.0f};
    angular_position_desired_gimbal_ = {0.0f, 0.0f};

#endif

#if defined(REAL_FLIGHT_OWN_DETECTOR) || defined(REAL_FLIGHT)
    geometry_msgs::Twist msg;
    msg.angular.y = (angular_position_desired_gimbal_[1] * 180.0 / M_PI - OFFSET_GIMBAL_REAL_WRT_SIM); // In degrees and with transformed axes
    msg.angular.z = angular_position_desired_gimbal_[0] * 180.0 / M_PI;
    camera_control_pub_.publish(msg);
#endif
    // Random initial point
    std::srand (static_cast <unsigned> (time(0)));

    float lower_lim_x = -environment_info_.max_pos_x_;
    float upper_lim_x = environment_info_.max_pos_x_;
    float lower_lim_y = -environment_info_.max_pos_y_;
    float upper_lim_y = environment_info_.max_pos_y_;
    float init_x = lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    float init_y = lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));

    float lower_lim_z = kUav_Altitude_-0.8;
    float upper_lim_z = kUav_Altitude_ + 0.8;
    float init_altitude = lower_lim_z + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_z-lower_lim_z)));

    init_x = -0.4f;
    init_y = 0.0f;

    std::cout << "Reseting gimbal position..." << std::endl;

    // Reset gimbal position

    // Set initial altitude
    droneMsgsROS::dronePositionRefCommandStamped altitude_msg;
    altitude_msg.header.stamp.sec = 0;
    altitude_msg.header.stamp.nsec = 0;
    altitude_msg.header.frame_id = "";
    altitude_msg.header.seq = 0;
    altitude_msg.position_command.x = 0;
    altitude_msg.position_command.y = 0;
    altitude_msg.position_command.z = init_altitude ;
    uav_pose_ref_pub_.publish(altitude_msg);

    // Spawn model to origin
    gazebo_msgs::SetModelState model_msg;
    model_msg.request.model_state.model_name = UAV_NAME;
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = init_altitude;
    //    model_msg.request.model_state.pose.orientation.x = 0;
    //    model_msg.request.model_state.pose.orientation.y = 0;
    //    model_msg.request.model_state.pose.orientation.z = 0;
    //    model_msg.request.model_state.pose.orientation.w = 1;

    // Store in member variable
    current_z_ = init_altitude;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting environment in x: " << init_x << "y: " << init_y << "z: "<<init_altitude<< std::endl;

    if (gazebo_client_.call(model_msg)){
        //        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
        //        return false;
    }
#ifdef LOG_DATA
    model_msg.request.model_state.model_name = UAV_LEADER_NAME;
    model_msg.request.model_state.pose.position.x = init_x+1;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = init_altitude - 0.3;
#else
    // Random target point
    lower_lim_x = MIN_TARGET_X - init_x;
    upper_lim_x = MAX_TARGET_X - init_x;
    init_x = init_x +lower_lim_x + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_x-lower_lim_x)));
    float ANGLE_Y = 0.942;
    lower_lim_y = - (init_x - init_y) * tanf(ANGLE_Y);
    upper_lim_y = (init_x - init_y) * tanf(ANGLE_Y);
    init_y = init_y + lower_lim_y + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_y-lower_lim_y)));
    float ANGLE_Z = 0.5281;
    lower_lim_z = - init_x * tanf(ANGLE_Z);
    upper_lim_z = init_x * tanf(ANGLE_Z);
    float init_z = lower_lim_z + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(upper_lim_z-lower_lim_z)));

    model_msg.request.model_state.model_name = UAV_LEADER_NAME;

#ifdef TRAINING_C_dA
    model_msg.request.model_state.pose.position.x = init_x+1;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = kUav_Altitude_ + init_z;
#else
    model_msg.request.model_state.pose.position.x = init_x;
    model_msg.request.model_state.pose.position.y = init_y;
    model_msg.request.model_state.pose.position.z = kUav_Altitude_-1;
#endif
#endif
    //    model_msg.request.model_state.pose.orientation.x = 0;
    //    model_msg.request.model_state.pose.orientation.y = 0;
    //    model_msg.request.model_state.pose.orientation.z = 0;
    //    model_msg.request.model_state.pose.orientation.w = 1;

    // Reseting environemnt
    std::cout << "RL_ENV_INFO: reseting target in x: " << init_x << "y: " << init_y << std::endl;

    if (gazebo_client_.call(model_msg)){
        //        return true;
    }
    else{
        ROS_ERROR("RL_ENV_INFO: Failed to call set model state");
        //        return false;
    }

    outFile_.open ("/home/alejandro/temporal/ground_truth.csv", std::ios::out | std::ios::ate | std::ios::app);
    outFile_ << 99999 << "," << 99999 << "," << 99999;
    outFile_ << "," <<99999 << "," << 99999 << "," << 99999;
    outFile_ << "," << 99999 << "," << 99999 << "," << 99999 << "," << 99999 << std::endl;
    outFile_.close();



}

void RlEnvironmentUavGimbalFollowingAdrian::PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg)
{

    cv::Point3f uav_pose;
    for (int i=0; i<msg->name.size(); i++)
    {
        if (msg->name[i].compare(UAV_NAME) == 0)
        {
            uav_pose.x = msg->pose[i].position.x;
            uav_pose.y = msg->pose[i].position.y;
            uav_pose.z = msg->pose[i].position.z;

            // Calculating Roll, Pitch, Yaw
            tf::Quaternion q(msg->pose[i].orientation.x, msg->pose[i].orientation.y, msg->pose[i].orientation.z, msg->pose[i].orientation.w);
            tf::Matrix3x3 m(q);

            //convert quaternion to euler angels
            double y, p, r;
            m.getEulerYPR(y, p, r);

            uav_state_.pos_x_ = uav_pose.x;
            uav_state_.pos_y_ = uav_pose.y;
            uav_state_.pos_z_ = uav_pose.z;

            uav_state_.yaw_ = y;
            uav_state_.pitch_ = p;
            uav_state_.roll_ = r;

        }
        else if(msg->name[i].compare(MOVING_TARGET_NAME) == 0)
        {
            target_pose.x = msg->pose[i].position.x;
            target_pose.y = msg->pose[i].position.y;
            target_pose.z = msg->pose[i].position.z;
        }
        else if(msg->name[i].compare(UAV_LEADER_NAME) == 0)
        {
            uav_leader_state_.pos_x_ = msg->pose[i].position.x;
            uav_leader_state_.pos_y_ = msg->pose[i].position.y;
            uav_leader_state_.pos_z_ = msg->pose[i].position.z;
        }
    }

#ifdef OBJECT_BASED_GROUND_TRUTH_GIMBAL
    //**** Object points w.r.t. WORLD frame of reference ****//
    // Upper side
    object_in_world_info_.current_points_mat_[0] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[0].at<float>(0,0) = target_pose.x + object_in_world_info_.width_ / 2.0;
    object_in_world_info_.current_points_mat_[0].at<float>(1,0) = target_pose.y + object_in_world_info_.width_ / 2.0;
    object_in_world_info_.current_points_mat_[0].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[0].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[1] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[1].at<float>(0,0) = target_pose.x + object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[1].at<float>(1,0) = target_pose.y - object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[1].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[1].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[2] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[2].at<float>(0,0) = target_pose.x - object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[2].at<float>(1,0) = target_pose.y - object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[2].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[2].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[3] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[3].at<float>(0,0) = target_pose.x - object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[3].at<float>(1,0) = target_pose.y + object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[3].at<float>(2,0) = target_pose.z + object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[3].at<float>(3,0) = 1.0;

    // Lower side
    object_in_world_info_.current_points_mat_[4] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[4].at<float>(0,0) = target_pose.x + object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[4].at<float>(1,0) = target_pose.y + object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[4].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[4].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[5] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[5].at<float>(0,0) = target_pose.x + object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[5].at<float>(1,0) = target_pose.y - object_in_world_info_.width_/2.0;
    object_in_world_info_.current_points_mat_[5].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[5].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[6] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[6].at<float>(0,0) = target_pose.x - object_in_world_info_.width_ / 2.0;
    object_in_world_info_.current_points_mat_[6].at<float>(1,0) = target_pose.y - object_in_world_info_.width_ / 2.0;
    object_in_world_info_.current_points_mat_[6].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[6].at<float>(3,0) = 1.0;

    object_in_world_info_.current_points_mat_[7] = cv::Mat(4, 1, CV_32FC1);
    object_in_world_info_.current_points_mat_[7].at<float>(0,0) = target_pose.x - object_in_world_info_.width_/ 2.0;
    object_in_world_info_.current_points_mat_[7].at<float>(1,0) = target_pose.y + object_in_world_info_.width_/ 2.0;
    object_in_world_info_.current_points_mat_[7].at<float>(2,0) = target_pose.z - object_in_world_info_.height_;
    object_in_world_info_.current_points_mat_[7].at<float>(3,0) = 1.0;

    cv::Point3f uav_target_relative_pose;
    uav_target_relative_pose.x = uav_pose.x - target_pose.x;
    uav_target_relative_pose.y = uav_pose.y - target_pose.y;
    uav_target_relative_pose.z = uav_pose.z - target_pose.z;
    ComputeGroundTruthObjectPositionInImageFishEye(uav_pose.x, uav_pose.y, uav_pose.z);
#endif


}

void  RlEnvironmentUavGimbalFollowingAdrian::RealImuCallback(const nav_msgs::Odometry::ConstPtr& msg){



    tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y,msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
    tf::Matrix3x3 m(q);

    //convert quaternion to euler angels
    double y, p, r;
    m.getEulerYPR(y, p, r);


    uav_state_.imu_yaw_ = y;
    uav_state_.imu_pitch_ = p;
    uav_state_.imu_roll_ = r;



}


void RlEnvironmentUavGimbalFollowingAdrian::CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr &msg)
{
    if(!camera_info_.camera_info_available_flag_)
    {
        std::cout<<"Camera Info Callback"<<std::endl;
        camera_info_.camera_info_available_flag_ = true;

        camera_info_.cx_ = msg->K.at(2);
        camera_info_.cy_ = msg->K.at(5);
        camera_info_.fx_ = msg->K.at(0);
        camera_info_.fy_ = msg->K.at(4);

        camera_info_.image_width_ = RESOLUTION_VIRTUAL_CAMERA_X;
        camera_info_.image_height_ = RESOLUTION_VIRTUAL_CAMERA_Y;

        camera_info_.K_matrix_ = cv::Mat(3, 3, CV_32FC1);
        camera_info_.K_matrix_.at<float>(0,0) = camera_info_.fx_;
        camera_info_.K_matrix_.at<float>(0,1) = 0.0;
        camera_info_.K_matrix_.at<float>(0,2) = RESOLUTION_VIRTUAL_CAMERA_X/2 + 0.5;

        camera_info_.K_matrix_.at<float>(1,0) = 0.0;
        camera_info_.K_matrix_.at<float>(1,1) = camera_info_.fy_;
        camera_info_.K_matrix_.at<float>(1,2) = RESOLUTION_VIRTUAL_CAMERA_Y/2 + 0.5;

        camera_info_.K_matrix_.at<float>(2,0) = 0.0;
        camera_info_.K_matrix_.at<float>(2,1) = 0.0;
        camera_info_.K_matrix_.at<float>(2,2) = 1.0;

        K_matrix_fish_eye_ = cv::Mat(3, 3, CV_32FC1);
        K_matrix_fish_eye_.at<float>(0,0) = camera_info_.fx_;
        K_matrix_fish_eye_.at<float>(0,1) = 0.0;
        K_matrix_fish_eye_.at<float>(0,2) = camera_info_.cx_;

        K_matrix_fish_eye_.at<float>(1,0) = 0.0;
        K_matrix_fish_eye_.at<float>(1,1) = camera_info_.fy_;
        K_matrix_fish_eye_.at<float>(1,2) = camera_info_.cy_;

        K_matrix_fish_eye_.at<float>(2,0) = 0.0;
        K_matrix_fish_eye_.at<float>(2,1) = 0.0;
        K_matrix_fish_eye_.at<float>(2,2) = 1.0;


//        std::cout<<"Camera Matrix: "<<std::endl<<camera_info_.K_matrix_<<std::endl<<std::endl;
    }
}

void RlEnvironmentUavGimbalFollowingAdrian::CameraFishEyeCallback(const sensor_msgs::ImageConstPtr& msg)
{
    try
    {
        captured_image_.release();
        cv::Mat img = cv_bridge::toCvShare(msg, "bgr8")->image;

        captured_image_ = GetPictureGimbalAgent(img.clone());

    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}

void RlEnvironmentUavGimbalFollowingAdrian::SetUavState(const float x, const float y, const float z, const float dx, const float dy)
{
    uav_mutex_.lock();
    uav_state_.pos_x_ = x;
    uav_state_.pos_y_ = y;
    uav_state_.pos_z_ = z;
    uav_state_.speed_x_ = dx;
    uav_state_.speed_y_ = dy;
    uav_mutex_.unlock();
}

void RlEnvironmentUavGimbalFollowingAdrian::GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float &pitch)
{
    uav_mutex_.lock();
    x = uav_state_.pos_x_;
    y = uav_state_.pos_y_;
    z = uav_state_.pos_z_;
    dx = uav_state_.speed_x_;
    dy = uav_state_.speed_y_;
    roll = uav_state_.roll_;
    pitch = uav_state_.pitch_;
    uav_mutex_.unlock();
}

cv::Mat RlEnvironmentUavGimbalFollowingAdrian::ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma)
{
    cv::Mat R = cv::Mat(3, 3, CV_32FC1);
    R.at<float>(0,0) = cos(alpha)*cos(beta);
    R.at<float>(0,1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
    R.at<float>(0,2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);

    R.at<float>(1,0) = sin(alpha)*cos(beta);
    R.at<float>(1,1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
    R.at<float>(1,2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);

    R.at<float>(2,0) = -sin(beta);
    R.at<float>(2,1) = cos(beta)*sin(gamma);
    R.at<float>(2,2) = cos(beta)*cos(gamma);

    return R;

}

cv::Mat RlEnvironmentUavGimbalFollowingAdrian::ComputeHomogeneousMatrixTransform(float px, float py, float pz, float yaw, float pitch, float roll)
{
    //Eigen::Matrix4d T_uav_world;
    cv::Mat R = ConvertFromYPRtoRotationMatrix(yaw, pitch, roll);

    cv::Mat T = cv::Mat::eye(4, 4, CV_32FC1);
    T.at<float>(0,0) = R.at<float>(0,0); T.at<float>(0,1) = R.at<float>(0,1); T.at<float>(0,2) = R.at<float>(0,2); T.at<float>(0,3) = px;
    T.at<float>(1,0) = R.at<float>(1,0); T.at<float>(1,1) = R.at<float>(1,1); T.at<float>(1,2) = R.at<float>(1,2); T.at<float>(1,3) = py;
    T.at<float>(2,0) = R.at<float>(2,0); T.at<float>(2,1) = R.at<float>(2,1); T.at<float>(2,2) = R.at<float>(2,2); T.at<float>(2,3) = pz;
    T.at<float>(3,0) = 0.0; T.at<float>(3,1) = 0.0; T.at<float>(3,2) = 0.0; T.at<float>(3,3) = 1.0;
    return T;
}

cv::Mat RlEnvironmentUavGimbalFollowingAdrian::ComputeHomogeneousMatrixTransform(const float px, const float py, const float pz, const cv::Mat R)
{
    cv::Mat T = cv::Mat::eye(4, 4, CV_32FC1);
    T.at<float>(0,0) = R.at<float>(0,0); T.at<float>(0,1) = R.at<float>(0,1); T.at<float>(0,2) = R.at<float>(0,2); T.at<float>(0,3) = px;
    T.at<float>(1,0) = R.at<float>(1,0); T.at<float>(1,1) = R.at<float>(1,1); T.at<float>(1,2) = R.at<float>(1,2); T.at<float>(1,3) = py;
    T.at<float>(2,0) = R.at<float>(2,0); T.at<float>(2,1) = R.at<float>(2,1); T.at<float>(2,2) = R.at<float>(2,2); T.at<float>(2,3) = pz;
    T.at<float>(3,0) = 0.0; T.at<float>(3,1) = 0.0; T.at<float>(3,2) = 0.0; T.at<float>(3,3) = 1.0;
    return T;
}

cv::Mat RlEnvironmentUavGimbalFollowingAdrian::StabilizeVirtualCamera(cv::Mat stabilizied_captured_image_){
    // From degrees to Pixels
    cv::Mat H = cv::Mat::ones(3, 3, CV_32FC1);

    cv::Mat R_uav_cam = ConvertFromYPRtoRotationMatrix(M_PI/2, M_PI, 0);
    cv::Mat Rot_cam_gimbal = ConvertFromYPRtoRotationMatrix(0,  0, M_PI/2-0.558);
    cv::Mat Rotacion_uav_gimbal = R_uav_cam * Rot_cam_gimbal;

#ifdef STABILIZE_YAW
    cv::Mat Rotacion_wordl_uav = ConvertFromYPRtoRotationMatrix( uav_state_.yaw_, uav_state_.pitch_, uav_state_.roll_);
#else
    cv::Mat Rotacion_wordl_uav = ConvertFromYPRtoRotationMatrix( 0, uav_state_.pitch_, uav_state_.roll_);
#endif

    cv::Mat Rat = Rotacion_uav_gimbal.inv() * Rotacion_wordl_uav * Rotacion_uav_gimbal;
    cv::Mat StabilizedVirtualCamera_;

#ifdef SHOW_IMAGES
    cv::Mat ImShowNotStabilized_;
    resize(stabilizied_captured_image_, ImShowNotStabilized_, cv::Size(640, 380), 0, 0, cv::INTER_AREA); //
    cv::imshow("Sin estabilizar", ImShowNotStabilized_);
    cv::waitKey(1);
#endif

    if ((!stabilizied_captured_image_.empty()) && (!K_matrix_fish_eye_.empty())){
        H = K_matrix_fish_eye_ * Rat * K_matrix_fish_eye_.inv();
        // Stabilize image
        cv::warpPerspective(stabilizied_captured_image_, StabilizedVirtualCamera_, H, stabilizied_captured_image_.size());

#ifdef SHOW_IMAGES
        cv::Mat ImShowStabilized_;
        resize(StabilizedVirtualCamera_, ImShowStabilized_, cv::Size(640, 380), 0, 0, cv::INTER_AREA); //
        cv::imshow("Estabilizada", ImShowStabilized_);
        cv::waitKey(1);
#endif
    }
    else{
        StabilizedVirtualCamera_ = stabilizied_captured_image_.clone();
    }

    return StabilizedVirtualCamera_;
}

int RlEnvironmentUavGimbalFollowingAdrian::AngularRotationToPixelDisplacement( const float HFOV, float angularDisplacement, const float resolutionAxis)
{
    float pixelDisplacement = (tan(angularDisplacement)* resolutionAxis) / (tan(HFOV/2) * 2);
    return round(pixelDisplacement);
}

std::vector<float> RlEnvironmentUavGimbalFollowingAdrian::MoveGimbalPosition(std::vector<float> goalPosition){

    float elpasedTime = (ros::Time::now() - previous_time_gimbal_).toSec();
    previous_time_gimbal_ = ros::Time::now();

    for (int i = 0; i < angular_position_actual_gimbal_.size(); i++){
        if (angular_position_actual_gimbal_[i] != goalPosition[i]){
            if(fabs(angular_position_actual_gimbal_[i] - goalPosition[i]) < elpasedTime * SPEED_MOVEMENT_GIMBAL_[i]){
                angular_position_actual_gimbal_[i] = goalPosition[i];
            }
            else
                if (angular_position_actual_gimbal_[i] > goalPosition[i]){
                    angular_position_actual_gimbal_[i] = angular_position_actual_gimbal_[i] - elpasedTime * SPEED_MOVEMENT_GIMBAL_[i];
                }

                else
                    angular_position_actual_gimbal_[i] = angular_position_actual_gimbal_[i] + elpasedTime * SPEED_MOVEMENT_GIMBAL_[i];
        }
    }

    for (int j = 0; j < 2; j++){
        if (angular_position_actual_gimbal_[j] > angular_position_desired_gimbal_MAX_[j]){
            angular_position_actual_gimbal_[j] = angular_position_desired_gimbal_MAX_[j];
        }else if(angular_position_actual_gimbal_[j] < angular_position_desired_gimbal_MIN_[j]){
            angular_position_actual_gimbal_[j] = angular_position_desired_gimbal_MIN_[j];
        }
    }

    return angular_position_actual_gimbal_;
}


cv::Mat RlEnvironmentUavGimbalFollowingAdrian::GetPictureFromVirtualGimbal(cv::Mat fish_eye_picture_, std::vector<float> gimbal_position_){
    if (fish_eye_picture_.total() != 0){

        cv::Mat stabilized_fish_eye = StabilizeVirtualCamera(fish_eye_picture_);

        std::vector<float> _ = MoveGimbalPosition(gimbal_position_);
        float pixelPosition_X = AngularRotationToPixelDisplacement(HFOV_, angular_position_actual_gimbal_[0], RESOLUTION_FISH_EYE_X - RESOLUTION_VIRTUAL_CAMERA_X);
        //Signo negativo porque en OpenCV esquina arriba izquierda es (0,0)
        float pixelPosition_Y = - AngularRotationToPixelDisplacement(VFOV_, angular_position_actual_gimbal_[1], RESOLUTION_FISH_EYE_Y - RESOLUTION_VIRTUAL_CAMERA_Y);

//        std::cout<<"angular_position_actual_gimbal_vector[0]" <<angular_position_actual_gimbal_[0] << " " <<angular_position_actual_gimbal_[0] *180/3.14  <<std::endl;
//        std::cout<<"angular_position_actual_gimbal_vector[1] sin Offset" <<angular_position_actual_gimbal_[1] << " " << angular_position_actual_gimbal_[1] *180/3.14  <<std::endl;
//        std::cout<<"angular_position_actual_gimbal_vector[1] con" <<angular_position_actual_gimbal_[1] << " " << angular_position_actual_gimbal_[1] *180/3.14 - 32.5 <<std::endl;

        virtual_gimbal_roi_.x = pixelPosition_X - RESOLUTION_VIRTUAL_CAMERA_X / 2 + RESOLUTION_FISH_EYE_X / 2;
        virtual_gimbal_roi_.y = pixelPosition_Y - RESOLUTION_VIRTUAL_CAMERA_Y / 2 + RESOLUTION_FISH_EYE_Y / 2;
        virtual_gimbal_roi_.width = RESOLUTION_VIRTUAL_CAMERA_X;
        virtual_gimbal_roi_.height = RESOLUTION_VIRTUAL_CAMERA_Y;

        roi_position_x = virtual_gimbal_roi_.x; // USEFUL FOR ROI!! Theese variables are used to keep the position of the gimbal
        roi_position_y = virtual_gimbal_roi_.y;

        cropped_picture_ = stabilized_fish_eye(virtual_gimbal_roi_);

//#ifdef RED_DETECTOR
//        (cropped_picture_, "Red");
//#endif

//        cv::Mat cropped_picture_resize_;

        // Get current ROI
        cv::Rect previous_roi, current_roi;
        GetTargetROI(previous_roi, current_roi);
//        resize(cropped_picture_, cropped_picture_resize_, cv::Size(CROPPED_PICTURE_WIDTH, CROPPED_PICTURE_WIDTH), 0, 0, cv::INTER_AREA);
        cv::rectangle(cropped_picture_,  current_roi, cv::Scalar(255, 255, 0), 2, 4);
        cv::rectangle(cropped_picture_,  target_roi_, cv::Scalar(255, 200, 0), 2, 4);

        // Show preview gimbal
        cv::Mat output = cropped_picture_.clone();
        float alpha = 0.3;
        cv::Rect preview_fish_eye(530 - int(RESOLUTION_FISH_EYE_X / (2.0 * SCALE_OF_PREVIEW)), 285 - int(RESOLUTION_FISH_EYE_Y / (2.0 * SCALE_OF_PREVIEW)), int(RESOLUTION_FISH_EYE_X / (SCALE_OF_PREVIEW)), int (RESOLUTION_FISH_EYE_Y / (SCALE_OF_PREVIEW)));
        cv::Rect preview_virtual_gimbal(preview_fish_eye.x + (virtual_gimbal_roi_.x / SCALE_OF_PREVIEW), preview_fish_eye.y + (virtual_gimbal_roi_.y / SCALE_OF_PREVIEW), int(virtual_gimbal_roi_.width / (SCALE_OF_PREVIEW)), int(virtual_gimbal_roi_.height / (SCALE_OF_PREVIEW)));
        cv::rectangle(cropped_picture_,  preview_fish_eye, cv::Scalar(127, 255, 0), -1);
        cv::rectangle(cropped_picture_,  preview_virtual_gimbal, cv::Scalar(0, 255, 127), -1);
        cv::addWeighted(cropped_picture_, alpha, output, 1 - alpha, 0, output);
        output.copyTo(cropped_picture_);

        // Print values in image
        if (keys_.size() > 0 && values_.size() > 0)
            PrintInfoInImage(keys_, values_);

        if (actions_.size() > 0)
            PrintAxesInImage(cropped_picture_, actions_);

        cv::imshow("Virtual Gimbal Output", cropped_picture_);
        cv::waitKey(1);

    } else{
        cropped_picture_ = fish_eye_picture_.clone();
    }

    return cropped_picture_;
}

void RlEnvironmentUavGimbalFollowingAdrian::MoveGimbalAgent(std::vector<float> move_gimbal_){
    float MAX_STEP_X = 0.22/0.2;
     float MAX_STEP_Y = 0.22/0.2;

     angular_position_desired_gimbal_[0] = angular_position_actual_gimbal_[0];
     angular_position_desired_gimbal_[1] = angular_position_actual_gimbal_[1];


     angular_position_desired_gimbal_[0] = angular_position_desired_gimbal_[0] + MAX_STEP_X * move_gimbal_[0];
     angular_position_desired_gimbal_[1] = angular_position_desired_gimbal_[1] + MAX_STEP_Y * move_gimbal_[1];
     for (int j = 0; j < 2; j++){
         if (angular_position_desired_gimbal_[j] > angular_position_desired_gimbal_MAX_[j]){
             angular_position_desired_gimbal_[j] = angular_position_desired_gimbal_MAX_[j];
         }else if(angular_position_desired_gimbal_[j] < angular_position_desired_gimbal_MIN_[j]){
             angular_position_desired_gimbal_[j] = angular_position_desired_gimbal_MIN_[j];
         }
     }
 }

cv::Mat RlEnvironmentUavGimbalFollowingAdrian::GetPictureGimbalAgent(cv::Mat fish_eye_picture_){
    return GetPictureFromVirtualGimbal(fish_eye_picture_, angular_position_desired_gimbal_);
}

void RlEnvironmentUavGimbalFollowingAdrian::ComputeGroundTruthObjectPositionInImageFishEye(const float &x_uav, const float &y_uav, const float &z_uav)
{
    if(camera_info_.camera_info_available_flag_)
    {
        // Transformations
#ifdef STABILIZE_YAW
        cv::Mat T_uav_world = ComputeHomogeneousMatrixTransform(-x_uav, -y_uav, -z_uav, 0, 0, 0);
#else
        cv::Mat T_uav_world =  ComputeHomogeneousMatrixTransform(0,0,0, -uav_state_.yaw_, 0, 0) * ComputeHomogeneousMatrixTransform(-x_uav, -y_uav, -z_uav, 0, 0, 0);
#endif
        cv::Mat T_cam_uav = ComputeHomogeneousMatrixTransform(0, 0, 0, ConvertFromYPRtoRotationMatrix(-M_PI/2, 0, M_PI));
        cv::Mat T_cam_gimbal = ComputeHomogeneousMatrixTransform(0, 0, 0, ConvertFromYPRtoRotationMatrix(0, 0, - (M_PI / 2 - 0.558)));

        // Destination = what_to_transform to reach origin
        cv::Mat T_cam_world = T_cam_gimbal * T_cam_uav * T_uav_world;

        //Points Referred to Camera frame of reference
        cv::Mat p1 = T_cam_world * object_in_world_info_.current_points_mat_[0]; p1.pop_back(1);
        cv::Mat p2 = T_cam_world * object_in_world_info_.current_points_mat_[1]; p2.pop_back(1);
        cv::Mat p3 = T_cam_world * object_in_world_info_.current_points_mat_[2]; p3.pop_back(1);
        cv::Mat p4 = T_cam_world * object_in_world_info_.current_points_mat_[3]; p4.pop_back(1);
        cv::Mat p5 = T_cam_world * object_in_world_info_.current_points_mat_[4]; p5.pop_back(1);
        cv::Mat p6 = T_cam_world * object_in_world_info_.current_points_mat_[5]; p6.pop_back(1);
        cv::Mat p7 = T_cam_world * object_in_world_info_.current_points_mat_[6]; p7.pop_back(1);
        cv::Mat p8 = T_cam_world * object_in_world_info_.current_points_mat_[7]; p8.pop_back(1);

        //Proyect the points Referred to Camera frame of reference into the image plane
        p1 = K_matrix_fish_eye_ * p1;
        p2 = K_matrix_fish_eye_ * p2;
        p3 = K_matrix_fish_eye_ * p3;
        p4 = K_matrix_fish_eye_ * p4;
        p5 = K_matrix_fish_eye_ * p5;
        p6 = K_matrix_fish_eye_ * p6;
        p7 = K_matrix_fish_eye_ * p7;
        p8 = K_matrix_fish_eye_ * p8;

        cv::Mat p1_image = p1/p1.at<float>(2,0);
        cv::Mat p2_image = p2/p2.at<float>(2,0);
        cv::Mat p3_image = p3/p3.at<float>(2,0);
        cv::Mat p4_image = p4/p4.at<float>(2,0);
        cv::Mat p5_image = p5/p5.at<float>(2,0);
        cv::Mat p6_image = p6/p6.at<float>(2,0);
        cv::Mat p7_image = p7/p7.at<float>(2,0);
        cv::Mat p8_image = p8/p8.at<float>(2,0);
        initial_points_.clear();
        initial_points_ = {p1_image, p2_image, p3_image, p4_image, p5_image, p6_image, p7_image, p8_image};

        // Search for maximum points
        float max_x = 0, max_y = 0;
        float min_x = 1E9, min_y = 1E9;
        for (int i = 0; i < initial_points_.size(); i++){
            if (initial_points_[i].at<float>(0,0) < min_x)
                min_x = initial_points_[i].at<float>(0,0);
            if (initial_points_[i].at<float>(1,0) < min_y)
                min_y = initial_points_[i].at<float>(1,0);
            if (initial_points_[i].at<float>(0,0) > max_x)
                max_x = initial_points_[i].at<float>(0,0);
            if (initial_points_[i].at<float>(1,0) > max_y)
                max_y = initial_points_[i].at<float>(1,0);
        }

        object_in_image_info_fish_eye_.ground_truth_detected_roi_rect_ = cv::Rect(cv::Point(p2_image.at<float>(0,0), p2_image.at<float>(1,0)),
                                                                                  cv::Point(p4_image.at<float>(0,0), p4_image.at<float>(1,0)));

        cv::Point2f roi_diag;
        roi_diag.x = p2_image.at<float>(0,0) - p1_image.at<float>(0,0);
        roi_diag.y = p2_image.at<float>(1,0) - p1_image.at<float>(1,0);
        object_in_image_info_fish_eye_.ground_truth_detected_roi_diameter_ = std::sqrt(std::pow(roi_diag.x, 2) + std::pow(roi_diag.y, 2));

        std::vector<cv::Mat> p_image;
        p_image.push_back(p1_image);
        p_image.push_back(p2_image);
        p_image.push_back(p3_image);
        p_image.push_back(p4_image);
        for(int i=0;i<p_image.size();i++)
        {
            object_in_image_info_fish_eye_.ground_truth_current_object_points_[i].x = p_image[i].at<float>(0,0);
            object_in_image_info_fish_eye_.ground_truth_current_object_points_[i].y = p_image[i].at<float>(1,0);
        }



        float ground_truth_roi_width= object_in_image_info_fish_eye_.ground_truth_current_object_points_[1].x
                - object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].x;

        float ground_truth_roi_height = object_in_image_info_fish_eye_.ground_truth_current_object_points_[1].y
                - object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].y;

        cv::Point2f ground_truth_roi_center;
        ground_truth_roi_center.x = object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].x
                + ground_truth_roi_width/2.0 - roi_position_x;
        ground_truth_roi_center.y = object_in_image_info_fish_eye_.ground_truth_current_object_points_[0].y
                + ground_truth_roi_height/2.0 - roi_position_y;

        //        object_in_image_info_fish_eye_.gimbal_ground_truth_previous_detected_roi_rect_ = object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_;
        object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_ = cv::Rect(cv::Point(min_x - roi_position_x, min_y - roi_position_y),
                                                                                         cv::Point(max_x - roi_position_x, max_y - roi_position_y));

#ifdef IMAGE_STATE
        float CornerUpLeftX;
        float CornerUpLeftY;
        float CornerDowRightX;
        float CornerDowRightY;

        if (((min_x - roi_position_x)/IMAGE_SCALE) < 0.0){
            CornerUpLeftX = 0.0;
        }else if(((min_x - roi_position_x)/IMAGE_SCALE) > RESOLUTION_VIRTUAL_CAMERA_X/IMAGE_SCALE) {
            CornerUpLeftX = RESOLUTION_VIRTUAL_CAMERA_X/IMAGE_SCALE;
        }
        else {

            CornerUpLeftX = (min_x - roi_position_x)/IMAGE_SCALE;
        }

        if (((min_y - roi_position_y)/IMAGE_SCALE) < 1){
           CornerUpLeftY = 0.0;
        }else if(((min_y - roi_position_y)/IMAGE_SCALE) > RESOLUTION_VIRTUAL_CAMERA_Y/IMAGE_SCALE){
            CornerUpLeftY = RESOLUTION_VIRTUAL_CAMERA_Y/IMAGE_SCALE;
        }else{
           CornerUpLeftY = (min_y - roi_position_y)/IMAGE_SCALE ;
        }


        if (((max_x - roi_position_x)/IMAGE_SCALE) > RESOLUTION_VIRTUAL_CAMERA_X/IMAGE_SCALE){
            CornerDowRightX = RESOLUTION_VIRTUAL_CAMERA_X/IMAGE_SCALE;
        }
        else if ( ((max_x - roi_position_x)/IMAGE_SCALE) <  0.0){
            CornerDowRightX  = 0.0 ;
        }else {
            CornerDowRightX = (max_x - roi_position_x)/IMAGE_SCALE;
        }


        if ( ((max_y - roi_position_y)/IMAGE_SCALE) >  RESOLUTION_VIRTUAL_CAMERA_Y/IMAGE_SCALE){
            CornerDowRightY = RESOLUTION_VIRTUAL_CAMERA_Y/IMAGE_SCALE;
        }
        else if ( ((max_y - roi_position_y)/IMAGE_SCALE) <  0.0){
            CornerDowRightY  = 0.0 ;
        }else {
            CornerDowRightY = (max_y - roi_position_y)/IMAGE_SCALE;
        }


        std::cout<< "CornerUpLeftX: " << CornerUpLeftX << " CornerUpLeftY: " << CornerUpLeftY<< " " << "CornerDowRightX: " << CornerDowRightX << " CornerDowRightY: " << CornerDowRightY<< " " <<std::endl;

        object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_scaled_ = cv::Rect(cv::Point(CornerUpLeftX, CornerUpLeftY),
                                                                                         cv::Point(CornerDowRightX, CornerDowRightY));

#endif

    }
}

void RlEnvironmentUavGimbalFollowingAdrian::GetTargetROI(cv::Rect &previous_roi, cv::Rect &current_roi){
    // Store in member variables
#ifdef RED_DETECTOR
    previous_roi = object_in_image_info_fish_eye_.gimbal_red_detector_previous_roi_rect_;
    current_roi = object_in_image_info_fish_eye_.gimbal_red_detector_roi_rect_;

#else
    previous_roi = object_in_image_info_fish_eye_.gimbal_ground_truth_previous_detected_roi_rect_;
    current_roi = object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_;
#endif

}

void RlEnvironmentUavGimbalFollowingAdrian::DetectObjectInImage(const cv::Mat &I, const std::string object_color)
{
    cv::Mat I_hsv;

    cv::cvtColor(I, I_hsv, CV_BGR2HSV);
    cv::Mat1b mask1_hsv, mask2_hsv;


    cv::Mat I_binarized;
    if(object_color == "Red")
    {
        //Real
        cv::inRange(I_hsv, cv::Scalar(0, 100, 100), cv::Scalar(15, 255, 255), mask1_hsv);
        cv::inRange(I_hsv, cv::Scalar(170, 100, 100), cv::Scalar(180, 255, 255), mask2_hsv);
//        cv::inRange(I_hsv, cv::Scalar(0, 70, 20), cv::Scalar(10, 255, 255), mask1_hsv);
//        cv::inRange(I_hsv, cv::Scalar(170, 70, 20), cv::Scalar(180, 255, 255), mask2_hsv);

        I_binarized = mask1_hsv | mask2_hsv;
    }
    else if(object_color == "Blue")
    {
        //cv::inRange(I_hsv, cv::Scalar(90, 100, 50), cv::Scalar(140, 255, 255), I_mask_hsv); //Real
        cv::inRange(I_hsv, cv::Scalar(110, 70, 50), cv::Scalar(130, 255, 255), I_binarized); //Simulation
    }


    std::vector<std::vector<cv::Point> > contours;
    cv::findContours(I_binarized.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

    cv::imshow("Binarized camera", I_binarized);
    cv::waitKey(1);

    std::vector<std::vector<cv::Point> >::iterator itc = contours.begin();
    while(itc != contours.end())
    {
        cv::RotatedRect rot_box = cv::minAreaRect(cv::Mat(*itc));
        float rot_box_area = (float)rot_box.size.width * (float)rot_box.size.height;

        if(rot_box_area < 500)
            itc = contours.erase(itc);
        else
        {
            ++itc;
        }
        if(rot_box_area <100){
            counter_no_detection_ = counter_no_detection_ + 1;
            if (counter_no_detection_ > 50){
                bool_no_detection_ = true;
            }
        }else {
            counter_no_detection_ = 0;

        }
    }


    cv::RotatedRect rotated_rect_item;
    cv::Rect rect_item;

    if(contours.size())
    {
        rotated_rect_item = cv::minAreaRect(cv::Mat(contours[0]));
        rect_item = cv::boundingRect(cv::Mat(contours[0]));
        object_in_image_info_fish_eye_.gimbal_red_detector_roi_rect_ = rect_item;

    }

}

#if defined(REAL_FLIGHT_OWN_DETECTOR) || defined(REAL_FLIGHT)
void RlEnvironmentUavGimbalFollowingAdrian::CameraBebopREALCallbackNoGimbal(const sensor_msgs::ImageConstPtr& msg){
  cv::Mat img;
  try
  {
      img = cv_bridge::toCvShare(msg, "bgr8")->image;
//      cv::resize(img, img, cv::Size(640, 368), 0, 0, cv::INTER_AREA);
  }
  catch (cv_bridge::Exception& e)
  {
      ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }


  float pixelPosition_X = AngularRotationToPixelDisplacement(HFOV_, angular_position_actual_gimbal_[0], RESOLUTION_FISH_EYE_X - RESOLUTION_VIRTUAL_CAMERA_X);
  //Signo negativo porque en OpenCV esquina arriba izquierda es (0,0)
  float pixelPosition_Y = - AngularRotationToPixelDisplacement(VFOV_, angular_position_actual_gimbal_[1], RESOLUTION_FISH_EYE_Y - RESOLUTION_VIRTUAL_CAMERA_Y);

  virtual_gimbal_roi_.x = pixelPosition_X - RESOLUTION_VIRTUAL_CAMERA_X / 2 + RESOLUTION_FISH_EYE_X / 2;
  virtual_gimbal_roi_.y = pixelPosition_Y - RESOLUTION_VIRTUAL_CAMERA_Y / 2 + RESOLUTION_FISH_EYE_Y / 2;
  virtual_gimbal_roi_.width = RESOLUTION_VIRTUAL_CAMERA_X;
  virtual_gimbal_roi_.height = RESOLUTION_VIRTUAL_CAMERA_Y;

  roi_position_x = pixelPosition_X - RESOLUTION_VIRTUAL_CAMERA_X / 2 + RESOLUTION_FISH_EYE_X / 2; // USEFUL FOR ROI!! Theese variables are used to keep the position of the gimbal
  roi_position_y = pixelPosition_Y - RESOLUTION_VIRTUAL_CAMERA_Y / 2 + RESOLUTION_FISH_EYE_Y / 2;

#ifdef RED_DETECTOR
  DetectObjectInImage(img.clone(), "Red");
#endif
  // Get current ROI
  cv::Rect previous_roi, current_roi;
  GetTargetROI(previous_roi, current_roi);
  cv::rectangle(img,  current_roi, cv::Scalar(255, 255, 0), 2, 4);
  cv::rectangle(img,  target_roi_, cv::Scalar(255, 200, 0), 2, 4);


//  // Show preview gimbal
//  cv::Mat output = img.clone();
//  float alpha = 0.3;
//  cv::Rect preview_fish_eye(530 - int(RESOLUTION_FISH_EYE_X / (2.0 * SCALE_OF_PREVIEW)), 285 - int(RESOLUTION_FISH_EYE_Y / (2.0 * SCALE_OF_PREVIEW)), int(RESOLUTION_FISH_EYE_X / (SCALE_OF_PREVIEW)), int (RESOLUTION_FISH_EYE_Y / (SCALE_OF_PREVIEW)));
//  std::cout << "angular_position_actual_gimbal_.x: " <<angular_position_actual_gimbal_[0] << "   " <<" virtual_gimbal_roi_.y" <<angular_position_actual_gimbal_[1] <<std::endl;
//  cv::Rect preview_virtual_gimbal(preview_fish_eye.x + (virtual_gimbal_roi_.x / SCALE_OF_PREVIEW), preview_fish_eye.y + (virtual_gimbal_roi_.y / SCALE_OF_PREVIEW), int(virtual_gimbal_roi_.width / (SCALE_OF_PREVIEW)), int(virtual_gimbal_roi_.height / (SCALE_OF_PREVIEW)));
//  cv::rectangle(img,  preview_fish_eye, cv::Scalar(127, 255, 0), -1);
//  cv::rectangle(img,  preview_virtual_gimbal, cv::Scalar(0, 255, 127), -1);
//  cv::addWeighted(img, alpha, output, 1 - alpha, 0, output);
//  output.copyTo(img);

//  cv::imshow("Real camera", img);
//  cv::waitKey(1);
}

#endif



#if defined(REAL_FLIGHT_OWN_DETECTOR) || defined(REAL_FLIGHT)

void RlEnvironmentUavGimbalFollowingAdrian::MoveGimbalAgentREAL(std::vector<float> move_gimbal){
  // Accumulate the angle in radians and convert to angles
  float MAX_STEP_X =0.3*(0.22/0.2) ;
  float MAX_STEP_Y = 0.3*(0.22/0.2);


  angular_position_desired_gimbal_[0] = angular_position_actual_gimbal_[0];
  angular_position_desired_gimbal_[1] = angular_position_actual_gimbal_[1];


  angular_position_desired_gimbal_[0] = angular_position_desired_gimbal_[0] + MAX_STEP_X * move_gimbal[0];
  angular_position_desired_gimbal_[1] = angular_position_desired_gimbal_[1] + MAX_STEP_Y * move_gimbal[1];

  for (int j = 0; j < 2; j++){
      if (angular_position_desired_gimbal_[j] > angular_position_desired_gimbal_MAX_[j]){
          angular_position_desired_gimbal_[j] = angular_position_desired_gimbal_MAX_[j];
      }else if(angular_position_desired_gimbal_[j] < angular_position_desired_gimbal_MIN_[j]){
          angular_position_desired_gimbal_[j] = angular_position_desired_gimbal_MIN_[j];
      }
  }


  // Send command to bebop 2
  geometry_msgs::Twist msg;
  msg.angular.y = (angular_position_desired_gimbal_[1] * 180.0 / M_PI - OFFSET_GIMBAL_REAL_WRT_SIM); // In degrees and with transformed axes
  msg.angular.z = angular_position_desired_gimbal_[0] * 180.0 / M_PI;
  std::cout << "Gimbal controller: " << angular_position_desired_gimbal_[0] << "   " << angular_position_desired_gimbal_[1] << std::endl;
  std::cout << "Move gimbal: " << move_gimbal[0] << "   " << move_gimbal[1] << std::endl;
  std::cout << "Final actions: " << msg.angular.z << "   " << msg.angular.y << std::endl;
  camera_control_pub_.publish(msg);
}

void RlEnvironmentUavGimbalFollowingAdrian::GimbalStateREALCallback(sensor_msgs::JointState::ConstPtr msg){
  // Read gimbal position in radians
  angular_position_actual_gimbal_[0] = -msg->position[0]; // In radians
  angular_position_actual_gimbal_[1] = -msg->position[1] + OFFSET_GIMBAL_REAL_WRT_SIM * M_PI / 180.0; // Compute offset of Bebop 2 gimbal

  std::cout << "Angular position gimbal: " << angular_position_actual_gimbal_[0] << "   " << angular_position_actual_gimbal_[1] << std::endl;
}

void RlEnvironmentUavGimbalFollowingAdrian::StdBbsREALCallback(const std_msgs::Float32MultiArrayConstPtr& msg){
  object_in_image_info_fish_eye_.gimbal_ground_truth_detected_roi_rect_ = cv::Rect(cv::Point(msg->data[0] /** (640.0 / 856.0)*/, msg->data[1] /** (368.0 / 480.0)*/),
                                                                                  cv::Point(msg->data[2] /** (640.0 / 856.0)*/, msg->data[3] /** (368.0 / 480.0)*/));
}

#endif

