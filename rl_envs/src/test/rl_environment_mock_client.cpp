/*
 *
 *
 *  Created on: May 26th 2017
 *      Author: Alejandro Rodríguez
 */

#include "opencv2/opencv.hpp"
#include "boost/thread.hpp"
#include "boost/bind.hpp"
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <ros/ros.h>
#include <iostream>
#include <string>
#include "rl_srvs/AgentSrv.h"
#include "rl_srvs/EnvDimensionalitySrv.h"

using namespace boost::interprocess;

int main(int argc, char **argv)
{
    // Init ROS
    ros::init(argc, argv, "rl_environment_mock_client");
    ros::NodeHandle n;

    // Init shared memory
    struct shared_memory_buffer
    {
       shared_memory_buffer()
          : check_action_(0), iterate_(0), iteration_finished_(1)
       {}

       // Semaphores to protect and synchronize access
       boost::interprocess::interprocess_semaphore
          check_action_, iterate_, iteration_finished_;

       // Items to fill
       unsigned int num_iterations;

    };

    shared_memory_object shm_;
    void * addr_;
    shared_memory_buffer * data_;

    // Create a shared memory object.
    shm_ = shared_memory_object
       (open_only                   //only open
       ,"CheckActionShm"            //name
       ,read_write                  //read-write mode
       );

    // Map the whole shared memory in this process
    mapped_region region
            (shm_                   //What to map
             ,read_write            //Map it as read-write
             );

    // Get the address of the mapped region
    addr_ = region.get_address();

    // Construct the shared structure in memory
    data_ = static_cast<shared_memory_buffer*>(addr_);

    // Init Ros services
    ros::ServiceClient env_dimensionality_client_ = n.serviceClient<rl_srvs::EnvDimensionalitySrv>("/rl_env_dimensionality_service");
    ros::ServiceClient env_step_client_ = n.serviceClient<rl_srvs::AgentSrv>("/rl_env_step_service");

    // First service  call
    rl_srvs::EnvDimensionalitySrv env_dimensionality_msg_;
    std::cout << "MOCK CLIENT INFO: Calling dimensionality service.." << std::endl;
    if (env_dimensionality_client_.call(env_dimensionality_msg_)){
      std::cout << "MOCK CLIENT INFO: Successful call to dimensionality service" << std::endl;
      // Print environment info
      std::cout << "MOCK CLIENT INFO: the actions dimension is " << env_dimensionality_msg_.response.action_dim << std::endl;
      std::cout << "MOCK CLIENT INFO: the actions max is " << env_dimensionality_msg_.response.action_max[0] << std::endl;
      std::cout << "MOCK CLIENT INFO: the actions min is " << env_dimensionality_msg_.response.action_min[0] << std::endl;
      std::cout << "MOCK CLIENT INFO: the state dimensions is " << env_dimensionality_msg_.response.state_dim_lowdim << std::endl;
    }
    else{
      std::cout << "Failed to call service dimensionality" << std::endl;
    }

    // Loop to test
    bool exit = false;
    float ACTION_STEP = 0.01;
    float action_base = 0;
    std::vector<float> actions;
    actions.push_back(0);
    actions.push_back(0);
    int counter = 0;

    while (!exit){
        // Sending action
        std::cout << "MOCK CLIENT INFO: sending action " << counter << std::endl;
        counter++;

        // Change actions
        actions[0] = action_base + ACTION_STEP;
        actions[1] = action_base + ACTION_STEP;
        action_base += ACTION_STEP;
//        if (action_base > (float) 1)
//            action_base = 0;

        if ((action_base >= (float) 1) || (action_base <= (float) 0))
            ACTION_STEP *= -1;

        // Wait for the action from the agent
        data_->check_action_.post();

        // Filling message
        rl_srvs::AgentSrv step_msg;
        step_msg.request.action = actions;
        std::cout << "MOCK CLIENT INFO: Calling step service.." << std::endl;
        if (env_step_client_.call(step_msg)){
          ROS_INFO("MOCK CLIENT INFO: Successful call step service");
        }
        else{
          ROS_ERROR("Failed to call step service");
        }

        // Sleep to separate tests
//        sleep(1);
    }

    return 1;

}

