/*
 *
 *
 *  Created on: May 26th 2017
 *      Author: Alejandro Rodríguez
 */

#include "rl_environment_position_based_visual_servoing.h"
#include "rl_environment_image_based_visual_servoing.h"
#include "rl_environment_image_based_visual_servoing_with_altitude.h"
#include "rl_environment_image_based_visual_servoing_test.h"
#include "rl_environment_visual_servoing.h"
#include "rl_environment_gazebo_ros.h"
#include "rl_environment_landing_constant.h"
#include "rl_environment_landing_with_z.h"
#include "rl_environment_landing_with_RPdYdA.h"
#include "rl_environment_landing_with_RPdYdAP.h"
#include "rl_environment_landing_with_RPdYdAPE.h"
#include "rl_environment_landing_with_RPdYdAPE_marker.h"
#include "rl_environment_swarm_obstacle.h"
#include "rl_environment_reactive_navigation.h"
#include "rl_environment_laser_based_navigation.h"
#include "rl_environment_laser_based_navigation_with_RPdY.h"
#include "rl_environment_laser_based_navigation_with_planner.h"
#include "rl_environment_laser_based_navigation_test.h"
#include "rl_environment_gimbal_uav_following.h"
#include "rl_environment_gimbal_uav_following_juako.h"
#include "rl_environment_gimbal_uav_following_adrian.h"
#include "ros/ros.h"

int main(int argc, char **argv)
{
    // Init ROS
    ros::init(argc, argv, "rl_environment_gazebo_ros");
    ros::NodeHandle n;


    std::string environment_name;
    ros::param::get("~environment_name", environment_name);
    if(environment_name.length() == 0)
    {
        environment_name = "RlEnvironmentSwarmObstacle";
    }
    std::cout<<"environment_name = "<<environment_name<<std::endl;


    // Polymorphism
    RlEnvironmentGazeboRos* rl_environment_gazebo_ros;
    if(environment_name == "RlEnvironmentVisualServoing")
        rl_environment_gazebo_ros = new RlEnvironmentVisualServoing;
    else if(environment_name == "RlEnvironmentLandingConstant")
        rl_environment_gazebo_ros = new RlEnvironmentLandingConstant;
    else if(environment_name == "RlEnvironmentImageBasedVisualServoing")
        rl_environment_gazebo_ros = new RlEnvironmentImageBasedVisualServoing;
    else if(environment_name == "RlEnvironmentImageBasedVisualServoingWithAltitude")
        rl_environment_gazebo_ros = new RlEnvironmentImageBasedVisualServoingWithAltitude;
    else if(environment_name == "RlEnvironmentImageBasedVisualServoingTest")
        rl_environment_gazebo_ros = new RlEnvironmentImageBasedVisualServoingTest;
    else if(environment_name == "RlEnvironmentPositionBasedVisualServoing")
        rl_environment_gazebo_ros = new RlEnvironmentPositionBasedVisualServoing;
    else if(environment_name == "RlEnvironmentSwarmObstacle")
        rl_environment_gazebo_ros = new RlEnvironmentSwarmObstacle;
    else if(environment_name == "RlEnvironmentLaserBasedNavigation")
        rl_environment_gazebo_ros = new RlEnvironmentLaserBasedNavigation;
    else if(environment_name == "RlEnvironmentLaserBasedNavigationTest")
        rl_environment_gazebo_ros = new RlEnvironmentLaserBasedNavigationTest;
    else if(environment_name == "RlEnvironmentLaserBasedNavigationWithRPdY")
        rl_environment_gazebo_ros = new RlEnvironmentLaserBasedNavigationWithRPdY;
    else if(environment_name == "RlEnvironmentLaserBasedNavigationWithPlanner")
        rl_environment_gazebo_ros = new RlEnvironmentLaserBasedNavigationWithPlanner;
    else if(environment_name == "RlEnvironmentReactiveNavigation")
        rl_environment_gazebo_ros = new RlEnvironmentReactiveNavigation;
    else if(environment_name == "RlEnvironmentLandingWithZ")
        rl_environment_gazebo_ros = new RlEnvironmentLandingWithZ;
    else if(environment_name == "RlEnvironmentLandingWithRPdYdA")
        rl_environment_gazebo_ros = new RlEnvironmentLandingWithRPdYdA;
    else if(environment_name == "RlEnvironmentLandingWithRPdYdAP")
        rl_environment_gazebo_ros = new RlEnvironmentLandingWithRPdYdAP;
    else if(environment_name == "RlEnvironmentLandingWithRPdYdAPE")
        rl_environment_gazebo_ros = new RlEnvironmentLandingWithRPdYdAPE;
    else if(environment_name == "RlEnvironmentUavGimbalFollowing")
        rl_environment_gazebo_ros = new RlEnvironmentUavGimbalFollowing;
    else if(environment_name == "RlEnvironmentUavGimbalFollowingJuako")
        rl_environment_gazebo_ros = new RlEnvironmentUavGimbalFollowingJuako;
    else if(environment_name == "RlEnvironmentUavGimbalFollowingAdrian")
        rl_environment_gazebo_ros = new RlEnvironmentUavGimbalFollowingAdrian;
#if HAS_OPENCV3 == 1
    else if(environment_name == "RlEnvironmentLandingWithRPdYdAPEMarker")
        rl_environment_gazebo_ros = new RlEnvironmentLandingWithRPdYdAPEMarker;
#endif



    // Init
    rl_environment_gazebo_ros->Init(n);

    std::cout << "RL_ENV_INFO: Sucessfuly initiated" << std::endl;

    // Ros spin
    ros::spin();

    return 1;

}

