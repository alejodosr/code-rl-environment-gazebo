/*
 *
 *
 *  Created on: May 26th 2017
 *      Author: Alejandro Rodríguez
 */

#include "rl_environment_landing_constant_test.h"

int main(int argc, char **argv)
{
    // Init ROS
    ros::init(argc, argv, "rl_environment_gazebo_ros");
    ros::NodeHandle n;

    // Polymorphism
//    RlEnvironmentGazeboRos* rl_environment_gazebo_ros = new RlEnvironmentVisualServoing;
    RlEnvironmentGazeboRos* rl_environment_gazebo_ros = new RlEnvironmentLandingConstantTest;


    // Init
    rl_environment_gazebo_ros->Init(n);

    std::cout << "RL_ENV_INFO: Test Sucessfuly initiated" << std::endl;

    // Ros spin
    ros::spin();

    return 1;

}

