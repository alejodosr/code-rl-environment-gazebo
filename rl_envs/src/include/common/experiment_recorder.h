
#ifndef _EXPERIMENT_RECORDER_H_
#define _EXPERIMENT_RECORDER_H_

#include "opencv2/opencv.hpp"
#include <ros/ros.h>
#include <iostream>
#include <string>
#include <rosbag/bag.h>
#include "rl_srvs/RecordExperimentSrv.h"
#include "gazebo_msgs/SpawnModel.h"
#include "gazebo_msgs/DeleteModel.h"
#include "sensor_msgs/Image.h"
#include <boost/filesystem.hpp>

class ExperimentRecorder
{
private:
    rosbag::Bag bag_;
//    std::vector<ros::Subscriber> subscribers_;
    std::vector<float> pose_;
    ros::NodeHandle node_handle_;
//    std::vector<std::string> subscribers_name_;
    bool recording_experiment_, record_with_camera_, switch_experiment_;
    std::string experiment_path_;
    int episode_num_;


    // Service Client
    ros::ServiceClient spawn_model_client_;
    ros::ServiceClient delete_model_client_;

    // Service Server
    ros::ServiceServer srv_server_record_;

    // Ros subscriber
    ros::Subscriber camera_sub_;

public:

    inline bool getRecording() const {return switch_experiment_; }
    inline void setRecording(bool recording) {switch_experiment_ = recording; }
    void CreateCamera();
    void DeleteCamera();
    void SetSubscribers(std::vector<std::string> subscribers_name);
    void ReleaseResources();
    void ShutdownSubscriber(int subscriber);
    void SetCameraPose(std::vector<float> pose);
    bool RecordExperimentSrv(rl_srvs::RecordExperimentSrv::Request &request, rl_srvs::RecordExperimentSrv::Response &response);
    void CameraCallback(sensor_msgs::ImageConstPtr msg);
    void Open(ros::NodeHandle n);
    void RecordExperiment();

    ExperimentRecorder();
    ~ExperimentRecorder();
};

#endif

