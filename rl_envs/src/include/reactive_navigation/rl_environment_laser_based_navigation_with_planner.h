
#ifndef _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_WITH_PLANNER_H_
#define _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_WITH_PLANNER_H_

#include "rl_environment_gazebo_ros.h"
#include "rl_environment_laser_based_navigation.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"

//STL
#include <ctime>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <fstream>

//ROS / Opencv
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "gazebo_msgs/SetLinkState.h"
#include "move_base_msgs/MoveBaseActionGoal.h"



class RlEnvironmentLaserBasedNavigationWithPlanner: public RlEnvironmentLaserBasedNavigation
{
private:

    // Ros publishers
    ros::Publisher goal_to_planner_pub_;

    ros::Subscriber drone_cmd_vel_subs_;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    void GenerateRandomPositionsHouse3(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void CmdVelCallback(const geometry_msgs::Twist& msg);
    void LaserScanCallback(const sensor_msgs::LaserScan& msg);
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);
    void SlamOutPoseCallback(const geometry_msgs::PoseStamped& msg);

    RlEnvironmentLaserBasedNavigationWithPlanner();
    virtual ~RlEnvironmentLaserBasedNavigationWithPlanner();
};

#endif

