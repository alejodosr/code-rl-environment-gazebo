
#ifndef _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_WITH_RPdY_H_
#define _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_WITH_RPdY_H_

#include "rl_environment_gazebo_ros.h"
#include "rl_environment_laser_based_navigation.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"

//STL
#include "ctime"
#include "stdlib.h"
#include <math.h>
#include <algorithm>
#include "stdio.h"
#include "fstream"

//ROS / Opencv
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "gazebo_msgs/SetLinkState.h"



class RlEnvironmentLaserBasedNavigationWithRPdY: public RlEnvironmentLaserBasedNavigation
{
private:

    // Ros publishers
    ros::Publisher roll_pitch_ref_pub_, dYaw_ref_pub_;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    void LaserScanCallback(const sensor_msgs::LaserScan& msg);
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);


    RlEnvironmentLaserBasedNavigationWithRPdY();
    virtual ~RlEnvironmentLaserBasedNavigationWithRPdY();
};

#endif

