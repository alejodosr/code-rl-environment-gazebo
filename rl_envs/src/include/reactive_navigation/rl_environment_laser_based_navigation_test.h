
#ifndef _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_TEST_H_
#define _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_TEST_H_

#include "rl_environment_gazebo_ros.h"
#include "rl_environment_laser_based_navigation.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"

//STL
#include "ctime"
#include "stdlib.h"
#include <math.h>
#include <algorithm>
#include "stdio.h"
#include "fstream"

//ROS / Opencv
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "gazebo_msgs/SetLinkState.h"
#include "std_msgs/Float32MultiArray.h"



class RlEnvironmentLaserBasedNavigationTest: public RlEnvironmentLaserBasedNavigation
{
private:

    // Ros publishers
    ros::Publisher rl_environment_state_pub_;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

//    void LaserScanCallback(const sensor_msgs::LaserScan& msg);
//    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
//    void DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);
//    void SlamOutPoseCallback(const geometry_msgs::PoseStamped& msg);


    RlEnvironmentLaserBasedNavigationTest();
    virtual ~RlEnvironmentLaserBasedNavigationTest();
};

#endif

