
#ifndef _RL_ENVIRONMENT_REACTIVE_NAVIGATION_H_
#define _RL_ENVIRONMENT_REACTIVE_NAVIGATION_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"

//STL
#include "ctime"
#include "stdlib.h"
#include <algorithm>
#include "stdio.h"
#include "fstream"

//ROS / Opencv
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include "nav_msgs/OccupancyGrid.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/Image.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_cv/grid_map_cv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>



class RlEnvironmentReactiveNavigation: public RlEnvironmentGazeboRos
{
private:

    const bool ENABLE_PAUSED_SIMULATION = false;

    std::vector<float> state_;
    std::vector<float> normalized_state_;

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_state_;

    struct {
        float pos_x_, pos_y_, pos_z_;
    }target_state_;

    struct {
        int actions_dim_;
        int state_dim_low_dim_;
        float max_pos_x_, max_pos_y_;
        float actions_max_value_, actions_min_value_;
        int num_iterations_;
        int num_episode_steps_;
        std::string name_;
    }environment_info_;

    struct {
        float width_, height_; //Size of the costmap in meters
        int image_width_, image_height_; //Size of the costmap in pixels (width*res, height*res)
        float res_;
    }local_costmap_info_;


    struct {
        cv::Size image_size_;
        int num_channels_;
        int state_dim_;
        std::vector<cv::Mat> state_;
        std::list<cv::Mat> list_state_;
    }image_based_state_;

    int area_uav_num_;
    int area_target_num_;
    const std::string UAV_NAME = "hummingbird1";
    const std::string TARGET_NAME = "goal";
    std::vector<std::vector<float> > hog_descriptors_;


    bool min_distance_from_laser_flag_;
    std::ofstream f_data_recorder;
    const float kUav_Altitude_ = 1.2;
    float pitch_roll_max_value_;
    boost::mutex uav_mutex_;
    boost::mutex target_mutex_;


    cv::HOGDescriptor hog_;
    cv::Size win_size_;
    cv::Size block_size_;
    cv::Size block_stride_;
    cv::Size cell_size_;
    int num_histogram_bins_;
    cv::Mat current_costmap_image_, prev_costmap_image_, local_costmap_double_res_;
    cv::Point2f d_obs_norm_;


    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_pose_ref_pub_;
    ros::Publisher hector_slam_reset_pub_;
    ros::Publisher rl_environment_state_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_subs_;
    ros::Subscriber visual_servoing_measurement_subs_;
    ros::Subscriber drone_estimated_pose_subs_;
    ros::Subscriber drone_local_costmap_subs_;
    ros::Subscriber drone_laser_scan_subs_;

    ros::Subscriber camera_image_subs_;
    ros::Subscriber camera_info_subs_;

    // Ros service clients
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;

    //ROS Messages
    droneMsgsROS::dronePose last_drone_estimated_GMRwrtGFF_pose_;
    ros::Time current_time_, previous_time_;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    void LaserScanCallback(const sensor_msgs::LaserScan& msg);
    void CostmapCallback(const nav_msgs::OccupancyGrid& msg);
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);
    std::vector<float> ComputeNormalizedState();
    void ComputeDistanceToObstacleInTrajectory();
    void GenerateRandomPositionsMaze(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsMaze3(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsMaze4(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse2(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse3(cv::Point2f &uav_pose, cv::Point2f &target_pose);


    void DrawResults();
    void arrowedLine(cv::Mat &img, cv::Point pt1, cv::Point pt2, const cv::Scalar& color,
               int thickness, int line_type, int shift, double tipLength);
    void ResetHectorSlam();


    // Getters and setters
    void SetUavState(const float x, const float y, const float z, const float dx, const float dy, const float dz);
    void SetTargetState(const float x, const float y, const float z);
    inline void GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &dz, float &roll, float &pitch, float &yaw);
    inline void GetTargetState(float &x, float &y, float &z);
    inline std::vector<float> GetNormalizedState() const {return normalized_state_;}


    RlEnvironmentReactiveNavigation();
    ~RlEnvironmentReactiveNavigation();
};

#endif

