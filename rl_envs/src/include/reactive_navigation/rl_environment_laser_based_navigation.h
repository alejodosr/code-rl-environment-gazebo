
#ifndef _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_H_
#define _RL_ENVIRONMENT_LASER_BASED_NAVIGATION_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"
#include "pugixml.hpp"

//STL
#include <ctime>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <fstream>

//ROS / Opencv
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "gazebo_msgs/SetLinkState.h"



class RlEnvironmentLaserBasedNavigation: public RlEnvironmentGazeboRos
{
protected:

    const bool ENABLE_PAUSED_SIMULATION = false;

    std::vector<float> state_;
    std::vector<float> normalized_state_;

    struct {
        float pos_x_, pos_y_, pos_z_;
        float prev_pos_x_, prev_pos_y_, prev_pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float prev_speed_x_, prev_speed_y_, prev_speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_state_;

    struct {
        float pos_x_, pos_y_, pos_z_;
    }target_state_;

    struct {
        int actions_dim_;
        int state_dim_low_dim_;
        float max_pos_x_, max_pos_y_;
        std::vector<float> actions_max_value_, actions_min_value_;
        int num_iterations_;
        int num_episode_steps_;
        std::string name_;
    }environment_info_;

    struct {
        float max_virtual_range_;
        float max_real_range_, min_real_range_;
        float min_range_reset_value_;
        int num_ranges_;
        int sampling_factor_;
        float angle_range_;
        float angle_sampling_factor_;
        float laser_state_normalization_factor_;
        std::vector<float> laser_ranges_;
        std::vector<float> min_laser_ranges_norm_;
    }laser_info_;


    struct {
        cv::Mat laser_scans_image_, obstacles_boundary_image_;
        cv::Size laser_scans_image_size_;
        float laser_scans_image_res_;
        float angle_ini_;
        float angle_ini_rad_;
        float angle_increment_;
        cv::Point p_origin_;
        std::vector<float> angles_ranges_;
        std::vector<float> cos_angles_ranges_;
        std::vector<float> sin_angles_ranges_;
    }laser_image_info_;

    std::vector<float> laser_ranges_state_, laser_ranges_state_norm_;
    std::vector<cv::Point3f> goals_sequence_;



    int area_uav_num_;
    int area_target_num_;
    int cont_ind_goal_in_sequence_;
    int num_goals_in_sequence_;
    std::string UAV_NAME;
    std::string TARGET_NAME;
    std::string configs_file_name_;


    cv::Point3f uav_initial_reset_position_;
    cv::Point3f slam_out_uav_position_;
    cv::Point3f uav_target_current_relative_position_;
    cv::Point3f uav_target_previous_relative_position_;
    float distance_to_target_;
    float min_distance_to_target_thresh_;
    float pitch_roll_max_value_;
    float virtual_ranges_offset_;
    const float kUav_Altitude_ = 1.2;
    std::ofstream f_data_recorder;
    boost::mutex uav_mutex_;
    boost::mutex target_mutex_;
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb_x_, filtered_derivative_wcb_y_;
    bool min_distance_from_laser_flag_;
    bool min_distance_target_reached_flag_;
    bool imshow_laser_state_scans_;
    bool imshow_laser_state_scans_and_goal_;
    bool imshow_obstacles_boundary_;
    bool imshow_all_saturated_ranges_;
    bool test_goal_sequence_mode_flag_;


    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_pose_ref_pub_;
    ros::Publisher hector_mapping_reset_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_subs_;
    ros::Subscriber drone_estimated_pose_subs_;
    ros::Subscriber drone_laser_scan_subs_;
    ros::Subscriber drone_slam_out_pose_subs_;


    // Ros service clients
    ros::ServiceClient gazebo_set_model_state_srv_;
    ros::ServiceClient gazebo_set_link_state_srv_;
    ros::ServiceClient estimator_client_;

    //ROS Messages
    droneMsgsROS::dronePose last_drone_estimated_GMRwrtGFF_pose_;
    ros::Time current_time_, previous_time_;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    bool ReadConfigs(std::string &configFile);
    float SaturateLaserRanges(float range);
    float NormalizeSaturatedLaserRanges(float range);
    float L2Norm(const std::vector<float>& vec);
    float ComputeGainForRepulsivePotentialField(const int gain_type, const float max_beta);
    float ComputeRepulsivePotentialField(const std::vector<float>& vec);
    float ComputeRepulsivePotentialFieldBasedOnImage();
    void UpdateUavTargetPreviousRelativePosition();
    std::vector<float> ComputeNormalizedStateBasedOnGroupOfLaserRanges();
    std::vector<float> ComputeNormalizedState();
    std::vector<int> ComputeMinLaserRangesToObstacles();
    void SlamOutPoseCallback(const geometry_msgs::PoseStamped& msg);
    void LaserScanCallback(const sensor_msgs::LaserScan& msg);
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);
    void GenerateRandomPositionsMaze(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsMaze3(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsMaze4(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse2(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse3(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse4(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse5(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    void GenerateRandomPositionsHouse6(cv::Point2f &uav_pose, cv::Point2f &target_pose);
    std::vector<cv::Point2f> GenerateRandomPositionForObstacles(const int num_obstacles);


    void DrawResults();
    void DrawArrowedLine(cv::Mat& img, cv::Point pt1, cv::Point pt2, const cv::Scalar& color,
               int thickness, int line_type, int shift, double tipLength);


    // Getters and setters
    void SetUavState(const float x, const float y, const float z, const float dx, const float dy, const float dz);
    void SetUavPreviousState(const float x, const float y, const float z, const float dx, const float dy, const float dz);
    void SetTargetState(const float x, const float y, const float z);
    void GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &dz, float &roll, float &pitch, float &yaw);
    void GetTargetState(float &x, float &y, float &z);
    inline float GetDistanceToTarget() const {return distance_to_target_;}
    inline std::vector<float> GetMinLaserRangesNorm() const {return laser_info_.min_laser_ranges_norm_;}
    inline std::vector<float> GetNormalizedState() const {return normalized_state_;}


    RlEnvironmentLaserBasedNavigation();
    virtual ~RlEnvironmentLaserBasedNavigation();
};

#endif

