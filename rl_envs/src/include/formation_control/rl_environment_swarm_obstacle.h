
#ifndef _RL_ENVIRONMENT_SWARM_OBSTACLE_H_
#define _RL_ENVIRONMENT_SWARM_OBSTACLE_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "gazebo_msgs/ContactsState.h"
#include "std_srvs/Empty.h"
#include <nav_msgs/Odometry.h>
#include "ctime"
#include "stdlib.h"
#include "experiment_recorder.h"

class RlEnvironmentSwarmObstacle: public RlEnvironmentGazeboRos
{
private:

    // Test configuration parameters
    const bool ENABLE_PAUSED_SIMULATION = false;

    std::vector<float> state_;

    enum {
            ACTIONS_DIM = 2,
            STATE_DIM_LOW_DIM = 8,
            NUM_ITERATIONS = 5,
            NUM_EPISODE_ITERATIONS = 900,
            MAX_POSE_X = 3,
            MAX_POSE_Y = 6
         };

    const std::string UAV_NAME = "hummingbird1";
    const std::string OBSTACLE_NAME = "obstacle_cylinder";
    const std::string VIRTUAL_LEADER_NAME = "virtual_leader";
    const float Z_STEP = 0.004;
    const float Z_INITIAL = 2.0;
    const float PLATFORM_SIZE = 0.3;
    const float MIN_DISTANCE_TO_OBSTACLE = 0.4;

    float current_z_;

    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_altitude_ref_pub_;
    ros::Publisher obstacle_ref_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_sub_;

    // Ros service clients
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;

    // Other members
    float uav_pose_x_, uav_pose_y_, uav_pose_z_, uav_velocity_x_, uav_velocity_y_;
    boost::mutex uav_mutex_;

    float obstacle_pose_x_, obstacle_pose_y_, obstacle_pose_z_, obstacle_velocity_x_, obstacle_velocity_y_;
    boost::mutex obstacle_mutex_;

    float virtual_leader_pose_x_, virtual_leader_pose_y_, virtual_leader_pose_z_;

    // Experiment recorder
//    ExperimentRecorder exp_rec_;

public:
    // Polymorphic member functions
    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    bool Reset();
    void InitChild(ros::NodeHandle n);

    // Member functions
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);    

    // Getters and setters
    void SetUavState(float x, float y, float z, float dx, float dy){
        uav_mutex_.lock();
        uav_pose_x_ = x;
        uav_pose_y_ = y;
        uav_pose_z_ = z;
        uav_velocity_x_ = dx;
        uav_velocity_y_ = dy;
        uav_mutex_.unlock();
    }

    void GetUavState(float &x, float &y, float &z, float &dx, float &dy){
        uav_mutex_.lock();
        x = uav_pose_x_;
        y = uav_pose_y_;
        z = uav_pose_z_;
        dx = uav_velocity_x_;
        dy = uav_velocity_y_;
        uav_mutex_.unlock();
    }

    void SetObstacleState(float x, float y, float z, float dx, float dy){
        obstacle_mutex_.lock();
        obstacle_pose_x_ = x;
        obstacle_pose_y_ = y;
        obstacle_pose_z_ = z;
        obstacle_velocity_x_ = dx;
        obstacle_velocity_y_ = dy;
        obstacle_mutex_.unlock();
    }

    void GetObstacleState(float &x, float &y, float &z, float &dx, float &dy){
        obstacle_mutex_.lock();
        x = obstacle_pose_x_;
        y = obstacle_pose_y_;
        z = obstacle_pose_z_;
        dx = obstacle_velocity_x_;
        dy = obstacle_velocity_y_;
        obstacle_mutex_.unlock();
    }

    RlEnvironmentSwarmObstacle(){}
    ~RlEnvironmentSwarmObstacle(){}
};

#endif

