
#ifndef _RL_ENVIRONMENT_LANDING_WITH_RPDYDAPE_H_
#define _RL_ENVIRONMENT_LANDING_WITH_RPDYDAPE_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/droneStatus.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "gazebo_msgs/ContactsState.h"
#include "std_srvs/Empty.h"
#include "ctime"
#include "stdlib.h"
#include "experiment_recorder.h"
#include <tf/transform_datatypes.h>

class RlEnvironmentLandingWithRPdYdAPE: public RlEnvironmentGazeboRos
{
private:

    // Test configuration parameters
    const bool ENABLE_PAUSED_SIMULATION = false;

    std::vector<float> state_;

    enum {
            ACTIONS_DIM = 5,
            STATE_DIM_LOW_DIM = 7,
            NUM_ITERATIONS = 5,
            NUM_EPISODE_ITERATIONS = 1100,
            MAX_POSE_X = 3,
            MAX_POSE_Y = 6
         };

    const std::string UAV_NAME = "hummingbird1";
    const std::string PLATFORM_NAME = "vision_landing_platform";
    const float Z_STEP = 0.01;
    const float Z_INITIAL = 2.0;
    const float Z_PLATFORM_OFFSET = 0.15;
    const float PLATFORM_SIZE = 0.3;
    const float MAX_ALLOWED_ALTIUDE = 3.0;
    const float SCALE_RP = 0.3;
    const float SCALE_YAW = 1.0;
    const float SCALE_ALTITUDE = 0.5;
    const float EMERGENCY_THRESHOLD = 0.8;
    const float MAX_EMERGENCY_ALTITUDE = 1.0;
    const float DESIRED_EMERGENCY_ALTITUDE = 0.4;
    const bool  ENABLE_GUIDED_STATES = true;
    const bool  ENABLE_EMERGENCY_INHIBITION = true;
    const int   GUIDED_STATES_INTERVAL = 20;

    float current_z_;
    float current_yaw_;
    float prev_x_relative_, prev_y_relative_;
    bool landing_executed_, reset_executed_;
    float emergency_altitude_;
    int status_, episode_no_;
    boost::mutex status_mutex_;

    // Ros publishers
    ros::Publisher roll_pitch_ref_pub_, dYaw_ref_pub_, dAltitude_ref_pub_;
    ros::Publisher uav_altitude_ref_pub_, drone_command_publ_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_sub_;
    ros::Subscriber bumper_states_sub_;
    ros::Subscriber pose_sub_;
    ros::Subscriber status_sub_;

    // Ros service clients
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;

    // Other members
    float uav_pose_x_, uav_pose_y_, uav_pose_z_, uav_velocity_x_, uav_velocity_y_;
    boost::mutex uav_mutex_;

    float platform_pose_x_, platform_pose_y_, platform_pose_z_, platform_velocity_x_, platform_velocity_y_;
    boost::mutex platform_mutex_;

    int bumper_state_;
    boost::mutex bumper_states_mutex_;

    // Experiment recorder
//    ExperimentRecorder exp_rec_;

public:
    // Polymorphic member functions
    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    bool Reset();
    void InitChild(ros::NodeHandle n);

    // Member functions
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);    
    void BumperStatesCallback(const gazebo_msgs::ContactsState::ConstPtr& msg);
    void PoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg);
    void StatusCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);


    // Getters and setters
    void SetUavState(float x, float y, float z, float dx, float dy){
        uav_mutex_.lock();
        uav_pose_x_ = x;
        uav_pose_y_ = y;
        uav_pose_z_ = z;
        uav_velocity_x_ = dx;
        uav_velocity_y_ = dy;
        uav_mutex_.unlock();
    }

    void GetUavState(float &x, float &y, float &z, float &dx, float &dy){
        uav_mutex_.lock();
        x = uav_pose_x_;
        y = uav_pose_y_;
        z = uav_pose_z_;
        dx = uav_velocity_x_;
        dy = uav_velocity_y_;
        uav_mutex_.unlock();
    }

    void SetPlatformState(float x, float y, float z, float dx, float dy){
        platform_mutex_.lock();
        platform_pose_x_ = x;
        platform_pose_y_ = y;
        platform_pose_z_ = z;
        platform_velocity_x_ = dx;
        platform_velocity_y_ = dy;
        platform_mutex_.unlock();
    }

    void GetPlatformState(float &x, float &y, float &z, float &dx, float &dy){
        platform_mutex_.lock();
        x = platform_pose_x_;
        y = platform_pose_y_;
        z = platform_pose_z_;
        dx = platform_velocity_x_;
        dy = platform_velocity_y_;
        platform_mutex_.unlock();
    }

    void SetBumperState(int state){
        bumper_states_mutex_.lock();
        bumper_state_ = state;
        bumper_states_mutex_.unlock();
    }

    void GetBumperState(int &state){
        bumper_states_mutex_.lock();
        state = bumper_state_;
        bumper_states_mutex_.unlock();
    }

    void SetStatus(int status){
        status_mutex_.lock();
        status_ =  status;
        status_mutex_.unlock();
    }

    void GetStatus(int &status){
        status_mutex_.lock();
        status = status_;
        status_mutex_.unlock();
    }

    RlEnvironmentLandingWithRPdYdAPE(){}
    ~RlEnvironmentLandingWithRPdYdAPE(){}
};

#endif

