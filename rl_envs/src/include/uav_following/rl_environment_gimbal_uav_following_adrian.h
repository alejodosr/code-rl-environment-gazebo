
#ifndef _RL_ENVIRONMENT_GIMBAL_UAV_FOLLOWING_ADRIAN_H_
#define _RL_ENVIRONMENT_GIMBAL_UAV_FOLLOWING_ADRIAN_H_


#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"
#include "pugixml.hpp"
#include "std_msgs/Float32MultiArray.h"
#include <ctime>


//STL
#include "stdlib.h"
#include <algorithm>
#include "stdio.h"
#include "fstream"
#include <math.h>


//ROS / Opencv
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "sensor_msgs/CameraInfo.h"
#include "sensor_msgs/Imu.h"
#include "nav_msgs/Odometry.h"
#include "std_srvs/Empty.h"
#include "opencv_apps/RotatedRectStamped.h"
#include <cv_bridge/cv_bridge.h>
#include "tf_conversions/tf_eigen.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/Float32MultiArray.h"
#include "sensor_msgs/JointState.h"
#include "droneMsgsROS/droneDYawCmd.h"




// Services

#include "rl_srvs/AgentActionSrv.h"
#include "rl_srvs/AgentStateSrv.h"

//#define REAL_FLIGHT_OWN_DETECTOR
// Real flight now working with parrot_arsdk 3.14.1 and bebop_autonomy forked https://github.com/itssme/bebop_autonomy (current way of getting the gimbal angles)
#define REAL_FLIGHT // Implemented for TRAINING_TWO_TWO and TRAINING_THREE

#define OBJECT_BASED_GROUND_TRUTH_GIMBAL
//#define RED_DETECTOR

//#define STABILIZE_YAW
//#define SHOW_IMAGES
#define ENABLE_ACTION_TRUNCATION
#define ENABLE_ACTION_SCALING
//#define EXTERNAL_YAW_CONTROLLER
//#define ACTION_SMOOTHING  // Currently not working in real flights

//#define IMAGE_STATE

#define LOG_DATA


// Training schedule
//#define TRAINING_A
//#define TRAINING_B
//#define SIGMOID
//#define TRAINING_B_diff
//#define PITCH_ROLL_SHAPING
//#define TRAINING_C
//#define TRAINING_C_dA

//#define TRAINING_D
//#define TRAINING_D_GIMBAL
//#define FAST
//#define NO_SIGMOID

#define TRAINING_E


#ifdef EXTERNAL_YAW_CONTROLLER
#include "control_toolbox/pid.h"
#endif

class RlEnvironmentUavGimbalFollowingAdrian: public RlEnvironmentGazeboRos
{
private:
    // Enable paused simulation
    const bool ENABLE_PAUSED_SIMULATION = false;

    const float MAX_PITCH = 0.20;
    const float MAX_ROLL = 0.10;

    // Gazebo models names
    const std::string UAV_LEADER_NAME = "iris";
    const std::string UAV_NAME = "hummingbird1";
    const std::string MOVING_TARGET_NAME = "iris";

    const float TARGET_ROI_WIDTH = 65;
    const float TARGET_ROI_HEIGHT = 35;

    // Resolution of fish-eye camera
    const float RESOLUTION_FISH_EYE_X = 1650;
    const float RESOLUTION_FISH_EYE_Y = 1650;

    // Field of view of fish-eye camera
    const float HFOV_ = 1.22;
    const float VFOV_ = 1.76;

    // Resolution of virtual gimbal camera
    const float RESOLUTION_VIRTUAL_CAMERA_X = 856;
    const float RESOLUTION_VIRTUAL_CAMERA_Y = 480;

//    // Gimbal angles
//    const float MAX_GIMBAL_ANGLE_X = 40;
//    const float MAX_GIMBAL_ANGLE_Y = 40;

    // Preview gimbal roi
    const float SCALE_OF_PREVIEW = 1.87 * 5;

    //Scale for image state
    const float IMAGE_SCALE = 4;

    // Constant velocity of the virtual gimbal camera
    const std::vector<float> SPEED_MOVEMENT_GIMBAL_ = {0.22, 0.22};
//    const std::vector<float> SPEED_MOVEMENT_GIMBAL_ = {0.11, 0.11};

    // Min and max rl state values
    const float MIN_STATE_ = -1.0;
    const float MAX_STATE_ = 1.0;
    const float MIN_TARGET_X = 0.3;
    const float MAX_TARGET_X = 1.0;
    const float NOMINAL_TARGET_Z = 1.07;
    float pitch_ant_, roll_ant_;


    // Max altitude for reset
    const float MAX_ALTITUDE_FOR_RESET =2.5;

    // int counter

    int counter_no_detection_ = 0;
    bool bool_no_detection_ = false;
    const float OFFSET_GIMBAL_REAL_WRT_SIM = 32.5;

    std::ofstream outFile_;


#ifdef EXTERNAL_YAW_CONTROLLER
    const double TARGET_YAW = 0; // In radians
#endif

#ifdef ACTION_SMOOTHING
    float pitch_ant_, roll_ant_;
#endif

    std::vector<float> state_;
    std::vector<float> normalized_state_;

    cv::Point3f target_pose;

    struct {
        int actions_dim_;
        int state_dim_low_dim_;
        int num_iterations_;
        int num_episode_steps_;
        float max_pos_x_, max_pos_y_;
        std::vector<float> actions_max_value_, actions_min_value_;
    }environment_info_;

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
        float imu_roll_, imu_pitch_, imu_yaw_;
    }uav_state_;

    struct {
        float roll_, pitch_, yaw_;
    }gimbal_state_;    

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_leader_state_;

    struct {
        bool camera_info_available_flag_;
        float fx_, fy_, cx_, cy_;
        float image_width_, image_height_;
        cv::Mat K_matrix_;
        cv::Mat R_matrix_;
        cv::Point3f uav_camera_offset_;
    }camera_info_;

    struct {
        std::vector<cv::Point> detected_roi_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_rpy_compensated_;
        cv::Rect ground_truth_detected_roi_rect_;
        cv::Rect gimbal_ground_truth_detected_roi_rect_;
        cv::Rect gimbal_ground_truth_previous_detected_roi_rect_;        
        cv::Rect gimbal_red_detector_roi_rect_;
        cv::Rect gimbal_red_detector_previous_roi_rect_;
        cv::Rect gimbal_ground_truth_detected_roi_rect_scaled_;
        cv::Rect stabilized_detected_roi_rect_;
        cv::Point2f ground_truth_current_object_points_[4];
        float detected_roi_diameter_;
        float ground_truth_detected_roi_diameter_;
        float stabilized_detected_roi_diameter_;
        float diameter_ref_;
        float diameter_ref_min_height_;
    }object_in_image_info_fish_eye_;

    struct {
        float width_, height_;
        cv::Point3f points_[4];
        cv::Mat points_mat_[4];
        cv::Mat points_min_height_mat_[4];
        cv::Mat current_points_mat_[8];
    }object_in_world_info_;

    std::string configs_file_name_;
    std::ofstream f_data_recorder;
    float kUav_Altitude_;
    float kUav_max_Altitude_;
    float kUav_min_Altitude_;
    float actions_max_value_, actions_min_value_;
    float pitch_roll_max_value_;
    int roi_position_x, roi_position_y;
    cv::Mat captured_image_;
    boost::mutex uav_mutex_;

    // Initial points
    std::vector<cv::Mat> initial_points_;

    cv::Mat K_matrix_fish_eye_;

    // Infos
    std::vector<std_msgs::String> keys_;
    std::vector<float> values_, values_max_, values_min_;

    // Reward members
    float prev_shaping_;

    // Picture to show
    cv::Mat cropped_picture_;

    // Target roi
    cv::Rect target_roi_, target_roi_normalized_;

    // Virtual gimbal roi
    cv::Rect virtual_gimbal_roi_;

    int gimbal_position_pixels_x = round(RESOLUTION_FISH_EYE_X/2);
    int gimbal_position_pixels_y = round(RESOLUTION_FISH_EYE_Y/2);

    float current_z_;
    std::vector<float> actions_;

    std::vector<float> angular_position_actual_gimbal_;

    std::vector<float> angular_position_desired_gimbal_;
    std::vector<float> angular_position_desired_gimbal_MAX_;
    std::vector<float> angular_position_desired_gimbal_MIN_;

#ifdef EXTERNAL_YAW_CONTROLLER
    control_toolbox::Pid pid_yaw_;
    ros::Time last_time_;
#endif

    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_pose_ref_pub_;
    ros::Publisher daltitude_ref_pub_;
    ros::Publisher rl_environment_state_pub_;
    ros::Publisher rl_environment_image_state_pub_;
    ros::Publisher uav_altitude_ref_pub_;
    ros::Publisher dYaw_ref_pub_;
    ros::Publisher roll_pitch_ref_pub_;
    ros::Publisher camera_control_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_subs_;
    ros::Subscriber visual_servoing_measurement_subs_;
    ros::Subscriber camera_info_subs_;
    ros::Subscriber camera_image_subs_;
    ros::Subscriber drone_estimated_pose_subs_;
    ros::Subscriber gimbal_state_subs_;
    ros::Subscriber real_camera_image_subs_;
    ros::Subscriber std_bbs_subs_;
    ros::Subscriber real_imu_subs_;

    // Ros service clients
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;

    //ROS Messages
    ros::Time previous_time_gimbal_;

    //ROS Service reservers
    ros::ServiceServer rl_env_step_state_srv;
    ros::ServiceServer rl_env_step_action_srv;

public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    float sigmoid(const float x);
    bool StepAction(rl_srvs::AgentActionSrv::Request &request, rl_srvs::AgentActionSrv::Response &response);
    bool StepState(rl_srvs::AgentStateSrv::Request &request, rl_srvs::AgentStateSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    bool ReadConfigs(std::string &configFile);
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void RealImuCallback(const nav_msgs::Odometry::ConstPtr& msg);

    void CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& msg);
    void CameraFishEyeCallback(const sensor_msgs::ImageConstPtr& msg);
    cv::Mat ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma);
    cv::Mat ComputeHomogeneousMatrixTransform(float px, float py, float pz,
                                              float yaw, float pitch, float roll);
    cv::Mat ComputeHomogeneousMatrixTransform(const float px, const float py, const float pz, const cv::Mat R);
    cv::Mat StabilizeVirtualCamera(cv::Mat stabilizied_captured_image_);
    int AngularRotationToPixelDisplacement(const float HFOV, float angularDisplacement, const float resolutionAxis);
    std::vector<float> MoveGimbalPosition(std::vector<float>  goalPosition);
    cv::Mat GetPictureFromVirtualGimbal(cv::Mat fish_eye_picture_, std::vector<float> gimbal_position_);
    void MoveGimbalAgent(std::vector<float> move_gimbal_);
    cv::Mat GetPictureGimbalAgent(cv::Mat fish_eye_picture_);
    void ComputeGroundTruthObjectPositionInImageFishEye(const float &x_uav, const float &y_uav, const float &z_uav);

    // Getters and setters
    void SetUavState(const float x, const float y, const float z, const float dx, const float dy);
    inline void GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float &pitch);
    void GetTargetROI(cv::Rect &previous_roi, cv::Rect &current_roi);
    void GetNormalizedState(std::vector<float> &normalized_state);
    void PrintInfoInImage(std::vector<std_msgs::String> keys, std::vector<float> values);
    void PrintAxesInImage(cv::Mat &src, std::vector<float> &axes);
    void DetectObjectInImage(const cv::Mat &I, const std::string object_color);
//    void CameraBebopREALCallbackNoGimbal(const sensor_msgs::ImageConstPtr& msg);
//    void GimbalStateREALCallback(sensor_msgs::JointState::ConstPtr msg);
//    void MoveGimbalAgentREAL(std::vector<float> move_gimbal);



    /******************** REAL-FLIGHT FUNCTIONS AND CALLBACKS **********************/
#if defined(REAL_FLIGHT_OWN_DETECTOR) || defined(REAL_FLIGHT)
    void MoveGimbalAgentREAL(std::vector<float> move_gimbal);
    void CameraBebopREALCallback(const sensor_msgs::ImageConstPtr& msg);
    void StdBbsREALCallback(const std_msgs::Float32MultiArrayConstPtr& msg);
    void CameraBebopREALCallbackNoGimbal(const sensor_msgs::ImageConstPtr& msg);
    void GimbalStateREALCallback(sensor_msgs::JointState::ConstPtr msg);
#endif
    /******************************************************************************/

    RlEnvironmentUavGimbalFollowingAdrian();
    ~RlEnvironmentUavGimbalFollowingAdrian();
};

#endif
