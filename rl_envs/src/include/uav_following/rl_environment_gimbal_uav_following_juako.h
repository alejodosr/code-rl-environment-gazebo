
#ifndef _RL_ENVIRONMENT_GIMBAL_UAV_FOLLOWING_JUAKO_H_
#define _RL_ENVIRONMENT_GIMBAL_UAV_FOLLOWING_JUAKO_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"
#include "pugixml.hpp"
#include "std_msgs/Float32MultiArray.h"
#include <ctime>


//STL
#include "ctime"
#include "stdlib.h"
#include <algorithm>
#include "stdio.h"
#include "fstream"
#include <math.h>


//ROS / Opencv
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "sensor_msgs/CameraInfo.h"
#include "std_srvs/Empty.h"
#include "opencv_apps/RotatedRectStamped.h"
#include <cv_bridge/cv_bridge.h>
#include "tf_conversions/tf_eigen.h"
#include "nav_msgs/Odometry.h"



// Services

#include "rl_srvs/AgentActionSrv.h"
#include "rl_srvs/AgentStateSrv.h"

class RlEnvironmentUavGimbalFollowingJuako: public RlEnvironmentGazeboRos
{
private:
    // Enable paused simulation
    const bool ENABLE_PAUSED_SIMULATION = false;

    // Gazebo models names
    const std::string UAV_LEADER_NAME = "iris";
    const std::string UAV_NAME = "hummingbird1";
    const std::string MOVING_TARGET_NAME = "iris_ball";

    // Resolution of fish-eye camera
    const float RESOLUTION_FISH_EYE_X = 1200;
    const float RESOLUTION_FISH_EYE_Y = 1200;

    // Field of view of fish-eye camera
    const float HFOV_ = 1.22;
    const float VFOV_ = 1.57;

    // Resolution of virtual gimbal camera
    const float RESOLUTION_VIRTUAL_CAMERA_X = 640;
    const float RESOLUTION_VIRTUAL_CAMERA_Y = 368;

    // Constant velocity of the virtual gimbal camera
    const std::vector<float> SPEED_MOVEMENT_GIMBAL_ = {0.22, 0.22};

    // Min and max rl state values
    const float MIN_STATE_ = -1.0;
    const float MAX_STATE_ = 1.0;

    std::vector<float> state_;
    std::vector<float> normalized_state_;

    struct {
        int actions_dim_;
        int state_dim_low_dim_;
        int num_iterations_;
        int num_episode_steps_;
        float max_pos_x_, max_pos_y_;
        std::vector<float> actions_max_value_, actions_min_value_;
    }environment_info_;

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_state_;

    struct {
        float roll_, pitch_, yaw_;
    }gimbal_state_;    

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_leader_state_;

    struct {
        bool camera_info_available_flag_;
        float fx_, fy_, cx_, cy_;
        float image_width_, image_height_;
        cv::Mat K_matrix_;
        cv::Mat R_matrix_;
        cv::Point3f uav_camera_offset_;
    }camera_info_;

    struct {
        std::vector<cv::Point> detected_roi_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_rpy_compensated_;
        cv::Rect ground_truth_detected_roi_rect_;
        cv::Rect gimbal_ground_truth_detected_roi_rect_;
        cv::Rect gimbal_ground_truth_previous_detected_roi_rect_;
        cv::Rect stabilized_detected_roi_rect_;
        cv::Point2f ground_truth_current_object_points_[4];
        float detected_roi_diameter_;
        float ground_truth_detected_roi_diameter_;
        float stabilized_detected_roi_diameter_;
        float diameter_ref_;
        float diameter_ref_min_height_;
    }object_in_image_info_fish_eye_;

    struct {
        float width_, height_;
        cv::Point3f points_[4];
        cv::Mat points_mat_[4];
        cv::Mat points_min_height_mat_[4];
        cv::Mat current_points_mat_[8];
    }object_in_world_info_;

    std::string configs_file_name_;
    std::ofstream f_data_recorder;
    float kUav_Altitude_;
    float kUav_max_Altitude_;
    float kUav_min_Altitude_;
    float actions_max_value_, actions_min_value_;
    float pitch_roll_max_value_;
    int roi_position_x, roi_position_y;
    cv::Mat captured_image_;
    boost::mutex uav_mutex_;

    // Initial points
    std::vector<cv::Mat> initial_points_;

    cv::Mat K_matrix_fish_eye_;

    int gimbal_position_pixels_x = round(RESOLUTION_FISH_EYE_X/2);
    int gimbal_position_pixels_y = round(RESOLUTION_FISH_EYE_Y/2);

    std::vector<int> v;
    std::vector<float> angular_position_gimbal_;

    std::vector<float> angular_position_gimbal_controller_;
    std::vector<float> angular_position_gimbal_controller_MAX_;
    std::vector<float> angular_position_gimbal_controller_MIN_;

    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_pose_ref_pub_;
    ros::Publisher daltitude_ref_pub_;
    ros::Publisher rl_environment_state_pub_;
    ros::Publisher rl_environment_image_state_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_subs_;
    ros::Subscriber visual_servoing_measurement_subs_;
    ros::Subscriber camera_info_subs_;
    ros::Subscriber camera_image_subs_;
    ros::Subscriber drone_estimated_pose_subs_;

    // Ros service clients
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;

    //ROS Messages
    ros::Time current_time_, previous_time_;
    clock_t previous_time_gimbal_;

    //ROS Service reservers
    ros::ServiceServer rl_env_step_state_srv;
    ros::ServiceServer rl_env_step_action_srv;

public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool StepAction(rl_srvs::AgentActionSrv::Request &request, rl_srvs::AgentActionSrv::Response &response);
    bool StepState(rl_srvs::AgentStateSrv::Request &request, rl_srvs::AgentStateSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    bool ReadConfigs(std::string &configFile);
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& msg);
    void CameraFishEyeCallback(const sensor_msgs::ImageConstPtr& msg);
    cv::Mat ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma);
    cv::Mat ComputeHomogeneousMatrixTransform(float px, float py, float pz,
                                              float yaw, float pitch, float roll);
    cv::Mat ComputeHomogeneousMatrixTransform(const float px, const float py, const float pz, const cv::Mat R);
    cv::Mat StabilizeVirtualCamera(cv::Mat stabilizied_captured_image_);
    int AngularRotationToPixelDisplacement(const float HFOV, float angularDisplacement, const float resolutionAxis);
    std::vector<float> MoveGimbalPosition(std::vector<float>  goalPosition);
    cv::Mat GetPictureFromVirtualGimbal(cv::Mat fish_eye_picture_, std::vector<float> gimbal_position_);
    void MoveGimbalAgent(std::vector<float> move_gimbal_);
    cv::Mat GetPictureGimbalAgent(cv::Mat fish_eye_picture_);
    void ComputeGroundTruthObjectPositionInImageFishEye(const float &x_uav, const float &y_uav, const float &z_uav);

    // Getters and setters
    void SetUavState(const float x, const float y, const float z, const float dx, const float dy);
    inline void GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float &pitch);
    void GetTargetROI(cv::Rect &previous_roi, cv::Rect &current_roi);
    void GetNormalizedState(std::vector<float> &normalized_state);

    RlEnvironmentUavGimbalFollowingJuako();
    ~RlEnvironmentUavGimbalFollowingJuako();
};

#endif
