
#ifndef _RL_ENVIRONMENT_IMAGE_BASED_VISUAL_SERVOING_WITH_ALTITUDE_H_
#define _RL_ENVIRONMENT_IMAGE_BASED_VISUAL_SERVOING_WITH_ALTITUDE_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"
#include "pugixml.hpp"
#include "std_msgs/Float32MultiArray.h"


//STL
#include "ctime"
#include "stdlib.h"
#include <algorithm>
#include "stdio.h"
#include "fstream"

//ROS / Opencv
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "sensor_msgs/CameraInfo.h"
#include "std_srvs/Empty.h"
#include "opencv_apps/RotatedRectStamped.h"
#include <cv_bridge/cv_bridge.h>
#include "tf_conversions/tf_eigen.h"
#include "nav_msgs/Odometry.h"


class RlEnvironmentImageBasedVisualServoingWithAltitude: public RlEnvironmentGazeboRos
{
private:

    const bool ENABLE_PAUSED_SIMULATION = false;

    std::vector<float> state_;
    std::vector<float> normalized_state_;

    struct {
        int actions_dim_;
        int state_dim_low_dim_;
        int num_iterations_;
        int num_episode_steps_;
        float max_pos_x_, max_pos_y_;
        std::vector<float> actions_max_value_, actions_min_value_;
    }environment_info_;

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_state_;

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_leader_state_;

    struct {
        bool camera_info_available_flag_;
        float fx_, fy_, cx_, cy_;
        float image_width_, image_height_;
        cv::Mat K_matrix_;
        cv::Mat R_matrix_;
        cv::Point3f uav_camera_offset_;
    }camera_info_;

    struct {
        std::vector<cv::Point> detected_roi_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_rpy_compensated_;
        cv::RotatedRect detected_roi_;
        cv::RotatedRect previous_detected_roi_;
        cv::RotatedRect ground_truth_detected_roi_;
        cv::RotatedRect ground_truth_previous_detected_roi_;
        cv::RotatedRect stabilized_detected_roi_;
        cv::RotatedRect stabilized_previous_detected_roi_;
        cv::Rect detected_roi_rect_;
        cv::Rect ground_truth_detected_roi_rect_;
        cv::Rect stabilized_detected_roi_rect_;
        cv::Point2f ground_truth_current_object_points_[4];
        float detected_roi_diameter_;
        float ground_truth_detected_roi_diameter_;
        float stabilized_detected_roi_diameter_;
        float diameter_ref_;
        float diameter_ref_min_height_;
    }object_in_image_info_;

    struct {
        float width_, height_;
        cv::Point3f points_[4];
        cv::Mat points_mat_[4];
        cv::Mat points_min_height_mat_[4];
        cv::Mat current_points_mat_[4];
    }object_in_world_info_;

    std::string configs_file_name_;
    std::ofstream f_data_recorder;
    float kUav_Altitude_;
    float kUav_max_Altitude_;
    float kUav_min_Altitude_;
    float camera_altitude_, camera_max_altitude_, camera_min_altitude_;
    float actions_max_value_, actions_min_value_;
    float pitch_roll_max_value_;
    float radius_ref_, radius_max_height_, radius_min_height_;
    float max_diameter_error_;
    float current_error_in_diameter_, previous_error_in_diameter_;
    float min_xy_distance_to_target_thresh_;
    float min_z_distance_to_target_thresh_;
    int image_boundaries_offset_;
    bool min_distance_target_reached_flag_;
    cv::Point2f offset_in_image_;
    cv::Point2f center_roi_ref_max_;
    cv::Point2f speed_roi_ref_max_;
    cv::Point2f error_in_image_;
    cv::Point2f error_in_image_norm_;
    cv::Point center_roi_ref_;
    cv::Rect roi_reference_;
    cv::Rect roi_reference_min_height_;
    cv::Rect roi_reference_from_ground_truth_;
    cv::Mat captured_image_;
    boost::mutex uav_mutex_;
    boost::mutex object_in_image_mutex_;
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb_x_, filtered_derivative_wcb_y_;



    //Define matrices for transformations
    cv::Mat rot_mat_z_90_, rot_mat_x180_, rot_mat_z90_;
    cv::Mat t_cam_world_;
    cv::Mat T_world_uav_ , T_uav_cam_;

    const std::string UAV_LEADER_NAME = "firefly0";
    const std::string UAV_NAME = "hummingbird1";
    const std::string MOVING_TARGET_NAME = "mix_cylinder";
    //const std::string MOVING_TARGET_NAME = "cylinder";

    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_pose_ref_pub_;
    ros::Publisher daltitude_ref_pub_;
    ros::Publisher rl_environment_state_pub_;
    ros::Publisher rl_environment_image_state_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_subs_;
    ros::Subscriber visual_servoing_measurement_subs_;
    ros::Subscriber camera_info_subs_;
    ros::Subscriber camera_image_subs_;
    ros::Subscriber drone_estimated_pose_subs_;

    // Ros service clients
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;

    //ROS Messages
    droneMsgsROS::dronePose last_drone_estimated_GMRwrtGFF_pose_;
    ros::Time current_time_, previous_time_;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    bool ReadConfigs(std::string &configFile);
    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void VisualServoingMeasurementCallback(const opencv_apps::RotatedRectStamped& msg);
    void CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& msg);
    void CameraImageCallback(const sensor_msgs::ImageConstPtr& msg);
    void DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);
    std::vector<float> ComputeNormalizedObjectInImageState();
    void ComputeObjectReferenceInImage();
    void ComputeGroundTruthObjectPositionInImage(const float &x_uav, const float &y_uav, const float &z_uav);
    void ComputeGroundTruthAndRealObjectPositionInImage(const float &x_uav, const float &y_uav, const float &z_uav);
    void DrawResults();
    cv::Mat ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma);
    void ComputeCompensatedRotatedRectByRpy();
    void ComputeObjectPositionBasedOnStabilizedDetection();
    void ComputeDetectedObjectInImageState(const cv::Mat &I, const std::string object_color);
    void DetectObjectInImage(const cv::Mat &I, const std::string object_color, cv::RotatedRect &rotated_rect_item, cv::Rect &rect_item);
    cv::Mat ComputeHomogeneousMatrixTransform(const float px, const float py, const float pz,
                                              const float yaw, const float pitch, const float roll);
    cv::Mat ComputeHomogeneousMatrixTransform(const float px, const float py, const float pz, const cv::Mat R);

    // Getters and setters
    void SetUavState(const float x, const float y, const float z, const float dx, const float dy);
    inline void GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float &pitch);
    inline void GetObjectInImageState(cv::RotatedRect &previous_roi, cv::RotatedRect &current_roi);
    inline std::vector<float> GetNormalizedState() const {return normalized_state_;}
    inline cv::Point2f GetErrorInImage() const {return error_in_image_;}

    RlEnvironmentImageBasedVisualServoingWithAltitude();
    ~RlEnvironmentImageBasedVisualServoingWithAltitude();
};

#endif
