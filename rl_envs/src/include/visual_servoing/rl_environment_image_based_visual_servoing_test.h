
#ifndef _RL_ENVIRONMENT_IMAGE_BASED_VISUAL_SERVOING_TEST_H_
#define _RL_ENVIRONMENT_IMAGE_BASED_VISUAL_SERVOING_TEST_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "droneMsgsROS/setControlMode.h"
#include "experiment_recorder.h"


#include "droneModuleROS.h"
//STL
#include "ctime"
#include "stdlib.h"
#include <algorithm>
#include "stdio.h"
#include "fstream"
#include <numeric>

//ROS / Opencv
#include "sensor_msgs/CameraInfo.h"
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include "std_srvs/Empty.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32MultiArray.h"
#include "opencv_apps/RotatedRectStamped.h"
#include <cv_bridge/cv_bridge.h>
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"
#include "tf_conversions/tf_eigen.h"




class RlEnvironmentImageBasedVisualServoingTest: public RlEnvironmentGazeboRos, public DroneModule
{
private:

    const bool ENABLE_PAUSED_SIMULATION = false;

    std::vector<float> state_;
    std::vector<float> normalized_state_;

    enum {
            ACTIONS_DIM = 2,
            STATE_DIM_LOW_DIM = 4,
            NUM_ITERATIONS = 5,
            NUM_EPISODE_ITERATIONS = 400,
            MAX_POSE_X = 1,
            MAX_POSE_Y = 1,
            REWARD_SHAPING = false
         };

    struct {
        float pos_x_, pos_y_, pos_z_;
        float speed_x_, speed_y_, speed_z_;
        float roll_, pitch_, yaw_;
        float roll_norm_, pitch_norm_, yaw_norm_;
    }uav_state_;

    struct {
        bool camera_info_available_flag_;
        float fx_, fy_, cx_, cy_;
        float image_width_, image_height_;
        cv::Mat K_matrix_;
        cv::Mat R_matrix_;
    }camera_info_;

    struct {
        std::vector<cv::Point> detected_roi_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_;
        std::vector<cv::Point2f> rotated_rect_points_rpy_compensated_;
        cv::RotatedRect detected_roi_;
        cv::Rect detected_roi_rect_;
        cv::RotatedRect previous_detected_roi_;
        cv::RotatedRect detected_roi_rpy_compensated_;
        cv::Point2f ground_truth_current_object_points_[4];
        cv::RotatedRect ground_truth_detected_roi_;
        cv::RotatedRect ground_truth_previous_detected_roi_;
        cv::RotatedRect stabilized_detected_roi_;
        cv::RotatedRect stabilized_previous_detected_roi_;
        cv::Rect stabilized_detected_roi_rect_;
    }object_in_image_info_;

    struct {
        float width_, height_;
        cv::Point2f center_offset_;
        cv::Point3f points_[4];
        cv::Mat points_mat_[4];
        cv::Mat current_points_mat_[4];
    }object_in_world_info_;

    cv_bridge::CvImagePtr cv_object_rl_reference_;
    std::ofstream f_data_recorder;
    const float kUav_Altitude_ = 1.2;
    float actions_max_value_, actions_min_value_;
    float pitch_roll_max_value_;
    cv::Rect roi_reference_;
    cv::Rect roi_reference_from_ground_truth_;
    cv::Point2d center_roi_ref_;
    cv::Point2f center_roi_ref_max_;
    cv::Point2f speed_roi_ref_max_;
    float radius_ref_;
    cv::Point2f error_in_image_;
    cv::Point2f error_in_image_norm_;
    int image_boundaries_offset_;
    cv::Mat captured_image_;
    boost::mutex uav_mutex_;
    boost::mutex object_in_image_mutex_;
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb_x_, filtered_derivative_wcb_y_;
    cv::Mat rot_mat_z_90_, rot_mat_x180_, t_cam_world_;
    cv::Mat rot_mat_2d_yaw_;
    std::vector<float> errors_array_;
    double mean_errors_array_;

    const std::string UAV_NAME = "hummingbird1";
    const std::string MOVING_TARGET_NAME = "mix_cylinder";
    //const std::string MOVING_TARGET_NAME = "cylinder";
    bool module_started_;
    int last_received_control_mode_;

    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_pose_ref_pub_;
    ros::Publisher visual_servoing_target_locked_pub_;
    ros::Publisher rl_environment_state_pub_;
    ros::Publisher rl_reference_bottom_image_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_subs_;
    ros::Subscriber visual_servoing_measurement_subs_;
    ros::Subscriber camera_info_subs_;
    ros::Subscriber camera_image_subs_;
    ros::Subscriber drone_estimated_pose_subs_;
    ros::Subscriber control_mode_subs_;


    // Ros service clients/servers
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;
    ros::ServiceServer start_server_srv_;

    //ROS Messages
    std_srvs::Empty start_agent_srv_;
    droneMsgsROS::dronePose last_drone_estimated_GMRwrtGFF_pose_;
    ros::Time current_time_, previous_time_;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();
    void ResetValues();
    bool startVal();
    bool stopVal();

    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void VisualServoingMeasurementCallback(const opencv_apps::RotatedRectStamped& msg);
    void droneImageFromBottomCameraCallback(const sensor_msgs::Image &msg);
    void CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& msg);
    void CameraImageCallback(const sensor_msgs::ImageConstPtr& msg);
    void DroneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);
    void ControlModeCallback(const droneMsgsROS::droneTrajectoryControllerControlMode::ConstPtr &msg);

    std::vector<float> ComputeNormalizedObjectInImageState();
    void ComputeObjectReferenceInImage();
    void ComputeDistanceToTargetLocked();
    void ComputeGroundTruthObjectPositionInImage(const float &x_uav, const float &y_uav, const float &z_uav);
    void DrawResults();
    cv::Mat convertFromRPYtoRotationMatrix(const float alpha, const float beta, const float gamma);
    void ComputeCompensatedRotatedRectByRpy();

    // Getters and setters
    void SetUavState(const float x, const float y, const float z, const float dx, const float dy);
    inline void GetUavState(float &x, float &y, float &z, float &dx, float &dy, float &roll, float pitch);
    inline void GetObjectInImageState(cv::RotatedRect &previous_roi, cv::RotatedRect &current_roi);
    inline std::vector<float> GetNormalizedState() const {return normalized_state_;}
    inline cv::Point2f GetErrorInImage() const {return error_in_image_;}


    void ComputeDetectedObjectInImageState(const cv::Mat &I, const std::string object_color);
    void ComputeObjectPositionBasedOnStabilizedDetection();
    cv::Mat ConvertFromYPRtoRotationMatrix(const float alpha, const float beta, const float gamma);
    void DetectObjectInImage(const cv::Mat &I, const std::string object_color, cv::RotatedRect &rotated_rect_item, cv::Rect &rect_item);

    RlEnvironmentImageBasedVisualServoingTest();
    ~RlEnvironmentImageBasedVisualServoingTest();
};

#endif

