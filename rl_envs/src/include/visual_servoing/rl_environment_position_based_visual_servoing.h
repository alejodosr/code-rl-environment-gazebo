
#ifndef _RL_ENVIRONMENT_POSITION_BASED_VISUAL_SERVOING_H_
#define _RL_ENVIRONMENT_POSITION_BASED_VISUAL_SERVOING_H_

#include "rl_environment_gazebo_ros.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/visualObjectRecognized.h"
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/SetModelState.h"
#include "experiment_recorder.h"
#include "LowPassFilter.h"
#include "filtered_derivative_wcb.h"

//STL
#include "ctime"
#include "stdlib.h"
#include "stdio.h"
#include "fstream"

//ROS / Opencv
#include "sensor_msgs/CameraInfo.h"
#include "std_srvs/Empty.h"
#include "opencv_apps/RotatedRectStamped.h"
#include <cv_bridge/cv_bridge.h>

class RlEnvironmentPositionBasedVisualServoing: public RlEnvironmentGazeboRos
{
private:

    const bool ENABLE_PAUSED_SIMULATION = false;

    std::vector<float> state_;

    enum {
            ACTIONS_DIM = 2,
            STATE_DIM_LOW_DIM = 4,
            NUM_ITERATIONS = 5,
            NUM_EPISODE_ITERATIONS = 300,
            MAX_POSE_X = 1,
            MAX_POSE_Y = 1
         };

    struct {
        double fx_, fy_, cx_, cy_;
        double image_width_, image_height_;
    }camera_info_;

    float uav_real_pos_x_, uav_real_pos_y_;
    float uav_real_vel_x_, uav_real_vel_y_;
    std::ofstream f_data_recorder;
    cv::RotatedRect detected_roi_;
    cv::Point2f detected_roi_vel_;
    cv::Rect roi_reference_;
    cv::Point2d center_roi_ref_;
    cv::Point2f error_in_image_;
    int image_boundaries_offset_;
    cv::Mat captured_image_;
    float uav_pose_x_, uav_pose_y_, uav_pose_z_, uav_velocity_x_, uav_velocity_y_;
    const float kUav_Altitude_ = 1.2;
    boost::mutex uav_mutex_;
    boost::mutex object_in_image_mutex_;
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb_x_, filtered_derivative_wcb_y_;

    const std::string UAV_NAME = "hummingbird1";

    // Ros publishers
    ros::Publisher uav_speed_ref_pub_;
    ros::Publisher uav_altitude_ref_pub_;

    // Ros subscribers
    ros::Subscriber uav_pose_velocity_subs_;
    ros::Subscriber visual_servoing_measurement_subs_;
    ros::Subscriber camera_info_subs_;
    ros::Subscriber camera_image_subs_;

    // Ros service clients
    ros::ServiceClient gazebo_client_;
    ros::ServiceClient estimator_client_;

    //ROS Messages
    droneMsgsROS::visualObjectRecognized visualObjectRecognized_msg;


public:

    bool Step(rl_srvs::AgentSrv::Request &request, rl_srvs::AgentSrv::Response &response);
    bool EnvDimensionality(rl_srvs::EnvDimensionalitySrv::Request &request, rl_srvs::EnvDimensionalitySrv::Response &response);
    bool ResetSrv(rl_srvs::ResetEnvSrv::Request &request, rl_srvs::ResetEnvSrv::Response &response);
    void InitChild(ros::NodeHandle n);
    bool Reset();

    void PoseVelocityCallback(const gazebo_msgs::ModelStates::ConstPtr& msg);
    void CameraInfoCallback(const sensor_msgs::CameraInfoConstPtr& msg);
    void VisualServoingMeasurementCallback(const droneMsgsROS::visualObjectRecognized &msg);
    void droneImageFromBottomCameraCallback(const sensor_msgs::Image &msg);
    void CameraImageCallback(const sensor_msgs::ImageConstPtr& msg);

    // Getters and setters
    void SetUavState(const float x, const float y, const float z, const float dx, const float dy);
    inline void GetUavState(float &x, float &y, float &z, float &dx, float &dy);
    std::vector<float> ComputeNormalizedState();


    RlEnvironmentPositionBasedVisualServoing();
    ~RlEnvironmentPositionBasedVisualServoing();
};

#endif

